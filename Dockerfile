##Install Python
FROM ubuntu:18.04

RUN apt-get update -y  && apt-get upgrade -y 
RUN apt-get install -y software-properties-common vim jq curl 
RUN apt-get install -y build-essential python3.6 python3.6-dev python3-pip python3.6-venv libsm6 libxext6 libglib2.0-0 libxrender-dev 
RUN  python3.6 -m pip install pip --upgrade 
RUN ln -s /usr/bin/python3.6 /usr/bin/python
RUN ln -s /usr/bin/pip3.6 /usr/bin/pip 
		
#Creating User and Group
RUN groupadd -r blackstraw && useradd -r -g blackstraw blackstraw 

##Copy the directory to container
COPY . /home/blackstraw/

##Changing the ownership
RUN chown -R blackstraw:blackstraw /home/blackstraw

##Install the dependencies
RUN pip install -r /home/blackstraw/requirements.txt

##Non root user
USER blackstraw

WORKDIR /home/blackstraw/webscrapperapi/
RUN ls -lrt
#Run time parameter
CMD python /home/blackstraw/webscrapperapi/webscrapping.py 2>&1
#ENTRYPOINT ["/home/blackstraw/start.sh"]
