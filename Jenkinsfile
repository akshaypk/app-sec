//Config for target environments
config = [
  "development": [
    "target_environment": "dev"
  ],
  "release": [
    "target_environment": "qa"
  ],
  "master": [
    "target_environment": "prod"
  ]
]

//Returns branch name
def getBranchParentDir() {
    rawBranch = env.BRANCH_NAME
    startIndex = rawBranch.indexOf('/')

    if(startIndex == -1){
      return rawBranch
    }

    return rawBranch.substring(0,startIndex)
}

//To get the configuration values based on branch name
def getConfigValue(name) {
    configHash = config[getBranchParentDir()]

    if (configHash == null) {
        return ""
    }

    return configHash[name]
}

//To get the target environment from the config array
def getBuildTargetEnvironment() {
    environment = getConfigValue("target_environment")

    if (environment == null || environment == "") {
        return "dev"
    }

    return environment
}

//Starting Jenkins pipeline
pipeline {
  agent none
    
  stages {
//Installing the project dependencies
    stage('Freezing the requirements.txt') {
      agent {label 'jenkins-master'}

      
      steps {
        sh 'pip freeze > requirements.txt'
      }
    }

    stage('Generate BOM') {
      agent {label 'jenkins-master'}
      
      steps {
        sh 'cyclonedx-py -i requirements.txt -o bom.xml'
      }
    }

    stage('Security Vulnerabilities') {
      
      parallel {
        stage('Using bandit') {
          agent {label 'jenkins-master'}
          
          
          steps {
            sh 'bandit -r webscrapperapi/ -f csv -o report.csv || true'
              }
          post {
            always {
              emailext attachmentsPattern:'report.csv' ,attachLog: true, body: "${currentBuild.currentResult}: Job ${env.JOB_NAME} build ${env.BUILD_NUMBER}\n More info at: ${env.BUILD_URL}",
                recipientProviders: [[$class: 'DevelopersRecipientProvider'], [$class: 'RequesterRecipientProvider']],
                subject: "Jenkins Build ${currentBuild.currentResult}: Job ${env.JOB_NAME}"
            }
          } 
        }
        stage('Checking for dependencies using Safety') {
          agent {label 'jenkins-master'}
          
          
          steps {
            sh 'safety check -r requirements.txt --full-report -o safetycheck.json || true'
          }
          post {
            always {
              emailext attachmentsPattern:'safetycheck.json' ,attachLog: true, body: "${currentBuild.currentResult}: Job ${env.JOB_NAME} build ${env.BUILD_NUMBER}\n More info at: ${env.BUILD_URL}",
                recipientProviders: [[$class: 'DevelopersRecipientProvider'], [$class: 'RequesterRecipientProvider']],
                subject: "Jenkins Build ${currentBuild.currentResult}: Job ${env.JOB_NAME}"
            }
          }
        }
      }
    }
  }
}