import json
from flask import Flask, redirect, request, jsonify, Blueprint, Response
from flask_restplus import Namespace, Resource, fields, reqparse
from core.utils import DbUtils
from core.LoggingManager import get_logger
from .products import products
from csv import DictReader
import werkzeug, csv, io
from werkzeug import datastructures
from werkzeug import _internal
from datetime import datetime
api = Namespace('dynamic_csv', description='upc dynamic scraper ')
post_parser = reqparse.RequestParser()
post_parser.add_argument('search_text', type=str, action='split', location = 'form')
post_parser.add_argument('site_id', type=int, location='form')
post_parser.add_argument('file_name', type=str, location='form')
post_parser.add_argument('scrape_using', type=str, choices=('product_upc', 'product_url', 'product_id','product_name'),
                         location='form')
post_parser.add_argument('corresponding_Column_in_CSV', type=str, location='form')
post_parser.add_argument('request_type', type=str, choices=('dynamic', 'ajax'),location='form')


get_parser = reqparse.RequestParser()
get_parser.add_argument('job_id', type=str)



@api.route('/add')
class post_csv(Resource):
    @api.doc("Uploading csv file")
    @api.expect(post_parser)
    def post(self):
        try:
            dbutils = DbUtils()
            values = []
            searchQuery = {}
            final_values =[]
            scheduleDict = {}
            dynamicDict = {}
            site_id = request.form['site_id']
            search_text = request.form['search_text']
            texts = search_text.split(",")
            scrape_using = request.form['scrape_using']
            corresponding_Column_in_CSV = request.form['corresponding_Column_in_CSV'] \
                if 'corresponding_Column_in_CSV' in request.form else ""
            request_type = request.form['request_type'] if 'request_type' in request.form else None

            file_name = request.form['file_name']
            today = datetime.now()
            file_name += str(today)
            # print("file", file_name)
            # print(type(site_id), site_id)
            searchQuery['site_id'] = site_id
            upc_length = None
            dynamic_parser_values = []
            if request_type != 'ajax':
                dynamic_parser_values, count = dbutils.get_dynamic_scrapper_details(0, 10, searchQuery)
                for row in dynamic_parser_values:
                    upc_length = row['upc_length']

            if scrape_using == 'product_upc' or scrape_using == 'product_id':
                for value in texts:
                    value = value.zfill(int(upc_length)) if upc_length else value
                    final_values.append(value)
                json_value = json.dumps(final_values)

            elif scrape_using == 'product_name':
                for values in texts:
                    final_values.append(values)
                json_value = json.dumps(final_values)
            else:
                json_value = json.dumps(texts)

            dynamic_results = dbutils.dynamic_csv({'site_id': site_id,
                                                   'file_name': file_name,
                                                    'file_data': json_value,
                                                    'scrape_using': scrape_using,
                                                    'request_type': request_type})
            dbutils.close()
            return {'response_code': 0, 'status': 'success', 'data': dynamic_results,
                    'message': 'Job Scheduled successfully'}, 200

        except Exception as e:
            print(str(e))
            dbutils.close()
            get_logger().error("Error ", exc_info=True)
            return {'message': str(e), 'response_code': 2, 'status': "failure"}


@api.route('/get_upc')
class get_upc(Resource):
    @api.doc("Get_products")
    @api.expect(get_parser)
    def get(self):
        try:
            dbutils = DbUtils()
            job_id = request.args.get('job_id')
            upc = dbutils.dynamic_products({'job_id':job_id})
            dbutils.close()
            return jsonify(upc)
        except Exception as e:
            dbutils.close()
            return {"messge":str(e)}
            #get_logger().error("Error ", exc_info=True)