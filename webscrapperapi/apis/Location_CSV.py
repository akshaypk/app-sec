import json
from flask import Flask, redirect, request, jsonify, Blueprint, Response
from flask_restplus import Namespace, Resource, fields, reqparse
from core.utils import DbUtils
from core.LoggingManager import get_logger
from .products import products
from csv import DictReader
import werkzeug, csv, io
from werkzeug import datastructures
from werkzeug import _internal
from datetime import datetime
api = Namespace('location_csv', description='location based scraper ')
post_parser = reqparse.RequestParser()
post_parser.add_argument('search_text', type=str, action='split', location = 'form')
post_parser.add_argument('site_id', type=int, location='form')
post_parser.add_argument('file_name', type=str, location='form')

get_parser = reqparse.RequestParser()
get_parser.add_argument('job_id', type=str)

get_location = reqparse.RequestParser()
get_location.add_argument('job_id', type=str)
get_location.add_argument('site_id', type=str)

@api.route('/add')
class post_csv(Resource):
    @api.doc("Uploading csv file")
    @api.expect(post_parser)
    def post(self):
        try:
            dbutils = DbUtils()
            values = []
            searchQuery = {}
            final_values =[]
            scheduleDict = {}
            dynamicDict = {}
            site_id = request.form['site_id']
            search_text = request.form['search_text']
            texts = search_text.split(",")
            # upc_length = request.form['upc_length']
            # decoded_file = csv_file.read().decode('utf-8').splitlines()
            # reader = csv.DictReader(decoded_file)
            # for row in texts:
            #     values.append(row['UPC'])
            file_name = request.form['file_name']
            today = datetime.now()
            file_name += str(today)
            print("file name=", file_name)
            print(type(site_id), site_id)
            searchQuery['site_id'] = site_id
            # dynamic_parser_values, count = dbutils.get_dynamic_scrapper_details(0, 10, searchQuery)
            dynamic_parser_values, count = dbutils.get_location_parser_details(0, 10, searchQuery)

            for value in texts:
                print("value", value)
                final_values.append(value)
            json_value = json.dumps(final_values)
            print("json", json_value)
            # dynamic_results = dbutils.dynamic_csv({'site_id': site_id, 'file_name': file_name, 'file_data': json_value})
            dynamic_results = dbutils.location_csv({'site_id': site_id, 'file_name': file_name, 'file_data': json_value})
            print("dynamic", dynamic_results)
            dbutils.close()
            return {'response_code': 0, 'status': 'success', 'data': dynamic_results,  'message': 'Job Scheduled successfully'}, 200
        except Exception as e:
            dbutils.close()
            get_logger().error("Error ", exc_info=True)


@api.route('/get_upc')
class get_keywords(Resource):
    @api.doc("Get_products")
    @api.expect(get_parser)
    def get(self):
        try:
            dbutils = DbUtils()
            job_id = request.args.get('job_id')
            # upc = dbutils.dynamic_products({'job_id':job_id})
            keywords = dbutils.location_products({'job_id': job_id})
            dbutils.close()
            return jsonify(keywords)
        except Exception as e:
            dbutils.close()
            get_logger().error("Error ", exc_info=True)


@api.route('/get_locations')
class get_scrapped_location(Resource):
    @api.doc("get_location")
    @api.expect(get_location)
    def get(self):
        try:
            dbutils = DbUtils()
            job_id = request.args.get('job_id')
            site_id = request.args.get('site_id')

            if job_id is not None:
                #print (job_id)
                data = dbutils.get_location_results({'job_id': job_id})
                item=[]
                for value in data:
                    item.append(dict(value))

                dbutils.close()
                return jsonify(item)
            else:
                return {'message':'Please sent valid site_id'}
        except Exception as e:
            dbutils.close()
            get_logger().error("Error ", exc_info=True)