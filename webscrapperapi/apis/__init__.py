from flask import Blueprint
from flask_restplus import Api
from .googleapi import api as googleapi
from .walmartapi import api as walmartapi
from .upc_dynamic import api as upc_dynamic
from .sites import api as sites
from .jobs import api as jobs
from .products import api as products
from .project_config import api as project_config
from .sitemap_parser_details import api as sitemap_parser
from .dynamic_scrapper_details import api as dynamic_scrapper_details
from .webpage_parser_details import api as webpage_parser_details
from .scrape_csv import api as csv_template
from .receivability_tool_config import api as  receivability_tool_config
from .import_products import api as import_products
from .scheduled_jobs import api as scheduled_jobs
from .category_scope import api as category_scope
from .webelement import api as webelement
from .category_parser_details import api as category_parser_details
from .Dynamic_CSV import api as dynamic_csv
# from .DynamicCSV_ProductName import api as dynamicCsv_productname
from .Location_CSV import api as location_csv
from .lookup_csv import api as lookup_csv
from .category_scraper import api as category_scraper
from .site import api as site
from .googlelocationapi import api as googlelocation
from .location_parser_details import api as location_parser_details
from .location_webparser_details import api as location_webparser_details
from .scrapped_location import api as scrapped_location
from.project_csv_file import api as project_csv_file
from .products import api as DownloadProducts
from .sitemonitoring import api as site_monitoring
from .cronjob import api as cronjob
from .image_puller import api as image_puller
from .ajax_parser_details import api as ajax_parser_details
from .user import api as user
from .login import api as login
from .scrape_twitter import api as scrape_twitter
from .scrape_instagram import api as scrape_instagram
from .scrape_youtube import  api as scrape_youtube


blueprint = Blueprint('api', __name__)

api = Api(blueprint,title='My Title',
    version='1.0',
    description='A description',
    # All API metadatas
    )
api.add_namespace(ajax_parser_details, path='/v1/ajax_parser_details')
api.add_namespace(project_csv_file,path='/v1/project_csv_file')
api.add_namespace(googleapi,path = '/v1/googleapi')
api.add_namespace(walmartapi, path ='/v1/walmartapi')
api.add_namespace(sites,path = '/v1/sites')
api.add_namespace(products,path = '/v1/products')
api.add_namespace(project_config, path='/v1/project_config')
api.add_namespace(sitemap_parser,path='/v1/sitemap_parser_details')
api.add_namespace(webelement, path='/v1/webelement')
api.add_namespace(dynamic_scrapper_details,path = '/v1/dynamic_scrapper_details')
api.add_namespace(webpage_parser_details,path = '/v1/webpage_parser_details')
api.add_namespace(csv_template,path = '/v1/scrape_csv')
api.add_namespace(category_scope, path='/v1/category_scope')
api.add_namespace(csv_template,path = '/v1/csv_template')
api.add_namespace(receivability_tool_config,path = '/v1/receivability_tool_config')
api.add_namespace(import_products,path = '/v1/import_products')
api.add_namespace(scheduled_jobs,path = '/v1/scheduled_jobs')
api.add_namespace(upc_dynamic, path='/v1/upc_dynamic')
api.add_namespace(category_parser_details, path='/v1/category_parser_details')
api.add_namespace(dynamic_csv, path='/v1/dynamic_csv')
# api.add_namespace(dynamicCsv_productname,path='/v1/dynamicCsv_productname')
api.add_namespace(location_csv, path='/v1/location_csv')
api.add_namespace(lookup_csv, path='/v1/lookup_csv')
api.add_namespace(category_scraper, path='/v1/category_scraper')
api.add_namespace(site,path = '/v1/site')
api.add_namespace(scheduled_jobs,path = '/v1/cancel_job')
api.add_namespace(googlelocation,path = '/v1/googleapi/location_search')
api.add_namespace(location_parser_details,path='/v1/location_parser_details')
api.add_namespace(location_webparser_details,path='/v1/location_webparser_details')
api.add_namespace(scrapped_location,path='/v1/scrapped_location')
api.add_namespace(DownloadProducts,path = '/v1/products/download')
api.add_namespace(site_monitoring,path='/v1/site_monitoring')
api.add_namespace(cronjob,path='/v1/cronjob')
api.add_namespace(image_puller,path='/v1/image_puller')
api.add_namespace(login, path='/v1/login')
api.add_namespace(user, path='/v1/user')
api.add_namespace(scrape_twitter, path='/v1/twitter_search')
api.add_namespace(scrape_instagram, path='/v1/instagram_search')
api.add_namespace(scrape_youtube,path='v1/scrape_youtube')
