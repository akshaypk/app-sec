from flask import Flask, abort, request, jsonify, Blueprint
from flask_restplus import Namespace, Resource, fields, reqparse

from core.Paginator import get_paginated_list
import decimal, datetime
import json
from sqlalchemy import null
from core.utils import DbUtils
from core.LoggingManager import get_logger

api = Namespace('ajax_parser_details', description='Ajax related operations')


get_parser = reqparse.RequestParser()
get_parser.add_argument('start', type=int, help='integer value')
get_parser.add_argument('limit', type=int, help='integer value')
get_parser.add_argument('site_id', type=int, help='Site id')
get_parser.add_argument('site_name', type=str, help='string')
get_parser.add_argument('request_level', type=int, help='integer value')
get_parser.add_argument('config_details', type=dict, help='json')
get_parser.add_argument('product_url', type=dict, help='json')

post_parser = reqparse.RequestParser()
post_parser.add_argument('site_id', required=True, help='Site id', type=int, location='form')
post_parser.add_argument('site_name', type=str, help='string', location='form')
post_parser.add_argument('request_level', type=int, location='form', help='integer value')
post_parser.add_argument('config_details', type=dict, location='form', help='json')
post_parser.add_argument('product_url', type=dict, location='form', help='json')

update_parser = reqparse.RequestParser()
update_parser.add_argument('site_id', required=True, type=int, location='form', help='Site id')
update_parser.add_argument('site_name', type=str, help='string', location='form')
update_parser.add_argument('request_level', type=int, location='form', help='integer value')
update_parser.add_argument('config_details', type=dict, location='form', help='json')
update_parser.add_argument('product_url', type=dict, location='form', help='json')


delete_parser = reqparse.RequestParser()
delete_parser.add_argument('site_id', required=True, type=int, location='form', help='Site id')

@api.route('/')
class Ajax(Resource):
    @api.doc('list_Ajax')
    # @api.marshal_with(sites)
    @api.expect(get_parser)
    def get(self):
        dbutils = DbUtils()
        start = int(request.args.get('start', 1))
        limit = int(request.args.get('limit', 10))
        site_id = request.args.get('site_id', None)
        site_name = request.args.get('site_name', None)
        searchQuery = {}
        if site_id and site_name:
            searchQuery['site_id'] = site_id if site_id else ''
            searchQuery['site_name'] = site_name if site_name else ''
        elif site_id is not None:
            searchQuery['site_id'] = site_id
        elif site_name is not None:
            searchQuery['site_name'] = site_name
        ajax_parser_details, count = dbutils.get_ajax_parser_details(int(start) - 1, limit, searchQuery)
        dbutils.close()
        if count:
            data = get_paginated_list(ajax_parser_details, '/api/v1/ajax_parser_details', count, start, limit=limit)
            return data
        else:
            return {'response_code': 2, 'status': 'failure', 'message': 'No data found'}, 200



@api.route('/add')
class AddAjax(Resource):
    @api.doc('post_ajax')
    @api.expect(post_parser)
    def post(self):
        dbutils = DbUtils()
        result = {}
        try:
            form = request.get_json(force=True)

            # check for the existing site
            site_id = form['site_id'] if 'site_id' in form else None
            request_level = form['request_level'] if 'request_level' in form else None
            config_details = form['config_details'] if 'config_details' in form else None
            product_url = form['product_url'] if 'product_url' in form else None

            try:
                config_details = json.loads(config_details)
                product_url = json.loads(product_url)
            except Exception as e:
                get_logger().error('unable to convert json object %s', str(e))

            get_logger().info('config_details %s', str(config_details))
            get_logger().info('product_url %s', str(product_url))

            print(site_id)
            site, sites_count = dbutils.get_sites(0, 1, {'site_id': site_id})
            ajax_parser_details, count = dbutils.get_ajax_parser_details(0, 1, {'site_id': site_id})

            if sites_count > 0:
                if count > 0:
                    dbutils.close()
                    return {'response_code': 2, 'status': 'failure',
                            'message': 'ajax parser details already exists.'}, 200
                else:
                    dbutils.close()
                    result = dbutils.create_ajax_parser_details({'site_id': site_id,
                                                                 'request_level': request_level,
                                                                 'config_details': config_details,
                                                                 'product_url': product_url})
            else:
                dbutils.close()
                return {'response_code': 2, 'status': 'failure', 'message': 'Site ID does not exist kindly add .'}, 200
            # return result,200
        except Exception as e:
            print(str(e))
            get_logger().error('Error in adding ajax parser details %s', str(e))
            dbutils.close()
            return {'response_code': 2, 'status': 'failure', 'message': str(e)}, 200
        else:
            dbutils.close()
            return {'response_code': 0, 'status': 'success',
                    'message': 'ajax parser details created successfully'}, 200


@api.route('/update')
class Update_Ajax(Resource):
    @api.doc('update_Ajax')
    @api.expect(update_parser)
    def put(self):
        dbutils = DbUtils()
        result = {}
        try:
            form = request.get_json(force=True)

            # check for the existing site
            site_id = form['site_id'] if 'site_id' in form else None
            request_level = form['request_level'] if 'request_level' in form else None
            config_details = form['config_details'] if 'config_details' in form else None
            product_url = form['product_url'] if 'product_url' in form else None


            ajax_parser_details, count = dbutils.get_ajax_parser_details(0, 1, {'site_id': site_id})

            if count > 0:
                result = dbutils.update_ajax_parser_details({'site_id': site_id,
                                                             'request_level': request_level,
                                                             'config_details': config_details,
                                                             'product_url': product_url})
            else:
                dbutils.close()
                return {'response_code': 2, 'status': 'failure',
                        'message': 'ajax parser details was not found kindly add it'}, 200

            # return result,200
        except Exception as e:
            print(e)
            dbutils.close()
            return {'response_code': 2, 'status': 'failure', 'message': 'unexpected error'}, 200
        else:
            dbutils.close()
            return {'response_code': 0, 'status': 'success',
                    'message': 'ajax parser details updated successfully'}, 200


@api.route('/delete')
class Delete_Ajax(Resource):
    @api.doc('delete_Ajax')
    @api.expect(delete_parser)
    def delete(self):
        try:

            dbutils = DbUtils()
            site_id = request.form['site_id']

            ajax_parser_details, count = dbutils.get_ajax_parser_details(0, 1, {'site_id': site_id})
            if count > 0:
                dbutils.delete_ajax_parser_details(site_id)
                # return {'response_code': 2, 'message': 'deleted'}, 200
            else:
                dbutils.close()
                return {'response_code': 2, 'status': 'failure', 'message': 'Site doesn\'t exist.'}, 200

        except Exception as e:
            print(e)
            dbutils.close()
            return {'response_code': 2, 'status': 'failure', 'message': 'unexpected error'}, 200
        else:
            dbutils.close()
            return {'response_code': 0, 'status': 'success',
                    'message': 'deleted ajax parser details  successfully'}, 200


