from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql+psycopg2://postgres:admin@localhost/scraper'
app.config['SQLCLHEMY_TRACK_MODIFICATIONS']=False
db = SQLAlchemy(app)

class category_parser_details(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    site_id=db.Column(db.Integer)
    category_name = db.Column(db.String(100))
    category_xpath=db.Column(db.String)
    levels_of_sub_categories=db.Column(db.Integer)
    subcategory1_xpath=db.Column(db.String)
    subcategory2_xpath=db.Column(db.String)
    subcategory3_xpath=db.Column(db.String)
    driver_id=db.Column(db.Integer)





    def __init__(self, category_name,category_xpath,levels_of_sub_categories,subcategory1_xpath,subcategory2_xpath,subcategory3_xpath):
        self.category_name = category_name
        self.category_xpath = category_xpath
        self.levels_of_sub_categories = levels_of_sub_categories
        self.subcategory1_xpath = subcategory1_xpath
        self.subcategory2_xpath = subcategory2_xpath
        self.subcategory3_xpath = subcategory3_xpath



    def __repr__(self):
        return '<User %r>' % self.category_name

db.create_all()



