from flask import Flask, abort, request, jsonify, Blueprint
from flask_restplus import Namespace, Resource, fields, reqparse
# from flask_sqlalchemy import SQLAlchemy
from core.Paginator import get_paginated_list
import decimal, datetime, json
import json
from sqlalchemy import null
from core.utils import DbUtils
api = Namespace('category_parser_details', description='category related operations')


category_parser_details = api.model('category_parser_details', {
    'id': fields.Integer(required=True, description='The site identifier'),
    'site_id': fields.Integer(required=True, description='The site identifier'),
    'base_search_url': fields.String(required=True, description='The base_serach_url identifier'),
    'category_name': fields.String(required=True, description='The category name'),
    'parse_with_selenium': fields.Boolean(required=True, description='the parse with selenium'),
    'category_xpath': fields.String(required=True, description='The category_xpat url'),
    'levels_of_sub_categories': fields.Integer(required=True, description='requires levels of sub_category'),
    'subcategory1_xpath': fields.String(required=True, description='requires subcategory1_xpath'),
    'subcategory2_xpath': fields.String(required=True, description='requires subcategory2_xpath'),
    'subcategory3_xpath': fields.String(required=True, description='requires subcategory3_xpath'),
    'product_url_xpath': fields.String(required=True, description='product_url_xpath'),
    'pagination_xpath': fields.String(required=True, description='pagination_xpath'),
    'zero_depth_xpath': fields.String(required=True, description='zero_depth_xpath'),
    'driver_id': fields.Integer(required=True, description='The driver_id')

})

CATEGORY_PARSER_DETAILS = [
    {'site_id': 'felix', 'base_search_url': 'Felix', 'category_name': 'Felix', 'parse_with_selenium': 'Felix',
     'category_xpath': 'Felix', 'levels_of_sub_categories': 'Felix',
     'subcategory1_xpath': 'Felix', 'subcategory2_xpath': 'Felix', 'subcategory3_xpath': 'Felix',
     'product_url_xpath': 'Felix', 'pagination_xpath': 'Felix', 'driver_id': 'Felix',
     },
]

get_parser = reqparse.RequestParser()
get_parser.add_argument('start', type='int', help='integer value')
get_parser.add_argument('limit', type='int', help='integer value')
get_parser.add_argument('site_id', help='Site id')
get_parser.add_argument('site_name',type=str,help='string')
get_parser.add_argument('category_name', type=str)

post_parser = reqparse.RequestParser()
post_parser.add_argument('site_id', required=True, type=int, location='form')
post_parser.add_argument('category_name', required=True, type=str, location='form')
post_parser.add_argument('base_search_url', required=True, type=str, location='form')
post_parser.add_argument('parse_with_selenium', required=True, type=bool, help='True', location='form')
post_parser.add_argument('category_xpath', required=True, type=str, location='form')
post_parser.add_argument('levels_of_sub_categories',required=True, type=int, location='form')
post_parser.add_argument('subcategory1_xpath', type=str, location='form')
post_parser.add_argument('subcategory2_xpath', type=str, location='form')
post_parser.add_argument('subcategory3_xpath', type=str, location='form')
post_parser.add_argument('pagination_xpath', type=str, location='form')
post_parser.add_argument('zero_depth_xpath', type=str, location='form')
post_parser.add_argument('product_url_xpath', type=str, location='form')
post_parser.add_argument('driver_id', type=int, location='form')

update_parser = reqparse.RequestParser()
update_parser.add_argument('site_id',required=True, type=int, location='form')
update_parser.add_argument('category_name', required=True,type=str, location='form')
update_parser.add_argument('base_search_url',required=True, type=str, location='form')
update_parser.add_argument('parse_with_selenium', type=bool, help='True/False', location='form')
update_parser.add_argument('category_xpath', required=True,type=str, location='form')
update_parser.add_argument('levels_of_sub_categories',required=True, type=int, location='form')
update_parser.add_argument('subcategory1_xpath', type=str, location='form')
update_parser.add_argument('subcategory2_xpath', type=str, location='form')
update_parser.add_argument('subcategory3_xpath', nullable=True, type=str, location='form')
update_parser.add_argument('pagination_xpath', type=str, location='form')
update_parser.add_argument('zero_depth_xpath', type=str, location='form')
update_parser.add_argument('product_url_xpath', type=str, location='form')
update_parser.add_argument('driver_id', type=int, location='form')


delete_parser = reqparse.RequestParser()
delete_parser.add_argument('site_id', type=int, location='form')
delete_parser.add_argument('category_name', type=str, location='form')



@api.route('/')
class Category(Resource):
    @api.doc('list_Category')
    # @api.marshal_with(sites)
    @api.expect(get_parser)
    def get(self):
        dbutils = DbUtils()
        start = int(request.args.get('start', 1))
        limit = int(request.args.get('limit', 10))
        site_id = request.args.get('site_id', None)
        site_name=request.args.get('site_name',None)
        category_name = request.args.get('category_name', None)
        searchQuery = {}
        if category_name and site_id and site_name:
            searchQuery['category_name'] = category_name if category_name else ''
            searchQuery['site_id'] = site_id if site_id else ''
            searchQuery['site_name']=site_name if site_name else ''
        elif category_name is not None:
            searchQuery['category_name'] = category_name
        elif site_id is not None:
            searchQuery['site_id'] = site_id
        elif site_name is not None:
            searchQuery['site_name'] = site_name
        category_parser_details, count = dbutils.get_category_parser_details(int(start) - 1, limit, searchQuery)
        dbutils.close()
        if count:
            data = get_paginated_list(category_parser_details, '/api/v1/category_parser_details', count, start, limit=limit)
            return data
        else:
            return {'response_code': 2, 'status': 'failure', 'message': 'No data found'}, 200



@api.route('/add')
class AddCategory(Resource):
    @api.doc('post_category')
    @api.expect(post_parser)
    def post(self):
        dbutils = DbUtils()
        result = {}
        try:
            # check for the existing site
            site_id = request.form['site_id']
            category_name = request.form['category_name']
            base_search_url = request.form['base_search_url']
            # parse_with_selenium = True if 'parse_with_selenium' in request.form else False
            parse_with_selenium = dbutils.get_status_bool(request, 'parse_with_selenium')
            category_xpath = request.form['category_xpath']
            levels_of_sub_categories = request.form['levels_of_sub_categories']
            subcategory1_xpath = request.form['subcategory1_xpath'] if 'subcategory1_xpath' in request.form else None
            subcategory2_xpath = request.form['subcategory2_xpath'] if 'subcategory2_xpath' in request.form else None
            subcategory3_xpath = request.form['subcategory3_xpath'] if 'subcategory3_xpath' in request.form else None
            product_url_xpath = request.form['product_url_xpath'] if 'product_url_xpath' in request.form else None
            pagination_xpath =  request.form['pagination_xpath'] if 'pagination_xpath' in request.form else None
            zero_depth_xpath = request.form['zero_depth_xpath'] if 'zero_depth_xpath' in request.form else None

            driver_id = request.form['driver_id'] if 'driver_id' in request.form else None
            print(site_id, category_name, parse_with_selenium, category_xpath, levels_of_sub_categories,
                  subcategory1_xpath, subcategory2_xpath,
                  subcategory3_xpath, driver_id)
            site, sites_count = dbutils.get_sites(0, 1, {'site_id': site_id})
            category_parser_details, count = dbutils.get_category_parser_details(0, 1, {'site_id': site_id, 'category_name':category_name})
            if sites_count > 0:
                if count > 0:
                    dbutils.close()
                    return {'response_code': 2, 'status': 'failure',
                            'message': 'category parser details already exists.'}, 200
                else:
                    dbutils.close()
                    result = dbutils.create_category_parser_details({'site_id': site_id,
                                                                     'category_name': category_name,
                                                                     'base_search_url': base_search_url,
                                                                     'parse_with_selenium': parse_with_selenium,
                                                                     'category_xpath': category_xpath,
                                                                     'levels_of_sub_categories': int(levels_of_sub_categories) if levels_of_sub_categories else 0,
                                                                     'subcategory1_xpath': subcategory1_xpath,
                                                                     'subcategory2_xpath': subcategory2_xpath,
                                                                     'subcategory3_xpath': subcategory3_xpath,
                                                                     'product_url_xpath': product_url_xpath,
                                                                     'pagination_xpath': pagination_xpath,
                                                                     'zero_depth_xpath' : zero_depth_xpath,
                                                                     'driver_id': int(driver_id) if driver_id else 4})
            else:
                dbutils.close()
                return {'response_code': 2, 'status': 'failure', 'message': 'Site ID does not exist kindly add .'}, 200
            # return result,200
        except Exception as e:
            print(e)
            dbutils.close()
            return {'response_code': 2, 'status': 'failure', 'message': 'unexpected error'}, 200
        else:
            dbutils.close()
            return {'response_code': 0, 'status': 'success',
                    'message': 'category parser details created successfully'}, 200


@api.route('/update')
class Update_Category(Resource):
    @api.doc('update_Category')
    @api.expect(update_parser)
    def put(self):
        dbutils = DbUtils()
        result = {}
        try:
            site_id = request.form['site_id']
            category_name = request.form['category_name']
            base_search_url = request.form['base_search_url']
            # parse_with_selenium = True if 'parse_with_selenium' in request.form else False
            parse_with_selenium = dbutils.get_status_bool(request, 'parse_with_selenium')
            category_xpath = request.form['category_xpath']
            levels_of_sub_categories = request.form['levels_of_sub_categories']
            subcategory1_xpath = request.form['subcategory1_xpath'] if 'subcategory1_xpath' in request.form else None
            subcategory2_xpath = request.form['subcategory2_xpath'] if 'subcategory2_xpath' in request.form else None
            subcategory3_xpath = request.form['subcategory3_xpath'] if 'subcategory3_xpath' in request.form else None
            product_url_xpath = request.form['product_url_xpath'] if 'product_url_xpath' in request.form else None
            pagination_xpath = request.form['pagination_xpath'] if 'pagination_xpath' in request.form else None
            zero_depth_xpath = request.form['zero_depth_xpath'] if 'zero_depth_xpath' in request.form else None
            driver_id = request.form['driver_id'] if 'driver_id' in request.form else None

            category_parser_details, count = dbutils.get_category_parser_details(0, 1, {'site_id': site_id,'category_name': category_name})

            if count > 0:
                result = dbutils.update_category_parser_details({'site_id': site_id,
                                                                 'category_name': category_name,
                                                                 'base_search_url': base_search_url,
                                                                 'parse_with_selenium': parse_with_selenium,
                                                                 'category_xpath': category_xpath,
                                                                 'levels_of_sub_categories': int(levels_of_sub_categories) if levels_of_sub_categories else 0,
                                                                 'subcategory1_xpath': subcategory1_xpath,
                                                                 'subcategory2_xpath': subcategory2_xpath,
                                                                 'subcategory3_xpath': subcategory3_xpath,
                                                                 'product_url_xpath': product_url_xpath,
                                                                 'pagination_xpath': pagination_xpath,
                                                                 'zero_depth_xpath': zero_depth_xpath,
                                                                 'driver_id': int(driver_id) if driver_id else 4
                                                                 })
            else:
                dbutils.close()
                return {'response_code': 2, 'status': 'failure',
                        'message': 'category parser details was not found kindly add it'}, 200

            # return result,200
        except Exception as e:
            print(e)
            dbutils.close()
            return {'response_code': 2, 'status': 'failure', 'message': 'unexpected error'}, 200
        else:
            dbutils.close()
            return {'response_code': 0, 'status': 'success',
                    'message': 'category parser details updated successfully'}, 200


@api.route('/delete')
class Delete_Category(Resource):
    @api.doc('delete_Category')
    @api.expect(delete_parser)
    def delete(self):
        try:

            dbutils = DbUtils()
            category_name = request.form['category_name']
            site_id = request.form['site_id']
            category_parser_details,count = dbutils.get_category_parser_details(0, 1, {'site_id': site_id,'category_name':category_name})
            if count > 0:
                dbutils.delete_category_parser_details({'site_id':site_id,'category_name':category_name})
            else:
                dbutils.close()
                return {'response_code': 2, 'status': 'failure', 'message': 'Site doesnt exist.'}, 200

        except Exception as e:
            print(e)
            dbutils.close()
            return {'response_code': 2, 'status': 'failure', 'message': 'unexpected error'}, 200
        else:
            dbutils.close()
            return {'response_code': 0, 'status': 'success',
                    'message': 'deleted category parser details  successfully'}, 200


