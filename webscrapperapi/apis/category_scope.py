from flask import Flask, abort, request, jsonify,Blueprint
from flask_restplus import Namespace, Resource, fields, reqparse
#from flask_sqlalchemy import SQLAlchemy
from core.Paginator import get_paginated_list
import decimal,datetime,json
import json
from core.utils import DbUtils
from core.LoggingManager import get_logger

api = Namespace('category_scope', description='Update category scope')

category_scope = api.model('category_scope', {
    'category':fields.String(required=True, description='Category'),
    'scope':fields.Boolean(required = True, description='Scope'),
    'project_id':fields.Integer(required=True, description='Project_id')
})

CATEGORY_SCOPE = [{
    'category':'felix', 'scope':'felix', 'project_id':'felix'
}]
get_parser = reqparse.RequestParser()
get_parser.add_argument('start', type='int', help='integer value')
get_parser.add_argument('limit', type='int', help='integer value')
get_parser.add_argument('project_id', type=int, help = 'Project ID')

update_parser = reqparse.RequestParser()
update_parser.add_argument('project_id', type=int, help='Project ID', location='form')
update_parser.add_argument('category', type=str, help='Category', location='form')
update_parser.add_argument('scope', type=bool, help='Scope of category', location='form')

post_parser = reqparse.RequestParser()
post_parser.add_argument('project_id', type=int, help='Project ID', location='form')
post_parser.add_argument('category', type=str, help='Category', location='form')
post_parser.add_argument('scope', type=bool, help='Scope of category', location='form')

#API route for listing category scope
@api.route('/')
class GetScope(Resource):
    @api.doc('list_scope')
    @api.expect(get_parser)
    def get(self):
        dbutils = DbUtils()
        start = int(request.args.get('start', 1))
        limit = int(request.args.get('limit', 10))
        project_id = request.args.get('project_id', None)
        searchQuery = {}
        if project_id is not None:
            searchQuery['project_id'] = int(project_id)
        else:
            dbutils.close()
            return {'status': 'failure', 'message': 'No value provided'}

        scope, count = dbutils.get_category_scope(int(start) - 1, limit, searchQuery)
        if count > 0:
            data = get_paginated_list(scope, 'api/v1/category_scope', count, start, limit=limit)
            dbutils.close()
            return data
        else:
            dbutils.close()
            return {'response_code': 2 ,'status':'failure', 'message': 'Project ID does not exist.'}, 200

#API route for updating category
@api.route('/update')
class UpdateScope(Resource):
    @api.doc('update_scope')
    @api.expect(update_parser)
    def put(self):
        dbutils = DbUtils()
        result = {}
        try:
            project_id = request.form['project_id']
            category = request.form['category']
            scope = True if 'scope' in request.form else False
            searchQuery = {}
            if project_id and category:
                searchQuery['project_id'] = int(project_id) if project_id else ''
                searchQuery['category'] = category if category else ''
                print(searchQuery)
                scopes, count = dbutils.get_category_scope(0, 1, searchQuery)
                print(count)
                if count > 0:
                    result = dbutils.update_category_scope({'project_id': project_id, 'category': category,
                                                            'scope': scope})
                else:
                    dbutils.close()
                    return {'status': 'failure', 'message': 'No Category found'}
            else:
                dbutils.close()
                return {'status': 'failure', 'message': 'No value provided'}
        except Exception as e:
            get_logger().error('Error in updating category scope:', exc_info=True)
            dbutils.close()
            return {'response_code': 2, 'status': 'failure', 'message': 'unexpected error'}, 200
        else:
            dbutils.close()
            return {'response_code': 0, 'status': 'success', 'message': 'Category scope updated successfully'}, 200

#API route for adding new category
@api.route('/add_category')
class AddCategory(Resource):
    @api.doc('Adding category')
    @api.expect(post_parser)
    def post(self):
        dbutils = DbUtils()
        result = {}
        try:
            project_id = request.form['project_id']
            scope = True if 'scope' in request.form else False
            category = request.form['category']
            scopes, count = dbutils.get_project_config(0, 1, {'project_id':project_id})
            print(count)
            if count > 0:
                result = dbutils.create_category_scope({'project_id': project_id, 'scope': scope, 'category': category})
            else:
                dbutils.close()
                return {'response_code': 2, 'status': 'failure', 'message': 'Project does not exist. Please add'}, 200
        except Exception as e:
            dbutils.close()
            return {'response_code': 2, 'status': 'failure', 'message': 'unexpected error'}, 200

        else:
            dbutils.close()
            return {'response_code': 0, 'status': 'success', 'message': 'New categories added successfully'}, 200
