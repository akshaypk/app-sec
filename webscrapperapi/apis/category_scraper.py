from flask import Flask, redirect, request, jsonify, Blueprint, Response
from flask_restplus import Namespace, Resource, fields, reqparse
import json
from core.utils import DbUtils
from core.LoggingManager import get_logger
from .products import products


api = Namespace('category_scraper', description='upc dynamic scraper ')
post_parser = reqparse.RequestParser()
post_parser.add_argument('category_to_scrape', type=str, action='split', location='form')
post_parser.add_argument('site_id', type=int,  location = 'form')


get_parser = reqparse.RequestParser()
get_parser.add_argument('site_id', required=True, type=int)
get_parser.add_argument('category_name', action='split', required=True, type = str)


@api.route('/add')
class post_upc(Resource):
    @api.doc('post_upc')
    @api.expect(post_parser)
    def post(self):
        dbutils = DbUtils()
        category = request.form['category_to_scrape']
        site_id = request.form['site_id']
        print(category, site_id)
        try:
            data = dbutils.category_parser({'category': category, 'site_id': site_id})
            dbutils.close()
            return data
        except Exception as e:
            get_logger().error('Error in creating site config :', exc_info=True)
            dbutils.close()
            return {'response_code': 0, 'status': 'success',
                    'message': 'category scraper added details created successfully'}, 200

@api.route('/get')
class Get_upc(Resource):
    @api.doc('get_products')
    @api.expect(get_parser)
    def get(self):
        dbutils = DbUtils()
        site_id = request.args.get('site_id')
        category_name = request.args.get('category_name')
        category_list = category_name.split(',')
        print(site_id, category_list)
        try:
            data = dbutils.get_catgeory_products({'site_id': site_id, 'category_list': category_list})
            return json.dumps(data)
        except Exception as e:
            dbutils.close()
            get_logger().error("Error ", exc_info=True)

