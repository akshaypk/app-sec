from flask import Flask, abort, request, jsonify, Blueprint, app
from flask_restplus import Namespace, Resource, fields, reqparse
# from flask_sqlalchemy import SQLAlchemy
from core.Paginator import get_paginated_list
import decimal, datetime
from core.LoggingManager import get_logger
import decimal, datetime, json
import datetime
# import DynamicWebScrapper.scraper.JobProcessor
from core.utils import DbUtils

api = Namespace('cronjob', description='cronjobs related operations')

cronjob = api.model('cronjob', {
    'cron_id': fields.Integer(required=True, description='The cron id identifier'),
    'minute': fields.Integer(required=True, description='The minute description'),
    'hour': fields.Integer(required=True, description='hour description'),
    'day_of_week': fields.Integer(required=True, description='The day_of_week description'),
    'day_of_month': fields.Integer(required=True, description=' day_of_month description'),
    'month_of_year': fields.Integer(required=True, description='The month_of_year description'),
    'last_executed': fields.DateTime(dt_format='UTC', description='last_executed'),
    'site_id': fields.Integer(required=True, description='siteid'),
    'category_name': fields.String(description='categoryname'),
})

SCHEDULED_JOBS = [
    {'cron_id': 'felix', 'minute': 'Felix', 'hour': 'Felix', 'day_of_week': 'Felix',
     'day_of_month': 'Felix', 'month_of_year': 'Felix', 'last_executed': 'Felix', 'site_id': 'felix',
     'category_name': 'Felix'},
]

post_parser = reqparse.RequestParser()
post_parser.add_argument('site_id', type=int, location='form')
post_parser.add_argument('category_name', type=str, location='form')
post_parser.add_argument('minute', type=str, location='form')
post_parser.add_argument('hour', type=str, location='form')
post_parser.add_argument('day_of_week', type=str, location='form')
post_parser.add_argument('day_of_month', type=str, location='form')
post_parser.add_argument('month_of_year', type=str, location='form')
post_parser.add_argument('status', type=bool, help='True/False', location='form')

get_parser = reqparse.RequestParser()
get_parser.add_argument('category_name', type='str', location='category name')
get_parser.add_argument('site_id', type='str', location='site id')


@api.route('/')
class Webpage(Resource):
    @api.doc('webpage_parser')
    # @api.marshal_with(sites)
    @api.expect(get_parser)
    def get(self):
        dbutils = DbUtils()

        site_id = request.args.get('site_id', None)
        category = request.args.get('category_name', None)
        # print(category,"==")
        searchQuery = {}

        if not site_id and not category:
            cronjob = dbutils.get_cron_details()
            task_list = []
            for item_cron in cronjob:
                task_list.append(dict(item_cron))

            return jsonify({"results": task_list, "count": len(task_list)})

        if site_id is not None:
            searchQuery['site_id'] = site_id
        if category is not None:
            searchQuery['category_name'] = category

        # print (searchQuery)

        cronjob = dbutils.get_cronjobs(searchQuery)
        print(cronjob, "cron job details")

        # if not site_id and not category:
        #     task_list=[]
        #     for item in cronjob:
        #         task_list.append(dict(item))
        #
        #     return jsonify({"results":task_list,"count":len(task_list)})

        result = {}
        if cronjob:
            cronid = cronjob[0]['cron_id']

            jobid = cronjob[0]['job_id']

            # data = get_paginated_list(cronjob, '/api/v1/cronjob')
            data = []
            dbutils.close()
            cronscheduler = dbutils.get_cronjob({'cron_id': cronid})

            if cronscheduler:
                for item in cronjob:
                    print(item, "item in cronjob")
                    result['id'] = item['id']
                    result['cron_id'] = item['cron_id']
                    result["total_run_count"] = item["total_run_count"]
                    result["last_run_at"] = item["last_run_at"]
                    result['category_name'] = item["category_name"]
                    result['job_id'] = item["job_id"]
                    result['site_id'] = item["site_id"]
                    result['status'] = item["status"]

                for item in cronscheduler:
                    result['minute'] = item["minute"]
                    result['hour'] = item["hour"]
                    result['day_of_week'] = item["day_of_week"]
                    result['day_of_month'] = item["day_of_month"]
                    result['month_of_year'] = item["month_of_year"]
                    result["last_executed"] = item['last_executed']

        return jsonify({"results": [result], "count": 1})


@api.route('/add')
class Addwebpage(Resource):
    @api.doc('post_sites')
    # @api.marshal_with(sites)
    @api.expect(post_parser)
    def post(self):
        dbutils = DbUtils()
        result = {}
        isUpdate = False
        try:
            minute = request.form['minute'] if 'minute' in request.form else ""
            hour = request.form['hour'] if 'hour' in request.form else ""
            day_of_week = request.form['day_of_week'] if 'day_of_week' in request.form else ""
            day_of_month = request.form['day_of_month'] if 'day_of_month' in request.form else ""
            month_of_year = request.form['month_of_year'] if 'month_of_year' in request.form else ""
            site_id = request.form['site_id'] if 'site_id' in request.form else ""
            category = request.form['category_name'] if 'category_name' in request.form else None
            status = dbutils.get_status_bool(request, 'status')
            job_id = request.form['job_id'] if 'job_id' in request.form else None

            if not category or category == 'null':
                category = None

            if not job_id or job_id == 'null':
                job_id = None

            get_logger().debug('category name:%s', category)
            get_logger().debug('site id:%s', site_id)

            if site_id is None or not site_id:
                return {'message': 'No site id is found'}

            searchQuery = {}
            if minute and hour and day_of_week and day_of_month and month_of_year:
                searchQuery['minute'] = minute if minute else ''
                searchQuery['hour'] = hour if hour else ''
                searchQuery['day_of_week'] = day_of_week if day_of_week else ''
                searchQuery['day_of_month'] = day_of_month if day_of_month else ''
                searchQuery['month_of_year'] = month_of_year if month_of_year else ''
            else:
                return {'response_code': 2, 'status': 'failure',
                        'message': 'cron job details doesnt exist please add.'}, 200

            project = dbutils.get_cronjob(searchQuery)
            cronid = None
            if project:
                cronid = project[0]["cron_id"]
                get_logger().debug('cron id :%s', cronid)
            else:
                result = dbutils.create_cronjob({'minute': minute,
                                                 'hour': hour,
                                                 'day_of_week': day_of_week,
                                                 'day_of_month': day_of_month,
                                                 'month_of_year': month_of_year, })

                project = dbutils.get_cronjob(searchQuery)
                get_logger().debug('crontab schedule table details :%s', project)
                if project:
                    cronid = project[0]["cron_id"]

            get_logger().debug('cron id:%s', cronid)
            if cronid:
                searchQuery = {}
                searchQuery['site_id'] = site_id
                searchQuery['category_name'] = category
                periodic_tasks = dbutils.get_periodictask(searchQuery)
                get_logger().debug('periodic_tasks table  details:%s', periodic_tasks)

                jobid = None
                if job_id:
                    jobid = job_id
                    isUpdate = True
                    get_logger().debug('Edit jobid:%s', jobid)

                else:
                    if periodic_tasks:
                        jobid = periodic_tasks[0]['job_id']
                        isUpdate = True
                        # return {'message': 'cron job already available for this job details, please create new details'}
                        # print ("already stored in periodic task")
                    else:
                        searchQuery = {}
                        searchQuery['site_id'] = site_id
                        searchQuery['category'] = category
                        scheduled_jobs, count = dbutils.get_scheduled_jobss(0, 1, searchQuery)
                        if count >= 1:
                            jobid = scheduled_jobs[0]['job_id'] if not job_id else job_id

                if jobid:
                    if isUpdate:
                        updatequery = {}
                        updatequery['cron_id'] = cronid
                        updatequery['status'] = status
                        updatequery['job_id'] = jobid
                        updatequery['site_id'] = site_id
                        periodic_task = dbutils.update_periodictask(updatequery)
                        get_logger().debug('updated periodic_tasks table  details:%s', periodic_task)
                    else:
                        searchQuery = {}
                        searchQuery['site_id'] = site_id
                        searchQuery['category_name'] = category
                        searchQuery['cron_id'] = cronid
                        searchQuery['job_id'] = jobid
                        searchQuery['status'] = status
                        result = dbutils.create_periodictask(searchQuery)
                        get_logger().debug('inserted table details:%s', result)
                        # return {'message': 'cron job already available for this job details, please create new details'},200
                else:
                    get_logger().debug('No job_id found:%s', site_id)
                    return {'response_code': 2, 'status': 'failure', 'message': "No job_id found"}, 200

        except Exception as e:
            print(e)
            dbutils.close()
            return {'response_code': 2, 'status': 'failure', 'message': str(e)}, 200
        else:
            dbutils.close()
            if isUpdate:
                return {'response_code': 0, 'status': 'success',
                        'message': 'cron job details updated successfully'}, 200
            else:
                return {'response_code': 0, 'status': 'success', 'message': 'cron job details added successfully'}, 200