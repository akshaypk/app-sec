from flask import Flask, abort, request, jsonify, Blueprint
from flask_restplus import Namespace, Resource, fields, reqparse
# from flask_sqlalchemy import SQLAlchemy
from core.Paginator import get_paginated_list
import decimal, datetime, json
import json
from core.utils import DbUtils
from core.LoggingManager import get_logger

api = Namespace('dynamic_scrapper_details', description='dynamic parser related operations')

dynamic_scrapper_details = api.model('dynamic_scrapper_details ', {
    'dynamic_scrapper_id': fields.Integer( description='The dynamic scrapepr identifier'),
    'site_id': fields.Integer( description='The site identifier'),
    'base_search_url': fields.String( description='The base search url'),
    'search_results_xpath': fields.String( description='the search results xpath'),
    'search_result_accuracy': fields.Integer( description='the search result accuracy'),
    'dynamic_scrapper_status': fields.Boolean( description='the dynamic scrapper status '),
    'search_result_type': fields.String( description='The search result type'),
    'search_result_json_xpath': fields.String( description='The search result json xpath'),
    'parse_with_selenium': fields.Boolean( description='the parse with selenium'),
    'append_srchtxt_to_url': fields.Boolean( description='The append search text to url'),
    'driver_id': fields.Integer( description='the driver identifier'),
})

DYNAMIC_SCRAPPER_DETAILS = [
    {'dynamic_scrapper_id': 'felix', 'site_id': 'Felix', 'base_search_url': 'Felix', 'search_results_xpath': 'Felix',
     'search_result_accuracy': 'Felix', 'dynamic_scrapper_status': 'Felix', 'search_result_type': 'Felix',
     'search_result_json_xpath': 'Felix', 'parse_with_selenium': 'Felix', 'append_srchtxt_to_url': 'Felix', 'driver_id': 'Felix'}
]


post_parser = reqparse.RequestParser()
post_parser.add_argument('site_id',  type=int, location='form')
post_parser.add_argument('base_search_url', type=str,location='form')
post_parser.add_argument('search_results_xpath',  type=str,location='form')
post_parser.add_argument('search_result_accuracy',  type=int, location='form')
post_parser.add_argument('dynamic_scrapper_status',  type=bool,  help='True/False',location='form')
post_parser.add_argument('search_result_type',  type=str, location='form')
post_parser.add_argument('search_result_json_xpath',  type=str, location='form')
post_parser.add_argument('parse_with_selenium',  type=bool,  help='True/False', location='form')
post_parser.add_argument('append_srchtxt_to_url',  type=bool, help='True/False', location='form')
post_parser.add_argument('driver_id',  type=int, location='form')
post_parser.add_argument('upc_length',  type=int, location='form')

get_parser = reqparse.RequestParser()
get_parser.add_argument('start', type=int, help='integer value')
get_parser.add_argument('limit', type=int, help='integer value')
get_parser.add_argument('site_id', type=int, help='integer value')
get_parser.add_argument('site_name',type=str,help='site name')
get_parser.add_argument('search_result_type', help='string')
get_parser.add_argument('base_search_url', help='url')


update_parser = reqparse.RequestParser()
update_parser.add_argument('site_id',  type=int, location='form')
update_parser.add_argument('base_search_url', help='http://test.com', location='form')
update_parser.add_argument('search_results_xpath',  type=str,  help='path',location='form')
update_parser.add_argument('search_result_accuracy',  type=int, location='form')
update_parser.add_argument('dynamic_scrapper_status',  type=bool,  help='True/False',location='form')
update_parser.add_argument('search_result_type',  type=str, location='form')
update_parser.add_argument('search_result_json_xpath',  type=str, help='path', location='form')
update_parser.add_argument('parse_with_selenium',  type=bool,  help='True/False', location='form')
update_parser.add_argument('append_srchtxt_to_url',  type=bool, help='True/False', location='form')
update_parser.add_argument('driver_id',  type=int, location='form')
update_parser.add_argument('upc_length',  type=int, location='form')


delete_parser = reqparse.RequestParser()
delete_parser.add_argument('site_id', type =int, location = 'form')



@api.route('/')
class Dynamic(Resource):
    @api.doc('dynamic_parser')
    # @api.marshal_with(sites)
    @api.expect(get_parser)
    def get(self):
        dbutils = DbUtils()
        start = int(request.args.get('start', 1))
        limit = int(request.args.get('limit', 10))
        site_id = request.args.get('site_id', None)
        site_name=request.args.get('site_name',None)
        search_result_type = request.args.get('search_result_type', None)
        base_search_url = request.args.get('base_search_url', None)
        searchQuery = {}
        if site_id and base_search_url and search_result_type:
            searchQuery['site_name']=site_name if site_name else ''
            searchQuery['site_id'] = site_id if site_id else ''
            searchQuery['base_search_url'] = base_search_url if base_search_url else ''
            searchQuery['search_result_type'] = search_result_type if search_result_type else ''
        elif site_id is not None:
            searchQuery['site_id'] = site_id
        elif site_name is not None:
            searchQuery['site_name'] = site_name
        elif base_search_url is not None:
            searchQuery['base_search_url'] = base_search_url
        elif search_result_type is not None:
            searchQuery['search_result_type'] = search_result_type

        dynamic_scrapper, count = dbutils.get_dynamic_scrapper_details(int(start) - 1, limit, searchQuery)
        dbutils.close()
        if count:
            data = get_paginated_list(dynamic_scrapper, '/api/v1/dynamic_scrapper_details', count, start, limit=limit)
            return data
        else:
            return {'response_code': 2, 'status': 'failure', 'message': 'No data found'}, 200



@api.route('/add')
class AddSite(Resource):
    @api.doc('post_sites')
    # @api.marshal_with(sites)
    @api.expect(post_parser)
    def post(self):
        dbutils = DbUtils()
        result = {}
        try:
            # check for the existing site
            site_id = request.form['site_id']
            base_search_url = request.form['base_search_url'] if 'base_search_url' in request.form else None
            search_results_xpath = request.form['search_results_xpath'] if 'search_results_xpath' in request.form else None
            search_result_accuracy = request.form['search_result_accuracy'] if 'search_result_accuracy' in request.form else None
            search_result_type = request.form['search_result_type'] if 'search_result_type' in request.form else None
            search_result_json_xpath = request.form['search_result_json_xpath'] if 'search_result_json_xpath' in request.form else None
            driver_id = request.form['driver_id'] if 'driver_id' in request.form else None
            upc_length = request.form['upc_length'] if 'upc_length' in request.form else None

            # dynamic_scrapper_status = True if 'dynamic_scrapper_status' in request.form else False
            # parse_with_selenium = True if 'parse_with_selenium' in request.form else False
            # append_srchtxt_to_url = True if 'append_srchtxt_to_url' in request.form else False

            dynamic_scrapper_status = dbutils.get_status_bool(request, 'dynamic_scrapper_status')
            parse_with_selenium = dbutils.get_status_bool(request, 'parse_with_selenium')
            append_srchtxt_to_url = dbutils.get_status_bool(request, 'append_srchtxt_to_url')

            print(site_id, base_search_url, search_results_xpath, search_result_accuracy,dynamic_scrapper_status, search_result_type,
                  search_result_json_xpath, parse_with_selenium, append_srchtxt_to_url,driver_id)

            dynamic_scrapper_details, count = dbutils.get_dynamic_scrapper_details(0, 1, {'site_id': site_id})
            if count > 0:
                dbutils.close()
                return {'response_code': 2, 'status': 'failure', 'message': 'Site already exists.'}, 200
            else:
                result = dbutils.create_dynamic_scrapper_details({'site_id': site_id, 'base_search_url': base_search_url,
                                              'search_results_xpath': search_results_xpath,
                                              'search_result_accuracy': int(search_result_accuracy) if search_result_accuracy else 0,
                                              'dynamic_scrapper_status': dynamic_scrapper_status,
                                              'search_result_type': search_result_type,
                                              'search_result_json_xpath': search_result_json_xpath,
                                              'parse_with_selenium': parse_with_selenium,
                                              'append_srchtxt_to_url': append_srchtxt_to_url,
                                              'upc_length': str(upc_length) if upc_length else None,
                                               'driver_id': int(driver_id) if driver_id else 4})

            # return result,200
        except Exception as e:
            print(e)
            dbutils.close()
            return {'response_code': 2, 'status': 'failure', 'message': 'unexpected error'}, 200
        else:
            dbutils.close()
            return {'response_code': 0, 'status': 'success',
                    'message': 'dynamic scrapper details created successfully'}, 200


@api.route('/update')
class Update_dynamic(Resource):
    @api.doc('update_dynamic')
    @api.expect(update_parser)
    def put(self):
        dbutils = DbUtils()
        result = {}
        try:
            site_id = request.form['site_id']
            base_search_url = request.form['base_search_url'] if 'base_search_url' in request.form else None
            search_results_xpath = request.form['search_results_xpath'] \
                if 'search_results_xpath' in request.form else None
            search_result_accuracy = request.form['search_result_accuracy'] \
                if 'search_result_accuracy' in request.form else None
            search_result_type = request.form['search_result_type'] \
                if 'search_result_type' in request.form else None
            search_result_json_xpath = request.form['search_result_json_xpath'] \
                if 'search_result_json_xpath' in request.form else None

            # parse_with_selenium = True if 'parse_with_selenium' in request.form else False
            # append_srchtxt_to_url = True if 'append_srchtxt_to_url' in request.form else False
            driver_id = request.form['driver_id'] if 'driver_id' in request.form else None
            upc_length = request.form['upc_length'] if 'upc_length' in request.form else None
            # print("========================",upc_length)

            dynamic_scrapper_status = dbutils.get_status_bool(request, 'dynamic_scrapper_status')
            parse_with_selenium = dbutils.get_status_bool(request, 'parse_with_selenium')
            append_srchtxt_to_url = dbutils.get_status_bool(request, 'append_srchtxt_to_url')

            dynamic_scrapper_details, dynamic_count = dbutils.get_dynamic_scrapper_details(0, 1, {'site_id':site_id})

            if dynamic_count > 0:
               result=dbutils.update_dynamic_scrapper_details({'site_id': site_id,
                                                                'base_search_url': base_search_url,
                                                                'search_results_xpath': search_results_xpath,
                                                                'search_result_accuracy': int(search_result_accuracy) if search_result_accuracy else 0,
                                                                'dynamic_scrapper_status': dynamic_scrapper_status,
                                                                'search_result_type': search_result_type,
                                                                'search_result_json_xpath': search_result_json_xpath,
                                                                'parse_with_selenium': parse_with_selenium,
                                                                'append_srchtxt_to_url': append_srchtxt_to_url,
                                                                'upc_length': str(upc_length) if upc_length else None,
                                                                'driver_id': int(driver_id) if driver_id else 4
                                                                  })
            else:
               dbutils.close()
               return {'response_code': 2, 'status': 'failure',
                       'message': 'dynamic scrapper Details was not found kindly add it'}, 200


            # return result,200
        except Exception as e:
            print(e)
            dbutils.close()
            return {'response_code': 2, 'status': 'failure', 'message': 'unexpected error'}, 200
        else:
            dbutils.close()
            return {'response_code': 0, 'status': 'success', 'message': 'dynamic scrapper details updated successfully'}, 200

@api.route('/delete')
class Delete_dynamic(Resource):
    @api.doc('delete_dynamic')
    @api.expect(delete_parser)
    def delete(self):
       try:

            dbutils = DbUtils()
            site_id = request.form['site_id']
            site, count = dbutils.get_dynamic_scrapper_details(0, 1, {'site_id': site_id})
            if count > 0:
               dbutils.delete_dynamic_scrapper_details(site_id)
            else:
               dbutils.close()
               return {'response_code': 2, 'status': 'failure', 'message': 'Site doesnt exist.'}, 200

       except Exception as e:
            print(e)
            dbutils.close()
            return {'response_code': 2, 'status': 'failure', 'message': 'unexpected error'}, 200
       else:
            dbutils.close()
            return {'response_code': 0, 'status': 'success', 'message': 'Deleted dynamic scrapper details  successfully'}, 200

