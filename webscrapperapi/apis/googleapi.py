import string

from flask_restplus import Namespace, Resource, fields,reqparse
from flask import Flask, Response, request, jsonify,Blueprint,send_file,after_this_request
import urllib3,json
import requests
import json
from datetime import datetime
from core import settings
from core.utils import DbUtils
import pandas
from io import StringIO, BytesIO
import time
import re
import zipfile
from core.LoggingManager import get_logger
import jsonpath
from lxml import html
try:
    from urllib.parse import urljoin
except:
    from urlparse import urljoin

api = Namespace('googleapi', description='Google custom search related operations')


GOOGLEAPI = [{'ean':'Felix','text':'Felix'}]

get_parser = reqparse.RequestParser()
get_parser.add_argument('ean', help = 'enter')
get_parser.add_argument('text', help = 'enter')
get_parser.add_argument('crawl_type', type=str, choices=('imageSearch', 'normal'))


post_parser = reqparse.RequestParser()
post_parser.add_argument('search_text', type=str, action='split', location = 'form')
post_parser.add_argument('site_id', type=int, location='form')
post_parser.add_argument('file_name', type=str, location='form')
post_parser.add_argument('region', type=str, location='form')
post_parser.add_argument('scrape_using', type=str, choices=('ean', 'text'),location = 'form')
post_parser.add_argument('crawl_type', type=str, choices=('imageSearch', 'normal'),location = 'form')

get_download = reqparse.RequestParser()
get_download.add_argument('job_id',help ='Enter job id')
get_download.add_argument('download_format', type=str, choices=('CSV', 'ZIP'))

engine_parser = reqparse.RequestParser()
engine_parser.add_argument('region', type=str, location='form')
engine_parser.add_argument('website', type=str, location='form')
engine_parser.add_argument('api_key', type=str, location='form')
engine_parser.add_argument('cx_key', type=str, location='form')

get_engine = reqparse.RequestParser()
get_engine.add_argument('region', help = 'enter')
get_engine.add_argument('website', help = 'enter')


@api.route('/search_engine_get')
class GoogleApiEngineList(Resource):
    @api.doc('Get Search Engine')
    #@api.marshal_list_with(cat)
    @api.expect(get_engine)
    def get(self):
        result = {}
        try:
            dbutils = DbUtils()
            region = request.args.get('region', None)
            website = request.args.get('website', None)

            searchQuery = {}
            if region:
                searchQuery['region'] = region
            if website:
                searchQuery['website'] = website

            data = dbutils.get_search_engine_details(searchQuery)
            result=[]
            for item in data:
                result.append(dict(item))

            dbutils.close()

            if result:
                return jsonify(result)
            else:
                return {'response_code': 2, 'status': 'failure', 'message': 'No data found'}, 200

        except Exception as e:
            pass

@api.route('/search_engine_add')
class InsertSearchEngine(Resource):
    @api.doc("add_engine")
    @api.expect(engine_parser)
    def post(self):
        try:
            dbutils = DbUtils()
            region = request.form['region'] if 'region' in request.form else None
            website = request.form['website'] if 'website' in request.form else None
            cx_key = request.form['cx_key'] if 'cx_key' in request.form else None
            api_key = request.form['api_key'] if 'api_key' in request.form else None
            get_logger().info('cx_key : %s', cx_key)

            if not region or not cx_key:
                return {'message': 'missing one of region or cx_key', 'response_code': 2, 'status': "failure"}


            searchQuery = {}
            searchQuery['region'] = region
            result = dbutils.get_search_engine_details(searchQuery)
            searchQuery['website'] = website
            searchQuery['cx_key'] = cx_key

            if api_key:
                searchQuery['api_key'] = api_key

            if len(result)>0:
                get_logger().info('Entered to update region : %s', region)
                update = dbutils.update_search_engine_details(searchQuery)
                dbutils.close()
                return {'response_code': 0, 'status': 'success',
                        'message': 'search engine details updated successfully'}
            else:
                get_logger().info('Entered to insert region : %s', region)
                insert = dbutils.insert_search_engine_details(searchQuery)
                dbutils.close()
                return {'response_code': 0, 'status': 'success',
                        'message': 'search engine details inserted successfully'}

        except Exception as e:
            return {'message': str(e), 'response_code': 2, 'status': "failure"}

@api.route('/csv_schedule')
class GoogleApiCsv(Resource):
    @api.doc("Uploading csv file")
    @api.expect(post_parser)
    def post(self):
        try:
            dbutils = DbUtils()
            query_string = []
            site_id = request.form['site_id'] if 'site_id' in request.form else None
            scrape_using = request.form['scrape_using'] if 'scrape_using' in request.form else None
            search_text = request.form['search_text'] if 'search_text' in request.form else None
            region = request.form['region'] if 'region' in request.form else None
            crawl_type = request.form['crawl_type'] if 'crawl_type' in request.form else None

            if not scrape_using or not search_text:
                return {'message': 'missing - scrape_using or search_text', 'response_code': 2, 'status': "failure"}

            file_name = request.form['file_name']
            print(file_name, "user")

            today = datetime.now()
            file_name += str(today)

            if scrape_using == 'ean':
                string_split = search_text.split(",")
                query_string = json.dumps({'product_upc':string_split})
                
            elif scrape_using == 'text':
                string_split = search_text.split(",")
                query_string = json.dumps({'product_name': string_split})

            schedule_google_api = {'site_id': site_id,
                                   'file_name': file_name,
                                   'file_data': query_string,
                                   'scrape_using': crawl_type,
                                   'job_type': 'GOOGLEAPI'
                                   }
            if region:
                schedule_google_api['region'] = region

            dynamic_results = dbutils.dynamic_csv(schedule_google_api)
            dbutils.close()
            return {'response_code': 0, 'status': 'success', 'job_id': dynamic_results,
                    'message': 'Successfully scheduled job'}

        except Exception as e:
            return {'message': str(e), 'response_code': 2, 'status': "failure"}

@api.route('/download')
class DownloadGoogleProducts(Resource):
    @api.doc('download_google_products')
    #@api.marshal_with(products)
    @api.expect(get_download)
    def get(self):

        @after_this_request
        def add_header(r):
            """
            Add headers to both force latest IE rendering engine or Chrome Frame,
            and also to cache the rendered page for 10 minutes.
            """
            r.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
            r.headers["Pragma"] = "no-cache"
            r.headers["Expires"] = "0"
            r.headers['Cache-Control'] = 'public, max-age=0'
            return r

        try:
            start = time.time()
            dbutils = DbUtils()
            job_id = request.args.get('job_id', None)
            format = request.args.get('download_format', None)

            download_format = 'CSV' if format=='CSV' else 'ZIP'

            if job_id is None:
                return {'message': 'No Site_id', 'response_code': 2, 'status': "failure"}

            where_condition = {}
            where_condition["job_id"] = job_id

            results = dbutils.get_googleapi_products(where_condition)
            matched_count = len(results)
            print(matched_count)
            if results is None or not results:
                return {'message': 'No search data found', 'response_code': 2, 'status': "failure"}

            _csv_file = StringIO()
            keys = None
            prod_list = []
            columns = results[0].keys()
            pd = pandas.DataFrame(results, columns=columns)
            pd.to_csv(_csv_file, index=False)

            _csv_file.seek(0)
            prod_list = []
            results = []
            dbutils.close()
            csv_file_name = "{site_id}_results_{size}.csv".format(site_id=job_id, size=matched_count)

            if download_format == 'CSV':
                headers = {"Content-disposition": "attachment; filename={file_name}".format(file_name=csv_file_name)}
                return Response(_csv_file.getvalue(),mimetype='text/csv',headers=headers)

            memory_file = BytesIO()
            with zipfile.ZipFile(memory_file, 'w') as zf:
                data = zipfile.ZipInfo(csv_file_name)
                data.date_time = time.localtime(time.time())[:6]
                data.compress_type = zipfile.ZIP_DEFLATED
                text = _csv_file.getvalue()
                zf.writestr(data, text)
                text = ""

            _csv_file.close()
            del _csv_file
            memory_file.seek(0)
            end = time.time()
            print("Processed Time : ", end - start)
            zip_file_name = "{site_id}_results_{size}.zip".format(site_id=job_id, size=matched_count)

            # return  None
            return send_file(memory_file, attachment_filename=zip_file_name, as_attachment=True)
        except Exception as e:
            return {'message': str(e), 'response_code': 2, 'status': "failure"}

@api.route('/')
class GoogleApiList(Resource):
    @api.doc('list_googleapi')
    #@api.marshal_list_with(cat)
    @api.expect(get_parser)
    def get(self):
        result = {}
        try:
            ean = request.args.get('ean', None)
            text = request.args.get('text', None)
            crawl_type = request.args.get('crawl_type', None)

            if crawl_type == 'imageSearch':
                template = settings.GOOGLE_API_BASE_URL+'&searchType=image'
            else:
                template = settings.GOOGLE_API_BASE_URL

            search_query, key = None, None
            if ean:
                key = 'product_upc'
                search_query = ean

            elif text:
                key = 'product_name'
                search_query = text.replace(' ', '+')

            if not search_query or not key:
                return {'message': "search_query or key not found", 'response_code': 2, 'status': "failure"}

            url = template.format(query=search_query)
            # print(url)
            response = self.make_request(url, True)
            json_data = json.loads(response.text)

            urls = []
            src_list = []
            url_list = []
            predicted_images = []
            json_results = json_data.get('items', [])

            if json_results:
                predicted_images = self.goolge_css_image_parse(json_data, search_query)

            for items in json_results:
                url = items['link'].strip() if 'link' in items else ''
                title = items['title'].strip() if 'title' in items else ''

                image_details = items.get("image", {})
                product_url = image_details.get('contextLink')
                image_url = items.get("link") if product_url else None

                img = self.goolge_css_image_parse(items)

                if product_url:
                    get_logger().info('image type search %s', product_url)
                    url = product_url
                else:
                    src_list = list(self.find('image', json_data))

                if not url or url.endswith('.pdf'):
                    continue

                if key == 'product_name':
                    url_list.append(url)
                    print (url)

                    # getting image url in google response
                    if image_url:
                        predicted_images.append(image_url)

                    elif img:
                        predicted_images.append(img[0])

                    if len(url_list) == 5:
                        break

                elif key == 'product_upc':
                    is_matched = False
                    # checking NAME/EAN/UPC in google response
                    if str(search_query) in url or str(search_query) in title:
                        is_matched = True
                    try:
                        # print(url)
                        get_logger().info('URL request %s', url)
                        r = self.make_request(url)
                        # getting image url in html page
                        _html = r.content
                        image_xpath = '//*/img[contains(@*,"{path}")]/@src'.format(path=search_query)
                        links = self.parse_image_from_html(_html, image_xpath)
                        for link in links:
                            img_url = urljoin(url, link)
                            predicted_images.append(img_url)

                        # checking NAME/EAN/UPC in html content
                        if search_query in r.text:
                            is_matched = True

                    except Exception as e:
                        get_logger().error('ERROR %s', str(e))

                    if is_matched:
                        url_list.append(url)
                        if image_url:
                            predicted_images.append(image_url)
                        elif img:
                            predicted_images.append(img[0])

            url_list = list(set(url_list))
            predicted_images = list(set(predicted_images))
            result = {"result": {'product_urls': url_list,
                                 'image_urls': src_list if src_list else predicted_images,
                                 'predicted_images': predicted_images}}
            return result

        except Exception as e:
            return {'message': str(e), 'response_code': 2, 'status': "failure"}

    def find(self, key, dictionary):
        for k, v in dictionary.items():
            if k == key:
                yield v
            elif isinstance(v, dict):
                for result in self.find(key, v):
                    yield result
            elif isinstance(v, list):
                for d in v:
                    for result in self.find(key, d):
                        yield result

    @staticmethod
    def make_request(url, is_header=False):
        get_logger().info('Inside fetching URL %s', url)
        if is_header:
            header = {"Content-Type": "application/json"}
            r = requests.get(url, headers=header, timeout=60)
        else:
            r = requests.get(url, timeout=60)
        return r

    @staticmethod
    def goolge_css_image_parse(items, query=None):
        try:
            if query:
                img = [x for x in jsonpath.jsonpath(items, '$..cse_image.[*].src') if query in x]
            else:
                img = [x for x in jsonpath.jsonpath(items, '$..cse_image.[*].src')]

            return img
        except Exception as e:
            get_logger().error('Error is google css image parse %s', str(e))
            return []

    @staticmethod
    def parse_image_from_html(html_data, xpath):
        tree = html.fromstring(str(html_data))
        links = tree.xpath(xpath)
        return links
