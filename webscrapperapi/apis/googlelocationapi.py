from flask_restplus import Namespace, Resource, fields,reqparse
from flask import request
import urllib3,json
import requests
import json
from core import settings
from core.googlemap import GoogleMapRest

api = Namespace('googlelocationapi', description='Google custom search related operations')

get_parser = reqparse.RequestParser()
get_parser.add_argument('location', help = 'Enter search string')

@api.route('/')
class GoogleLocationAPI(Resource):

    @api.doc('list_googlemapapi')
    #@api.marshal_list_with(cat)
    @api.expect(get_parser)
    def get(self):
        try:
            #api_url = "https://www.googleapis.com/customsearch/v1?key=AIzaSyB8PfpHmME_aX_26gdYMo09D0AFrMNQDw4&cx=003329888468530643586:qhn85v-mmf8&q="
            text = request.args.get('location', None)
            #text='storelocator+ril+near+bangalore'

            pagetoken = None
            result={}
            print (text)
            if text is not None:
                print(text)
                input = {"query": text, "pagetoken": pagetoken}
                obj_init = GoogleMapRest()
                result = obj_init.start_crawl(input)
                del obj_init

            return result
        except Exception as e:
            return {'status':str(e)}