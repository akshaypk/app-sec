from flask import Flask, abort, request, jsonify, after_this_request, app,make_response,Response
from flask_restplus import Namespace, Resource, fields, reqparse
from core.LoggingManager import get_logger
from core.utils import DbUtils
import pandas as pd
import json
import time
import re
import io
from datetime import datetime
try:
    from urllib.parse import urljoin
except:
    from urlparse import urljoin
import urllib.parse
import re
try:
    from gensim.test.utils import common_corpus, common_dictionary, get_tmpfile
    import gensim
except Exception as e:
    pass

api = Namespace('upload_image_data_file', description='upload_image_data_file')

engine_parser = reqparse.RequestParser()
engine_parser.add_argument('project_id', type=int, location='form')
engine_parser.add_argument('scrape_using', type=str, location='form')
engine_parser.add_argument('file_header', type=str, location='form')
engine_parser.add_argument('type', type=str, choices=('DB', 'FILE','GOOGLE_FILE', 'GOOGLE_DB'),location='form')
engine_parser.add_argument('job_id', type=int, location='form')

get_download = reqparse.RequestParser()
get_download.add_argument('job_id',help='job_id',type=int,action='split')
get_download.add_argument('project_id',help='project_id',type=int,action='split')

get_schedule = reqparse.RequestParser()
get_schedule.add_argument('job_id',help='job_id',location='form')
get_schedule.add_argument('project_id',help='project_id',location='form')

get_parser = reqparse.RequestParser()
get_parser.add_argument('job_id',help='job_id')
get_parser.add_argument('project_id',help='project_id')
get_parser.add_argument('start', type='int',help = 'integer value')
get_parser.add_argument('limit',type='int',help = 'integer value')

@api.route('/')
class form(Resource):
    @api.doc('form data')
    @api.expect(get_parser)
    def get(self):
        job_id = request.args.get('job_id')
        project_id = request.args.get('project_id')
        start = int(request.args.get('start', 1))
        limit = int(request.args.get('limit', 10))
        search_dict = {}
        try:
            dbutils = DbUtils()
            if project_id:
                search_dict['project_id'] = project_id
            if job_id:
                search_dict['job_id'] = job_id

            data = dbutils.get_image_puller_lookup_by_query(search_dict)
            if data:
                item = []
                for value in data:
                    item.append(dict(value))

                dbutils.close()
                return jsonify(item)
            else:
                dbutils.close()
                return {'response_code': 2, 'status': 'failure', 'message': 'no result found'}
        except Exception as e:
            dbutils.close()
            return {'response_code': 2, 'status': 'failure', 'message': str(e)}

@api.route('/uploader')
class ImagePuller(Resource):
    @api.doc("add_engine")
    @api.expect(engine_parser)
    def post(self):
        try:
            dbutils = DbUtils()
            f = request.files['data_file'] if 'data_file' in request.files else None
            file_name = ""
            project_id = request.form['project_id'] if 'project_id' in request.form else None
            job_id = request.form['job_id'] if 'job_id' in request.form else None
            scrape_using = request.form['scrape_using'] if 'scrape_using' in request.form else None
            site_id = request.form['site_id'] if 'site_id' in request.form else None
            flag = request.form['flag'] if 'flag' in request.form else None
            start_time = request.form['start_time'] if 'start_time' in request.form else None
            file_header = request.form['file_header'] if 'file_header' in request.form else None
            _type = request.form['type'] if 'type' in request.form else None

            if not project_id:
                return {'response_code': 2, 'status': 'failure', 'message': 'no project found'}

            if job_id:
                scheduled_jobs, count = dbutils.get_scheduled_jobss(0, 1, {"job_id": job_id})
                if count <= 0:
                    dbutils.close()
                    return {'response_code': 2, 'status': 'failure', 'message': 'Invalid job id'}

            job_status = dbutils.get_scheduled_running_job(project_id)
            if len(job_status) >= 1 and _type == "FILE":
                dbutils.close()
                return {'response_code': 2, 'status': 'failure', 'message': 'Project Already scheduled or Running'}

            if f:
                stream = io.StringIO(f.stream.read().decode("UTF8"), newline=None)
                df = pd.read_csv(stream, dtype=str)
                df.replace(to_replace=[pd.np.nan,None], value="", inplace=True)
                columns = df.columns.tolist()
                file_name = f.filename
            else:
                # print ("dummy dataframe")
                df = pd.DataFrame(columns=['dummy'])

            today = datetime.utcnow()
            if not start_time:
                start_time = today

            file_name += str(today)
            insertquerry = {}
            insertquerry['project_id'] = project_id
            insertquerry['job_type'] = 'DB'
            insertquerry['status'] = 'UNSCHEDULED'
            insertquerry['file_name'] = file_name
            insertquerry['start_time'] = start_time
            # print ("==============",job_id)
            is_lookup_update = True
            if not job_id:
                result = dbutils.create_scheduled_jobs(insertquerry)
                job_id = int(result['data'])
            else:
                is_lookup_update = False
                dbutils.update_scheduled_jobs({'job_id': job_id, 'status': 'UNSCHEDULED'})

            get_logger().info('job_id %s',job_id)
            bulk_insert = []
            for index,row in df.iterrows():
                # print (row)
                # print (row[file_header],scrape_using)
                product_name = row[file_header] \
                    if scrape_using == 'product_name' else ''
                product_upc = row[file_header] \
                    if scrape_using == 'product_upc' else ''
                product_url = row[file_header] \
                    if scrape_using == 'product_url' else ''

                if _type == "DB":
                    product_upc = str(product_upc).zfill(15)

                insert_dict = {'project_id': project_id,
                               'job_id': job_id,
                               'file_name': file_name if file_name else None,
                               'product_name': product_name,
                               'product_upc': product_upc,
                               'product_url': product_url,
                               'scrape_using': scrape_using,
                               'description': dict(row)
                               }
                # print (insert_dict)
                bulk_insert.append(insert_dict)
            # print(insert_dict)

            del insertquerry['job_type']
            if is_lookup_update:
                insertquerry['job_id'] = job_id
                insertquerry['total'] = len(bulk_insert)
                dbutils.insert_image_puller_lookup(insertquerry)

            # print (len(bulk_insert),"length")
            if bulk_insert:
                dynamic_results = dbutils.project_csv(bulk_insert)

            if _type == 'GOOGLE_DB':
                dbutils.update_scheduled_jobs({'job_id': job_id,
                                               'job_type': 'GOOGLEIMAGE',
                                               'status': 'COMPLETED'})
                dbutils.close()
                return {'response_code': 0,
                        'status': 'success',
                        'message': 'Scheduled Job',
                        'job_id': job_id}

            elif _type == 'GOOGLE_FILE':
                dbutils.update_scheduled_jobs({'job_id': job_id,
                                               'job_type': 'GOOGLEIMAGE',
                                               'status': 'SCHEDULED'})
                dbutils.close()
                return {'response_code': 0,
                        'status': 'success',
                        'message': 'Scheduled Job',
                        'job_id': job_id}

            elif _type == 'FILE':
                get_logger().info('Inside -> File')
                dbutils.update_scheduled_jobs({'job_id': job_id, 'status': 'SCHEDULED'})
                dbutils.close()
                return {'response_code': 0,
                        'status': 'success',
                        'message': 'Scheduled Job',
                        'job_id': job_id}

            elif _type == 'DB':
                # print("DB inside")
                where_dict = {}
                where_dict['project_id'] = project_id
                details,count = dbutils.get_project_config(0,1,where_dict)
                if count >= 1:
                    primary_site_id = details[0].get('primary_site_id')
                    secondary_site = details[0].get('secondary_site_ids')
                    if secondary_site:
                        sites = str(primary_site_id) + ',' + secondary_site
                    else:
                        sites = str(primary_site_id)
                    if scrape_using == 'product_upc':
                        get_logger().info('Inside -> DB -> product_upc')
                        status = dbutils.insert_data_job_id_map_table(job_id,sites)

                    elif scrape_using == 'product_name':
                        # print("product_name")
                        try:
                            get_logger().info('Inside -> DB -> product_name')
                            lookup = {}
                            desc_list = []
                            description = dbutils.get_import_product_desc(job_id)
                            for index,item in enumerate(description):
                                # print(item)
                                prod_name = item["product_name"]
                                imp_id = item["id"]
                                desc_list.append(self.string_normalize(prod_name))
                                lookup[index] = {'product_name':prod_name,'id':imp_id}

                            tf_idf, sims, dictionary = self.create_tf_idf_object(desc_list)
                            result = dbutils.get_product_meta_info(sites)
                            for item in result:
                                name = item["product_name"]
                                product_id = item["product_id"]
                                site_id = item["site_id"]
                                text = self.string_normalize(name)
                                query_doc_bow = self.create_test_doc(text,dictionary)
                                query_doc_tf_idf = tf_idf[query_doc_bow]
                                prob = sims[query_doc_tf_idf]
                                index = prob.argmax(axis=0)
                                if prob[index] >= 0.5:
                                    get_logger().info('matched product name : %s',name)
                                    in_data = lookup[index]
                                    _id = in_data.get('id')
                                    product_to_insert = {}
                                    product_to_insert['site_id'] = site_id
                                    product_to_insert['product_id'] = product_id
                                    product_to_insert['import_product_id'] = _id
                                    product_to_insert['job_id'] = job_id
                                    dbutils.insert_jobid_mapping_product(product_to_insert)
                                    # print(text, in_data.get('product_name'),prob[index])
                        except Exception as e:
                            get_logger().error('Error in DB Product Name process %s',str(e))
                    dbutils.update_scheduled_jobs({'job_id': job_id, 'status': 'COMPLETED'})
                    return {'response_code': 0,
                            'status': 'success',
                            'message': 'Scheduled Job',
                            'job_id': job_id}

        except Exception as e:
            dbutils.close()
            return {'response_code': 2, 'status': 'failure', 'message': str(e)}

    def word_tokenize(self, text):
        # text=re.sub(r' \d+ ',' ',text)
        return re.split(';|,|\*|\n| |\(|\)|\/|\.', text)

    def string_normalize(self, text):
        prd_name = text.replace(',', ' ') if type(text) == str else str(text)
        sp_prd_name = prd_name.split(' ')
        text = ' '.join(set(sp_prd_name))
        return text

    def create_test_doc(self,text, dictionay):
        query_doc = [w.lower() for w in self.word_tokenize(text.lower())]
        query_doc_bow = dictionay.doc2bow(query_doc)
        return query_doc_bow

    def create_corpus(self, raw_documents):
        gen_docs = [[w.lower() for w in self.word_tokenize(text)]
                    for text in raw_documents]
        dictionary = gensim.corpora.Dictionary(gen_docs)
        corpus = [dictionary.doc2bow(gen_doc) for gen_doc in gen_docs]

        return dictionary,corpus

    def create_tf_idf_object(self, raw_documents):
        # model
        dictionary,corpus = self.create_corpus(raw_documents)
        tf_idf = gensim.models.TfidfModel(corpus)
        index_temp_file = get_tmpfile("index")
        sims = gensim.similarities.Similarity(index_temp_file, tf_idf[corpus],
                                              num_features=len(dictionary))
        get_logger().info('created tf-idf object')
        return tf_idf, sims, dictionary



@api.route('/schedule')
class JobSchedule(Resource):
    @api.doc('schedule_job')
    @api.expect(get_schedule)
    def post(self):
        try:
            job_id = request.form['job_id'] if 'job_id' in request.form else None

            if job_id is None:
                job_id = request.args.get('job_id', None)

            if job_id is None:
                return {'message': 'No job_id', 'response_code': 2, 'status': "failure"}
            # project_id = request.args.get('project_id', None)
            project_id = request.form['project_id'] if 'project_id' in request.form else None
            if not project_id:
                project_id = request.args.get('project_id', None)

            get_logger().info('job id %s project id %s :', project_id, job_id)

            dbutils = DbUtils()
            where_dict = {'category': job_id}
            job_status = dbutils.get_scheduled_job_search(where_dict)
            if job_status and len(job_status) >= 1:
                dbutils.close()
                return {'response_code': 2, 'status': 'failure', 'message': 'Job already running'}
            else:
                scheduled_job = self.schedule_a_job(dbutils,job_id,project_id)
                # print(scheduled_job)
                dbutils.close()
                return {'response_code': 0,
                        'status': 'success',
                        'message': 'Scheduled Job',
                        'job_id': scheduled_job}

        except Exception as e:
            dbutils.close()
            return {'response_code': 2, 'status': 'failure', 'message': str(e)}

    def schedule_a_job(self, dbutils, category, project_id):
        start_time = datetime.utcnow()
        insertquerry = {}
        if project_id:
            insertquerry['project_id'] = project_id

        insertquerry['job_type'] = 'IMAGELOAD'
        insertquerry['status'] = 'SCHEDULED'
        insertquerry['start_time'] = start_time
        insertquerry['category'] = category
        result = dbutils.create_scheduled_jobs(insertquerry)
        job_id = int(result['data'])
        return job_id

@api.route('/download')
class DownloadJsonFile(Resource):
    @api.doc('download_products')
    @api.expect(get_download)
    def get(self):
        try:
            job = request.args.get('job_id', None)
            project = request.args.get('project_id', None)

            if job is None or project is None:
                return {'message': 'No job_id', 'response_code': 2, 'status': "failure"}

            dbutils = DbUtils()
            job_id_list = job.split(',')
            project_list = project.split(',')
            lookup = {}
            for key,value in zip(project_list,job_id_list):
                if key not in lookup:
                    lookup[key] = []
                lookup[key].append(value)

            final_result = []
            processed_url = []
            for project_id, job_ids in lookup.items():
                for job_id in job_ids:
                    get_logger().info('job id %s project id %s :', project_id,job_id)
                    sites = self.get_sites(dbutils,project_id)
                    site_details = dbutils.get_site_names(sites)
                    site_id_name = {}
                    is_google = False
                    for details in site_details:
                        site_id = details["site_id"]
                        site_name = details["site_name"]
                        site_url = details["site_url"]
                        if 'www.google.' in site_url:
                            is_google = True
                        site_config = []
                        require_sitemap_parsing = details["require_sitemap_parsing"]
                        require_dynamic_scrapping = details["require_dynamic_scrapping"]
                        require_category_parsing = details["require_category_parsing"]
                        if require_sitemap_parsing:
                            site_config.append('Sitemap')
                        if require_dynamic_scrapping:
                            site_config.append('Dynamic')
                        if require_category_parsing:
                            site_config.append('Category')
                        crawl_type = ','.join(site_config)
                        site_id_name[site_id] = {}
                        site_id_name[site_id]['site_name'] = site_name
                        site_id_name[site_id]['crawl_type'] = crawl_type

                    if is_google:
                        image_details = dbutils.get_google_products_details(job_id)
                    else:
                        image_details = dbutils.get_project_details_json_file(job_id)

                    for row in image_details:
                        try:
                            upc = row['product_upc']
                            if not upc:
                                upc = 'UPC'
                            name = row['product_name'] if row['product_name'] else upc
                            site_id = row['site_id'] if 'site_id' in row else None
                            site_data = site_id_name.get(site_id, {})
                            site_name = site_data.get('site_name')
                            crawl_type = site_data.get('crawl_type')
                            if not site_name:
                                site_name, crawl_type = (row['site_name'],'Google') if 'site_name' in row \
                                    else ("",None)
                                print(site_name, crawl_type)
                            image = row['product_imageurl'] if 'product_imageurl' in row else ""
                            product_url = row['product_url'] if 'product_url' in row else ""
                            scrape_using = row['scrape_using'] if 'scrape_using' in row else ""
                            product_additional_images = row["product_additional_images"] if row else []

                            # avoid duplicate entries in final result
                            if product_url not in project_list:
                                project_list.append(product_url)
                            else:
                                continue

                            scrape_using = 'EAN' if scrape_using == 'product_upc' else 'Product Name'
                            product_images = []
                            if product_additional_images:
                                product_images = product_additional_images if type(product_additional_images) == list \
                                    else product_additional_images.split(';')

                            if image:
                                product_images.insert(0,image)

                            azure_images = []
                            abs_images = []
                            processed=[]
                            for index,src in enumerate(product_images):
                                try:
                                    img_count = index + 1
                                    if img_count > 10:
                                        break

                                    img_url = src.strip()
                                    if not src.startswith('http'):
                                        img_url = urljoin(product_url, src)
                                    if 'e-coop' in img_url:
                                        img_url = img_url.replace('&f=png&w=&w=45&f=png','&f=png&w=&w=1000')

                                    if img_url not in processed:
                                        processed.append(img_url)
                                    else:
                                        continue

                                    abs_images.append(img_url)

                                    """if want to change this file name format also needs to 
                                                change dynamic scrapper"""
                                    # p_name = ''.join(filter(str.isalnum, name))
                                    # p_name = urllib.parse.quote_plus(p_name)
                                    value = re.findall('[A-Za-z0-9 ]', name)
                                    p_name = ''.join(value)
                                    file_name = str(upc) + '/' + \
                                                str(site_id) + '_' + \
                                                p_name.replace(' ','_') + '_' + \
                                                str(img_count) + '.jpg'
                                    azure_images.append('API_WSP/'+file_name)
                                except Exception as e:
                                    get_logger().error('Error in updating search_engine_details config :', exc_info=True)

                            json_dict = {}
                            json_dict['source_type'] = scrape_using
                            json_dict['site_name'] = site_name
                            json_dict['crawl_type'] = crawl_type
                            for key,value in row.items():
                                if 'product_imageurl' == key \
                                        or 'product_additional_images' == key \
                                        or 'scrape_using' == key:
                                    continue
                                json_dict[key] = value

                            json_dict['image_url'] = abs_images
                            json_dict['image_download_url'] = azure_images
                            final_result.append(json_dict)

                        except Exception as e:
                            print (e)
                            get_logger().error('Error in updating search_engine_details config :', exc_info=True)

            # print(final_result)
            @after_this_request
            def add_header(r):
                """
                Add headers to both force latest IE rendering engine or Chrome Frame,
                and also to cache the rendered page for 10 minutes.
                """
                r.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
                r.headers["Pragma"] = "no-cache"
                r.headers["Expires"] = "0"
                r.headers['Cache-Control'] = 'public, max-age=0'
                return r

            print(len(final_result))
            file_name = "attachment;filename=result_{count}.json".format(count=len(final_result))
            return Response(json.dumps(final_result),
                            mimetype='application/json',
                            headers={'Content-Disposition': file_name})
            # return final_result

        except Exception as e:
            return {'message': str(e), 'response_code': 2, 'status': "failure"}

    def get_sites(self, dbutils,project_id):
        """
            get site details using project configuration
        """
        proj_details = dbutils.get_project_details(project_id)
        sites = []
        primary_site = str(proj_details['primary_site_id'])
        secondary_site = proj_details['secondary_site_ids']
        sites.append(primary_site)
        if secondary_site:
            secondary_sites = [str(x) for x in secondary_site.split(',')]
            if secondary_sites:
                sites.extend(secondary_sites)
        return sites

