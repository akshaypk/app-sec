from flask import Flask, abort, request, jsonify, Blueprint
from flask_restplus import Namespace, Resource, fields, reqparse
# from flask_sqlalchemy import SQLAlchemy
from core.Paginator import get_paginated_list
import decimal, datetime, json,string
import json
from core.utils import DbUtils

api = Namespace('import_products', description='import_products related operations')

import_products = api.model('import_products ', {
    'id': fields.Integer(required=True, description='id identifier'),
    'period_code': fields.Integer(required=True, description='Period code identifier'),
    'product_upc': fields.Integer(required=True, description='The product_upc'),
    'source_id': fields.Integer(required=True, description='the source_id'),
    'product_category': fields.String(required=True, description='the product_category'),
    'product_subcategory1': fields.String(required=True, description='the product_subcategory1'),
    'product_subcategory2': fields.String(required=True, description='The product_subcategory2'),
    'product_subcategory3': fields.String(required=True, description='The  product_subcategory3'),
    'product_name': fields.String(required=True, description='The product_name'),
    'product_discount_price': fields.String(required=True, description='the product_brand'),
    'product_offer_category': fields.String(required=True, description='The product_price'),
    'product_sku': fields.String(required=True, description='The product_imageurl'),
    'product_desc': fields.String(required=True, description='the product_name'),
    'product_ingredients': fields.Boolean(required=True, description='the scrap_status'),
    'product_additional_images': fields.Integer(required=True, description='the scrap_source'),
    'product_brand': fields.String(required=True, description='the product_brand'),
    'product_url': fields.String(required=True, description='the product_name'),
    'scrap_status': fields.Boolean(required=True, description='the scrap_status'),
    'scrap_source': fields.Integer(required=True, description='the scrap_source'),
    'product_mapid': fields.Integer(required=True, description='the product_mapid'),
    'product_manufacturer_part_number': fields.String(required=True, description='the product_manufacturer_part_number'),
    'scope': fields.Boolean(required=True, description='the scope'),
    'file_name': fields.String(required=True, description='the file_name'),
    'project_id': fields.Integer(required=True, description='theproject_id'),
    'job_id': fields.Integer(required=True, description='the job_id'),
    'update_time': fields.DateTime(dt_format='UTC', description=' update time'),
    'product_price': fields.String(required=True, description='The product_price'),
    'product_imageurl': fields.String(required=True, description='The product_imageurl'),

    })


IMPORT_PRODUCTS = [
    {'id': 'felix', 'period_code': 'Felix', 'product_upc': 'Felix', 'source_id': 'Felix'
     ,'product_category': 'Felix', 'product_subcategory1': 'Felix', 'product_subcategory2': 'Felix','product_subcategory3': 'Felix',
     'product_brand': 'Felix', 'product_name': 'Felix','product_url': 'Felix','scrap_status': 'Felix',
     'scrap_source': 'Felix','product_mapid': 'Felix','product_manufacturer_part_number': 'Felix',
     'product_discount_price': 'Felix', 'product_offer_category': 'Felix','product_sku': 'Felix',
     'product_desc': 'Felix','product_ingredients': 'Felix', 'product_additional_images': 'Felix',
     'scope': 'Felix','file_name': 'Felix','project_id': 'Felix','job_id': 'Felix','update_time': 'Felix','product_price': 'Felix','product_imageurl': 'Felix'},
]



get_parser = reqparse.RequestParser()
get_parser.add_argument('start', type=int, help='integer value')
get_parser.add_argument('limit', type=int, help='integer value')
get_parser.add_argument('file_name', type=str, help='string')
get_parser.add_argument('project_id', type=str, help='string')



@api.route('/')
class product (Resource):
    @api.doc('product_parser')
    # @api.marshal_with(sites)
    @api.expect(get_parser)
    def get(self):
        dbutils = DbUtils()
        start = int(request.args.get('start', 1))
        limit = int(request.args.get('limit', 10))
        file_name = request.args.get('file_name', None)
        project_id = request.args.get('project_id', None)

        searchQuery = {}
        if  file_name and project_id :
            searchQuery['file_name'] = file_name if file_name else ''
            searchQuery['project_id'] = project_id if project_id else ''
        elif file_name is not None:
            searchQuery['file_name'] = file_name
        elif project_id is not None:
            searchQuery['project_id'] = project_id

        import_products, count = dbutils.get_import_products(int(start) - 1, limit, searchQuery)
        dbutils.close()
        if count:
            data = get_paginated_list(import_products, '/api/v1/import_products', count,start, limit=limit)
            return data
        else:
            return {'response_code': 2, 'status': 'failure', 'message': 'No data found'}, 200


