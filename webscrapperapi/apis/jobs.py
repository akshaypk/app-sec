from flask import Flask, abort, request, jsonify,Blueprint
from flask_restplus import Namespace, Resource, fields,reqparse
#from flask_sqlalchemy import SQLAlchemy
from core.Paginator import get_paginated_list
import decimal,datetime,json
import json
from core.utils import DbUtils
api = Namespace('jobs', description='Jobs related operations')

jobs = api.model('Job', {
    'job_id': fields.Integer(required=True, description='The job identifier'),
    'site_id': fields.Integer(required=True, description='The site name'),
    'total_products_scraped': fields.Integer(required=True, description='The total products scraped'),
    'project_id': fields.Integer(required=True, description=' project name'),
    'start_time': fields.String(required=True, description='start time'),
    'file_name': fields.String(required=True, description='file name'),
    'end_time': fields.String(required=True, description='end time'),
    'job_type': fields.String(required=True, description='job type'),
    'timeout': fields.Integer(required=True, description='time out'),
    'status': fields.String(required=True, description='status'),
    'update_time': fields.String(required=True, description=' update time'),
    'comments': fields.String(required=True, description='comments '),
    'total_products_to_scrape': fields.Integer(required=True, description='total products scraped'),
})
	

JOBS = [
    {'job_id': 'felix', 'site_id': 'Felix', 'total_products_scraped': 'Felix', 'project_id': 'Felix'
    , 'start_time': 'Felix', 'file_name': 'Felix', 'end_time': 'Felix', 'job_type': 'Felix'
    , 'timeout': 'Felix','status':'Felix','update_time':'Felix','comments':'Felix',
    'total_products_to_scrape':'Felix','start':'Felix','limit':'Felix'},
]

post_parser = reqparse.RequestParser()
post_parser.add_argument('site_name',  required = True, help='test.com',location='form')
post_parser.add_argument('site_url', required = True,  help='http://test.com',location='form')
post_parser.add_argument('require_sitemap_parsing',required = True, type = bool, default = False, help='True/False',location='form')
post_parser.add_argument('require_webpage_parsing',required = True, type = bool,default = False, help='True/False',location='form')
post_parser.add_argument('require_dynamic_parsing', required = True,type = bool, default = False,help='True/False',location='form')
post_parser.add_argument('parse_status',required = True,  type = bool,default = False,help='True',location='form')
post_parser.add_argument('site_status', required = True,type = bool,default = True,help='True',location='form')
post_parser.add_argument('image_download', required = True,type = bool, default = False, help='path',location='form')

get_parser = reqparse.RequestParser()
get_parser.add_argument('start', type='int',help = 'integer value')
get_parser.add_argument('limit',type='int',help = 'integer value')
get_parser.add_argument('site_name',help = 'site name')
get_parser.add_argument('project_name',help = 'project name')
get_parser.add_argument('start_time',help = 'start time')
get_parser.add_argument('status',help = 'status')



@api.route('/')
class Job(Resource):
	@api.doc('list_jobs')
	#@api.marshal_with(jobs)
	@api.expect(get_parser)	
	def get(self):
		dbutils = DbUtils()
		start = int(request.args.get('start',1))
		limit = int(request.args.get('limit',10))
		site_name = request.args.get('site_name',None)
		project_name = request.args.get('project_name', None)
		start_time = request.args.get('start_time', None)
		status = request.args.get('status', None)
		searchQuery={}
		if project_name is not None:
			searchQuery['project_name']=project_name
		if site_name is not None:
			searchQuery['site_id'] = site_name
		if start_time is not None:
			searchQuery['start_time'] = start_time
		if status is not None:
			searchQuery['status'] = status
		print(searchQuery)
		#sites =dbutils.get_sites({'site_name':site_name,'site_url':site_url})
		jobs,count =dbutils.get_jobs(int(start)-1,limit,searchQuery)
		print(count)
		data = get_paginated_list(jobs,'/api/v1/jobs',count,start,limit=limit)
		dbutils.close()
		return data


	