from flask import Flask, abort, request, jsonify, Blueprint
from flask_restplus import Namespace, Resource, fields, reqparse
from sqlalchemy import Column, String
from core.Paginator import get_paginated_list
import decimal, datetime, json
import json
from core.utils import DbUtils

api = Namespace('location_parser_details', description='location parser details operations')

location_parser_details = api.model('location_parser_details ', {
    'id': fields.String(required=True, description='id'),
    'site_id': fields.String(required=True, description='the site identifier'),
    'site_url': fields.String(required=True, description='the site url identifier'),
    'search_xpath': fields.String(required=True, description='The search xpath '),
    'category_xpath': fields.String(required=True,description='The category xpath'),
    'subcategory1_xpath': fields.String(required=True, description='the subcategory1 xpath '),
    'subcategory2_xpath': fields.String(required=True, description='the subcategory2 xpath'),
    'pagination_xpath': fields.String(required=True, description='the pagination xpath'),
    'zero_depth_xpath': fields.String(required=True, description='The zero depth xpath'),
    'product_url_xpath': fields.String(required=True, description='The product url xpath'),
    'parse_with_selenium': fields.Boolean(required=True, description='the parse with selenium'),
    'driver_id': fields.String(required=True, description='the driver id'),

        })

LOCATION_PARSER_DETAILS = [
    {   'id': 'felix', 'site_id': 'Felix', 'site_url': 'Felix','search_xpath': 'Felix', 'category_xpath': 'Felix'
        , 'subcategory1_xpath': 'Felix', 'subcategory2_xpath': 'Felix', 'pagination_xpath': 'Felix','zero_depth_xpath': 'Felix'
        , 'product_url_xpath': 'Felix', 'parse_with_selenium': 'Felix', 'driver_id': 'Felix',

          },
]

post_parser = reqparse.RequestParser()
post_parser.add_argument('site_id', required=True, type=int, location='form')
post_parser.add_argument('site_url', required=True, type=str, location='form')
post_parser.add_argument('search_xpath',type=str, help='path',location='form')
post_parser.add_argument('category_xpath',type=str, help='path',location='form')
post_parser.add_argument('subcategory1_xpath',  type=str, help='path',location='form')
post_parser.add_argument('subcategory2_xpath', type=str,help='path',location='form')
post_parser.add_argument('pagination_xpath', type=str, help='path', location='form')
post_parser.add_argument('zero_depth_xpath',  type=str, help='path', location='form')
post_parser.add_argument('product_url_xpath',  type=str, help='path', location='form')
post_parser.add_argument('parse_with_selenium',  type=bool,  help='True/False', location='form')
post_parser.add_argument('driver_id', type=int,  help='path', location='form')





get_parser = reqparse.RequestParser()
get_parser.add_argument('start', type=int, help='integer value')
get_parser.add_argument('limit', type=int, help='integer value')
get_parser.add_argument('site_id',type=int, help='integer value')
get_parser.add_argument('site_name',type=str,help='string')
get_parser.add_argument('parse_with_selenium',type=str, help='string')




update_parser = reqparse.RequestParser()
update_parser.add_argument('site_id', required=True, type=int, help='site id', location='form')
update_parser.add_argument('site_url',  type=str, help='path',location='form')
update_parser.add_argument('search_xpath',  type=str, help='path',location='form')
update_parser.add_argument('category_xpath', type=str,  help='path',location='form')
update_parser.add_argument('subcategory1_xpath',  type=str,  help='path',location='form')
update_parser.add_argument('subcategory2_xpath',  type=str, help='path', location='form')
update_parser.add_argument('pagination_xpath', type=str, help='path', location='form')
update_parser.add_argument('zero_depth_xpath',type=str, help='path', location='form')
update_parser.add_argument('product_url_xpath', type=str,  help='path', location='form')
update_parser.add_argument('parse_with_selenium', type=bool,  help='True/False', location='form')
update_parser.add_argument('driver_id', type=int,help='path', location='form')





delete_parser = reqparse.RequestParser()
delete_parser.add_argument('site_id', type =int, location = 'form')




@api.route('/')
class Locationparser(Resource):
    @api.doc('location_parser_details')
    # @api.marshal_with(sites)
    @api.expect(get_parser)
    def get(self):
        dbutils = DbUtils()
        start = int(request.args.get('start', 1))
        limit = int(request.args.get('limit', 10))
        parse_with_selenium = request.args.get('parse_with_selenium', None)
        site_id = request.args.get('site_id', None)
        site_name=request.args.get('site_name',None)
        searchQuery = {}
        if parse_with_selenium and site_id :
            searchQuery['parse_with_selenium'] = parse_with_selenium if parse_with_selenium else ''
            searchQuery['site_id'] = site_id if site_id else ''
            searchQuery['site_name'] = site_name if site_name else ''
        elif parse_with_selenium is not None:
            searchQuery['parse_with_selenium'] = parse_with_selenium
        elif site_id is not None:
            searchQuery['site_id'] = site_id
        elif site_name is not None:
            searchQuery['site_name'] = site_name
        location_parser_details, count = dbutils.get_location_parser_details(int(start) - 1, limit, searchQuery)
        dbutils.close()
        if count:
            data = get_paginated_list(location_parser_details, '/api/v1/location_parser_details', count, start, limit=limit)
            return data
        else:
            return {'response_code': 2, 'status': 'failure', 'message': 'No data found'}, 200




@api.route('/add')
class Addlocationparser(Resource):
    @api.doc('postlocationparser')
    # @api.marshal_with(sites)
    @api.expect(post_parser)
    def post(self):
        dbutils = DbUtils()
        result = {}

        try:
                # check for the existing location_parser_details
                site_id = request.form['site_id']
                site_url = request.form['site_url'] if 'site_url' in request.form else ''
                search_xpath = request.form['search_xpath'] if 'search_xpath' in request.form else ''
                category_xpath = request.form['category_xpath'] if 'category_xpath' in request.form else ''
                subcategory1_xpath = request.form['subcategory1_xpath'] if 'subcategory1_xpath' in request.form else ''
                subcategory2_xpath = request.form['subcategory2_xpath'] if 'subcategory2_xpath' in request.form else ''
                pagination_xpath = request.form['pagination_xpath'] if 'pagination_xpath' in request.form else ''
                zero_depth_xpath = request.form['zero_depth_xpath'] if 'zero_depth_xpath' in request.form else ''
                product_url_xpath = request.form['product_url_xpath'] if 'product_url_xpath' in request.form else ''
                is_selenium = request.form.get('parse_with_selenium', 'false')
                if is_selenium:
                    parse_with_selenium = True if is_selenium == 'true' else False
                else:
                    parse_with_selenium = False

                driver_id = request.form['driver_id'] if 'driver_id' in request.form else None

                site, site_count = dbutils.get_sites(0, 1, {'site_id': site_id})

                location_parser_details, count = dbutils.get_location_parser_details(0, 1, { 'parse_with_selenium': parse_with_selenium,'site_id': site_id,})
                if site_count>0:
                   if count > 0:
                        dbutils.close()
                        return {'response_code': 2, 'status': 'failure', 'message': 'location_parser_details already exists.'}, 200
                   else:
                         result = dbutils.create_location_parser_details({ 'site_id': site_id,

                                              'site_url': site_url,
                                              'search_xpath': search_xpath,
                                              'category_xpath': category_xpath,
                                              'subcategory1_xpath': subcategory1_xpath,
                                              'subcategory2_xpath': subcategory2_xpath,
                                              'pagination_xpath': pagination_xpath,
                                              'zero_depth_xpath': zero_depth_xpath,
                                              'product_url_xpath':product_url_xpath,
                                              'parse_with_selenium': parse_with_selenium,
                                              'driver_id': driver_id,
                                              })

                else:
                    dbutils.close()
                    return {'response_code': 2, 'status': 'failure',
                            'message': ' site id  does not exists kindly add.'}, 200
        # return result,200
        except Exception as e:
            print(e)
            dbutils.close()
            return {'response_code': 2, 'status': 'failure', 'message': 'unexpected error'}, 200
        else:
            dbutils.close()
            return {'response_code': 0, 'status': 'success', 'message': 'location_parser_details  added successfully'}, 200




@api.route('/update')
class update_webpage(Resource):
  @api.doc('updatelocationparser')
  @api.expect(update_parser)
  def put(self):
     dbutils = DbUtils()
     result = {}
     try:
                    # check for the existing location parser
                    site_id = request.form['site_id']
                    site_url = request.form['site_url'] if 'site_url' in request.form else ''
                    search_xpath = request.form['search_xpath'] if 'search_xpath' in request.form else ''
                    category_xpath = request.form['category_xpath'] if 'category_xpath' in request.form else ''
                    subcategory1_xpath = request.form[
                        'subcategory1_xpath'] if 'subcategory1_xpath' in request.form else ''
                    subcategory2_xpath = request.form[
                        'subcategory2_xpath'] if 'subcategory2_xpath' in request.form else ''
                    pagination_xpath = request.form['pagination_xpath'] if 'pagination_xpath' in request.form else ''
                    zero_depth_xpath = request.form['zero_depth_xpath'] if 'zero_depth_xpath' in request.form else ''
                    product_url_xpath = request.form[
                        'product_url_xpath'] if 'product_url_xpath' in request.form else ''

                    is_selenium=request.form.get('parse_with_selenium','false')
                    if is_selenium:
                        parse_with_selenium = True if is_selenium=='true' else False
                    else:
                        parse_with_selenium= False

                    driver_id = request.form['driver_id'] if 'driver_id' in request.form else None

                    category, count = dbutils.get_location_parser_details (0, 1, {'site_id': site_id})

                    if count > 0:
                        result = dbutils.update_location_parser_details (
                            {'site_id': site_id,
                             'site_url': site_url,
                             'search_xpath': search_xpath,
                             'category_xpath': category_xpath,
                             'subcategory1_xpath': subcategory1_xpath,
                             'subcategory2_xpath': subcategory2_xpath,
                             'pagination_xpath': pagination_xpath,
                             'zero_depth_xpath': zero_depth_xpath,
                             'product_url_xpath':product_url_xpath,
                             'parse_with_selenium':parse_with_selenium,
                             'driver_id': driver_id,})
                    else:
                      dbutils.close()
                      return {'response_code': 2, 'status': 'failure', 'message': 'Site doesnt exist please add .'}, 200

     # return result,200
     except Exception as e:
         print(e)
         dbutils.close()
         return {'response_code': 2, 'status': 'failure', 'message': 'unexpected error'}, 200

     else:
         dbutils.close()
         return {'response_code': 0, 'status': 'success',
                 'message': 'location parser details updated successfully'}, 200

@api.route('/delete')
class Delete_locationparser(Resource):
    @api.doc('delete_locationparser')
    @api.expect(delete_parser)
    def delete(self):
        try:

            dbutils = DbUtils()

            site_id = request.form['site_id']
            site, count = dbutils.get_sites(0, 1, {'site_id': site_id})

            if count > 0:
                dbutils.delete_location_parser_details(site_id)
            else:
                dbutils.close()
                return {'response_code': 2, 'status': 'failure', 'message': 'Site doesnt exist.'}, 200

        except Exception as e:
            print(e)
            dbutils.close()
            return {'response_code': 2, 'status': 'failure', 'message': 'unexpected error'}, 200
        else:
            dbutils.close()
            return {'response_code': 0, 'status': 'success', 'message': 'Deleted location parser details  successfully'}, 200
