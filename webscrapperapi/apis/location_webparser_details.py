from flask import Flask, abort, request, jsonify, Blueprint
from flask_restplus import Namespace, Resource, fields, reqparse
from sqlalchemy import Column, String
from core.Paginator import get_paginated_list
import decimal, datetime, json
import json
from core.utils import DbUtils

api = Namespace('location_webparser_details', description='location webparser details operations')

location_webparser_details = api.model('location_webparser_details ', {
    'id': fields.String(required=True, description='id'),
    'site_id': fields.String(required=True, description='the site identifier'),
    'storename_xpath': fields.String(required=True, description='the storename xpath'),
    'chainname_xpath': fields.String(required=True, description='The chainname xpath '),
    'address_xpath': fields.String(required=True,description='The address xpath'),
    'country_xpath': fields.String(required=True, description='the country xpath '),
    'state_xpath': fields.String(required=True, description='the state xpath'),
    'city_xpath': fields.String(required=True, description='the city xpath'),
    'latitude_xpath': fields.String(required=True, description='The latitude xpath'),
    'longitude_xpath': fields.String(required=True, description='The longitude xpath'),
    'phone_xpath': fields.String(required=True, description='the phone xpath'),
    'email_xpath': fields.String(required=True, description='the email xpath'),
    'workinghours_xpath': fields.String(required=True, description='the workinhours xpath'),
    'storeurl_xpath': fields.String(required=True, description='the storeurl xpath'),

        })

LOCATION_WEBPARSER_DETAILS = [
    {   'id': 'felix', 'site_id': 'Felix', 'storename_xpath': 'Felix','chainname_xpath': 'Felix', 'address_xpath': 'Felix'
        , 'country_xpath': 'Felix', 'state_xpath': 'Felix', 'city_xpath': 'Felix','latitude_xpath': 'Felix'
        , 'longitude_xpath': 'Felix', 'phone_xpath': 'Felix', 'email_xpath': 'Felix', 'workinghours_xpath': 'Felix',
        'storeurl_xpath': 'Felix',

          },
]

post_parser = reqparse.RequestParser()
post_parser.add_argument('site_id', required=True, type=int, location='form')
post_parser.add_argument('storename_xpath', type=str, help='path',location='form')
post_parser.add_argument('chainname_xpath',type=str, help='path',location='form')
post_parser.add_argument('address_xpath',type=str, help='path',location='form')
post_parser.add_argument('country_xpath',  type=str, help='path',location='form')
post_parser.add_argument('state_xpath', type=str,help='path',location='form')
post_parser.add_argument('city_xpath', type=str, help='path', location='form')
post_parser.add_argument('latitude_xpath',  type=str, help='path', location='form')
post_parser.add_argument('longitude_xpath',  type=str, help='path', location='form')
post_parser.add_argument('phone_xpath',  type=str, help='path', location='form')
post_parser.add_argument('email_xpath', type=str,  help='path', location='form')
post_parser.add_argument('workinghours_xpath', type=str,  help='path', location='form')
post_parser.add_argument('storeurl_xpath', type=str,  help='path', location='form')





get_parser = reqparse.RequestParser()
get_parser.add_argument('start', type=int, help='integer value')
get_parser.add_argument('limit', type=int, help='integer value')
get_parser.add_argument('site_id',type=int, help='integer value')
get_parser.add_argument('site_name',type=str,help='string value')


update_parser = reqparse.RequestParser()
update_parser.add_argument('site_id', required=True, type=int, help='site id', location='form')
update_parser.add_argument('storename_xpath',  type=str, help='path',location='form')
update_parser.add_argument('chainname_xpath',  type=str, help='path',location='form')
update_parser.add_argument('address_xpath', type=str,  help='path',location='form')
update_parser.add_argument('country_xpath',  type=str,  help='path',location='form')
update_parser.add_argument('state_xpath',  type=str, help='path', location='form')
update_parser.add_argument('city_xpath', type=str, help='path', location='form')
update_parser.add_argument('latitude_xpath',type=str, help='path', location='form')
update_parser.add_argument('longitude_xpath', type=str,  help='path', location='form')
update_parser.add_argument('phone_xpath', type=str,  help='path', location='form')
update_parser.add_argument('email_xpath', type=str,help='path', location='form')
update_parser.add_argument('workinghours_xpath', type=str,help='path', location='form')
update_parser.add_argument('storeurl_xpath', type=str,help='path', location='form')





delete_parser = reqparse.RequestParser()
delete_parser.add_argument('site_id', type =int, location = 'form')




@api.route('/')
class Locationwebparser(Resource):
    @api.doc('location_webparser_details')
    # @api.marshal_with(sites)
    @api.expect(get_parser)
    def get(self):
        dbutils = DbUtils()
        start = int(request.args.get('start', 1))
        limit = int(request.args.get('limit', 10))
        site_id = request.args.get('site_id', None)
        site_name=request.args.get('site_name',None)

        searchQuery = {}
        if site_id and site_name:
            searchQuery['site_id'] = site_id
            searchQuery['site_name']=site_name
        elif site_id is not None:
            searchQuery['site_id'] = site_id
        elif site_name is not None:
            searchQuery['site_name'] =site_name

        location_webparser_details, count = dbutils.get_location_webparser_details(int(start) - 1, limit, searchQuery)
        data = get_paginated_list(location_webparser_details, '/api/v1/location_webparser_details', count, start, limit=limit)
        dbutils.close()
        return data



@api.route('/add')
class Addlocationwebparser(Resource):
    @api.doc('postlocationwebparser')
    # @api.marshal_with(sites)
    @api.expect(post_parser)
    def post(self):
        dbutils = DbUtils()
        result = {}

        try:
                # check for the existing location_webparser_details
                site_id = request.form['site_id']
                storename_xpath = request.form['storename_xpath'] if 'storename_xpath' in request.form else ''
                chainname_xpath = request.form['chainname_xpath'] if 'chainname_xpath' in request.form else ''
                address_xpath = request.form['address_xpath'] if 'address_xpath' in request.form else ''
                country_xpath = request.form['country_xpath'] if 'country_xpath' in request.form else ''
                state_xpath = request.form['state_xpath'] if 'state_xpath' in request.form else ''
                city_xpath = request.form['city_xpath'] if 'city_xpath' in request.form else ''
                latitude_xpath = request.form['latitude_xpath'] if 'latitude_xpath' in request.form else ''
                longitude_xpath = request.form['longitude_xpath'] if 'longitude_xpath' in request.form else ''
                phone_xpath = request.form['phone_xpath'] if 'phone_xpath' in request.form else ''
                email_xpath = request.form['email_xpath'] if 'email_xpath' in request.form else ''
                workinghours_xpath = request.form['workinghours_xpath'] if 'workinghours_xpath' in request.form else ''
                storeurl_xpath = request.form['storeurl_xpath'] if 'storeurl_xpath' in request.form else ''




                site, site_count = dbutils.get_sites(0, 1, {'site_id': site_id})

                location_webparser_details, count = dbutils.get_location_webparser_details(0, 1, {'site_id': site_id,})
                if site_count>0:
                   if count > 0:
                        dbutils.close()
                        return {'response_code': 2, 'status': 'failure', 'message': 'location_webparser_details already exists.'}, 200
                   else:
                         result = dbutils.create_location_webparser_details({ 'site_id': site_id,
                                              'storename_xpath': storename_xpath,
                                              'chainname_xpath': chainname_xpath,
                                              'address_xpath': address_xpath,
                                              'country_xpath': country_xpath,
                                              'state_xpath': state_xpath,
                                              'city_xpath': city_xpath,
                                              'latitude_xpath': latitude_xpath,
                                              'longitude_xpath':longitude_xpath,
                                              'phone_xpath':phone_xpath,
                                              'email_xpath': email_xpath,
                                              'workinghours_xpath': workinghours_xpath,
                                              'storeurl_xpath': storeurl_xpath,
                                              })
                else:
                    dbutils.close()
                    return {'response_code': 2, 'status': 'failure',
                            'message': ' site id  does not exists kindly add.'}, 200
        # return result,200
        except Exception as e:
            print(e)
            dbutils.close()
            return {'response_code': 2, 'status': 'failure', 'message': 'unexpected error'}, 200
        else:
            dbutils.close()
            return {'response_code': 0, 'status': 'success', 'message': 'location_webparser_details  added successfully'}, 200




@api.route('/update')
class updatelocationwebparser(Resource):
  @api.doc('updatelocationwebparser')
  @api.expect(update_parser)
  def put(self):
     dbutils = DbUtils()
     result = {}
     try:
                    # check for the existing location webparser
                    site_id = request.form['site_id']
                    storename_xpath = request.form['storename_xpath'] if 'storename_xpath' in request.form else ''
                    chainname_xpath = request.form['chainname_xpath'] if 'chainname_xpath' in request.form else ''
                    address_xpath = request.form['address_xpath'] if 'address_xpath' in request.form else ''
                    country_xpath = request.form['country_xpath'] if 'country_xpath' in request.form else ''
                    state_xpath = request.form['state_xpath'] if 'state_xpath' in request.form else ''
                    city_xpath = request.form['city_xpath'] if 'city_xpath' in request.form else ''
                    latitude_xpath = request.form['latitude_xpath'] if 'latitude_xpath' in request.form else ''
                    longitude_xpath = request.form['longitude_xpath'] if 'longitude_xpath' in request.form else ''
                    phone_xpath = request.form['phone_xpath'] if 'phone_xpath' in request.form else ''
                    email_xpath = request.form['email_xpath'] if 'email_xpath' in request.form else ''
                    workinghours_xpath = request.form[
                        'workinghours_xpath'] if 'workinghours_xpath' in request.form else ''
                    storeurl_xpath = request.form['storeurl_xpath'] if 'storeurl_xpath' in request.form else ''

                    category, count = dbutils.get_location_webparser_details (0, 1, {'site_id': site_id})

                    if count > 0:
                        result = dbutils.update_location_webparser_details (
                            {'site_id': site_id,
                            'storename_xpath': storename_xpath,
                                              'chainname_xpath': chainname_xpath,
                                              'address_xpath': address_xpath,
                                              'country_xpath': country_xpath,
                                              'state_xpath': state_xpath,
                                              'city_xpath': city_xpath,
                                              'latitude_xpath': latitude_xpath,
                                              'longitude_xpath':longitude_xpath,
                                              'phone_xpath':phone_xpath,
                                              'email_xpath': email_xpath,
                                              'workinghours_xpath': workinghours_xpath,
                                              'storeurl_xpath': storeurl_xpath,
                                              })
                    else:
                      dbutils.close()
                      return {'response_code': 2, 'status': 'failure', 'message': 'Site doesnt exist please add .'}, 200

     # return result,200
     except Exception as e:
         print(e)
         dbutils.close()
         return {'response_code': 2, 'status': 'failure', 'message': 'unexpected error'}, 200

     else:
         dbutils.close()
         return {'response_code': 0, 'status': 'success',
                 'message': 'location webparser details updated successfully'}, 200

@api.route('/delete')
class Delete_locationparser(Resource):
    @api.doc('delete_locationparser')
    @api.expect(delete_parser)
    def delete(self):
        try:

            dbutils = DbUtils()

            site_id = request.form['site_id']
            site, count = dbutils.get_sites(0, 1, {'site_id': site_id})

            if count > 0:
                dbutils.delete_location_webparser_details(site_id)
            else:
                dbutils.close()
                return {'response_code': 2, 'status': 'failure', 'message': 'Site doesnt exist.'}, 200

        except Exception as e:
            print(e)
            dbutils.close()
            return {'response_code': 2, 'status': 'failure', 'message': 'unexpected error'}, 200
        else:
            dbutils.close()
            return {'response_code': 0, 'status': 'success', 'message': 'Deleted location webparser details  successfully'}, 200
