from flask import Blueprint, Flask, abort, jsonify, request
from flask_restplus import Namespace, Resource, fields, reqparse

from datetime import datetime, date

import decimal,json
from json import dumps

from core.Paginator import get_paginated_list
from core.utils import DbUtils


api = Namespace('login', description='User login')

def json_serial(obj):
    """JSON serializer for objects not serializable by default json code"""

    if isinstance(obj, (datetime, date)):
        return obj.isoformat()
    raise TypeError ("Type %s not serializable" % type(obj))


user = api.model('user',{
'id': fields.String(required=True, description='Id'),
'first_name': fields.String(required=True, description='First Name'),
'last_name': fields.String(required=True, description='Last Name'),
'username': fields.String(required=True, description='Username'),
'email':fields.String(required=True, description='Email'),
'password':fields.String(required=True, description='Password'),
'image_url':fields.String(required=True, description='Image URL'),
'active': fields.Boolean(required=True, description='Active'),
'last_login': fields.String(required=True, description='Last Login'),
'login_count': fields.String(required=True, description='Login Count'),
'created_on': fields.String(required=True, description='Created On'),
})


post_parser = reqparse.RequestParser()
# post_parser.add_argument('id', required = True, type=int, help = 'integer value', location='form')
post_parser.add_argument('first_name',required = True, help = 'string value', location='form')
post_parser.add_argument('last_name',required = True, help = 'string value', location='form')
post_parser.add_argument('username',required = True, help = 'string value', location='form')
post_parser.add_argument('email',required = True, help = 'string value', location='form')
post_parser.add_argument('password',required = True, help = 'string value', location='form')
post_parser.add_argument('image_url',required = True, help = 'string value', location='form')
# post_parser.add_argument('active',  type = bool, default = False, help = 'true/false', location='form')
# post_parser.add_argument('last_login', help = 'timestamp string value', location='form')
# post_parser.add_argument('login_count',  type=int, help = 'integer value', location='form')
# post_parser.add_argument('fail_login_count',  type=int, help = 'integer value', location='form')
# post_parser.add_argument('created_on', help = 'timestamp string value', location='form')


@api.route('/add')
class Add_user(Resource):
    @api.doc('post_user')
    @api.expect(post_parser)
    def post(self):
        dbutils = DbUtils()
        result = {}
        try:
            today = datetime.now()

            first_name = request.form['first_name']  if 'first_name' in request.form else ""
            last_name = request.form['last_name'] if 'last_name' in request.form else ""
            username = request.form['username']  if 'username' in request.form else ""
            image_url = request.form['image_url']  if 'image_url' in request.form else None
            email = request.form['email']  if 'email' in request.form else ""
            password = request.form['password']  if 'password' in request.form else None
            # active = True if 'active' in request.form else False
            last_login = request.form['last_login']  if 'last_login' in request.form else today
            login_count = request.form['login_count'] if 'login_count' in request.form else 0
            fail_login_count = request.form['fail_login_count'] if 'fail_login_count' in request.form else 0

            user,count = dbutils.get_users(0,1,{'username':username},'login')


            if password is None:
                return {'response_code': 2 ,'status':'failure', 'message': 'Password not found'},200
            else:
                if count > 0:
                    user,count = dbutils.get_user_with_password(0,1,{'username':username},'login')
                    if password == user[0]['password']:
                        user_details = {
                                'name':user[0]['username'],
                                'image':image_url if user[0].get('image_url') else 'https://htmlcolors.com/img/change-user.png',
                                'email': user[0]['email'],                  
                            }
                        return {'response_code': 0 ,'status':'failure', 'message': 'User logged in succesfully', 'user':user_details},200
                    else:
                        return {'response_code': 2 ,'status':'failure', 'message': 'Incorrect password'},200

                else:
                    return {'response_code': 2 ,'status':'failure', 'message': "User does not exist"},200

        except Exception as e:
            print('--->',e)
            dbutils.close()
            return {'response_code': 2, 'status': 'failure', 'message': 'Unexpected error'}, 200

        else:
            dbutils.close()
            return {'response_code': 0, 'status': 'success', 'message': 'user created successfully'}, 200
