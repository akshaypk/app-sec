import json
from flask import Flask, redirect, request, jsonify, Blueprint, Response
from flask_restplus import Namespace, Resource, fields, reqparse
from core.utils import DbUtils
from core.LoggingManager import get_logger
from .products import products
from csv import DictReader
import werkzeug, csv, io
from werkzeug import datastructures
from werkzeug import _internal
from datetime import datetime

api = Namespace('lookup_csv', description='Lookup products using csv')
post_parser = reqparse.RequestParser()
post_parser.add_argument('upload_file', type = str,action='split', location= 'form')
# type=werkzeug.datastructures.FileStorage, location='files')
post_parser.add_argument('add_desired_columns', type = str,action='split', location= 'form')
post_parser.add_argument('column_to_look_up', choices=('product_upc', 'product_dpci', 'product_sku'), location='form')
post_parser.add_argument('Look_up_column_header', location='form')

@api.route('/get_products')
class post_csv(Resource):
    @api.doc("Uploading csv file")
    @api.expect(post_parser)
    def post(self):
        # print(request.files['upload_file'])
        # print(request.form['upload_file'])
        try:
            final_values = []
            # print(request.files['upload_file'])
            dbutils = DbUtils()
            values = request.form['upload_file'].split(',')
            print(values)
            # csv_file = request.files['upload_file']
            desired_columns = request.form['add_desired_columns']
            lookup_coloumns = request.form['column_to_look_up']
            column_headers = request.form['Look_up_column_header']
            # decoded_file = csv_file.read().decode('utf-8').splitlines()
            # print(request.files['upload_file'])
            # reader = csv.DictReader(decoded_file)
            # for row in reader:
            #     values.append(row[column_headers])
            for value in values:
                print("value", value)
                value = value.zfill(int(15))
                final_values.append(value)
            print(final_values)
            lookup_results = dbutils.lookup_csv({'header': lookup_coloumns, 'file_data': final_values})
            print('Lookup', lookup_results)
            dbutils.close()
            return jsonify(lookup_results)
        except Exception as e:
            # print(request.files['upload_file'])
            print('error occured', e)
            dbutils.close()
            get_logger().error("Error ", exc_info=True)



