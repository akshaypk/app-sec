from flask import Flask, abort, request, jsonify,Blueprint,send_file,after_this_request
from flask_restplus import Namespace, Resource, fields,reqparse
#from flask_sqlalchemy import SQLAlchemy
from core.Paginator import get_paginated_list
from core.LoggingManager import get_logger
from core.utils import DbUtils

from flask import Flask, Response, jsonify, request
from flask_restplus import Api, Resource
#from flask_cors import CORS
# app = Flask(__name__)
# CORS(app)
#api = Api(app, version='1.0', title='My API', validate=False
from core.LoggingManager import get_logger
from io import StringIO, BytesIO
import csv
import zipfile
import time
import os
from sys import getsizeof
import codecs
import pandas

api = Namespace('products', description='Product related operations')

products = api.model('products', {
    'product_id': fields.Integer(required=True, description='The product identifier'),
    'product_upc': fields.String(required=True, description='The product upc'),
    'product_name': fields.String(required=True, description='The product name'),
    'product_price': fields.String(required=True, description=' product price name'),
    'site_id': fields.Integer(required=True, description='site id '),
    'product_url': fields.String(required=True, description='product url name'),
    'product_ingredients': fields.String(required=True, description='product ingredients'),
    'product_brand': fields.String(required=True, description='product brand'),
    'product_category': fields.String(required=True, description='product category'),
    'product_subcategory1': fields.String(required=True, description='product sub category'),
    'product_subcategory2': fields.String(required=True, description='product sub category'),
    'product_imageurl': fields.String(required=True, description='product image url '),
    'product_subcategory3': fields.Integer(required=True, description='product sub category'),
    'timestamp': fields.String(required=True, description='time stamp'),
    'product_size': fields.String(required=True, description='product size'),
    'product_dropdown1': fields.String(required=True, description='product dropdown'),
    'product_dropdown2': fields.String(required=True, description='product dropdown2'),
    'product_dpci': fields.String(required=True, description='product dpci'),
    'product_desc': fields.String(required=True, description='product desc'),
    'product_packaging': fields.String(required=True, description='product pacakaging'),
    'product_serving_per_container': fields.String(required=True, description='product serving per container'),
    'product_features': fields.String(required=True, description='product features'),
    'product_serving_size': fields.String(required=True, description='product serving size'),
    'product_flavor': fields.String(required=True, description='product flavor'),
    'product_type': fields.String(required=True, description='product type'),
    'product_assembled_product_dimensions': fields.String(required=True, description='product assembled product dimension'),
    'product_model': fields.String(required=True, description='product model'),
    'product_sku': fields.String(required=True, description='product sku'),
    'product_manufacturer_part_number': fields.String(required=True, description='product manufacturer part number'),
    'product_status': fields.String(required=True, description='product status'),
    'product_additional_images':fields.String(required = True, description = 'product additional images'),
    'product_nutrition_info': fields.String(required=True, description='product nutrition info'),
    'product_feeding_instructions': fields.String(required=True, description='product feeding instruction'),
    'product_total_reviews': fields.String(required=True, description='product total reviews'),
    'product_rating': fields.String(required=True, description='product rating'),
    'product_customer_reviews': fields.String(required=True, description='product customer reviews'),
    'product_additional_sizes': fields.String(required=True, description='product additional sizes'),
    'product_rating_image_url': fields.String(required=True, description='product rating image url'),
    'product_recommended_retail_price': fields.String(required=True, description='product_recommended_retail_price'),

})



PRODUCTS = [{
   'product_id': 'felix',
    'product_upc':'felix',
    'product_name': 'felix',
    'product_price':'felix',
    'site_id': 'felix',
    'product_url': 'felix',
    'product_ingredients': 'felix',
    'product_brand':'felix',
    'product_category': 'felix',
    'product_subcategory1': 'felix',
    'product_subcategory2': 'felix',
    'product_imageurl': 'felix',
    'product_subcategory3': 'felix',
    'timestamp':'felix',
    'product_size': 'felix',
    'product_dropdown1': 'felix',
    'product_dropdown2': 'felix',
    'product_dpci': 'felix',
    'product_desc': 'felix',
    'product_packaging': 'felix',
    'product_serving_per_container': 'felix',
    'product_features': 'felix',
    'product_serving_size': 'felix',
    'product_flavor': 'felix',
    'product_type': 'felix',
    'product_assembled_product_dimensions': 'felix',
    'product_model': 'felix',
    'product_sku': 'felix',
    'product_manufacturer_part_number':'felix',
    'product_status': 'felix',
    'product_additional_images':'felix',
    'product_nutrition_info': 'felix',
    'product_feeding_instructions': 'felix',
    'product_total_reviews': 'felix',
    'product_rating': 'felix',
    'product_customer_reviews': 'felix',
    'product_additional_sizes': 'felix',
    'product_rating_image_url': 'felix',
    'product_recommended_retail_price':'felix'}
]


get_parser = reqparse.RequestParser()
get_parser.add_argument('start', type='int',help = 'integer value')
get_parser.add_argument('limit',type='int',help = 'integer value')
get_parser.add_argument('site_id',help ='site_id')
get_parser.add_argument('site_name',help='site_name')
get_parser.add_argument('product_upc',help = 'product upc')
get_parser.add_argument('product_name',help = 'product name')
get_parser.add_argument('start_date',help='start_date')
get_parser.add_argument('end_date',help='end_date')
get_parser.add_argument('product_status',type = bool,help='product status')

get_download = reqparse.RequestParser()
get_download.add_argument('site_id',help ='site_id')
get_download.add_argument('site_name',help='site_name')
get_download.add_argument('product_upc',help = 'product upc')
get_download.add_argument('product_name',help = 'product name')
get_download.add_argument('start_date',help = 'start_date')
get_download.add_argument('end_date',help = 'end_date')
get_download.add_argument('product_status',type = bool,help='product status')
get_download.add_argument('duplicate_by',help = 'duplicate_by column')
get_download.add_argument('group_by',type = bool,help='group_by')

# post_parser = reqparse.RequestParser()
# post_parser.add_argument('product_upc', type=int, location='form')
# post_parser.add_argument('Add_Desired_Columns_for_Final_Report', type='str', action='split', location='form')
# post_parser.add_argument('Column_To_Look_Up', type='str', choices=('product_upc',' product_dpci', 'product_sku'),
#                          location='form')
# post_parser.add_argument('Lookup_column_Header_in_File', location='form')


@api.route('/')
class Product(Resource):
    @api.doc('list_products')
    #@api.marshal_with(products)
    @api.expect(get_parser)
    def get(self):
        dbutils = DbUtils()
        start = int(request.args.get('start',1))
        limit = int(request.args.get('limit',10))
        site_id = request.args.get('site_id',None)
        site_name=request.args.get('site_name',None)
        product_name = request.args.get('product_name', None)
        product_upc = request.args.get('product_upc', None)
        start_date = request.args.get('start_date', None)
        end_date = request.args.get('end_date', None)

        _product_status = request.args.get('product_status', None)
        if not _product_status:
            product_status = None
        else:
            # print (_product_status,'///////////////s')
            product_status = True if _product_status == 'true' or _product_status == True else False

        # product_status = dbutils.get_status_bool(request, 'product_status')

        searchQuery={}
        if product_name is not None:
            searchQuery['product_name']=product_name
        if site_id is not None:
            searchQuery['site_id'] = site_id
        if site_name is not None:
            searchQuery['site_name'] = site_name
        if product_upc is not None:
            searchQuery['product_upc'] = product_upc
        if start_date is not None:
            searchQuery['start_date'] = start_date
        if end_date is not None:
            searchQuery['end_date'] = end_date

        if product_status !='' and product_status is not None:
            # print (product_status,"===================")
            searchQuery['product_status'] = product_status


        products,count =dbutils.get_products(int(start)-1,limit,searchQuery)
        dbutils.close()
        if count:
            data = get_paginated_list(products,'/api/v1/products',count,start,limit=limit)
            return data
        else:
            return {'response_code': 2, 'status': 'failure', 'message': 'No data found'}, 200


@api.route('/download')
class DownloadProducts(Resource):
    @api.doc('download_products')
    #@api.marshal_with(products)
    @api.expect(get_download)
    def get(self):
        try:
            start=time.time()
            site_id = request.args.get('site_id', None)
            if site_id is None:
                return {'message': 'No Site_id', 'response_code': 2, 'status': "failure"}

            dbutils = DbUtils()
            site_name = request.args.get('site_name', None)
            product_name = request.args.get('product_name', None)
            product_upc = request.args.get('product_upc', None)
            start_date = request.args.get('start_date', None)
            end_date = request.args.get('end_date', None)
            duplicate_by = request.args.get('duplicate_by', None)
            add_info = request.args.get('group_by', None)
            group_by = True if add_info == 'true' else False

            get_logger().info('duplicate_by : %s', duplicate_by)
            get_logger().info('group_by : %s', group_by)

            _product_status = request.args.get('product_status', None)
            if not _product_status:
                product_status = None
            else:
                product_status = True if _product_status == 'true' or _product_status == True else False

            if not site_name:
                _site_name=dbutils.get_site_name_by_id(site_id)
                if _site_name:
                    site_name=_site_name[0]
                    print (site_name)

            # duplicate_by = None
            # site_url = None
            if not duplicate_by:
                webpage_parser_details, count = dbutils.get_webpage_parser_details(0, 1, {'site_id': site_id, })
                if count > 0:
                    page_details = webpage_parser_details[0] if webpage_parser_details else {}
                    if page_details:
                        if 'product_upc_xpath' in page_details and page_details['product_upc_xpath']:
                            duplicate_by = 'Product UPC'
                        elif 'product_name_xpath' in page_details and page_details['product_name_xpath']:
                            duplicate_by = 'Product Name'
                else:
                    dbutils.close()
                    return {'message': 'No web page parser details found', 'response_code': 2, 'status': "failure"}

            where_condition={}
            where_condition["site_id"]=site_id
            where_condition["product_name"] = product_name
            where_condition["product_upc"] = product_upc
            where_condition["site_name"] = site_name
            if start_date is not None:
                where_condition['start_date'] = start_date
            if end_date is not None:
                where_condition['end_date'] = end_date

            results=dbutils.get_product_details_download(where_condition)
            matched_count=len(results)
            # print(matched_count)
            if results is None or not results:
                dbutils.close()
                return {'message': 'No search data found', 'response_code': 2, 'status': "failure"}

            _csv_file = StringIO()
            keys=None
            prod_list=[]
            columns=results[0].keys()
            pd = pandas.DataFrame(results, columns = columns)
            pd.insert(0, 'Site Name', site_name)

            # dropping ALL duplicate values
            if duplicate_by and duplicate_by != 'no':
                # print ("site_name",site_name)
                if (site_name and 'boots' in site_name.lower()) or group_by:
                    get_logger().info('Inside grouping(additional info capture) %s',site_name)
                    # capture additional information for boots website while removing duplicates
                    pd.replace(to_replace=[None,""], value=pandas.np.nan, inplace=True)
                    #pd.fillna(value=pandas.np.nan, inplace=True)
                    pd = self.grouping_rows(pd)

                else:
                    get_logger().info('Duplicate remove field :  %s', duplicate_by)
                    try:
                        pd.drop_duplicates(subset=duplicate_by,
                                           keep='first', inplace=True)
                    except Exception as e:
                        dbutils.close()
                        return {'message': 'Entered valid column name for de-duplication', 'response_code': 2, 'status': "failure"}
            else:
                get_logger().info('Not removed duplicates : %s', group_by)
            if product_status != '' and product_status is not None:
                pd_exp = pd.loc[pd['Status'] == product_status]
                matched_count = pd_exp.shape
                pd_exp.to_csv(_csv_file, index=False)
            else:
                matched_count = pd.shape
                pd.to_csv(_csv_file,index=False)

            get_logger().info('download_count %s', str(matched_count))
            _csv_file.seek(0)
            prod_list=[]
            results=[]
            dbutils.close()
            csv_file_name = "{site_id}_results_{size}.csv".format(site_id=site_id, size=matched_count)
            memory_file = BytesIO()
            with zipfile.ZipFile(memory_file, 'w') as zf:
                data = zipfile.ZipInfo(csv_file_name)
                data.date_time = time.localtime(time.time())[:6]
                data.compress_type = zipfile.ZIP_DEFLATED
                text = _csv_file.getvalue()
                zf.writestr(data, text)
                text=""

            _csv_file.close()
            del _csv_file
            memory_file.seek(0)
            end=time.time()
            print ("Processed Time : ",end-start)
            zip_file_name = "{site_id}_results_{size}.zip".format(site_id=site_id,size=matched_count)

            @after_this_request
            def add_header(r):
                """
                Add headers to both force latest IE rendering engine or Chrome Frame,
                and also to cache the rendered page for 10 minutes.
                """
                r.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
                r.headers["Pragma"] = "no-cache"
                r.headers["Expires"] = "0"
                r.headers['Cache-Control'] = 'public, max-age=0'
                return r

            # return  None
            return send_file(memory_file, attachment_filename=zip_file_name, as_attachment=True)
        except Exception as e:
            return {'message': str(e), 'response_code': 2, 'status': "failure"}

    def grouping_rows(self,df):
        columns = df.columns
        column_list = columns.tolist()
        filter_dict = {}
        for column in column_list:
            if 'Site Name' in column or 'Product UPC' in column:
                continue
            filter_dict[column] = 'first'

        df = df.groupby(['Site Name', 'Product UPC']).agg(filter_dict).reset_index()
        return df



# @api.route('/lookup_csv')
# class Lookup(Resource):
#     @api.doc('export to csv')
#     @api.expect(post_parser)
#     def post(self):
#         dbutils = DbUtils()
#         result = {}
#         try:
#             #product_sku=request.form['produtc_sku']
#             product_upc = request.form['product_upc']
#             Desired_Columns = request.form['Add_Desired_Columns_for_Final_Report']
#             Column_To_Look_Up = request.form['Column_To_Look_Up']
#             Lookup_column_Header_in_File = request.form['Lookup_column_Header_in_File']
#             Desired_column = Desired_Columns.split(",")
#             updateQuery = {}
#             result = dbutils.get_products_csv({'product_upc': product_upc, 'Desired_Columns': Desired_column,
#                                                'Column_To_Look_Up':Column_To_Look_Up, 'Lookup_column_Header_in_File':Lookup_column_Header_in_File})
#         except Exception as e:
#             get_logger().error('Error in getting products:', exc_info=True)
#             return {'response_code': 2, 'status': 'failure', 'message': 'unexpected error'}, 200
#         else:
#             return {'response_code': 0, 'status': 'success', 'message': 'Project configuration added successfully'}, 200
#
#




