from flask import Flask, abort, request, jsonify,Blueprint
from flask_restplus import Namespace, Resource, fields,reqparse
#from flask_sqlalchemy import SQLAlchemy
from core.Paginator import get_paginated_list
from core.utils import DbUtils
from core.LoggingManager import get_logger
import datetime

from sqlalchemy import ForeignKey
from sqlalchemy.orm import joinedload
from .products import products
from sqlalchemy.sql import func
header_list = []
for headers in products:
    header_list.append(headers)



api = Namespace('project_config', description='Sitemap related operations')
project_config = api.model('project_config', {
    'project_id': fields.Integer( description= 'Project ID'),
    'project_name': fields.String( description='The project name'),
    'primary_site_id': fields.Integer( ForeignKey('primary_site_id'),nullable=False, index=True,description='ID of primary site'),
    'secondary_site_ids': fields.Integer(ForeignKey('secondary_site_ids'),nullable=False, index=True,description='Secondary Site_id',unique=True),
    'input_file_headers': fields.String( description='Select multiple header'),
    'output_file_headers': fields.String( description='Select multiple output file headers'),
    'last_updated_timestamp': fields.DateTime(timezone=True, server_default=func.now(), description='The CreationDate'),
    'requires_input_file': fields.Boolean( description= 'input file required for project'),
})
# matches = session.query(project_config).options(joinedload(project_config.primary_site_id),
#                                        joinedload(project_config.secondary_site_ids))
# for m in matches:
#     print(m.secondary_site_ids, m.primary_site_id)
PROJECT_CONFIG = [
    {'project_name': 'felix', 'primary_site_id': 'Felix', 'secondary_site_ids': 'Felix', 'input_file_headers': 'Felix',
     'output_file_headers': 'Felix', 'last_updated_timestamp': 'Felix', 'requires_input_file': 'Felix'
     }
]

post_parser = reqparse.RequestParser()
post_parser.add_argument('project_name', type=str, help='Project name', location='form')
post_parser.add_argument('primary_site_id', type = int, help='Primary Site ID', location='form')
post_parser.add_argument('secondary_site_ids', type = int,action='split', help='Secondary Site_id',  location='form')
post_parser.add_argument('input_file_headers', type = str,choices=("Product_upc", "Product_url","Product_name"),help='Select multiple input header',
                         location='form')
post_parser.add_argument('output_file_headers', type=str,action='split', help='Select multiple output headers',
                         location='form')
post_parser.add_argument('requires_input_file', type =bool, default=True, help='True/False',location='form')

update_parser = reqparse.RequestParser()
update_parser.add_argument('project_id', required = True, location='form')
update_parser.add_argument('project_name', type=str, help='Project name', location='form')
update_parser.add_argument('primary_site_id', type=int, help='Primary Site ID', location='form')
update_parser.add_argument('secondary_site_ids', type=int, action='split', help='Secondary Site_id', location='form')
update_parser.add_argument('input_file_headers', type=str,choices=("Product_upc", "Product_url","Product_name"), help='Select multiple input header',location='form')
update_parser.add_argument('output_file_headers', type=str, action= 'split', help='Select multiple output headers',location='form')
update_parser.add_argument('requires_input_file', type =bool, default=True, help='True/False', location='form')

get_parser = reqparse.RequestParser()
get_parser.add_argument('start', type=int, help='integer value')
get_parser.add_argument('limit', type=int, help='integer value')
get_parser.add_argument('project_id', type=int, help='ID of project')
get_parser.add_argument('project_name', type=str, help='Project name')

delete_parser = reqparse.RequestParser()
delete_parser.add_argument('project_id', type=int, location = 'form')
@api.route('/')
class Project(Resource):
    @api.doc('list_projects')
    # @api.marshal_with(sites)
    @api.expect(get_parser)
    def get(self):
        dbutils = DbUtils()
        start = int(request.args.get('start', 1))
        limit = int(request.args.get('limit', 10))
        project_id = request.args.get('project_id', None)
        project_name = request.args.get('project_name', None)
        searchQuery = {}
        if project_id and project_name:
            searchQuery['project_id'] = project_id if project_id else ''
            searchQuery['project_name'] = project_name if project_name else ''
        elif project_id is not None:
            searchQuery['project_id'] = project_id
        elif project_name is not None:
            searchQuery['project_name'] = project_name

        # sites =dbutils.get_sites({'site_name':site_name,'site_url':site_url})
        # sitemap, count = dbutils.get_sitemap_parser_details(site)
        projects, count = dbutils.get_project_config(int(start) - 1, limit, searchQuery)
        data = get_paginated_list(projects, '/api/v1/project_config', count, start, limit=limit)
        dbutils.close()
        return data


@api.route('/add')
class Add_project(Resource):
    @api.doc('add_projects')
    # @api.marshal_with(sites)
    @api.expect(post_parser)
    def post(self):
        dbutils = DbUtils()
        result = {}
        try:
            # check for the existing site
            project_name = request.form['project_name']
            primary_site_id = request.form['primary_site_id']
            secondary_site_ids =request.form['secondary_site_ids'] if 'secondary_site_ids' in request.form else None
            input_file_headers = request.form['input_file_headers']
            output_file_headers = request.form['output_file_headers']
            requires_input_file = True if 'requires_input_file' in request.form else False
            output_headers = output_file_headers.split(",")
            input_header = input_file_headers.split(",")
            # if input_header=='product_upc':

            # for header in input_header:
            #
            #     if header not in header_list:
            #         dbutils.close()
            #         return {'response_code': 2, 'status': 'failure','message': 'Input header list is not valid.'}, 200
            #     else:
            #         continue

            for header in output_headers:
                if header not in header_list:
                    dbutils.close()
                    return {'response_code': 2, 'status': 'failure','message': 'Output header list is not valid.'}, 200
                else:
                    continue
            project, count = dbutils.get_project_name(0, 1, {'project_name': project_name})
            sites, sites_count = dbutils.get_sites(0, 1, {'site_id': primary_site_id})
            if sites_count > 0:
                if count > 0:
                    dbutils.close()
                    return {'response_code': 2, 'status': 'failure', 'message': 'Project Name already exists Please update.'}, 200
                else:
                    result = dbutils.create_project_config({'project_name': project_name,
                                                                'primary_site_id': primary_site_id,
                                                                'secondary_site_ids': secondary_site_ids,
                                                                'input_file_headers': input_file_headers,
                                                                'output_file_headers': output_file_headers,
                                                                   'requires_input_file': requires_input_file})
            else:
                dbutils.close()
                return {'response_code': 2, 'status': 'failure', 'message': 'Primary site Does not exist please add.'}, 200
        except Exception as e:

            get_logger().error('Error in creating project config :', exc_info=True)
            dbutils.close()
            return {'response_code': 2, 'status': 'failure', 'message': 'unexpected error'}, 200
        else:
            dbutils.close()
            return {'response_code': 0, 'status': 'success',
                    'message': 'Project configuration added successfully'}, 200


@api.route('/update')
class Update_sitemap(Resource):
    @api.doc('update_sitemap')
    @api.expect(update_parser)
    def put(self):
        dbutils = DbUtils()
        result = {}
        try:
            project_id = request.form['project_id']
            project_name = request.form['project_name']
            primary_site_id = request.form['primary_site_id']
            secondary_site_ids = request.form['secondary_site_ids']
            input_file_headers = request.form['input_file_headers']
            output_file_headers = request.form['output_file_headers']
            requires_input_file = True if 'requires_input_file' in request.form else False
            output_headers = output_file_headers.split(",")
            input_header = input_file_headers.split(",")
            # for header in input_header:
            #
            #     if header not in header_list:
            #         dbutils.close()
            #         return {'response_code': 2, 'status': 'failure','message': 'Input header list is not valid.'}, 200
            #     else:
            #         continue
            for header in output_headers:
                if header not in header_list:
                    dbutils.close()
                    return {'response_code': 2, 'status': 'failure','message': 'Output header list is not valid.'}, 200
                else:
                    continue
            project, count = dbutils.get_project_config(0, 1, {'project_id': project_id})
            sites, sites_count = dbutils.get_sites(0, 1, {'site_id': primary_site_id})
            if sites_count > 0:
                if count > 0:
                    result = dbutils.update_project_config({'project_id': project_id, 'project_name': project_name,
                                                   'primary_site_id': primary_site_id,
                                                   'secondary_site_ids': secondary_site_ids,
                                                   'input_file_headers': input_file_headers,
                                                   'output_file_headers': output_file_headers,
                                                   'requires_input_file': requires_input_file})
                else:
                    dbutils.close()
                    return {'response_code': 2, 'status': 'failure','message': 'Project Config does not exist.'}, 200
            else:
                dbutils.close()
                return {'response_code': 2, 'status': 'failure','message': 'Primary site does not exist.'}, 200
        except Exception as e:
            get_logger().error('Error in creating updating project config :', exc_info=True)
            dbutils.close()
            return {'response_code': 2, 'status': 'failure', 'message': 'unexpected error'}, 200
        else:
            dbutils.close()
            return {'response_code': 0, 'status': 'success', 'message': 'Project configuration updated successfully'}, 200
@api.route('/delete')
class Delete_project(Resource):
    @api.doc('delete_project')
    @api.expect(delete_parser)
    def delete(self):
        try:
            dbutils = DbUtils()
            project_id = request.form['project_id']
            site, count = dbutils.get_project_config(0, 1, {'project_id': project_id})
            if count > 0:
                status=dbutils.delete_project_config(project_id)
                if status["status"]=="success":
                    return {'response_code': 0, 'status': 'success', 'message': 'Deleted project successfully'}, 200
                else:
                    status["response_code"]=2
                    status["status"]='failure'
                    return status,200

            else:
                dbutils.close()
                return {'response_code': 2, 'status': 'failure', 'message': 'project  doesnt exist'}, 200



        except Exception as e:
            print(e)
            dbutils.close()
            return {'response_code': 2, 'status': 'failure', 'message': 'unexpected error'}, 200


