import json
from flask import Flask, redirect, request, jsonify, Blueprint, Response
from flask_restplus import Namespace, Resource, fields, reqparse
from core.utils import DbUtils
from core.LoggingManager import get_logger
from .products import products
from csv import DictReader
import werkzeug, csv, io
from werkzeug import datastructures
from werkzeug import _internal
from datetime import datetime
api = Namespace('project_csv_file', description='upc cascading scraper ')


get_parser = reqparse.RequestParser()
get_parser.add_argument('job_id', type=str)







@api.route('/download')
class get_upc(Resource):
    @api.doc("Get_products")
    @api.expect(get_parser)
    def get(self):
        try:
            dbutils = DbUtils()
            job_id = request.args.get('job_id')
            upc = dbutils.get_data_import_products({'job_id':job_id})
            dbutils.close()
            return jsonify(upc)
        except Exception as e:
            dbutils.close()
            return {"messge":str(e)}
            #get_logger().error("Error ", exc_info=True)

