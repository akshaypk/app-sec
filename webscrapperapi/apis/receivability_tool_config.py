from flask import Flask, abort, request, jsonify, Blueprint
from flask_restplus import Namespace, Resource, fields, reqparse
# from flask_sqlalchemy import SQLAlchemy
from core.Paginator import get_paginated_list
import decimal, datetime, json,string
import json
from core.utils import DbUtils

api = Namespace('receivability_tool_config', description='receivability_tool_config related operations')

receivability_tool_config = api.model('receivability_tool_config ', {
    'domain': fields.String(required=True, description='The domain identifier'),
    'main_method': fields.String(required=True, description='the main_method identifier'),
    'channel_name_xpath': fields.String(required=True, description='The channel name xpath'),
    'channel_number_xpath': fields.String(required=True, description='the channel number xpath'),
    'structure': fields.String(required=True, description='the structure'),
    'callback': fields.String(required=True, description='the callback'),
    'scroll_function': fields.String(required=True, description='The scroll_function'),
    'scroll_reset_function': fields.String(required=True, description='The scroll reset function'),
    'load_url': fields.String(required=True, description='the load url'),
    'channel_name': fields.String(required=True, description='The channel name'),
    'sort': fields.String(required=True, description='the sort'),})


RECEIVABILITY_TOOL_CONFIG = [
    {'domain': 'felix', 'main_method': 'Felix', 'channel_name_xpath': 'Felix', 'channel_number_xpath': 'Felix'
        , 'structure': 'Felix', 'callback': 'Felix', 'scroll_function': 'Felix','scroll_reset_function': 'Felix',
     'load_url': 'Felix', 'channel_name': 'Felix','sort': 'Felix'},
]



post_parser = reqparse.RequestParser()
post_parser.add_argument('domain', required=True, type=str, location='form')
post_parser.add_argument('main_method', required=True, type=str, location='form')
post_parser.add_argument('channel_name_xpath', required=True, type=str, location='form')
post_parser.add_argument('channel_number_xpath', required=True, type=str,  location='form')
post_parser.add_argument('structure', required=True, type=str,   location='form')
post_parser.add_argument('callback', required=True, type=str,  location='form')
post_parser.add_argument('scroll_function', required=True, type=str,  location='form')
post_parser.add_argument('scroll_reset_function', required=True, type=str,  location='form')
post_parser.add_argument('load_url', required=True, type=str,  location='form')
post_parser.add_argument('channel_name', required=True, type=str,  location='form')
post_parser.add_argument('sort', required=True, type=str, location='form')



get_parser = reqparse.RequestParser()
get_parser.add_argument('start', type=int, help='integer value')
get_parser.add_argument('limit', type=int, help='integer value')
get_parser.add_argument('domain', type=str, help='string')


update_parser = reqparse.RequestParser()
update_parser.add_argument('domain', required=True, type=str, location='form')
update_parser.add_argument('main_method', required=True, type=str, location='form')
update_parser.add_argument('channel_name_xpath', required=True, type=str, location='form')
update_parser.add_argument('channel_number_xpath', required=True, type=str,  location='form')
update_parser.add_argument('structure', required=True, type=str,   location='form')
update_parser.add_argument('callback', required=True, type=str,  location='form')
update_parser.add_argument('scroll_function', required=True, type=str,  location='form')
update_parser.add_argument('scroll_reset_function', required=True, type=str,  location='form')
update_parser.add_argument('load_url', required=True, type=str,  location='form')
update_parser.add_argument('channel_name', required=True, type=str,  location='form')
update_parser.add_argument('sort', required=True, type=str, location='form')


delete_parser = reqparse.RequestParser()
delete_parser.add_argument('domain',type=str, location='form')


@api.route('/')
class receivability(Resource):
    @api.doc('receivability_parser')#webpage_parser
    # @api.marshal_with(sites)
    @api.expect(get_parser)
    def get(self):
        dbutils = DbUtils()
        start = int(request.args.get('start', 1))
        limit = int(request.args.get('limit', 10))
        domain = request.args.get('domain', None)
        searchQuery = {}
        if domain:
            searchQuery['domain'] = domain

        receivability_tool_config, count = dbutils.get_receivability_tool_config(int(start) - 1, limit, searchQuery)
        data = get_paginated_list(receivability_tool_config, '/api/v1/receivability_tool_config', count,
                                  start, limit=limit)
        dbutils.close()
        return data


@api.route('/add')
class AddSite(Resource):
    @api.doc('post_receivability')
    # @api.marshal_with(sites)
    @api.expect(post_parser)
    def post(self):
        dbutils = DbUtils()
        result = {}

        try:
                # check for the existing site
                domain = request.form['domain']
                main_method = request.form['main_method']
                channel_name_xpath = request.form['channel_name_xpath']
                channel_number_xpath = request.form['channel_number_xpath']
                structure = request.form['structure']
                callback = request.form['callback']
                scroll_function = request.form['scroll_function']
                scroll_reset_function = request.form['scroll_reset_function']
                load_url = request.form['load_url']
                channel_name = request.form['channel_name']
                sort = request.form['sort']

                print(domain,main_method,channel_name_xpath,channel_number_xpath,structure,callback,scroll_function,
                      scroll_reset_function,load_url,channel_name,sort)

                receivability_tool_config, count = dbutils.get_receivability_tool_config(0,1,{'domain': domain,})
                if count > 0:
                    dbutils.close()
                    return {'response_code': 2, 'status': 'failure', 'message': 'receivability tool details already exists.'}, 200
                else:
                    result = dbutils.create_receivability_tool_config({'domain': domain,'main_method': main_method,'channel_name_xpath': channel_name_xpath,
                                                                       'channel_number_xpath': channel_number_xpath,'structure': structure,
                                                                       'callback': callback,'scroll_function': scroll_function,
                                                                       'scroll_reset_function': scroll_reset_function,'load_url': load_url,
                                                                       'channel_name': channel_name,'sort': sort,})

        # return result,200
        except Exception as e:
            print(e)
            dbutils.close()
            return {'response_code': 2, 'status': 'failure', 'message': 'unexpected error'}, 200
        else:
            dbutils.close()
            return {'response_code': 0, 'status': 'success', 'message': 'receivability_tool_config details added successfully'}, 200



@api.route('/update')
class updatereceivability (Resource):
    @api.doc('update_receivability')
    # @api.marshal_with(sites)
    @api.expect(update_parser)
    def put(self):
        dbutils = DbUtils()
        result = {}

        try:
            domain = request.form['domain']
            main_method = request.form['main_method']
            channel_name_xpath = request.form['channel_name_xpath']
            channel_number_xpath = request.form['channel_number_xpath']
            structure = request.form['structure']
            callback = request.form['callback']
            scroll_function = request.form['scroll_function']
            scroll_reset_function = request.form['scroll_reset_function']
            load_url = request.form['load_url']
            channel_name = request.form['channel_name']
            sort = request.form['sort']

            print(domain, main_method, channel_name_xpath, channel_number_xpath, structure, callback, scroll_function,
              scroll_reset_function, load_url, channel_name, sort)

            result = dbutils.update_receivability_tool_config({'domain': domain,'main_method': main_method,'channel_name_xpath': channel_name_xpath,
                                                                       'channel_number_xpath': channel_number_xpath,'structure': structure,
                                                                       'callback': callback,'scroll_function': scroll_function,
                                                                       'scroll_reset_function': scroll_reset_function,'load_url': load_url,
                                                                       'channel_name': channel_name,'sort': sort,})

        # return result,200
        except Exception as e:
            print(e)
            dbutils.close()
            return {'response_code': 2, 'status': 'failure', 'message': 'unexpected error'}, 200
        else:
            dbutils.close()
            return {'response_code': 0, 'status': 'success', 'message': 'receivability tool config details updated successfully'}, 200


@api.route('/delete')
class Deletereceivability(Resource):
    @api.doc('delete_receivability')
    @api.expect(delete_parser)
    def delete(self):
        try:

            dbutils = DbUtils()

            domain = request.form['domain']
            dbutils.delete_receivability_tool_config(domain)

        except Exception as e:
            print(e)
            dbutils.close()
            return {'response_code': 2, 'status': 'failure', 'message': 'unexpected error'}, 200
        else:
            dbutils.close()
            return {'response_code': 0, 'status': 'success', 'message': 'Deleted receivability_tool_config details  successfully'}, 200
