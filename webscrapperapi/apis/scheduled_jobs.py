from flask import Flask, abort, request, jsonify, Blueprint, app
from flask_restplus import Namespace, Resource, fields,reqparse
#from flask_sqlalchemy import SQLAlchemy
from core.Paginator import get_paginated_list
import decimal,datetime
from core.LoggingManager import get_logger
import decimal,datetime,json
import datetime
# import DynamicWebScrapper.scraper.JobProcessor
from core.utils import DbUtils
api = Namespace('scheduled_jobs', description='scheduled_jobs related operations')

scheduled_jobs = api.model('scheduled_jobs', {
    'job_id': fields.Integer(required=True, description='The job identifier'),
    'site_id': fields.Integer(required=True, description='The site name'),
    'total_products_scraped': fields.Integer(required=True, description='The total products scraped'),
    'total_products_attempted': fields.Integer(required=True, description='The total products attempted'),
    'project_id': fields.Integer(required=True, description=' project_id name'),
    'start_time': fields.DateTime(dt_format='UTC', description='start time'),
    'file_name': fields.String(required=True, description='file name'),
    'end_time': fields.DateTime(dt_format='UTC', description='end time'),
    'job_type': fields.String(required=True, description='job type'),
    'timeout': fields.Integer(required=True, description='time out'),
    'status': fields.String(required=True, description='status'),
    'update_time': fields.DateTime(dt_format='UTC', description=' update time'),
    'comments': fields.String(required=True, description='comments '),
    'total_products_to_scrape': fields.Integer(required=True, description='total products scraped'),
    'site_name': fields.String(description='Site name'),
})


SCHEDULED_JOBS = [
    {'job_id': 'felix', 'site_id': 'Felix', 'total_products_scraped': 'Felix', 'total_products_attempted': 'Felix',
     'project_id': 'Felix', 'start_time': 'Felix', 'file_name': 'Felix', 'end_time': 'Felix', 'job_type': 'Felix',
     'timeout': 'Felix','status':'Felix','update_time':'Felix','comments':'Felix',
    'total_products_to_scrape':'Felix','site_name':'Felix'},
]

post_parser = reqparse.RequestParser()
post_parser.add_argument('site_id', type=int,location='form')
post_parser.add_argument('project_id', type=int,location='form')
post_parser.add_argument('start_time',type=str, location='form')
post_parser.add_argument('type_of_job', type=str, choices=("SITEMAP", "CATEGORY","LOCATION","DB","SITECHECK","EXPIRED"), location='form')
post_parser.add_argument('category', type=str, location='form')
post_parser.add_argument('flag', choices=('project', 'site'), required =True, location='form')
post_parser.add_argument('file_name',type=str,location='form')
post_parser.add_argument('search_text',type=str,action='split',location='form')
post_parser.add_argument('scrape_using', type=str, choices=('product_upc', 'product_url','product_name'),location='form')


get_parser = reqparse.RequestParser()
get_parser.add_argument('start', type='int',help = 'integer value')
get_parser.add_argument('limit',type='int',help = 'integer value')
get_parser.add_argument('site_id',type='str',help = 'site id')
get_parser.add_argument('project_id',type='str',help = 'project id')
get_parser.add_argument('job_id',type='int',help = 'job id')
get_parser.add_argument('job_type',type='str',help = 'job type')
get_parser.add_argument('start_time',help = 'start time')
get_parser.add_argument('status',help = 'status')


update_parser = reqparse.RequestParser()
update_parser.add_argument('job_id', type =int, location = 'form')
update_parser.add_argument('site_id', type =int, location = 'form')
update_parser.add_argument('total_products_scraped', type =int, location = 'form')
update_parser.add_argument('total_products_attempted', type =int, location = 'form')
update_parser.add_argument('project_id', type =int, location = 'form')
update_parser.add_argument('start_time',  location = 'form')
update_parser.add_argument('file_name', type =str, location = 'form')
update_parser.add_argument('end_time', location = 'form')
update_parser.add_argument('job_type', type =str, location = 'form')
update_parser.add_argument('timeout', type =int, location = 'form')
update_parser.add_argument('status', type =str, location = 'form')
update_parser.add_argument('update_time',type=datetime.date.fromtimestamp, location = 'form')
update_parser.add_argument('comments', type =str, location = 'form')
update_parser.add_argument('total_products_to_scrape', type =int, location = 'form')




delete_parser = reqparse.RequestParser()

delete_parser.add_argument('site_id', type=int, location='form')
delete_parser.add_argument('project_id', type=int, location='form')
delete_parser.add_argument('flag', choices=('project','site'), location='form')

cancel_parser = reqparse.RequestParser()
cancel_parser.add_argument('job_id', type =int, location = 'form',required=True)

@api.route('/')
class Job(Resource):
    @api.doc('list_jobs')
    #@api.marshal_with(jobs)
    @api.expect(get_parser)
    def get(self):
        dbutils = DbUtils()
        start = int(request.args.get('start',1))
        limit = int(request.args.get('limit',10))
        site_id = request.args.get('site_id',None)
        project_id = request.args.get('project_id', None)
        start_time = request.args.get('start_time', None)
        status = request.args.get('status', None)
        job_id = request.args.get('job_id', None)
        job_type = request.args.get('job_type', None)
        searchQuery={}
        if site_id is not None:
            searchQuery['site_id']=site_id
        if project_id is not None:
            searchQuery['project_id'] = project_id
        if start_time is not None:
            searchQuery['start_time'] = start_time
        if status is not None:
            searchQuery['status'] = status
        if job_id is not None:
            searchQuery['job_id'] = job_id
        if job_type is not None:
            searchQuery['job_type'] = job_type
        # print(searchQuery)
        #sites =dbutils.get_sites({'site_name':site_name,'site_url':site_url})
        scheduled_jobs,count =dbutils.get_scheduled_jobs(int(start)-1,limit,searchQuery)
        # print(count)
        data = get_paginated_list(scheduled_jobs,'/api/v1/scheduled_jobs',count,start,limit=limit)
        dbutils.close()
        return data


@api.route('/add')
class Addjobs(Resource):
    @api.doc('post_jobs')
    # @api.marshal_with(sites)
    @api.expect(post_parser)
    def post(self):
        try:
            dbutils = DbUtils()
            values = []
            result = {}
            updatequerry = {}
            searchQuery = {}
            final_values =[]
            scheduleDict = {}
            dynamicDict = {}
            type_of_job = request.form['type_of_job'] if 'type_of_job' in request.form else None
            category = request.form['category'] if 'category' in request.form else None
            flag = request.form['flag']
            site_id=request.form['site_id'] if 'site_id' in request.form else None
            if type_of_job == 'SITECHECK':
                site_id = 0
            project_id=request.form['project_id'] if 'project_id' in request.form else None
            file_name = request.form['file_name'] if 'file_name' in request.form else None
            scrape_using= request.form['scrape_using'] if 'scrape_using' in request.form else None


            if flag == 'project' and request.form['project_id'] is not None and type_of_job == 'DB':
                search_text = request.form['search_text'] if 'search_text' in request.form else None
                print(search_text,"===")
                if search_text and search_text is not None:
                    texts = search_text.split(",")
                    for value in texts:
                        print("value", value)
                        final_values.append(value)
                    json_value = json.dumps(final_values)
                    print("json", json_value)
                    job_status = dbutils.get_scheduled_running_job(project_id)
                    print (job_status,type(job_status),len(job_status))
                    if len(job_status)>=1:
                        dbutils.close()
                        return {'response_code': 2, 'status': 'failure', 'message': 'Project Already scheduled or Running'}
                    # if type_of_job == 'DB':
                    insertquerry = {}
                    insertquerry['project_id'] = request.form['project_id']
                    insertquerry['job_type'] = 'DB'
                    insertquerry['status'] = 'UNSCHEDULED'
                    insertquerry['file_name'] = request.form['file_name']
                    #insertquerry['search_text'] = json_value
                    insertquerry['start_time'] = request.form['start_time']
                    print("update", insertquerry)
                    result = dbutils.create_scheduled_jobs(insertquerry)
                    job_id = int(result['data'])
                    project, count = dbutils.get_project_config(0, 1,{'project_id': insertquerry['project_id']})
                    # check for the existing project configuration
                    if count > 0:
                        pass
                    else:
                        # else delet the created job
                        dbutils.delete_scheduled_jobs({'job_id':job_id})
                        dbutils.close()
                        return {'response_code': 2, 'status': 'failure', 'message': 'project_id is not available'}
                    bulk_insert =[]
                    if scrape_using == 'product_name':
                        for upc in texts:
                            bulk_insert.append({'project_id': project_id,'job_id':job_id, 'file_name': file_name, 'product_name': upc,
                             'scrape_using': scrape_using})
                        dynamic_results = dbutils.project_csv(bulk_insert)
                    elif scrape_using =='product_upc':
                        for upc in texts:
                            bulk_insert.append(
                               {'project_id': project_id, 'job_id': job_id, 'file_name': file_name, 'product_upc': upc,
                                'scrape_using': scrape_using})
                        dynamic_results = dbutils.project_csv(bulk_insert)

                        #dynamic_results = dbutils.project_csv({'project_id': project_id,'job_id':job_id, 'file_name': file_name,'product_upc':upc,
                        #                                         'scrape_using': scrape_using})
                    elif scrape_using =='product_url':
                        for upc in texts:
                            bulk_insert.append(
                               {'project_id': project_id, 'job_id': job_id, 'file_name': file_name, 'product_url': upc,
                                'scrape_using': scrape_using})
                        dynamic_results = dbutils.project_csv(bulk_insert)
                        # dynamic_results = dbutils.project_csv({'project_id': project_id,'job_id':job_id, 'file_name': file_name,'product_url':upc,
                        #                                          'scrape_using': scrape_using})
                    dbutils.update_scheduled_jobs({'job_id': job_id, 'status': 'SCHEDULED'})

                    # print('dynamic',dynamic_results)
                    # print('file_data',file_data)
                    # if count > 0:
                    #     pass
                    # else:
                    #     dbutils.close()
                    #     return {'response_code': 2, 'status': 'failure', 'message': 'project_id is not available'}
                else:
                    return {'response_code': 2, 'status': 'failure', 'message': 'File not uploaded'}

            elif flag == 'site' and (site_id is not None):
                job_status = dbutils.get_scheduled_running_jobs(site_id)
                print (job_status,type(job_status),len(job_status),"====")
                if len(job_status)>=1:
                    dbutils.close()
                    return {'response_code': 2, 'status': 'failure', 'message': 'Project Already scheduled or Running'}
                if type_of_job == 'CATEGORY':
                    updatequerry['category'] = category
                    updatequerry['site_id'] = site_id
                    updatequerry['job_type'] = 'CATEGORY'
                    updatequerry['status'] = 'SCHEDULED'
                elif type_of_job == 'SITEMAP':
                    updatequerry['site_id'] = site_id
                    updatequerry['job_type'] = 'SITEMAP'
                    updatequerry['status'] = 'SCHEDULED'
                elif type_of_job == 'LOCATION':
                    updatequerry['site_id'] = site_id
                    updatequerry['job_type'] = 'LOCATION'
                    updatequerry['status'] = 'SCHEDULED'
                elif type_of_job == 'SITECHECK':
                    updatequerry['site_id'] = site_id
                    updatequerry['job_type'] = 'SITECHECK'
                    updatequerry['status'] = 'SCHEDULED'

                elif type_of_job == 'EXPIRED':
                    updatequerry['site_id'] = site_id
                    updatequerry['job_type'] = 'EXPIRED'
                    updatequerry['status'] = 'SCHEDULED'

                # updatequerry['start_time'] = request.form['start_time']
                site, count = dbutils.get_sites(0, 1, {'site_id': updatequerry['site_id']})
                if count > 0 or type_of_job == 'SITECHECK':
                    pass
                else:
                    dbutils.close()
                    return {'response_code': 2, 'status': 'failure', 'message': 'site_id is not available'}
                # result = dbutils.create_scheduled_jobs(updatequerry)
                # print("update", updatequerry)
                updatequerry['start_time'] = request.form['start_time']
                print("update", updatequerry)
                result = dbutils.create_scheduled_jobs(updatequerry)
            elif request.form['site_id'] and request.form['project_id']:
                dbutils.close()
                return {'response_code': 2, 'status': 'failure', 'message': 'Both site_id and project_id provided '}, 200
            else:
                return {'response_code': 2, 'status': 'failure', 'message': 'Job type not found '}, 200
            # updatequerry['start_time'] = request.form['start_time']
            # print("update", updatequerry)
            # result = dbutils.create_scheduled_jobs(updatequerry)


        except Exception as e:
            print(e)
            get_logger().error('Error in creating scheduled jobs:', exc_info=True)
            dbutils.close()
            return {'response_code': 2, 'status': 'failure', 'message': 'unexpected error'}, 200
        else:
            dbutils.close()
            return {'response_code': 0,
                    'status': 'success',
                    'data': result['data'] if 'data' in result else "",
                    'message': 'scheduled jobs details added successfully'}, 200



@api.route('/delete')
class Deletejob(Resource):
    @api.doc('delete_job')
    @api.expect(delete_parser)
    def delete(self):
        dbutils = DbUtils()
        deletequerry = {}
        try:
            flag = request.form['flag']
            if flag == 'project' and request.form['project_id'] is not None:
                deletequerry['project_id'] = request.form['project_id']
            elif flag == 'site' and request.form['site_id'] is not None:
                deletequerry['site_id'] = request.form['site_id']
            elif request.form['site_id'] and request.form['project_id']:
                dbutils.close()
                return {'response_code': 2, 'status': 'failure',
                        'message': 'Both site_id and project_id provided '}, 200
            jobs, count = dbutils.get_scheduled_jobs(0, 1, deletequerry)
            if count > 0:
                dbutils.delete_scheduled_jobs(deletequerry)
            else:
                dbutils.close()
                return {'response_code': 2, 'status': 'failure', 'message': 'Site_id or project_id is not available'}, 200
        except Exception as e:
            print(e)
            get_logger().error('Error in creating scheduled jobs:', exc_info=True)
            dbutils.close()
            return {'response_code': 2, 'status': 'failure', 'message': 'unexpected error'}, 200
        else:
            dbutils.close()
            return {'response_code': 0, 'status': 'success', 'message': 'Deleted scheduled_jobs details  successfully'}, 200

@api.route('/cancel_job')
class updatejob (Resource):
    @api.doc('cancel_job')
    # @api.marshal_with(sites)
    @api.expect(cancel_parser)
    def put(self):
        print ("Inside cancel")
        try:
            job_id = request.form['job_id']
            dbutils = DbUtils()
            scheduled_jobs, count = dbutils.get_scheduled_jobs(0, 1, {'job_id': job_id})
            if count<=0:
                dbutils.close()
                return {"message":"No scheduled job entry available in DB",'response_code':2,'status':"failure"}
            db_status=scheduled_jobs[0]["status"]
            updateValuesDict = {}
            updateValuesDict['status']='CANCEL'
            updateValuesDict['alert_sent'] = False
            updateValuesDict['job_id']=job_id
            if db_status=='STARTED' or db_status=='SCHEDULED':
                result = dbutils.update_scheduled_jobs(updateValuesDict)
                updateValuesDict['response_code'] = 0
                updateValuesDict['status'] = "success"
                updateValuesDict['message'] = 'Triggered job to kill'
            else:
                updateValuesDict['response_code'] = 2
                updateValuesDict['status'] = "failure"
                updateValuesDict['message'] = 'Currently,job is not running'
            dbutils.close()


            return updateValuesDict
        except Exception as e:
            dbutils.close()
            return "Facing issue while processing request : {}".format(str(e))

