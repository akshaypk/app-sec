from flask import Flask, abort, request, jsonify, Blueprint
from flask_restplus import Namespace, Resource, fields, reqparse
# from flask_sqlalchemy import SQLAlchemy
from core.Paginator import get_paginated_list
import decimal, datetime, json
import json
from core.utils import DbUtils
from core.LoggingManager import get_logger
import datetime
from sqlalchemy.sql import func


api = Namespace('csv_template', description='Scrap products using csv ')

csv_template = api.model('csv_template ', {
    'id': fields.Integer(required=True, description='The template identifier'),
    'site_id': fields.Integer(required=True, description='The site identifier'),
    'product_url_column_in_csv': fields.Integer(required=True, description='The product url column in csv'),
    'search_columns_in_csv': fields.String(required=True, description='the search columns in csv'),
    'search_sites_id': fields.String(required=True, description='the search sites id'),
    'filter_column': fields.Integer(required=True, description='the filter column '),
    'filter_value': fields.String(required=True, description='The filter value'),
    'CreationDate': fields.DateTime(timezone=True, server_default=func.now(), description='The CreationDate'),
    'product_upc_column': fields.Integer(required=True, description='the product upc column '),

})

CSV_TEMPLATE=[{'id': 'felix', 'site_id': 'Felix', 'product_url_column_in_csv': 'Felix', 'search_columns_in_csv': 'Felix',
               'search_sites_id': 'Felix', 'filter_column': 'Felix', 'filter_value': 'Felix',
               'CreationDate': 'Felix', 'product_upc_column': 'Felix',}]


post_parser = reqparse.RequestParser()
post_parser.add_argument('id', required=True, type=int, location='form')
post_parser.add_argument('site_id', required=True, type=int, location='form')
post_parser.add_argument('product_url_column_in_csv', required=True,type=int, location='form')
post_parser.add_argument('search_columns_in_csv',  type=str, required=True,action='split', location='form')
post_parser.add_argument('search_sites_id', required=True, type=str, action='split',location='form')
post_parser.add_argument('filter_column', required=True, type=int,location='form')
post_parser.add_argument('filter_value', required=True, type=str, location='form')
post_parser.add_argument('product_upc_column', required=True, type=int, location='form')


@api.route('/add')
class Add_csv(Resource):
    @api.doc('post_csv')
    @api.expect(post_parser)
    def post(self):
        dbutils = DbUtils()
        result = {}
        try:
            # check for the existing csv template
            id = request.form['id']
            site_id = request.form['site_id']
            product_url_column_in_csv = request.form['product_url_column_in_csv']
            search_columns_in_csv = request.form['search_columns_in_csv']
            search_sites_id = request.form['search_sites_id']
            filter_column = request.form['filter_column']
            filter_value=request.form['filter_value']
            product_upc_column = request.form['product_upc_column']

            print(id, site_id, product_url_column_in_csv, search_columns_in_csv, search_sites_id,
                  filter_column,filter_value,product_upc_column)
            site, site_count = dbutils.get_sites(0, 1, {'site_id': site_id})
            csv_template, csv_count = dbutils.get_csv_template(0, 1, {'site_id': site_id})
            print(site_count, csv_count)
            if site_count > 0:
                if csv_count > 0:
                   dbutils.close()
                   return {'response_code': 2, 'status': 'failure', 'message': 'template already exists.'}, 200
                else:
                   result = dbutils.create_csv_template({'id': id, 'site_id': site_id,
                                              'product_url_column_in_csv': product_url_column_in_csv,
                                              'search_columns_in_csv': search_columns_in_csv,
                                              'search_sites_id': search_sites_id,
                                              'filter_column': filter_column,
                                              'filter_value':filter_value,
                                              'product_upc_column':product_upc_column})
            else:
                dbutils.close()
                return {'response_code': 2, 'status': 'failure',
                           'message': ' site id  does not exists kindly add.'}, 200
        # return result,200
        except Exception as e:
          print(e)
          dbutils.close()
          return {'response_code': 2, 'status': 'failure', 'message': 'unexpected error'}, 200

        else:
          dbutils.close()
          return {'response_code': 0, 'status': 'success', 'message': 'csv template created successfully'}, 200




