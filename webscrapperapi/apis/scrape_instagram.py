import string

from flask_restplus import Namespace, Resource, reqparse
from flask import request,jsonify
from core.instagram_search import InstagramSearch

api = Namespace('instagram_search', description='instagram_search related operations')

get_parser = reqparse.RequestParser()
get_parser.add_argument('search_text', help='enter')

@api.route('/')
class GetInstagramSearch(Resource):
    @api.doc('Get instagram_search')
    @api.expect(get_parser)
    def get(self):
        search_text = request.args.get('search_text', None)
        result = []
        if search_text:
            tag_name = search_text
            init = InstagramSearch()
            result = init.process_request(tag_name)

        return jsonify(result)