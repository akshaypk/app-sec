import string

from flask_restplus import Namespace, Resource, reqparse
from flask import request,jsonify
from core.twitter_search import TwitterSearch

api = Namespace('twitter_search', description='twitter_search related operations')


get_parser = reqparse.RequestParser()
get_parser.add_argument('search_text', help='enter')
get_parser.add_argument('search_type', type=str, choices=('handler', 'hashtag'))
get_parser.add_argument('max_limit', type=int,help = 'enter')

@api.route('/')
class GoogleApiEngineList(Resource):
    @api.doc('Get twitter_search')
    @api.expect(get_parser)
    def get(self):
        search_text = request.args.get('search_text', None)
        search_type = request.args.get('search_type', None)
        max_limit = int(request.args.get('max_limit', 1))
        print(search_text,search_type,max_limit)
        obj = TwitterSearch()

        if search_type == "handler":
            query = "@"+search_text
            scrapped_items = obj.get_user_info(query)

        else:
            if search_type == "hashtag":
                query = "#" + search_text
            else:
                query = search_text

            scrapped_items = obj.get_hash_info_method2(query, max_limit)

        results = []
        for item in scrapped_items[:max_limit]:
            results.append(item._json)

        return jsonify(results)