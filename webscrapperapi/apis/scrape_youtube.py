import string

from flask_restplus import Namespace, Resource, reqparse
from flask import request,jsonify
from core.youtube_search import you_tube

api = Namespace('youtube_search', description='youtube_search related operations')
get_parser = reqparse.RequestParser()
get_parser.add_argument('search_text', help='enter')
get_parser.add_argument('item_per_page', type=int,help = 'enter')
get_parser.add_argument('page', type=int,help = 'enter')
get_parser.add_argument('channel_id', type=str,help = 'enter')
get_parser.add_argument('order_by', type=str, choices=('rating', 'date','relevance','relevance','title','videoCount','viewCount'))

@api.route('/')
class GoogleApiEngineList(Resource):
    @api.doc('Get youtube_search')
    @api.expect(get_parser)
    def get(self):
        search_text = request.args.get('search_text', None)
        item_per_page = int(request.args.get('item_per_page', 10))
        page = int(request.args.get('page', 1))
        channel_id = request.args.get('channel_id', None)
        order_by = request.args.get('order_by', None)
        obj=you_tube(search_text,item_per_page,page,channel_id,order_by)
        return(jsonify(obj))