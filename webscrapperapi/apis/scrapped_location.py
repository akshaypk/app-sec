from flask import Flask, abort, request, jsonify, Blueprint
from flask_restplus import Namespace, Resource, fields, reqparse
from sqlalchemy import Column, String
from core.Paginator import get_paginated_list
import decimal, datetime, json
import json
from core.utils import DbUtils

api = Namespace('scrapped_location', description='scrapped location operations')
scrapped_location = api.model('scrapped location details ', {
    'id': fields.String(required=True, description='id'),
    'site_id': fields.String(required=True, description='the site identifier'),
    'storename': fields.String(required=True, description='the storename '),
    'chainname': fields.String(required=True, description='The chainname  '),
    'address': fields.String(required=True,description='The address '),
    'country': fields.String(required=True, description='the country  '),
    'state': fields.String(required=True, description='the state '),
    'city': fields.String(required=True, description='the city '),
    'latitude': fields.String(required=True, description='The latitude '),
    'longitude': fields.String(required=True, description='The longitude '),
    'phone': fields.String(required=True, description='the phone '),
    'email': fields.String(required=True, description='the email '),
    'workinghours': fields.String(required=True, description='the workinhours '),
    'storeurl': fields.String(required=True, description='the storeurl '),

        })

LOCATION_WEBPARSER_DETAILS = [
    {   'id': 'felix', 'site_id': 'Felix', 'storename': 'Felix','chainname': 'Felix', 'address': 'Felix'
        , 'country': 'Felix', 'state': 'Felix', 'city': 'Felix','latitude': 'Felix'
        , 'longitude': 'Felix', 'phone': 'Felix', 'email': 'Felix', 'workinghours': 'Felix',
        'storeurl': 'Felix',

          },
]





get_parser = reqparse.RequestParser()
get_parser.add_argument('start', type=int, help='integer value')
get_parser.add_argument('limit', type=int, help='integer value')
get_parser.add_argument('site_id',type=int, help='integer value')
get_parser.add_argument('location',help = 'string value')

get_filter = reqparse.RequestParser()
get_filter.add_argument('site_id',type=int, help='integer value')
get_filter.add_argument('location',help = 'string value')

@api.route('/')
class Locationparser(Resource):
    @api.doc('scrapped_location')
    # @api.marshal_with(sites)
    @api.expect(get_parser)
    def get(self):
        dbutils = DbUtils()
        start = int(request.args.get('start', 1))
        limit = int(request.args.get('limit', 10))
        site_id = request.args.get('site_id', None)
        location = request.args.get('location', None)
        if location is None or not location.strip():
            searchQuery = {}
            if site_id:
                searchQuery['site_id'] = site_id
            scrapped_location, count = dbutils.get_scrapped_location(int(start) - 1, limit, searchQuery)
            dbutils.close()
            if count:
                data = get_paginated_list(scrapped_location, '/api/v1/scrapped_location', count, start, limit=limit)
                return data
            else:
                return {'response_code': 2, 'status': 'failure', 'message': 'No data found'}, 200


        elif site_id is not None:
            location_keyword = location.strip().lower()
            print(location_keyword, site_id)
            data = dbutils.get_scrapped_location_filter(site_id)
            filtered_list = []
            for item in data:
                string_line = ' '
                address, city, state, country = item['address'], item['city'], item['state'], item['country']
                if city:
                    search_list=[city, state, country]
                else:
                    search_list=[address, city, state, country]

                for column in search_list:
                    if type(column) is str:
                        string_line += ' ' + column
                if string_line.lower().find(location_keyword) != -1:
                    filtered_list.append(item)

            dbutils.close()
            obj = {}
            obj['start'] = start
            obj['limit'] = len(filtered_list)
            obj['count'] = len(filtered_list)
            obj['previous'] = ''
            obj['next'] = ''
            obj['results'] = filtered_list

            return obj

@api.route('/filter')
class LocationFIlter(Resource):
    @api.doc('location_filter')
    # @api.marshal_with(sites)
    @api.expect(get_filter)
    def get(self):
        location = request.args.get('location', '')
        site_id = request.args.get('site_id', None)
        if location.strip() and site_id:
            dbutils = DbUtils()
            location_keyword = location.strip().lower()
            print (location_keyword,site_id)
            data = dbutils.get_scrapped_location_filter(site_id)
            if data:
                filtered_list=[]
                for item in data:
                    string_line=' '
                    address,city,state,country = item['address'], item['city'], item['state'], item['country']
                    for column in [address,city,state,country]:
                        if type(column) is str:
                            string_line += ' '+column

                    if string_line.lower().find(location_keyword) != -1:
                        filtered_list.append(dict(item))
                return jsonify(filtered_list)
            else:
                return {'message':"No record found in Database"}
            dbutils.close()