from flask import Flask, abort, request, jsonify,Blueprint
from flask_restplus import Namespace, Resource, fields,reqparse
#from flask_sqlalchemy import SQLAlchemy
from core.Paginator import get_paginated_list
import decimal,datetime,json
import json
from core.utils import DbUtils
api = Namespace('site', description='Sites related operations')

site= api.model('site', {
	'site_id': fields.String(required=True, description='The site identifier'),
	'site_name': fields.String(required=True, description='The site name'),
	'site_url': fields.String(required=True, description='The site url'),
	'require_sitemap_parsing': fields.Boolean(required=True, description='requires sitemap parsing'),
	'require_webpage_parsing': fields.Boolean(required=True, description='requires webpage parsing'),
	'require_dynamic_parsing': fields.Boolean(required=True, description='requires dynamic parsing'),
	'parse_status': fields.String(required=True, description='The site parsing status'),
	'site_status': fields.String(required=True, description='The site status'),
	'image_download': fields.String(required=True, description='The site image download'),
})

SITE = [
	{'site_id': 'felix', 'site_name': 'Felix', 'site_url': 'Felix', 'require_sitemap_parsing': 'Felix'
	, 'require_webpage_parsing': 'Felix', 'require_dynamic_parsing': 'Felix', 'parse_status': 'Felix', 'site_status': 'Felix'
	, 'image_download': 'Felix','start':'Felix','limit':'Felix'},
]
get_parser = reqparse.RequestParser()
get_parser.add_argument('start', type='int',help = 'integer value')
get_parser.add_argument('limit',type='int',help = 'integer value')
get_parser.add_argument('site_id', type=int, help = 'Site id')
get_parser.add_argument('site_name',help = 'integer value')
get_parser.add_argument('site_url',help = 'integer value')

@api.route('/')
class Site(Resource):
	@api.doc('list_sites')
	#@api.marshal_with(sites)
	@api.expect(get_parser)	
	def get(self):
		dbutils = DbUtils()
		start = int(request.args.get('start',1))
		limit = int(request.args.get('limit',10))
		site_id = request.args.get('site_id', None)
		site_name = request.args.get('site_name',None)
		site_url = request.args.get('site_url', None)
		searchQuery={}
		if site_name and site_url and site_id:
			searchQuery['site_name']=site_name if site_name else ''
			searchQuery['site_url']=site_url if site_url else ''
			searchQuery['site_id'] = site_id if site_id else ''
		elif site_name is not None:
			searchQuery['site_name'] = site_name
		elif site_url is not None:
			searchQuery['site_url'] = site_url
		elif site_id is not None:
			searchQuery['site_id'] = site_id
		
		site,count =dbutils.get_site(int(start)-1,limit,searchQuery)
		data = get_paginated_list(site,'/api/v1/site',count,start,limit=limit)
		dbutils.close()
		return data
