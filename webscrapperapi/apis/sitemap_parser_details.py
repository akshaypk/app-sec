from flask import Flask, abort, request, jsonify,Blueprint
from flask_restplus import Namespace, Resource, fields,reqparse
#from flask_sqlalchemy import SQLAlchemy
from core.Paginator import get_paginated_list
import decimal,datetime,json
import json
from core.utils import DbUtils


api = Namespace('sitemap_parser', description='Sitemap related operations')

sitemap = api.model('sitemap', {
    'site_id': fields.Integer(required=True, description='The site ID'),
    'sitemap_url': fields.String(required=True, description='The sitemap url'),
    'sitemap_parser_pattern': fields.String(required=True, description='Sitemap_url_pattern'),
    'sitemap_parser_zipped': fields.Boolean(required=True, description='Sitemap parser zipped'),
    'sitemap_product_tag': fields.Boolean(required=True, description='tag of prodcuts in sitemap'),
    'sitemap_product_url_pattern': fields.String(required=True, description='Sitemap for product urls'),
    'sitemap_xml_namespace': fields.String(required=True, description='Xml namespace of sitemap'),
})

SITEMAP = [
    {'site_id': 'felix', 'sitemap_url': 'Felix', 'sitemap_parser_pattern': 'Felix', 'sitemap_parser_zipped': 'Felix'
    , 'sitemap_product_tag': 'Felix', 'sitemap_product_url_pattern': 'Felix','sitemap_xml_namespace': 'Felix'
     },
]

post_parser = reqparse.RequestParser()
post_parser.add_argument('site_id',  required = True, type=int,  help='Site ID',location='form')
post_parser.add_argument('sitemap_url', required = True,  help='http://test.com',location='form')
post_parser.add_argument('sitemap_parser_pattern',required = True, help='https://www.test.com/sitemap_p_[0-9]+.xml.gz',location='form')
post_parser.add_argument('sitemap_parser_zipped',required = True, type = bool,default = False, help='True/False',location='form')
post_parser.add_argument('sitemap_product_tag', required = True, help='loc',location='form')
post_parser.add_argument('sitemap_product_url_pattern',required = True, help='https://www.test.com/site/[A-Za-z0-9-\\\\/.]+\\\\?skuId=[0-9]+',location='form')
post_parser.add_argument('sitemap_xml_namespace', required = True,help='http://www.sitemaps.org/schemas/sitemap/0.9',location='form')


update_parser = reqparse.RequestParser()
update_parser.add_argument('site_id', required  = True, type=int, help='Site ID', location='form')
update_parser.add_argument('sitemap_url', required  = True, type=str,  location='form')
update_parser.add_argument('sitemap_parser_pattern', required  = True, type=str, location='form')
update_parser.add_argument('sitemap_parser_zipped', required  = True, type=bool,default = False, help='True/False', location='form')
update_parser.add_argument('sitemap_product_tag', required  = True, type=str, help='loc', location='form')
update_parser.add_argument('sitemap_product_url_pattern', required  = True, type=str, location='form')
update_parser.add_argument('sitemap_xml_namespace', required  = True, type=str, location='form')


get_parser = reqparse.RequestParser()
get_parser.add_argument('start', type='int',help = 'integer value')
get_parser.add_argument('limit',type='int',help = 'integer value')
get_parser.add_argument('site_id', help= 'Site ID')
get_parser.add_argument('site_name',help='site_name')
get_parser.add_argument('sitemap_url',help = 'URL of sitemap')


delete_parser = reqparse.RequestParser()
delete_parser.add_argument('site_id', type =int, location = 'form')
@api.route('/')
class Sitemap(Resource):
    @api.doc('list_sitemap')
    # @api.marshal_with(sites)
    @api.expect(get_parser)
    def get(self):
        dbutils = DbUtils()
        start = int(request.args.get('start', 1))
        limit = int(request.args.get('limit', 10))
        site_id = request.args.get('site_id', None)
        site_name=request.args.get('site_name',None)
        sitemap_url = request.args.get('sitemap_url', None)
        searchQuery = {}
        if site_id and sitemap_url and site_name:
            searchQuery['site_id'] = site_id if site_id else ''
            searchQuery['site_name']=site_name if site_name else ''
            searchQuery['sitemap_url'] = sitemap_url if sitemap_url else ''
        elif site_name is not None:
            searchQuery['site_name']=site_name
        elif site_id is not None:
            searchQuery['site_id'] = site_id
        elif sitemap_url is not None:
            searchQuery['sitemap_url'] = sitemap_url

        # sites =dbutils.get_sites({'site_name':site_name,'site_url':site_url})
        # sitemap, count = dbutils.get_sitemap_parser_details(site)
        sitemap, count = dbutils.get_sitemap_parser_details(int(start) - 1, limit, searchQuery)
        data = get_paginated_list(sitemap, '/api/v1/sites', count, start, limit=limit)
        dbutils.close()
        return data


@api.route('/add')
class Add_sitemap(Resource):
    @api.doc('post_sitemap')
    @api.expect(post_parser)
    def post(self):
        dbutils = DbUtils()
        result = {}
        try:
            # check for the existing site
            site_id = request.form['site_id']
            sitemap_url = request.form['sitemap_url']
            sitemap_parser_pattern = request.form['sitemap_parser_pattern']
            sitemap_parser_zipped =True if 'sitemap_parser_zipped' in request.form else False
            sitemap_product_tag = request.form['sitemap_product_tag']
            sitemap_product_url_pattern = request.form['sitemap_product_url_pattern']
            sitemap_xml_namespace = request.form['sitemap_xml_namespace']
            print(site_id, sitemap_url, sitemap_parser_pattern, sitemap_parser_zipped, sitemap_product_tag,
                  sitemap_product_url_pattern, sitemap_xml_namespace)
            site, sites_count = dbutils.get_sites(0,1,{'site_id':site_id})
            sitemap, sitemap_count = dbutils.get_sitemap_parser_details(0, 1, {'site_id': site_id,
                                                                               'sitemap_url': sitemap_url})
            print(sites_count, sitemap_count)
            if sites_count > 0:
                if sitemap_count > 0:
                    dbutils.close()
                    return {'response_code': 2 ,'status':'failure', 'message': 'Site already exists Please update.'},200
                else:

                    result = dbutils.create_sitemap_parser({'site_id': site_id, 'sitemap_url': sitemap_url,
                                              'sitemap_parser_pattern': sitemap_parser_pattern,
                                              'sitemap_parser_zipped': sitemap_parser_zipped,
                                              'sitemap_product_tag': sitemap_product_tag,
                                              'sitemap_product_url_pattern': sitemap_product_url_pattern,
                                              'sitemap_xml_namespace': sitemap_xml_namespace})
            else:
                dbutils.close()
                return {'response_code': 2 ,'status':'failure', 'message': 'Site ID does not exist kindly add .'},200
            # return result,200
        except Exception as e:
            print(e)
            dbutils.close()
            return {'response_code': 2, 'status': 'failure', 'message': 'unexpected error'}, 200

        else:
            dbutils.close()
            return {'response_code': 0, 'status': 'success','data': result, 'message': 'Sitemap parser details created successfully'}, 200
@api.route('/update')
class Update_sitemap(Resource):
    @api.doc('update_sitemap')
    @api.expect(update_parser)
    def put(self):
        dbutils = DbUtils()
        result = {}
        try:
            # check for the existing site
            site_id = request.form['site_id']
            sitemap_url = request.form['sitemap_url']
            sitemap_parser_pattern = request.form['sitemap_parser_pattern']
            sitemap_parser_zipped = True if 'sitemap_parser_zipped' in request.form else False
            sitemap_product_tag = request.form['sitemap_product_tag']
            sitemap_product_url_pattern = request.form['sitemap_product_url_pattern']
            sitemap_xml_namespace = request.form['sitemap_xml_namespace']

            # print(site_name, site_url, require_sitemap_parsing, require_webpage_parsing, require_dynamic_parsing,
            # 	  site_status, parse_status, image_download)

            sitemap, count = dbutils.get_sitemap_parser_details(0, 1, {'site_id': site_id})
            print(count)
            if count > 0:
                result = dbutils.update_sitemap_parser({'site_id': site_id, 'sitemap_url': sitemap_url,
                                                        'sitemap_parser_pattern': sitemap_parser_pattern,
                                              'sitemap_parser_zipped': sitemap_parser_zipped,
                                              'sitemap_product_tag': sitemap_product_tag,
                                              'sitemap_product_url_pattern': sitemap_product_url_pattern,
                                              'sitemap_xml_namespace': sitemap_xml_namespace,
                                              })
            else:
                dbutils.close()
                return {'response_code': 2, 'status': 'failure', 'message': 'Site doesnt exist please add .'}, 200
        # return result,200
        except Exception as e:
            print(e)
            dbutils.close()
            return {'response_code': 2, 'status': 'failure', 'message': 'unexpected error'}, 200
        else:
            dbutils.close()
            return {'response_code': 0, 'status': 'success', 'message': 'Sitemap parser details updated successfully'}, 200


@api.route('/delete')
class Delete_sitemap(Resource):
    @api.doc('delete_sitemap')
    @api.expect(delete_parser)
    def delete(self):
        try:

            dbutils = DbUtils()

            site_id = request.form['site_id']
            site, count = dbutils.get_sitemap_parser_details(0, 1, {'site_id': site_id})
            if count > 0:
                dbutils.delete_sitemap_parser_details(site_id)
            else:
                dbutils.close()
                return {'response_code': 2, 'status': 'failure', 'message': 'Sitemap parser details does not exist.'}, 200



        except Exception as e:
            print(e)
            dbutils.close()
            return {'response_code': 2, 'status': 'failure', 'message': 'unexpected error'}, 200
        else:
            dbutils.close()
            return {'response_code': 0, 'status': 'success', 'message': 'Deleted sitemap parser details updated successfully'}, 200



