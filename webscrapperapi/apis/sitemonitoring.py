
from flask import Flask, redirect, request, jsonify, Blueprint, Response
from flask import Flask, redirect, request, jsonify, Blueprint, Response
from flask_restplus import Namespace, Resource, fields, reqparse
import json

from core.Paginator import get_paginated_list
import decimal, datetime, json
import json
from core.utils import DbUtils
from core.LoggingManager import get_logger
from .products import products


api = Namespace('site_monitoring', description='site monitoring scraper ')
post_parser = reqparse.RequestParser()
# post_parser.add_argument('search_text', type=str, action='split', location = 'form')
post_parser.add_argument('site_id', type=int,  location = 'form')

update_parser = reqparse.RequestParser()
update_parser.add_argument('site_id', required = True, location='form')
update_parser.add_argument('site_change_status', type = bool, default = False, help='path',location='form')

@api.route('/add')
class post_upc(Resource):
    @api.doc('post_siteid')
    @api.expect(post_parser)
    def post(self):
            dbutils = DbUtils()
            site_id = request.form['site_id']
            # product_upc = request.form['product_upc']
            jsondata = dbutils.processproducturl({'site_id': site_id})
            get_logger().info("data is %s",jsondata)


            if 'response_code' in jsondata and jsondata['response_code']:

                return {'response_code': 2, 'status': 'failure', 'message':jsondata["message"]}


            upc_list=jsondata.get('product_upc',[])
            get_logger().info("upc is %s", upc_list)
            # result = []
            result=[{},{}]
            if upc_list:
                upc=(upc_list[0].lstrip())


                searchQuery={}
                searchQuery['product_upc']=str(upc)
                searchQuery['site_id']=site_id

                products, count = dbutils.get_products(0, 10, searchQuery)
                get_logger().info("products is %s", products)


                temp = products[0]

                # print(products)
                result[0] = products[0]

                temp = products[0].copy()

                for key in temp.keys():
                    temp[key] = ""

                for key in jsondata.keys():


                    temp[key] = jsondata[key][0]


                result[1] = temp


            dbutils.close()
            return jsonify(result)


@api.route('/update')
class UpdateSite(Resource):

    @api.doc('update_sites')
    @api.expect(update_parser)
    def put(self):

        # try:

        dbutils = DbUtils()
        result = {}
        site_id = request.form['site_id']

        # site_change_status = request.form['site_change_status']
        is_site_change_status = request.form.get('site_change_status', 'false')

        if is_site_change_status:
            site_change_status = True if is_site_change_status == 'true' else False
        else:
            site_change_status = False

        # query = {}
        #
        # query['site_id'] = site_id

        #
        # site = dbutils.get_sites(0, 1, query)[0][0]
        # print('site',site)

        site={}
        site['site_change_status'] = site_change_status
        site['site_id'] = site_id

        result = dbutils.update_site(site)
        get_logger().info("updated site results is %s", result)

        dbutils.close()
        return {'response_code': 0, 'status': 'success', 'message': 'Site status has been updated .'}, 200
            # if count > 0:
            # result = dbutils.update_site({'site_id':site_id,'site_name': site_name, 'site_url': site_url,
            # ................................  'require_sitemap_parsing': require_sitemap_parsing,
            # ................................  'require_webpage_parsing': require_webpage_parsing,
            # ................................  'required_location_parsing': require_location_parsing,
            # ................................  'require_dynamic_scrapping': require_dynamic_scrapping,
            # ................................  'site_status': site_status,
            # ................................  'parse_status': parse_status,
            # ................................  'image_download': image_download})
        # else:
        # ....dbutils.close()
        # ....return {'response_code': 2, 'status': 'failure', 'message': 'Site doesnt exist please add .'}, 200
        # return result,200

        # except Exception as e:
        #     print(e)
        # ....dbutils.close()
        #     return {'response_code': 2, 'status': 'failure', 'message': 'unexpected error'}, 200
        # else:
        # ....dbutils.close()
        # ....return {'response_code': 0, 'status': 'success', 'message': 'Site updated successfully'}, 200