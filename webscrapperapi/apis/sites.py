from flask import Flask, abort, request, jsonify,Blueprint
from flask_restplus import Namespace, Resource, fields,reqparse
#from flask_sqlalchemy import SQLAlchemy
from core.Paginator import get_paginated_list
import decimal,datetime,json
import json
from core.utils import DbUtils
api = Namespace('sites', description='Sites related operations')

sites = api.model('sites', {
    'site_id': fields.String(required=True, description='The site identifier'),
    'site_name': fields.String(required=True, description='The site name'),
    'site_url': fields.String(required=True, description='The site url'),
    'require_sitemap_parsing': fields.Boolean(required=True, description='requires sitemap parsing'),
    'require_webpage_parsing': fields.Boolean(required=True, description='requires webpage parsing'),
    'require_dynamic_scrapping': fields.Boolean(required=True, description='requires dynamic parsing'),
    'require_catgeory_parsing':fields.Boolean(required=True, description='require_catgeory_parsing'),
    'require_location_parsing':fields.Boolean(required=True, description='require location parsing'),
    'require_ajax_parsing' : fields.Boolean(required=True, description='require ajax parsing'),
    'parse_status': fields.String(required=True, description='The site parsing status'),
    'site_status': fields.String(required=True, description='The site status'),
    'image_download': fields.String(required=True, description='The site image download'),
    'site_change_status': fields.String(required=True, description='The site_change_status'),
    'expiry_check': fields.Boolean(required=True, description='expiry_check'),
})

SITES = [
    {'site_id': 'felix', 'site_name': 'Felix', 'site_url': 'Felix', 'require_sitemap_parsing': 'Felix',
     'require_webpage_parsing': 'Felix', 'require_dynamic_scrapping': 'Felix','require_catgeory_parsing':'Felix',
     'require_location_parsing':'Felix','parse_status': 'Felix', 'site_status': 'Felix','image_download': 'Felix','site_change_status': 'Felix','start':'Felix','limit':'Felix'}
]

post_parser = reqparse.RequestParser()
post_parser.add_argument('site_name',  required = True, help='test.com',location='form')
post_parser.add_argument('site_url', required = True,  help='http://test.com',location='form')
post_parser.add_argument('require_sitemap_parsing',required = True, type = bool, default = False, help='True/False',location='form')
post_parser.add_argument('require_webpage_parsing',required = True, type = bool,default = False, help='True/False',location='form')
post_parser.add_argument('require_dynamic_scrapping', required = True,type = bool, default = False,help='True/False',location='form')
post_parser.add_argument('require_category_parsing', required = True,type = bool, default = False,help='True/False',location='form')
post_parser.add_argument('require_location_parsing', required = True,type = bool, default = False,help='True/False',location='form')
post_parser.add_argument('require_ajax_parsing', required = True,type = bool, default = False,help='True/False',location='form')
post_parser.add_argument('parse_status',required = True,  type = bool,default = False,help='True',location='form')
post_parser.add_argument('site_status', required = True,type = bool,default = True,help='True',location='form')
post_parser.add_argument('image_download', required = True,type = bool, default = False, help='path',location='form')
post_parser.add_argument('site_change_status', type = bool, default = False, help='path',location='form')
post_parser.add_argument('expiry_check', type = bool, default = False, help='path',location='form')


get_parser = reqparse.RequestParser()
get_parser.add_argument('start', type='int',help = 'integer value')
get_parser.add_argument('limit',type='int',help = 'integer value')
get_parser.add_argument('site_id', type=int, help = 'Site id')
get_parser.add_argument('site_name',help = 'integer value')
get_parser.add_argument('site_url',help = 'integer value')
get_parser.add_argument('require_dynamic_scrapping', help = 'true/false value')
get_parser.add_argument('require_category_parsing',help = 'true/falsevalue')
get_parser.add_argument('required_location_parsing',help = 'true/false value')
get_parser.add_argument('require_ajax_parsing',help = 'true/false value')
get_parser.add_argument('require_webpage_parsing',help = 'true/false value')
get_parser.add_argument('require_sitemap_parsing',help = 'true/false value')
get_parser.add_argument('site_status',help = 'true/false value')
get_parser.add_argument('parse_status',help = 'true/false value')

update_parser = reqparse.RequestParser()
update_parser.add_argument('site_id', required = True, location='form')
update_parser.add_argument('site_name',  required = True, help='test.com',location='form')
update_parser.add_argument('site_url', required = True,  help='http://test.com',location='form')
update_parser.add_argument('require_sitemap_parsing', required=True, type = bool, default = False, help='True/False',location='form')
update_parser.add_argument('require_webpage_parsing', required=True, type = bool,default = False, help='True/False',location='form')
update_parser.add_argument('require_dynamic_scrapping', required=True, type = bool, default = False,help='True/False',location='form')
update_parser.add_argument('require_category_parsing', required = True,type = bool, default = False,help='True/False',location='form')
update_parser.add_argument('required_location_parsing', required=True, type = bool,default = False, help='True/False',location='form')
update_parser.add_argument('require_ajax_parsing', required = True,type = bool, default = False,help='True/False',location='form')
update_parser.add_argument('parse_status',required = True,  type = bool,default = False,help='True',location='form')
update_parser.add_argument('site_status', required = True,type = bool,default = True,help='True',location='form')
update_parser.add_argument('image_download', required = True,type = bool, default = False, help='path',location='form')
update_parser.add_argument('site_change_status', type = bool, default = False, help='path',location='form')
update_parser.add_argument('expiry_check', type = bool, default = False, help='path',location='form')

delete_parser = reqparse.RequestParser()
delete_parser.add_argument('site_id', type =int, location = 'form')

@api.route('/')
class Site(Resource):
    @api.doc('list_sites')
    #@api.marshal_with(sites)
    @api.expect(get_parser)
    def get(self):
        dbutils = DbUtils()
        start = int(request.args.get('start',1))
        limit = int(request.args.get('limit',10))
        site_id = request.args.get('site_id', None)
        site_name = request.args.get('site_name',None)
        site_url = request.args.get('site_url', None)
        required_location_parsing= request.args.get('required_location_parsing', None)
        require_dynamic_scrapping= request.args.get('require_dynamic_scrapping', None)
        require_category_parsing= request.args.get('require_category_parsing', None)
        require_webpage_parsing= request.args.get('require_webpage_parsing', None)
        require_sitemap_parsing= request.args.get('require_sitemap_parsing', None)
        require_ajax_parsing = request.args.get('require_ajax_parsing', None)
        searchQuery={}
        # if site_name and site_url and site_id:
        #     searchQuery['site_name']=site_name if site_name else ''
        #     searchQuery['site_url']=site_url if site_url else ''
        #     searchQuery['site_id'] = site_id if site_id else ''
        if site_name is not None:
            searchQuery['site_name'] = site_name
        if site_url is not None:
            searchQuery['site_url'] = site_url
        if site_id is not None:
            searchQuery['site_id'] = site_id
        if require_sitemap_parsing is not None:
            searchQuery['require_sitemap_parsing'] = require_sitemap_parsing
        if require_webpage_parsing is not None:
            searchQuery['require_webpage_parsing'] = require_webpage_parsing
        if require_category_parsing is not None:
            searchQuery['require_category_parsing'] = require_category_parsing
        if required_location_parsing is not None:
            searchQuery['required_location_parsing'] = required_location_parsing
        if require_dynamic_scrapping is not None:
            searchQuery['require_dynamic_scrapping'] = require_dynamic_scrapping
        # if require_ajax_parsing is not None:
        #     searchQuery['require_ajax_parsing'] = require_ajax_parsing


        #sites =dbutils.get_sites({'site_name':site_name,'site_url':site_url})
        sites,count =dbutils.get_sites(int(start)-1,limit,searchQuery)
        dbutils.close()
        if count:
            data = get_paginated_list(sites,'/api/v1/sites',count,start,limit=limit)
            return data
        else:
            return {'response_code': 2, 'status': 'failure', 'message': 'No data found'}, 200

# @api.route('/list')
# class Site(Resource):
# 	#@api.doc('list_sites')
# 	@api.marshal_with(sites)
# 	def get(self):
# 		dbutils = DbUtils()
# 		data =dbutils.list_sites()
# 		return data
#
# 	# def post(self,site_name):
    # 	print(request.site_name)
    # 	dbutils = DbUtils()
@api.route('/add')
class AddSite(Resource):
    @api.doc('post_sites')
    #@api.marshal_with(sites)
    @api.expect(post_parser)
    def post(self):
        dbutils = DbUtils()
        result = {}
        try:
            #check for the existing site
            site_name = request.form['site_name']
            site_url = request.form['site_url']
            require_sitemap_parsing = dbutils.get_status_bool(request,'require_sitemap_parsing')
            require_webpage_parsing = dbutils.get_status_bool(request,'require_webpage_parsing')
            require_dynamic_scrapping = dbutils.get_status_bool(request,'require_dynamic_scrapping')
            require_category_parsing = dbutils.get_status_bool(request,'require_category_parsing')
            require_ajax_parsing = dbutils.get_status_bool(request, 'require_ajax_parsing')
            require_location_parsing = dbutils.get_status_bool(request,'require_location_parsing')
            expiry_check = dbutils.get_status_bool(request, 'expiry_check')
            site_status = dbutils.get_status_bool(request,'site_status')
            parse_status = dbutils.get_status_bool(request,'parse_status')
            image_download = dbutils.get_status_bool(request,'image_download')

            is_site_change_status = request.form.get('site_change_status', 'false')
            if is_site_change_status:
                site_change_status = True if is_site_change_status == 'true' else False
            else:
                site_change_status = False
            #print(site_name,site_url,require_sitemap_parsing,require_webpage_parsing,require_dynamic_scrapping,site_status,parse_status,image_download)
            sitename,count_name = dbutils.get_sitename(0,1,{'site_name':site_name})
            siteurl, count_url = dbutils.get_sitename(0, 1, {'site_url': site_url})
            if count_name> 0 :
                    dbutils.close()
                    return {'response_code': 2 ,'status':'failure', 'message': 'Site Name already exists please update.'},200
            if count_url >0:
                dbutils.close()
                return {'response_code': 2, 'status': 'failure', 'message': 'Site Url already exists please update.'}, 200
            else:
                  result = dbutils.create_site({'site_name':site_name,'site_url':site_url,
                    'require_sitemap_parsing':require_sitemap_parsing,
                    'require_webpage_parsing':require_webpage_parsing,
                    'required_location_parsing': require_location_parsing,
                    'require_dynamic_scrapping':require_dynamic_scrapping,
                    'require_category_parsing':require_category_parsing,
                    # 'require_ajax_parsing': require_ajax_parsing,
                    'site_status':site_status,
                    'parse_status':parse_status,
                    'image_download':image_download,
                    'site_change_status':site_change_status,
                    'expiry_check':expiry_check,})

        except Exception as e:
            print(e)
            dbutils.close()
            return {'response_code':2,'status':'failure', 'message':'unexpected error'},200
        else:
            dbutils.close()
            return {'response_code':0,'status':'success','data': result,'message': 'Site created successfully'},200


@api.route('/update')
class UpdateSite(Resource):
    @api.doc('update_sites')
    # @api.marshal_with(sites)
    @api.expect(update_parser)
    def put(self):
        dbutils = DbUtils()
        result = {}
        try:
            # check for the existing site
            site_id = request.form['site_id']
            site_name = request.form['site_name']
            site_url = request.form['site_url']
            require_sitemap_parsing = dbutils.get_status_bool(request, 'require_sitemap_parsing')
            require_webpage_parsing = dbutils.get_status_bool(request, 'require_webpage_parsing')
            require_dynamic_scrapping = dbutils.get_status_bool(request, 'require_dynamic_scrapping')
            require_category_parsing = dbutils.get_status_bool(request, 'require_category_parsing')
            require_ajax_parsing = dbutils.get_status_bool(request, 'require_ajax_parsing')
            require_location_parsing = dbutils.get_status_bool(request, 'require_location_parsing')
            site_status = dbutils.get_status_bool(request, 'site_status')
            parse_status = dbutils.get_status_bool(request, 'parse_status')

            image_download = dbutils.get_status_bool(request, 'image_download')
            expiry_check = dbutils.get_status_bool(request, 'expiry_check')

            # site_change_status = True if 'site_change_status' in request.form else False
            is_site_change_status = request.form.get('site_change_status', 'false')
            if is_site_change_status:
                site_change_status = True if is_site_change_status == 'true' else False
            else:
                site_change_status = False
            # print(site_name, site_url, require_sitemap_parsing, require_webpage_parsing, require_dynamic_scrapping,
            # 	  site_status, parse_status, image_download)

            site, count = dbutils.get_sites(0, 1, {'site_id': site_id,'site_name': site_name})
            # print(count)
            if count > 0:
                result = dbutils.update_site({'site_id':site_id,'site_name': site_name, 'site_url': site_url,
                                              'require_sitemap_parsing': require_sitemap_parsing,
                                              'require_webpage_parsing': require_webpage_parsing,
                                              'required_location_parsing': require_location_parsing,
                                              'require_dynamic_scrapping': require_dynamic_scrapping,
                                              'require_category_parsing' : require_category_parsing,
                                              # 'require_ajax_parsing': require_ajax_parsing,
                                              'site_status': site_status,
                                              'parse_status': parse_status,
                                              'image_download': image_download,
                                              'site_change_status':site_change_status,
                                              'expiry_check':expiry_check})
            else:
                dbutils.close()
                return {'response_code': 2, 'status': 'failure', 'message': 'Site  doesnt exist please add .'}, 200
        # return result,200
        except Exception as e:
            print(e)
            dbutils.close()
            return {'response_code': 2, 'status': 'failure', 'message': 'unexpected error'}, 200
        else:
            dbutils.close()
            return {'response_code': 0, 'status': 'success', 'message': 'Site updated successfully'}, 200


@api.route('/delete')
class Delete_site(Resource):
    @api.doc('delete_site')
    @api.expect(delete_parser)
    def delete(self):
        try:
            dbutils = DbUtils()
            site_id = request.form['site_id']
            site, count = dbutils.get_sites(0, 1, {'site_id': site_id})
            if count > 0:
                status=dbutils.delete_site(site_id)
                if status["status"]=="success":
                    return {'response_code': 0, 'status': 'success', 'message': 'Deleted site successfully'}, 200
                else:
                    status["response_code"]=2
                    status["status"]='failure'
                    return status,200

            else:
                dbutils.close()
                return {'response_code': 2, 'status': 'failure', 'message': 'Site doesnt exist'}, 200



        except Exception as e:
            print(e)
            dbutils.close()
            return {'response_code': 2, 'status': 'failure', 'message': 'unexpected error'}, 200


