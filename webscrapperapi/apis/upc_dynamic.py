from flask import Flask, redirect, request, jsonify, Blueprint, Response
from flask_restplus import Namespace, Resource, fields, reqparse
# from flask_sqlalchemy import SQLAlchemy
from core.Paginator import get_paginated_list
import decimal, datetime, json
import json
from core.utils import DbUtils
from core.LoggingManager import get_logger
from .products import products


api = Namespace('upc_dynamic_scrape', description='upc dynamic scraper ')
post_parser = reqparse.RequestParser()
post_parser.add_argument('search_text', type=str, action='split', location = 'form')
post_parser.add_argument('site_id', type=int,  location = 'form')
post_parser.add_argument('request_type', type=str, choices=('dynamic', 'ajax'),location='form')


@api.route('/add')
class post_upc(Resource):
    @api.doc('post_upc')
    @api.expect(post_parser)
    def post(self):
            dbutils = DbUtils()
            search_text = request.form['search_text'] if 'search_text' in request.form else ''
            site_id = request.form['site_id'] if 'site_id' in request.form else ''
            request_type = request.form['request_type'] if 'request_type' in request.form else ''
            texts = search_text.split(",")
            prodList = []
            header_list = {}
            proddataList=[]
            jsondata = []
            for headers in products:
                header_list[headers] = ''
            try:
                for itm in texts:
                    jsondata = dbutils.dynamic({'site_id':site_id,
                                                'search_text':itm,
                                                'request_type':request_type})
                    break

                for data in jsondata:
                    temp = header_list.copy()
                    for key, value in data.items():
                        temp.update({key:value})
                    prodList.append(temp)
                #print(prodList)
                #print(len(prodList))
                dbutils.close()
                return jsonify(prodList)
            except Exception as e:
                dbutils.close()
                get_logger().error('Error in creating site config :', exc_info=True)
                return {'message': str(e), 'response_code': 2, 'status': "failure"}


