from flask import Blueprint, Flask, abort, jsonify, request
from flask_restplus import Namespace, Resource, fields, reqparse

import decimal,datetime,json
from json import dumps

from core.Paginator import get_paginated_list
from core.utils import DbUtils


api = Namespace('user', description='user manipulations')


user = api.model('user',{
'id': fields.String(required=True, description='Id'),
'first_name': fields.String(required=True, description='First Name'),
'last_name': fields.String(required=True, description='Last Name'),
'username': fields.String(required=True, description='Username'),
'password': fields.String(required=True, description='Password'),
'active': fields.Boolean(required=True, description='Active'),
'last_login': fields.String(required=True, description= 'Last Login'),
'login_count': fields.String(required=True, description='Login Count'),
'fail_login_count': fields.String(required=True, description='Fail Login Count'),
'created_on': fields.String(required=True, description='Created On'),
})

get_parser = reqparse.RequestParser()
get_parser.add_argument('start', type='int', help = 'integer value')
get_parser.add_argument('limit', type='int', help = 'integer value')
get_parser.add_argument('id', type='int', help = 'integer value')
get_parser.add_argument('username', help = 'string value')


post_parser = reqparse.RequestParser()
# post_parser.add_argument('id', required = True, type=int, help = 'integer value', location='form')
post_parser.add_argument('first_name',required = True, help = 'string value', location='form')
post_parser.add_argument('last_name',required = True, help = 'string value', location='form')
post_parser.add_argument('username',required = True, help = 'string value', location='form')
post_parser.add_argument('password',required = True, help = 'string value', location='form')
post_parser.add_argument('email',required = True, help = 'string value', location='form')
post_parser.add_argument('active',  type = bool, default = False, help = 'true/false', location='form')
post_parser.add_argument('last_login', help = 'timestamp string value', location='form')
post_parser.add_argument('login_count',  type=int, help = 'integer value', location='form')
post_parser.add_argument('fail_login_count',  type=int, help = 'integer value', location='form')
post_parser.add_argument('created_on', help = 'timestamp string value', location='form')


# update_parser = reqparse.RequestParser()
# update_parser.add_argument('id', required = True, type=int, help = 'integer value', location='form')
# update_parser.add_argument('first_name',required = True, help = 'string value', location='form')
# update_parser.add_argument('last_name',required = True, help = 'string value', location='form')
# update_parser.add_argument('username',required = True, help = 'string value', location='form')
# update_parser.add_argument('active',  type = bool, default = False, help = 'true/false', location='form')
# update_parser.add_argument('last_login', help = 'timestamp string value', location='form')
# update_parser.add_argument('login_count',  type=int, help = 'integer value', location='form')
# update_parser.add_argument('fail_login_count',  type=int, help = 'integer value', location='form')
# update_parser.add_argument('created_on', help = 'timestamp string value', location='form')



delete_parser = reqparse.RequestParser()
delete_parser.add_argument('id',  type=int, help = 'integer value', location='form')



@api.route('/')
class User(Resource):
    @api.doc('list_user')
    
    @api.expect(get_parser)
    def get(self):
        dbutils = DbUtils()
        start = int(request.args.get('start',1))
        limit = int(request.args.get('limit',10))
     
        id = request.args.get('id',None)
        username = request.args.get('username',None)
   
        searchQuery={}
        
        if id is not None:
            searchQuery['id'] = id

        if username is not None:
            searchQuery['username'] = username

        user,count =dbutils.get_users(int(start)-1,limit,searchQuery)
        dbutils.close()
        if count:
            data = get_paginated_list(user,'/api/v1/user',count,start,limit=limit)
            return jsonify(data)
        else:
            return {'response_code': 2, 'status': 'failure', 'message': 'No data found'}, 200



@api.route('/add')
class Add_user(Resource):
    @api.doc('post_user')
    @api.expect(post_parser)
    def post(self):
        dbutils = DbUtils()
        result = {}
        # print('add user')
        try:
            id = request.form['id']  if 'id' in request.form else None
            first_name = request.form['first_name']  if 'first_name' in request.form else ""
            last_name = request.form['last_name'] if 'last_name' in request.form else ""
            username = request.form['username']  if 'username' in request.form else ""
            email = request.form['email']  if 'email' in request.form else ""
            password = request.form['password']  if 'password' in request.form else None
            # active = True if 'active' in request.form else False
            # last_login = request.form['last_login']  if 'last_login' in request.form else None
            # login_count = request.form['login_count'] if 'login_count' in request.form else 0
            # fail_login_count = request.form['fail_login_count'] if 'fail_login_count' in request.form else 0
            # created_on = request.form['created_on'] if 'created_on' in request.form else None

            user,count = dbutils.get_users(0,1,{'username':username})

            if count > 0:
                
                dbutils.close()
                return {'response_code': 2 ,'status':'failure', 'message': 'User already successfully.'},200
            else:
                result = dbutils.create_user({ 
            	# 'id':0,
            	'first_name':first_name,
            	'last_name':last_name,
            	'username':username,
                'email':email,
                'password':password,
            	'active':True
            	# 'last_login':last_login,
            	# 'login_count':login_count,
            	# 'fail_login_count':fail_login_count,
            	# 'created_on':created_on, 
                })

                post_user,user_count = dbutils.get_users(0,1,{'username':username})

                dbutils.close()
                return {'response_code': 0 ,'status':'success', 'message': 'Registered successfully.'},200
            # return result,200
        except Exception as e:
            print('--->',e)
            dbutils.close()
            return {'response_code': 2, 'status': 'failure', 'message': 'unexpected error'}, 200

        else:
            dbutils.close()
            return {'response_code': 0, 'status': 'success', 'message': 'user created successfully'}, 200


@api.route('/delete')
class Delete_user(Resource):
    @api.doc('delete_user')
    @api.expect(delete_parser)
    def delete(self):
        try:

            dbutils = DbUtils()

            id = request.form['id']
            user, count = dbutils.get_users(0, 1, {'id': id})
            if count > 0:
                dbutils.delete_user(id)
                return {'response_code': 0, 'status': 'success', 'message': 'user deleted successfully'}, 200
            else:
                dbutils.close()
                return {'response_code': 2, 'status': 'failure', 'message': 'user does not exist.'}, 200

        except Exception as e:
            print(e)
            dbutils.close()
            return {'response_code': 2, 'status': 'failure', 'message': 'unexpected error'}, 200
        else:
            dbutils.close()
            return {'response_code': 0, 'status': 'success', 'message': 'Deleted user successfully'}, 200


