from flask_restplus import Namespace, Resource, fields,reqparse
from flask import request
import urllib3,json


api = Namespace('Walmartapi', description='Walmart custom search related operations')


WALMARTAPI = [{'upc':'Felix'}]

get_parser = reqparse.RequestParser()
get_parser.add_argument('start', type=int, help='integer value')
get_parser.add_argument('limit', type=int, help='integer value')
get_parser.add_argument('upc', help = 'Enter upc Value')

@api.route('/')
class WalmartAPi(Resource):
    @api.doc('list_Walmartapi')
    #@api.marshal_list_with(cat)
    @api.expect(get_parser)
    def get(self):
        result = {}
        http = urllib3.PoolManager()
        upc = request.args.get('upc', None)
        if upc is not None:
            r = http.request('GET', 'http://api.walmartlabs.com/v1/items?apiKey=vkyt4qm5h4542gf4xmvqub45&upc='+str(request.args.get('upc')))
            # r = http.request('GET', 'http://10.246.1.8:8055/?ean='+str(request.args.get('ean')))
            #r = http.request('GET', 'http://127.0.0.1:8055/?ean='+str(request.args.get('ean')))
            result = {"result":json.loads(r.data.decode())}
        else:
            result = {"result": "Error Occurred"}
        return result