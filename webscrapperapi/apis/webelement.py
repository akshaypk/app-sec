from flask import Flask, abort, request, jsonify,Blueprint
from flask_restplus import Namespace, Resource, fields,reqparse
#from flask_sqlalchemy import SQLAlchemy
from core.Paginator import get_paginated_list
import decimal,datetime,json
import json
from core.utils import DbUtils

api = Namespace('webelement', description='Addition of new column in webpage parser table')
post_parser = reqparse.RequestParser()
post_parser.add_argument('column_name',  required = True, type=str,location='form')
post_parser.add_argument('column_datatype', required = True, type=str, choices=('Boolean', 'Character Varying', 'Date', 'Text'),location='form')
#API route for adding a new column in database
@api.route('/add')
class Add_Column(Resource):
    @api.doc('add_column')
    @api.expect(post_parser)
    def post(self):
        dbutils = DbUtils()
        try:
            column_name = request.form['column_name']
            column_datatype = request.form['column_datatype']
            dbutils.webpage_add_column(column_name,column_datatype)
            dbutils.product_add_column(column_name,column_datatype)
        except Exception as e:
            dbutils.close()
            return {'response_code': 2 ,'status':'failure', 'message': 'Error in creating column.'},200
        else:
            dbutils.close()
            return {'response_code': 0, 'status': 'success', 'message': 'Added a new column successfully'}, 200