from flask import Flask, abort, request, jsonify, Blueprint
from flask_restplus import Namespace, Resource, fields, reqparse
from sqlalchemy import Column, String
from core.Paginator import get_paginated_list
import decimal, datetime, json
import json
from core.utils import DbUtils

api = Namespace('webpage_parser_details', description='webpage_parser related operations')

webpage_parser_details = api.model('webpage_parser_details ', {
    'webpage_parser_id': fields.String(required=True, description='The webpage parser identifier'),
    'site_id': fields.String(required=True, description='the site identifier'),
    'product_upc_xpath': fields.String(required=True,description='The product upc xpath'),
    'product_name_xpath': fields.String(required=True, description='the product name xpath'),
    'product_price_xpath': fields.String(required=True, description='the product price xpath'),
    'product_packaging_xpath': fields.String(required=True, description='the product packaging xpath'),
    'product_imageurl_xpath': fields.Boolean(required=True, description='The product imageurl xpath'),
    'webpage_parser_status': fields.String(required=True, description='The webpage parser status'),
    'product_ingredients_xpath': fields.String(required=True, description='the product ingredients xpath'),
    'product_brand_xpath': fields.String(required=True, description='The product brand xpath'),
    'product_category_xpath': fields.String(required=True, description='the product category xpath'),
    'product_subcategory1_xpath': fields.String(required=True, description='The product subcategory1 xpath'),
    'product_subcategory2_xpath': fields.String(required=True, description='The products ubcategory2 xpath '),
    'product_subcategory3_xpath': fields.String(required=True, description='The product subcategory3 xpath'),
    'parse_with_selenium': fields.Boolean(required=True, description='the parse with selenium'),
    'product_size_xpath': fields.String(required=True, description='the product price xpath'),
    'driver_id': fields.Integer(required=True, description='the product size xpath'),
    'product_dropdown1_xpath': fields.String(required=True, description='The product dropdown1 xpath'),
    'product_dropdown2_xpath': fields.String(required=True, description='The product dropdown 2xpath'),
    'product_dpci_xpath': fields.String(required=True, description='the product dpci xpath'),
    'product_desc_xpath': fields.String(required=True, description='The product desc xpath'),
    'product_serving_per_container_xpath': fields.String(required=True, description='the product serving per container xpath'),
    'product_features_xpath': fields.String(required=True, description='The product features xpath'),
    'product_serving_size_xpath': fields.String(required=True, description='the product serving size xpath'),
    'product_flavor_xpath': fields.String(required=True, description='The webpage parser identifier'),
    'product_type_xpath': fields.String(required=True, description='The product flavor xpath'),
    'product_assembled_product_dimensions_xpath': fields.String(required=True, description='The product assembled product dimensions xpath'),
    'product_model_xpath': fields.String(required=True, description='the product model xpath'),
    'product_sku_xpath': fields.String(required=True, description='the product sku xpath'),
    'product_manufacturer_part_number_xpath': fields.String(required=True, description='the product manufacturer part number xpath'),
    'product_additional_images_xpath': fields.String(required=True, description='The product additional images xpath'),
    'product_nutrition_info_xpath': fields.String(required=True, description='The product nutrition info xpath'),
    'product_customer_reviews_xpath': fields.String(required=True, description='the product customer reviews xpath'),
    'product_total_reviews_xpath': fields.String(required=True, description='The product total reviews xpath'),
    'product_rating_xpath': fields.String(required=True, description='the product rating xpath'),
    'product_feeding_instructions_xpath': fields.String(required=True, description='the product feeding instructions xpath'),
    'product_additional_sizes_xpath': fields.String(required=True, description='The product additional sizes xpath'),
    'product_rating_image_url_xpath': fields.String(required=True, description='the product rating image url xpath'),
    'product_discount_price_xpath': fields.String(required=True, description='product_discount_price_xpath'),
    'product_offer_category_xpath': fields.String(required=True, description='product_offer_category_xpath'),
    'test_product_url': fields.String(required=True, description='test product url'),
    'product_recommended_retail_price_xpath': fields.String(required=True, description='the product_recommended_retail_price xpath'),

        })

WEBPAGE_PARSER_DETAILS = [
    {   'webpage_parser_id': 'felix', 'site_id': 'Felix', 'product_upc_xpath': 'Felix', 'product_name_xpath': 'Felix'
        , 'product_price_xpath': 'Felix', 'product_packaging_xpath': 'Felix', 'product_imageurl_xpath': 'Felix','webpage_parser_status': 'Felix'
        , 'product_ingredients_xpath': 'Felix', 'product_brand_xpath': 'Felix', 'product_category_xpath': 'Felix',
        'product_subcategory1_xpath': 'felix', 'product_subcategory2_xpath': 'Felix', 'product_subcategory3_xpath': 'Felix', 'parse_with_selenium': 'Felix'
        , 'product_size_xpath': 'Felix', 'driver_id': 'Felix', 'product_dropdown1_xpath': 'Felix','product_dropdown2_xpath': 'Felix'
        , 'product_dpci_xpath': 'Felix', 'product_desc_xpath': 'Felix', 'product_serving_per_container_xpath': 'Felix',
        'product_features_xpath': 'Felix', 'product_serving_size_xpath': 'Felix', 'product_flavor_xpath': 'Felix','product_type_xpath': 'Felix'
        , 'product_assembled_product_dimensions_xpath': 'Felix', 'product_model_xpath': 'Felix', 'product_sku_xpath': 'Felix','product_manufacturer_part_number_xpath': 'Felix'
        , 'product_additional_images_xpath': 'Felix', 'product_nutrition_info_xpath': 'Felix', 'product_feeding_instructions_xpath': 'Felix','product_total_reviews_xpath': 'Felix'
        , 'product_rating_xpath': 'Felix', 'product_customer_reviews_xpath': 'Felix', 'product_additional_sizes_xpath': 'Felix','product_rating_image_url_xpath': 'Felix',
        'test_product_url': 'Felix','product_discount_price_xpath': 'Felix','product_offer_category_xpath': 'Felix','product_recommended_retail_price_xpath':'Felix',

     },
]

post_parser = reqparse.RequestParser()
post_parser.add_argument('site_id', required=True, type=int, location='form')
post_parser.add_argument('site_name', required=True, type=str, location='form')
post_parser.add_argument('product_upc_xpath',type=str, help='path',location='form')
post_parser.add_argument('product_name_xpath',  type=str, help='True',location='form')
post_parser.add_argument('product_price_xpath', type=str,help='path',location='form')
post_parser.add_argument('product_packaging_xpath', type=str, help='path', location='form')
post_parser.add_argument('product_imageurl_xpath',  type=str, help='path', location='form')
post_parser.add_argument('webpage_parser_status',  type=bool, help='True/False', location='form')
post_parser.add_argument('product_ingredients_xpath',  type=str,  help='path', location='form')
post_parser.add_argument('product_brand_xpath', type=str,  help='path', location='form')
post_parser.add_argument('product_category_xpath', type=str,help='path', location='form')
post_parser.add_argument('product_subcategory1_xpath',  help='path', location='form')
post_parser.add_argument('product_subcategory2_xpath',type=str, help='path',location='form')
post_parser.add_argument('product_subcategory3_xpath',  type=str,  help='path',location='form')
post_parser.add_argument('parse_with_selenium',  type = bool,help='True',location='form')
post_parser.add_argument('product_size_xpath',type=str,  help='path', location='form')
post_parser.add_argument('driver_id',  type=int, location='form')
post_parser.add_argument('product_dropdown1_xpath',type=str,  help='True', location='form')
post_parser.add_argument('product_dropdown2_xpath',  type=str,  help='True', location='form')
post_parser.add_argument('product_dpci_xpath', type=str,  help='path', location='form')
post_parser.add_argument('product_desc_xpath',  type=str,  help='path',location='form')
post_parser.add_argument('product_serving_per_container_xpath', type=str, help='path',location='form')
post_parser.add_argument('product_features_xpath',type=str, help='path', location='form')
post_parser.add_argument('product_serving_size_xpath',  type=str,  help='path', location='form')
post_parser.add_argument('product_flavor_xpath',  type=str,  help='path', location='form')
post_parser.add_argument('product_type_xpath',  type=str, help='path', location='form')
post_parser.add_argument('product_assembled_product_dimensions_xpath', type=str,  help='path', location='form')
post_parser.add_argument('product_model_xpath', type=str, help='path', location='form')
post_parser.add_argument('product_sku_xpath',  type=str,  help='path', location='form')
post_parser.add_argument('product_manufacturer_part_number_xpath',  type=str,  help='path', location='form')
post_parser.add_argument('product_additional_images_xpath',  type=str,  help='path', location='form')
post_parser.add_argument('product_nutrition_info_xpath',  type=str,  help='path', location='form')
post_parser.add_argument('product_feeding_instructions_xpath', type=str,  help='path', location='form')
post_parser.add_argument('product_total_reviews_xpath', type=str,  help='path', location='form')
post_parser.add_argument('product_rating_xpath', type=str,  help='path', location='form')
post_parser.add_argument('product_customer_reviews_xpath',type=str,  help='path', location='form')
post_parser.add_argument('product_additional_sizes_xpath', type=str, help='path', location='form')
post_parser.add_argument('product_rating_image_url_xpath',  type=str,  help='path', location='form')
post_parser.add_argument('product_discount_price_xpath', type=str, help='path', location='form')
post_parser.add_argument('product_offer_category_xpath',  type=str,  help='path', location='form')
post_parser.add_argument('test_product_url',  type=str,  help='path', location='form')
post_parser.add_argument('product_recommended_retail_price_xpath',  type=str,  help='path', location='form')


get_parser = reqparse.RequestParser()
get_parser.add_argument('start', type=int, help='integer value')
get_parser.add_argument('limit', type=int, help='integer value')
get_parser.add_argument('site_id',type=int, help='integer value')
get_parser.add_argument('site_name',type=str,help='string')
get_parser.add_argument('parse_with_selenium',type=str, help='string')

update_parser = reqparse.RequestParser()
update_parser.add_argument('site_id', required=True, type=int, help='site id', location='form')
update_parser.add_argument('product_upc_xpath',  type=str, help='path',location='form')
update_parser.add_argument('product_name_xpath', type=str,  help='True',location='form')
update_parser.add_argument('product_price_xpath',  type=str,  help='path',location='form')
update_parser.add_argument('product_packaging_xpath',  type=str, help='path', location='form')
update_parser.add_argument('product_imageurl_xpath', type=str, help='path', location='form')
update_parser.add_argument('webpage_parser_status',type=bool, help='True/False', location='form')
update_parser.add_argument('product_ingredients_xpath', type=str,  help='path', location='form')
update_parser.add_argument('product_brand_xpath', type=str,  help='path', location='form')
update_parser.add_argument('product_category_xpath', type=str,help='path', location='form')
update_parser.add_argument('product_subcategory1_xpath',help='path', location='form')
update_parser.add_argument('product_subcategory2_xpath',  type=str, help='path',location='form')
update_parser.add_argument('product_subcategory3_xpath',type=str,  help='path',location='form')
update_parser.add_argument('parse_with_selenium', type = bool,help='True/False',location='form')
update_parser.add_argument('product_size_xpath', type=str,  help='path', location='form')
update_parser.add_argument('driver_id',  type=int, help='driver id',location='form')
update_parser.add_argument('product_dropdown1_xpath',  type=str,  help='path', location='form')
update_parser.add_argument('product_dropdown2_xpath', type=str,  help='path', location='form')
update_parser.add_argument('product_dpci_xpath', type=str,  help='path', location='form')
update_parser.add_argument('product_desc_xpath',  type=str,  help='path',location='form')
update_parser.add_argument('product_serving_per_container_xpath',  type=str, help='path',location='form')
update_parser.add_argument('product_features_xpath', type=str, help='path', location='form')
update_parser.add_argument('product_serving_size_xpath',  type=str,  help='path', location='form')
update_parser.add_argument('product_flavor_xpath', type=str,  help='path', location='form')
update_parser.add_argument('product_type_xpath', type=str, help='path', location='form')
update_parser.add_argument('product_assembled_product_dimensions_xpath',  type=str,  help='path', location='form')
update_parser.add_argument('product_model_xpath',  type=str, help='path', location='form')
update_parser.add_argument('product_sku_xpath', type=str,  help='path', location='form')
update_parser.add_argument('product_manufacturer_part_number_xpath', type=str,  help='path', location='form')
update_parser.add_argument('product_additional_images_xpath',  type=str,  help='path', location='form')
update_parser.add_argument('product_nutrition_info_xpath', type=str,  help='path', location='form')
update_parser.add_argument('product_feeding_instructions_xpath',  type=str,  help='path', location='form')
update_parser.add_argument('product_total_reviews_xpath',  type=str,  help='path', location='form')
update_parser.add_argument('product_rating_xpath',  type=str,  help='path', location='form')
update_parser.add_argument('product_customer_reviews_xpath', type=str,  help='path', location='form')
update_parser.add_argument('product_additional_sizes_xpath',  type=str, help='path', location='form')
update_parser.add_argument('product_rating_image_url_xpath', type=str,  help='path', location='form')
update_parser.add_argument('product_discount_price_xpath', type=str, help='path', location='form')
update_parser.add_argument('product_offer_category_xpath',  type=str,  help='path', location='form')
update_parser.add_argument('test_product_url', type=str,  help='path', location='form')
update_parser.add_argument('product_recommended_retail_price_xpath', type=str,  help='path', location='form')


delete_parser = reqparse.RequestParser()
delete_parser.add_argument('site_id', type =int, location = 'form')




@api.route('/')
class Webpage(Resource):
    @api.doc('webpage_parser')
    # @api.marshal_with(sites)
    @api.expect(get_parser)
    def get(self):
        dbutils = DbUtils()
        start = int(request.args.get('start', 1))
        limit = int(request.args.get('limit', 10))
        parse_with_selenium = request.args.get('parse_with_selenium', None)
        site_id = request.args.get('site_id', None)
        site_name=request.args.get('site_name',None)

        searchQuery = {}
        # if parse_with_selenium and site_id and site_name :
        #     searchQuery['parse_with_selenium'] = parse_with_selenium if parse_with_selenium else ''
        #     searchQuery['site_id'] = site_id if site_id else ''
        #     searchQuery['site_name'] = site_name if site_name else ''

        if parse_with_selenium is not None:
            searchQuery['parse_with_selenium'] = parse_with_selenium
        if site_id is not None:
            searchQuery['site_id'] = site_id
        if site_name is not None:
            searchQuery['site_name'] = site_name

        webpage_parser_details, count = dbutils.get_webpage_parser_details(int(start) - 1, limit, searchQuery)
        dbutils.close()
        if count:
            data = get_paginated_list(webpage_parser_details, '/api/v1/webpage_parser_details', count, start, limit=limit)
            return data
        else:
            return {'response_code': 2, 'status': 'failure', 'message': 'No data found'}, 200




@api.route('/add')
class Addwebpage(Resource):
    @api.doc('post_sites')
    # @api.marshal_with(sites)
    @api.expect(post_parser)
    def post(self):
        dbutils = DbUtils()
        result = {}
        try:
                # check for the existing webpage_parser_details
                site_id = request.form['site_id']
                product_upc_xpath = request.form['product_upc_xpath'] if 'product_upc_xpath' in request.form else None
                product_name_xpath = request.form['product_name_xpath'] if 'product_name_xpath' in request.form else None
                product_price_xpath = request.form['product_price_xpath'] if 'product_price_xpath' in request.form else None
                product_packaging_xpath = request.form['product_packaging_xpath'] if 'product_packaging_xpath' in request.form else None
                product_imageurl_xpath = request.form['product_imageurl_xpath'] if 'product_imageurl_xpath' in request.form else None
                # webpage_parser_status = True if 'webpage_parser_status' in request.form else False
                webpage_parser_status = dbutils.get_status_bool(request, 'webpage_parser_status')
                product_ingredients_xpath = request.form['product_ingredients_xpath'] if 'product_ingredients_xpath' in request.form else None
                product_brand_xpath = request.form['product_brand_xpath'] if 'product_brand_xpath' in request.form else None
                product_category_xpath = request.form['product_category_xpath'] if 'product_category_xpath' in request.form else None
                product_subcategory1_xpath = request.form['product_subcategory1_xpath'] if 'product_subcategory1_xpath' in request.form else None
                product_subcategory2_xpath = request.form['product_subcategory2_xpath'] if 'product_subcategory2_xpath' in request.form else None
                product_subcategory3_xpath = request.form['product_subcategory3_xpath'] if 'product_subcategory3_xpath' in request.form else None
                # parse_with_selenium = True if 'parse_with_selenium' in request.form else False
                parse_with_selenium = dbutils.get_status_bool(request, 'parse_with_selenium')
                product_size_xpath = request.form['product_size_xpath'] if 'product_size_xpath' in request.form else None
                driver_id = request.form['driver_id'] if 'driver_id' in request.form else None
                product_dropdown1_xpath = request.form['product_dropdown1_xpath'] if 'product_dropdown1_xpath' in request.form else None
                product_dropdown2_xpath = request.form['product_dropdown2_xpath'] if 'product_dropdown2_xpath' in request.form else None
                product_dpci_xpath = request.form['product_dpci_xpath'] if 'product_dpci_xpath' in request.form else None
                product_desc_xpath = request.form['product_desc_xpath'] if 'product_desc_xpath' in request.form else None
                product_serving_per_container_xpath = request.form['product_serving_per_container_xpath'] if 'product_serving_per_container_xpath' in request.form else None
                product_features_xpath = request.form['product_features_xpath'] if 'product_features_xpath' in request.form else None
                product_serving_size_xpath = request.form['product_serving_size_xpath'] if 'product_serving_size_xpath' in request.form else None
                product_flavor_xpath = request.form['product_flavor_xpath'] if 'product_flavor_xpath' in request.form else None
                product_type_xpath = request.form['product_type_xpath'] if 'product_type_xpath' in request.form else None
                product_assembled_product_dimensions_xpath = request.form['product_assembled_product_dimensions_xpath'] if 'product_assembled_product_dimensions_xpath' in request.form else None
                product_model_xpath = request.form['product_model_xpath'] if 'product_model_xpath' in request.form else None
                product_sku_xpath = request.form['product_sku_xpath'] if 'product_sku_xpath' in request.form else None
                product_manufacturer_part_number_xpath = request.form['product_manufacturer_part_number_xpath'] if 'product_manufacturer_part_number_xpath' in request.form else None
                product_additional_images_xpath = request.form['product_additional_images_xpath'] if 'product_additional_images_xpath' in request.form else None
                product_nutrition_info_xpath = request.form['product_nutrition_info_xpath'] if 'product_nutrition_info_xpath' in request.form else None
                product_feeding_instructions_xpath = request.form['product_feeding_instructions_xpath'] if 'product_feeding_instructions_xpath' in request.form else None
                product_total_reviews_xpath = request.form['product_total_reviews_xpath'] if 'product_total_reviews_xpath' in request.form else None
                product_rating_xpath = request.form['product_rating_xpath'] if 'product_rating_xpath' in request.form else None
                product_customer_reviews_xpath = request.form['product_customer_reviews_xpath'] if 'product_customer_reviews_xpath' in request.form else None
                product_additional_sizes_xpath = request.form['product_additional_sizes_xpath'] if 'product_additional_sizes_xpath' in request.form else None
                product_rating_image_url_xpath = request.form['product_rating_image_url_xpath'] if 'product_rating_image_url_xpath' in request.form else None
                product_discount_price_xpath = request.form['product_discount_price_xpath'] if 'product_discount_price_xpath' in request.form else None
                product_offer_category_xpath = request.form['product_offer_category_xpath'] if 'product_offer_category_xpath' in request.form else None
                test_product_url = request.form['test_product_url'] if 'test_product_url' in request.form else None
                product_recommended_retail_price_xpath = request.form['product_recommended_retail_price_xpath'] if 'product_recommended_retail_price_xpath' in request.form else None

                site, site_count = dbutils.get_sites(0, 1, {'site_id': site_id})

                webpage_parser_details, count = dbutils.get_webpage_parser_details(0, 1, { 'parse_with_selenium': parse_with_selenium,'site_id': site_id,})
                if site_count>0:
                   if count > 0:
                        dbutils.close()
                        return {'response_code': 2, 'status': 'failure', 'message': 'web page parser details already exists.'}, 200
                   else:
                         result = dbutils.create_webpage_parser_details({ 'site_id': site_id,

                                              'product_upc_xpath': product_upc_xpath,
                                              'product_name_xpath': product_name_xpath,
                                              'product_price_xpath': product_price_xpath,
                                              'product_packaging_xpath': product_packaging_xpath,
                                              'product_imageurl_xpath': product_imageurl_xpath,
                                              'webpage_parser_status': webpage_parser_status,
                                              'product_ingredients_xpath':product_ingredients_xpath,
                                              'product_brand_xpath':product_brand_xpath,
                                              'product_category_xpath': product_category_xpath, 'product_subcategory1_xpath': product_subcategory1_xpath,
                                              'product_subcategory2_xpath': product_subcategory2_xpath,
                                              'product_subcategory3_xpath': product_subcategory3_xpath,
                                              'product_size_xpath': product_size_xpath,
                                              'driver_id':int(driver_id) if driver_id else 4,
                                              'parse_with_selenium': parse_with_selenium,
                                              'product_dropdown1_xpath': product_dropdown1_xpath,
                                              'product_dropdown2_xpath': product_dropdown2_xpath,
                                              'product_dpci_xpath': product_dpci_xpath,
                                              'product_desc_xpath': product_desc_xpath,
                                              'product_serving_per_container_xpath': product_serving_per_container_xpath, 'product_features_xpath': product_features_xpath,
                                              'product_serving_size_xpath': product_serving_size_xpath,
                                              'product_flavor_xpath': product_flavor_xpath,
                                              'product_type_xpath': product_type_xpath,
                                              'product_assembled_product_dimensions_xpath': product_assembled_product_dimensions_xpath,
                                              'product_model_xpath': product_model_xpath,
                                              'product_sku_xpath': product_sku_xpath,
                                              'product_manufacturer_part_number_xpath': product_manufacturer_part_number_xpath,
                                              'product_additional_images_xpath': product_additional_images_xpath,
                                              'product_nutrition_info_xpath': product_nutrition_info_xpath,
                                              'product_feeding_instructions_xpath': product_feeding_instructions_xpath,
                                              'product_total_reviews_xpath': product_total_reviews_xpath,
                                              'product_rating_xpath': product_rating_xpath,
                                              'product_customer_reviews_xpath': product_customer_reviews_xpath,
                                              'product_additional_sizes_xpath': product_additional_sizes_xpath,
                                              'product_rating_image_url_xpath': product_rating_image_url_xpath,
                                              'product_discount_price_xpath': product_discount_price_xpath,
                                               'product_offer_category_xpath': product_offer_category_xpath,
                                              'test_product_url': test_product_url,
                                                'product_recommended_retail_price_xpath': product_recommended_retail_price_xpath,
                                              })
                else:
                    dbutils.close()
                    return {'response_code': 2, 'status': 'failure',
                            'message': ' site id  does not exists kindly add.'}, 200
        # return result,200
        except Exception as e:
            print(e)
            dbutils.close()
            return {'response_code': 2, 'status': 'failure', 'message': 'unexpected error'}, 200
        else:
            dbutils.close()
            return {'response_code': 0, 'status': 'success', 'message': 'web page parser details added successfully'}, 200




@api.route('/update')
class update_webpage(Resource):
  @api.doc('update_webpage')
  @api.expect(update_parser)
  def put(self):
     dbutils = DbUtils()
     result = {}
     try:
                    # check for the existing webpage_parser_details
                    site_id = request.form['site_id']
                    product_upc_xpath = request.form['product_upc_xpath'] if 'product_upc_xpath' in request.form else None
                    product_name_xpath = request.form['product_name_xpath'] if 'product_name_xpath' in request.form else None
                    product_price_xpath = request.form['product_price_xpath'] if 'product_price_xpath' in request.form else None
                    product_packaging_xpath = request.form['product_packaging_xpath'] if 'product_packaging_xpath' in request.form else None
                    product_imageurl_xpath = request.form['product_imageurl_xpath'] if 'product_imageurl_xpath' in request.form else None
                    # webpage_parser_status = True if 'webpage_parser_status' in request.form else False
                    webpage_parser_status = dbutils.get_status_bool(request, 'webpage_parser_status')
                    product_ingredients_xpath = request.form['product_ingredients_xpath'] if 'product_ingredients_xpath' in request.form else None
                    product_brand_xpath = request.form['product_brand_xpath'] if 'product_brand_xpath' in request.form else None
                    product_category_xpath = request.form['product_category_xpath'] if 'product_category_xpath' in request.form else None
                    product_subcategory1_xpath = request.form['product_subcategory1_xpath'] if 'product_subcategory1_xpath' in request.form else None
                    product_subcategory2_xpath = request.form['product_subcategory2_xpath'] if 'product_subcategory2_xpath' in request.form else None
                    product_subcategory3_xpath = request.form['product_subcategory3_xpath'] if 'product_subcategory3_xpath' in request.form else None
                    # parse_with_selenium = True if 'parse_with_selenium' in request.form else False
                    parse_with_selenium = dbutils.get_status_bool(request, 'parse_with_selenium')
                    product_size_xpath = request.form['product_size_xpath'] if 'product_size_xpath' in request.form else None
                    driver_id = request.form['driver_id'] if 'driver_id' in request.form else None
                    product_dropdown1_xpath = request.form['product_dropdown1_xpath'] if 'product_dropdown1_xpath' in request.form else None
                    product_dropdown2_xpath = request.form['product_dropdown2_xpath'] if 'product_dropdown2_xpath' in request.form else None
                    product_dpci_xpath = request.form['product_dpci_xpath'] if 'product_dpci_xpath' in request.form else None
                    product_desc_xpath = request.form['product_desc_xpath'] if 'product_desc_xpath' in request.form else None
                    product_serving_per_container_xpath = request.form['product_serving_per_container_xpath'] if 'product_serving_per_container_xpath' in request.form else None
                    product_features_xpath = request.form['product_features_xpath'] if 'product_features_xpath' in request.form else None
                    product_serving_size_xpath = request.form['product_serving_size_xpath'] if 'product_serving_size_xpath' in request.form else None
                    product_flavor_xpath = request.form['product_flavor_xpath'] if 'product_flavor_xpath' in request.form else None
                    product_type_xpath = request.form['product_type_xpath'] if 'product_type_xpath' in request.form else None
                    product_assembled_product_dimensions_xpath = request.form['product_assembled_product_dimensions_xpath'] if 'product_assembled_product_dimensions_xpath' in request.form else None
                    product_model_xpath = request.form['product_model_xpath'] if 'product_model_xpath' in request.form else None
                    product_sku_xpath = request.form['product_sku_xpath'] if 'product_sku_xpath' in request.form else None
                    product_manufacturer_part_number_xpath = request.form['product_manufacturer_part_number_xpath'] if 'product_manufacturer_part_number_xpath' in request.form else None
                    product_additional_images_xpath = request.form['product_additional_images_xpath'] if 'product_additional_images_xpath' in request.form else None
                    product_nutrition_info_xpath = request.form['product_nutrition_info_xpath'] if 'product_nutrition_info_xpath' in request.form else None
                    product_feeding_instructions_xpath = request.form['product_feeding_instructions_xpath'] if 'product_feeding_instructions_xpath' in request.form else None
                    product_total_reviews_xpath = request.form['product_total_reviews_xpath'] if 'product_total_reviews_xpath' in request.form else None
                    product_rating_xpath = request.form['product_rating_xpath'] if 'product_rating_xpath' in request.form else None
                    product_customer_reviews_xpath = request.form['product_customer_reviews_xpath'] if 'product_customer_reviews_xpath' in request.form else None
                    product_additional_sizes_xpath = request.form['product_additional_sizes_xpath'] if 'product_additional_sizes_xpath' in request.form else None
                    product_rating_image_url_xpath = request.form['product_rating_image_url_xpath'] if 'product_rating_image_url_xpath' in request.form else None
                    product_discount_price_xpath = request.form['product_discount_price_xpath'] if 'product_discount_price_xpath' in request.form else None
                    product_offer_category_xpath = request.form['product_offer_category_xpath'] if 'product_offer_category_xpath' in request.form else None
                    test_product_url = request.form['test_product_url'] if 'test_product_url' in request.form else None
                    product_recommended_retail_price_xpath = request.form['product_recommended_retail_price_xpath'] if 'product_recommended_retail_price_xpath' in request.form else None

                    webpage, count = dbutils.get_webpage_parser_details(0, 1, {'site_id': site_id})

                    if count > 0:
                        result = dbutils.update_webpage_parser_details(
                            {'site_id': site_id,
                             'product_upc_xpath': product_upc_xpath,
                             'product_name_xpath': product_name_xpath,
                             'product_price_xpath': product_price_xpath,
                             'product_packaging_xpath': product_packaging_xpath,
                             'product_imageurl_xpath': product_imageurl_xpath,
                             'webpage_parser_status': webpage_parser_status,
                             'product_ingredients_xpath': product_ingredients_xpath,
                             'product_brand_xpath': product_brand_xpath,
                             'product_category_xpath': product_category_xpath,
                             'product_subcategory1_xpath': product_subcategory1_xpath,
                             'product_subcategory2_xpath': product_subcategory2_xpath,
                             'product_subcategory3_xpath': product_subcategory3_xpath,
                             'parse_with_selenium': parse_with_selenium,
                             'product_size_xpath': product_size_xpath,
                             'driver_id': int(driver_id) if driver_id else 4,
                             'product_dropdown1_xpath': product_dropdown1_xpath,
                             'product_dropdown2_xpath': product_dropdown2_xpath,
                             'product_dpci_xpath': product_dpci_xpath,
                             'product_desc_xpath': product_desc_xpath,
                             'product_serving_per_container_xpath': product_serving_per_container_xpath,
                             'product_features_xpath': product_features_xpath,
                             'product_serving_size_xpath': product_serving_size_xpath,
                             'product_flavor_xpath': product_flavor_xpath,
                             'product_type_xpath': product_type_xpath,
                             'product_assembled_product_dimensions_xpath': product_assembled_product_dimensions_xpath,
                             'product_model_xpath': product_model_xpath,
                             'product_sku_xpath': product_sku_xpath,
                             'product_manufacturer_part_number_xpath': product_manufacturer_part_number_xpath,
                             'product_additional_images_xpath': product_additional_images_xpath,
                             'product_nutrition_info_xpath': product_nutrition_info_xpath,
                             'product_feeding_instructions_xpath': product_feeding_instructions_xpath,
                             'product_total_reviews_xpath': product_total_reviews_xpath,
                             'product_rating_xpath': product_rating_xpath,
                             'product_customer_reviews_xpath': product_customer_reviews_xpath,
                             'product_additional_sizes_xpath': product_additional_sizes_xpath,
                             'product_rating_image_url_xpath': product_rating_image_url_xpath,
                             'product_discount_price_xpath': product_discount_price_xpath,
                             'product_offer_category_xpath': product_offer_category_xpath,
                             'test_product_url': test_product_url,
                             'product_recommended_retail_price_xpath': product_recommended_retail_price_xpath,
                             })
                    else:
                      dbutils.close()
                      return {'response_code': 2, 'status': 'failure', 'message': 'Site doesnt exist please add .'}, 200

     # return result,200
     except Exception as e:
         print(e)
         dbutils.close()
         return {'response_code': 2, 'status': 'failure', 'message': 'unexpected error'}, 200

     else:
         dbutils.close()
         return {'response_code': 0, 'status': 'success',
                 'message': 'webpage parser details updated successfully'}, 200

@api.route('/delete')
class Delete_webpage(Resource):
    @api.doc('delete_webpage')
    @api.expect(delete_parser)
    def delete(self):
        try:

            dbutils = DbUtils()

            site_id = request.form['site_id']
            site, count = dbutils.get_sites(0, 1, {'site_id': site_id})

            if count > 0:
                dbutils.delete_webpage_parser_details(site_id)
            else:
                dbutils.close()
                return {'response_code': 2, 'status': 'failure', 'message': 'Site doesnt exist.'}, 200

        except Exception as e:
            print(e)
            dbutils.close()
            return {'response_code': 2, 'status': 'failure', 'message': 'unexpected error'}, 200
        else:
            dbutils.close()
            return {'response_code': 0, 'status': 'success', 'message': 'Deleted webpage parser details  successfully'}, 200
