import logging
import os
from logging import handlers
import sys
import settings
# The below properties can be changed to contol the behaviour of logger.

file_formatter = logging.Formatter('%(asctime)s - %(filename)s -  %(funcName)s - %(lineno)i - %(levelname)s - %(message)s')
console_formatter = logging.Formatter('%(asctime)s - %(filename)s -  %(funcName)s - %(lineno)i - %(levelname)s - %(message)s')
file_log_level = logging.DEBUG
console_log_level = logging.INFO
log_to_file=True
log_to_console=False
root_logging_level=logging.DEBUG

fileNamePrefix = settings.ROOT_DIR + '/logs/scraper'
fileNameExtension = '.log'
fileMaxBytes = 10485760
backUpCount = 20
encoding = 'utf8'


console_stream=sys.stdout

logger_map={}


'''
For multiprocessing methods call the get_logger method with a jobId at the begining of the worker method.
this would create a new logger for the process with file name appended with job id.
'''
		
def get_logger(jobId=None):
	
	pid=os.getpid()
	
	if pid and pid in logger_map:
		return logger_map[pid]
	else:	
		logName=''
		if not jobId:
			logName='_main'
		else:
			logName='_'+str(jobId)
			
		logger = logging.getLogger(logName)
		
		if log_to_file:
			filehandler = handlers.RotatingFileHandler(fileNamePrefix+logName+fileNameExtension,maxBytes=fileMaxBytes,backupCount=backUpCount,encoding=encoding)        
			filehandler.setFormatter(file_formatter)
			filehandler.setLevel(file_log_level)
			logger.addHandler(filehandler)
		if log_to_console:
			consolehandler=logging.StreamHandler(stream=console_stream)
			consolehandler.setFormatter(console_formatter)
			consolehandler.setLevel(console_log_level)
			logger.addHandler(consolehandler)
		
		logger.setLevel(root_logging_level)
		
		logger_map[pid]=logger
		
		return logger
	
