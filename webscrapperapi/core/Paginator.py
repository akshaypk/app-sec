from flask import abort

def get_paginated_list(results, url,count, start, limit):
    # check if page exists
    #results = klass.query.all()
    
    if (int(count) < int(start)):
        abort(404)
    # make response
    obj = {}
    obj['start'] = start
    obj['limit'] = limit
    obj['count'] = count
    # make URLs
    # make previous url
    if int(start) == 1:
        obj['previous'] = ''
    else:
        start_copy = max(1, int(start) - int(limit))
        limit_copy = int(start) - 1
        obj['previous'] = url + '?start=%d&limit=%d' % (int(start_copy), int(limit_copy))
    # make next url
    if int(start) + int(limit) > int(count):
        obj['next'] = ''
    else:
        start_copy = int(start) + int(limit)
        obj['next'] = url + '?start=%d&limit=%d' % (int(start_copy), int(limit))
    # finally extract result according to bounds
    #obj['results'] = results[(int(start) - 1):(int(start) - 1 + int(limit))]
    obj['results'] = results
    return obj