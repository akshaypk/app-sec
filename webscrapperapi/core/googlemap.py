import sys
import requests
import json
import time
import hashlib
try:
    from core import settings
except:
    import settings

class GoogleMapRest(object):

    def __init__(self,):

        self.APIKEY = settings.API_KEY
        self.url_format=settings.URL_FORMAT_PLACE
        #self.url_format1="https://maps.googleapis.com/maps/api/place/nearbysearch/json?{query}={SearchString}&key={APIKEY}{pagetoken}{location}{radius}{type}"
        self.url_place_details=settings.URL_PLACE_DETAILS
        self.stored_hash=[]
        self.get_stored_hash()
        self.result=[]

    def get_stored_hash(self,):
        self.stored_hash=[]#get already processed data in DB to avoid duplicate entry

    def close(self,):
        pass

    def get_response(self,url):
        headers = {
        'Content-Type': 'application/json',
        'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36',
        }
        try:
            response = requests.get(url,headers=headers)
            res = json.loads(response.text)
            return res
        except Exception as e:
            return {}

    def get_places(self,query="",pagetoken=None,loc=None,radius=None,type="restaurant"):

        item={}
        if loc is not None and radius is not None:
            lat, lng = loc
            url=self.url_format.format(location="&location={lat},{lng}".format(lat=lat,lng=lng),
                                       radius="&radius={rad}".format(rad=radius),
                                       type="&type={t}".format(t=type),
                                       APIKEY=self.APIKEY,
                                       SearchString=query,
                                       query="keyword",
                                       pagetoken="&pagetoken=" + pagetoken if pagetoken else "")
        else:
            url=self.url_format.format(location="",
                                       radius="",
                                       type="",
                                       APIKEY=self.APIKEY,
                                       SearchString=query,
                                       query="query",
                                       pagetoken="&pagetoken=" + pagetoken if pagetoken else "")

        print (url)
        res=self.get_response(url)
        pagetoken = res.get("next_page_token", None)

        if res is not None and 'results' in res:
            for result in res["results"]:
                place_url = self.url_place_details.format (APIKEY=self.APIKEY,placeid = result["place_id"])
                place_res=self.get_response(place_url)
                time.sleep(1)
                place_details=place_res.get('result', {})

                print (place_url)
                item={}
                item["name"] = result.get('name')
                item["formatted_address"] = result.get('formatted_address')
                item["compound_code"] = result["plus_code"]["compound_code"] if 'plus_code' in result and 'compound_code' in result["plus_code"] else ""
                item["global_code"] = result["plus_code"]["global_code"] if 'plus_code' in result and 'global_code' in result["plus_code"] else ""
                item["place_id"] = result.get('place_id')
                geometry=result.get('geometry',{})
                location=geometry.get('location',{})
                item["lat"] = location.get('lat', {})
                item["lng"] = location.get('lng', {})
                item["phone"] = place_details.get('formatted_phone_number')
                item["website"] = place_details.get('website')
                item["url"] = place_details.get('url')
                #print (item)

                self.store_item(item)

        return pagetoken

    def createHashKey(self,fields):

        fieldList = ""
        key = ""
        #print('Create hash key.')
        try:
            key = hashlib.md5(fields.encode('utf-8')).hexdigest()
            return key
        except Exception as e:
            print (str(e))
            return None

    def store_item(self,item):
        # check whether location details already stored or not
        # Store location details in DB
        phone=item.get("phone",'')
        name=item.get("name",'')
        formatted_address=item.get("formatted_address",'')

        fields=[phone,name,formatted_address]

        joined_text=''.join(str(field) for field in fields)
        hash_key=self.createHashKey(joined_text)
        item['hash_key']=hash_key

        self.result.append(item)

        #self.write_as_csv(item)

    def write_as_csv(self,item):
        str_out=''
        for x in ['name', 'formatted_address', 'compound_code', 'global_code', 'place_id', 'phone', 'website', 'url', 'hash_key']:
            str_out +='^'+str(item[x])
        str_out=str_out.lstrip('^')

        with open('result.csv','a') as f:
            f.write(str_out)
            f.write('\n')

    def start_crawl(self,input=None):
        if input:
            #pagetoken=None
            while True:
                #input={"query":'storelocator+ril',"pagetoken":pagetoken,'loc':(12.9716,77.5946),'radius':1000}
                pagetoken=self.get_places(**input)
                input['pagetoken']=pagetoken
                time.sleep(2)
                #break
                if not pagetoken or pagetoken is None:
                    break
            return self.result
        else:
            print ("Please send query string")
            return [{"message":"Please enter text"}]


if __name__ == "__main__":
    pagetoken = None
    result = {}
    text='storelocator+ril+near+bangalore'
    if text is not None:
        print(text)
        input = {"query": text, "pagetoken": pagetoken}
        obj_init = GoogleMapRest()
        result = obj_init.start_crawl(input)
        #print (result)
        del obj_init




