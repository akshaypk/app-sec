import requests
from lxml import html
import re
import json
from jsonpath_rw import parse


class InstagramSearch(object):

    def __init__(self):
        self.url = "https://www.instagram.com/explore/tags/{tag_name}/"

    def process_request(self, tag_name):
        url = self.url.format(tag_name=tag_name)
        r = requests.get(url)
        html_data = r.content
        return self.parse_info(html_data)

    def parse_info(self, html_data):
        try:
            tree = html.fromstring(html_data)
            result = []
            for item in tree.xpath('//script[contains(.,"window._sharedData =")]//text()'):
                data = re.sub(r"window._sharedData = (\{.*\}).*", r"\1", item)
                json_info = json.loads(data)
                data = parse("$..edge_hashtag_to_media.edges[*].node").find(json_info)
                for text in data:
                    item_data = text.value
                    # print(item_data)

                    dict_paths = {"edge_media_to_comment": "$.edge_media_to_comment.count",
                                  "edge_liked_by": "$.edge_liked_by.count",
                                  "edge_media_preview_like": "$.edge_media_preview_like.count", "owner": "$.owner.id",
                                  "thumbnail_src": "$.thumbnail_src",
                                  "accessibility_caption": "$.accessibility_caption", "shortcode": "$.shortcode",
                                  "id": "$.id", "edge_media_to_caption": "$.edge_media_to_caption..text"}
                    temp = {}
                    for key, path in dict_paths.items():
                        # print(path)
                        item = parse(path).find(item_data)
                        for xx in item:
                            # print(xx.value)
                            temp[key] = xx.value
                    result.append(temp)
            return result
        except Exception as e:
            print(str(e))
            return []


if __name__ == "__main__":
    # window._sharedData
    tag_name = "nature"
    init = InstagramSearch()
    result = init.process_request(tag_name)
    for item in result:
        try:
            print(item)
        except:
            pass

