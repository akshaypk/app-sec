from sqlalchemy import *
from sqlalchemy.orm import relationship,sessionmaker
from sqlalchemy.ext.declarative import declarative_base
#from sqlalchemy.dialects.postgres import *
import json
engine = create_engine('postgresql+psycopg2://postgres:sasa123@localhost:5432/scrappertest')
Session = sessionmaker(bind=engine)
session = Session()


Base = declarative_base()


class ImportProductsOld(Base):
    __tablename__ = u'import_products_old'

    # column definitions
    product_id = Column(INTEGER(), Sequence('import_products_product_id_seq'),primary_key=True, nullable=False)
    period_code = Column(VARCHAR(length=25))
    prod_id = Column(VARCHAR(length=25))
    source_id = Column(INTEGER())
    category_level1 = Column(TEXT())
    category_level2 = Column(TEXT())
    category_level3 = Column(TEXT())
    category_level4 = Column(TEXT())
    brand = Column(TEXT())
    product_desc_raw = Column(TEXT())
    oversea = Column(VARCHAR(length=50))
    acn_item_code = Column(VARCHAR(length=50))
    irh = Column(VARCHAR(length=50))
    product_url = Column(VARCHAR(length=500))
    category = Column(VARCHAR(length=500))
    manu_desc = Column(TEXT())
    brand_desc = Column(TEXT())
    scrap_status = Column(BOOLEAN(), server_default=text('false'))
    scrap_source = Column(VARCHAR(length=50))
    product_mapid = Column(INTEGER())
    manu = Column(TEXT())
    scope = Column(BOOLEAN(), server_default=text('true'))
    file_name = Column(TEXT())

    def to_json(self):
        obj = {
            'product_id': self.product_id,
            'period_code': self.period_code,
            'prod_id': self.prod_id,
            'source_id': self.source_id,
            'category_level1': self.category_level1,
            'category_level2': self.category_level2,
            'category_level3': self.category_level3,
            'category_level4': self.category_level4,
            'brand': self.brand,
            'product_desc_raw': self.product_desc_raw,
            'oversea': self.oversea,
            'acn_item_code': self.acn_item_code,
            'irh': self.irh,
            'product_url': self.product_url,
            'category': self.category,
            'manu_desc': self.manu_desc,
            'brand_desc': self.brand_desc,
            'scrap_status': self.scrap_status,
            'scrap_source': self.scrap_source,
            'product_mapid': self.product_mapid,
            'manu': self.manu,
            'scope': self.scope,
            'file_name': self.file_name,
        }
        return json.dumps(obj)


class Country(Base):
    __tablename__ = u'Country'

    # column definitions
    CountryId = Column(INTEGER(), Sequence('"Country_CountryId_seq"'), primary_key=True, nullable=False)
    CountryName = Column(VARCHAR(length=-5))
    CountryCode = Column(VARCHAR(length=-5))

    def to_json(self):
        obj = {
            'CountryId': self.CountryId,
            'CountryName': self.CountryName,
            'CountryCode': self.CountryCode,
        }
        return json.dumps(obj)


class ReceivabilityToolConfig(Base):
    __tablename__ = u'receivability_tool_config'

    # column definitions
    domain = Column(TEXT(), primary_key=True, nullable=False)
    main_method = Column(TEXT())
    channel_name_xpath = Column(TEXT())
    channel_number_xpath = Column(TEXT())
    structure = Column(TEXT())
    callback = Column(TEXT())
    scroll_function = Column(TEXT())
    scroll_reset_function = Column(TEXT())
    load_url = Column(TEXT())
    channel_name = Column(TEXT())
    sort = Column(TEXT())

    def to_json(self):
        obj = {
            'domain': self.domain,
            'main_method': self.main_method,
            'channel_name_xpath': self.channel_name_xpath,
            'channel_number_xpath': self.channel_number_xpath,
            'structure': self.structure,
            'callback': self.callback,
            'scroll_function': self.scroll_function,
            'scroll_reset_function': self.scroll_reset_function,
            'load_url': self.load_url,
            'channel_name': self.channel_name,
            'sort': self.sort,
        }
        return json.dumps(obj)


class SeleniumDrivers(Base):
    __tablename__ = u'selenium_drivers'

    # column definitions
    id = Column(INTEGER(), Sequence('selenium_drivers_id_seq'), primary_key=True, nullable=False)
    driver_name = Column(VARCHAR(length=100))
    executable_path = Column(VARCHAR(length=500))
    additional_args = Column(VARCHAR(length=500))
    pgLoadTimeOut = Column(INTEGER(), server_default=text('120'), nullable=False)
    user_agent = Column(TEXT())

    def to_json(self):
        obj = {
            'id': self.id,
            'driver_name': self.driver_name,
            'executable_path': self.executable_path,
            'additional_args': self.additional_args,
            'pgLoadTimeOut': self.pgLoadTimeOut,
            'user-agent': self.user-agent,
        }
        return json.dumps(obj)


class SitemapParserDetails(Base):
    __tablename__ = u'sitemap_parser_details'

    # column definitions
    sitemap_parser_id = Column(INTEGER(), Sequence('sitemap_parser_details_sitemap_parser_id_seq'), primary_key=True, nullable=False)
    site_id = Column(INTEGER())
    sitemap_url = Column(VARCHAR(length=200))
    sitemap_product_tag = Column(VARCHAR(length=30))
    sitemap_product_url_pattern = Column(VARCHAR(length=300))
    sitemap_parser_status = Column(BOOLEAN(), server_default=text('true'))
    sitemap_xml_namespace = Column(VARCHAR(length=100))
    sitemap_parser_pattern = Column(VARCHAR(length=500))
    sitemap_parser_zipped = Column(BOOLEAN(), server_default=text('false'))

    def to_json(self):
        obj = {
            'sitemap_parser_id': self.sitemap_parser_id,
            'site_id': self.site_id,
            'sitemap_url': self.sitemap_url,
            'sitemap_product_tag': self.sitemap_product_tag,
            'sitemap_product_url_pattern': self.sitemap_product_url_pattern,
            'sitemap_parser_status': self.sitemap_parser_status,
            'sitemap_xml_namespace': self.sitemap_xml_namespace,
            'sitemap_parser_pattern': self.sitemap_parser_pattern,
            'sitemap_parser_zipped': self.sitemap_parser_zipped,
        }
        return json.dumps(obj)


class Sites(Base):
    __tablename__ = u'sites'

    # column definitions
    site_id = Column(INTEGER(), Sequence('sites_site_id_seq'), primary_key=True, nullable=False)
    site_name = Column(VARCHAR(length=35))
    site_url = Column(VARCHAR(length=350))
    require_sitemap_parsing = Column(BOOLEAN(), server_default=text('true'))
    require_webpage_parsing = Column(BOOLEAN(), server_default=text('true'))
    require_dynamic_scrapping = Column(BOOLEAN(), server_default=text('false'))
    parse_status = Column(BOOLEAN(), server_default=text('false'))
    site_status = Column(BOOLEAN(), server_default=text('true'))
    image_download = Column(BOOLEAN(), server_default=text('false'))

    def to_json(self):
        obj = {
            'site_id': self.site_id,
            'site_name': self.site_name,
            'site_url': self.site_url,
            'require_sitemap_parsing': self.require_sitemap_parsing,
            'require_webpage_parsing': self.require_webpage_parsing,
            'require_dynamic_scrapping': self.require_dynamic_scrapping,
            'parse_status': self.parse_status,
            'site_status': self.site_status,
            'image_download': self.image_download,
        }
        return json.dumps(obj)


class WebpageParserDetails(Base):
    __tablename__ = u'webpage_parser_details'

    # column definitions
    webpage_parser_id = Column(INTEGER(), Sequence('webpage_parser_details_webpage_parser_id_seq'), primary_key=True, nullable=False)
    site_id = Column(INTEGER())
    product_upc_xpath = Column(VARCHAR(length=-5))
    product_name_xpath = Column(VARCHAR(length=-5))
    product_price_xpath = Column(VARCHAR(length=-5))
    product_packaging_xpath = Column(VARCHAR(length=-5))
    product_imageurl_xpath = Column(VARCHAR(length=-5))
    webpage_parser_status = Column(BOOLEAN(), server_default=text('true'))
    product_ingredients_xpath = Column(VARCHAR(length=-5))
    product_brand_xpath = Column(VARCHAR(length=-5))
    product_category_xpath = Column(VARCHAR(length=-5))
    product_subcategory1_xpath = Column(VARCHAR(length=-5))
    product_subcategory2_xpath = Column(VARCHAR(length=-5))
    product_subcategory3_xpath = Column(VARCHAR(length=-5))
    parse_with_selenium = Column(BOOLEAN(), server_default=text('false'))
    product_size_xpath = Column(TEXT())
    driver_id = Column(INTEGER())
    product_dropdown1_xpath = Column(TEXT())
    product_dropdown2_xpath = Column(TEXT())
    product_dpci_xpath = Column(TEXT())
    product_desc_xpath = Column(TEXT())
    product_serving_per_container_xpath = Column(TEXT())
    product_features_xpath = Column(TEXT())
    product_serving_size_xpath = Column(TEXT())
    product_flavor_xpath = Column(TEXT())
    product_type_xpath = Column(TEXT())
    product_assembled_product_dimensions_xpath = Column(TEXT())
    product_model_xpath = Column(TEXT())
    product_sku_xpath = Column(TEXT())
    product_manufacturer_part_number_xpath = Column(TEXT())
    product_additional_images_xpath = Column(TEXT())
    product_nutrition_info_xpath = Column(TEXT())
    product_feeding_instructions_xpath = Column(TEXT())
    product_total_reviews_xpath = Column(TEXT())
    product_rating_xpath = Column(TEXT())
    product_customer_reviews_xpath = Column(TEXT())
    product_additional_sizes_xpath = Column(TEXT())
    product_rating_image_url_xpath = Column(TEXT())

    def to_json(self):
        obj = {
            'webpage_parser_id': self.webpage_parser_id,
            'site_id': self.site_id,
            'product_upc_xpath': self.product_upc_xpath,
            'product_name_xpath': self.product_name_xpath,
            'product_price_xpath': self.product_price_xpath,
            'product_packaging_xpath': self.product_packaging_xpath,
            'product_imageurl_xpath': self.product_imageurl_xpath,
            'webpage_parser_status': self.webpage_parser_status,
            'product_ingredients_xpath': self.product_ingredients_xpath,
            'product_brand_xpath': self.product_brand_xpath,
            'product_category_xpath': self.product_category_xpath,
            'product_subcategory1_xpath': self.product_subcategory1_xpath,
            'product_subcategory2_xpath': self.product_subcategory2_xpath,
            'product_subcategory3_xpath': self.product_subcategory3_xpath,
            'parse_with_selenium': self.parse_with_selenium,
            'product_size_xpath': self.product_size_xpath,
            'driver_id': self.driver_id,
            'product_dropdown1_xpath': self.product_dropdown1_xpath,
            'product_dropdown2_xpath': self.product_dropdown2_xpath,
            'product_dpci_xpath': self.product_dpci_xpath,
            'product_desc_xpath': self.product_desc_xpath,
            'product_serving_per_container_xpath': self.product_serving_per_container_xpath,
            'product_features_xpath': self.product_features_xpath,
            'product_serving_size_xpath': self.product_serving_size_xpath,
            'product_flavor_xpath': self.product_flavor_xpath,
            'product_type_xpath': self.product_type_xpath,
            'product_assembled_product_dimensions_xpath': self.product_assembled_product_dimensions_xpath,
            'product_model_xpath': self.product_model_xpath,
            'product_sku_xpath': self.product_sku_xpath,
            'product_manufacturer_part_number_xpath': self.product_manufacturer_part_number_xpath,
            'product_additional_images_xpath': self.product_additional_images_xpath,
            'product_nutrition_info_xpath': self.product_nutrition_info_xpath,
            'product_feeding_instructions_xpath': self.product_feeding_instructions_xpath,
            'product_total_reviews_xpath': self.product_total_reviews_xpath,
            'product_rating_xpath': self.product_rating_xpath,
            'product_customer_reviews_xpath': self.product_customer_reviews_xpath,
            'product_additional_sizes_xpath': self.product_additional_sizes_xpath,
            'product_rating_image_url_xpath': self.product_rating_image_url_xpath,
        }
        return json.dumps(obj)


class CsvTemplate(Base):
    __tablename__ = u'csv_template'

    # column definitions
    id = Column(INTEGER(), Sequence('"Csv_template_Id_seq"'), primary_key=True, nullable=False)
    site_id = Column(INTEGER(), nullable=False)
    product_url_column_in_csv = Column(INTEGER())
    search_columns_in_csv = Column(VARCHAR(length=100))
    search_sites_id = Column(VARCHAR(length=100))
    filter_column = Column(INTEGER())
    filter_value = Column(VARCHAR(length=100))
    CreationDate = Column(TIMESTAMP(), server_default=text('now()'))
    product_upc_column = Column(INTEGER())

    def to_json(self):
        obj = {
            'id': self.id,
            'site_id': self.site_id,
            'product_url_column_in_csv': self.product_url_column_in_csv,
            'search_columns_in_csv': self.search_columns_in_csv,
            'search_sites_id': self.search_sites_id,
            'filter_column': self.filter_column,
            'filter_value': self.filter_value,
            'CreationDate': self.CreationDate.strftime('%a, %d %b %Y %H:%M:%S +0000') if self.CreationDate else None,
            'product_upc_column': self.product_upc_column,
        }
        return json.dumps(obj)


class Project(Base):
    __tablename__ = u'Project'

    # column definitions
    ProjectId = Column(INTEGER(), Sequence('"Project_ProjectId_seq"'), primary_key=True, nullable=False)
    ProjectName = Column(VARCHAR(length=-5))
    CountryId = Column(INTEGER())

    def to_json(self):
        obj = {
            'ProjectId': self.ProjectId,
            'ProjectName': self.ProjectName,
            'CountryId': self.CountryId,
        }
        return json.dumps(obj)


class RegistrationPlatformUser(Base):
    __tablename__ = u'registration_platform_user'

    # column definitions
    user_id = Column(INTEGER(), Sequence('"RegistrationPlatformUser_UserId_seq"'), primary_key=True, nullable=False)
    project_id = Column(INTEGER())
    full_name = Column(VARCHAR(length=255))
    user_name = Column(VARCHAR(length=255))
    password_hash = Column(VARCHAR(length=255))
    session_token = Column(VARCHAR(length=255))
    last_login = Column(TIMESTAMP())
    creation_timestamp = Column(TIMESTAMP(), server_default=text('now()'))
    active = Column(BOOLEAN(), server_default=text('false'), nullable=False)
    subscription_allowed = Column(BOOLEAN(), server_default=text('false'), nullable=False)
    unsubscription_allowed = Column(BOOLEAN(), server_default=text('false'), nullable=False)
    update_allowed = Column(BOOLEAN(), server_default=text('false'), nullable=False)
    hh_assignment_allowed = Column(BOOLEAN(), server_default=text('true'), nullable=False)
    wrong_authentication_attempts = Column(INTEGER(), server_default=text('0'))
    blocked = Column(BOOLEAN(), server_default=text('false'))

    def to_json(self):
        obj = {
            'user_id': self.user_id,
            'project_id': self.project_id,
            'full_name': self.full_name,
            'user_name': self.user_name,
            'password_hash': self.password_hash,
            'session_token': self.session_token,
            'last_login': self.last_login.strftime('%a, %d %b %Y %H:%M:%S +0000') if self.last_login else None,
            'creation_timestamp': self.creation_timestamp.strftime('%a, %d %b %Y %H:%M:%S +0000') if self.creation_timestamp else None,
            'active': self.active,
            'subscription_allowed': self.subscription_allowed,
            'unsubscription_allowed': self.unsubscription_allowed,
            'update_allowed': self.update_allowed,
            'hh_assignment_allowed': self.hh_assignment_allowed,
            'wrong_authentication_attempts': self.wrong_authentication_attempts,
            'blocked': self.blocked,
        }
        return json.dumps(obj)


class SeleniumActions(Base):
    __tablename__ = u'selenium_actions'

    # column definitions
    id = Column(INTEGER(), Sequence('"Selenium_Actions_id_seq"'), primary_key=True,nullable=False)
    action = Column(VARCHAR(length=50))

    def to_json(self):
        obj = {
            'id': self.id,
            'action': self.action,
        }
        return json.dumps(obj)


class CategoryMapping(Base):
    __tablename__ = u'category_mapping'

    # column definitions
    id = Column(INTEGER(),primary_key=True,nullable=False)
    category = Column(VARCHAR(length=-5))
    category1 = Column(VARCHAR(length=-5))
    category2 = Column(VARCHAR(length=-5))
    category3 = Column(VARCHAR(length=-5))
    category4 = Column(VARCHAR(length=-5))

    def to_json(self):
        obj = {
            'category': self.category,
            'category1': self.category1,
            'category2': self.category2,
            'category3': self.category3,
            'category4': self.category4,
        }
        return json.dumps(obj)


class CategoryScope(Base):
    __tablename__ = u'category_scope'

    # column definitions
    category = Column(VARCHAR(length=-5), primary_key=True, nullable=False)
    scope = Column(BOOLEAN(), server_default=text('true'))
    project_id = Column(INTEGER(), nullable=False)

    def to_json(self):
        obj = {
            'category': self.category,
            'scope': self.scope,
            'project_id': self.project_id,
        }
        return json.dumps(obj)


class DynamicScrapperDetails(Base):
    __tablename__ = u'dynamic_scrapper_details'

    # column definitions
    dynamic_scrapper_id = Column(INTEGER(), Sequence('dynamic_scrapper_details_dynamic_scrapper_id_seq'), primary_key=True, nullable=False)
    site_id = Column(INTEGER())
    base_search_url = Column(VARCHAR(length=300))
    search_results_xpath = Column(TEXT())
    search_result_accuracy = Column(INTEGER(), server_default=text('1'))
    dynamic_scrapper_status = Column(BOOLEAN(), server_default=text('true'))
    search_result_type = Column(TEXT(), server_default=text('true'))
    search_result_json_xpath = Column(TEXT())
    parse_with_selenium = Column(BOOLEAN(), server_default=text('false'))
    append_srchtxt_to_url = Column(BOOLEAN(), server_default=text('true'))
    driver_id = Column(INTEGER())

    def to_json(self):
        obj = {
            'dynamic_scrapper_id': self.dynamic_scrapper_id,
            'site_id': self.site_id,
            'base_search_url': self.base_search_url,
            'search_results_xpath': self.search_results_xpath,
            'search_result_accuracy': self.search_result_accuracy,
            'dynamic_scrapper_status': self.dynamic_scrapper_status,
            'search_result_type': self.search_result_type,
            'search_result_json_xpath': self.search_result_json_xpath,
            'parse_with_selenium': self.parse_with_selenium,
            'append_srchtxt_to_url': self.append_srchtxt_to_url,
            'driver_id': self.driver_id,
        }
        return json.dumps(obj)


class ImportProducts(Base):
    __tablename__ = u'import_products'

    # column definitions
    id = Column(INTEGER(), Sequence('import_products_product_id_seq'),primary_key=True, nullable=False)
    period_code = Column(VARCHAR(length=25))
    product_upc = Column(VARCHAR(length=25))
    source_id = Column(INTEGER())
    product_category = Column(TEXT())
    product_subcategory1 = Column(TEXT())
    product_subcategory2 = Column(TEXT())
    product_subcategory3 = Column(TEXT())
    product_brand = Column(TEXT())
    product_name = Column(TEXT())
    product_url = Column(VARCHAR(length=500))
    scrap_status = Column(BOOLEAN(), server_default=text('false'))
    scrap_source = Column(VARCHAR(length=50))
    product_mapid = Column(INTEGER())
    product_manufacturer_part_number = Column(TEXT())
    scope = Column(BOOLEAN())
    file_name = Column(TEXT())
    project_id = Column(INTEGER())
    job_id = Column(INTEGER())
    update_time = Column(TIMESTAMP(), server_default=text('now()'))

    def to_json(self):
        obj = {
            'id': self.id,
            'period_code': self.period_code,
            'product_upc': self.product_upc,
            'source_id': self.source_id,
            'product_category': self.product_category,
            'product_subcategory1': self.product_subcategory1,
            'product_subcategory2': self.product_subcategory2,
            'product_subcategory3': self.product_subcategory3,
            'product_brand': self.product_brand,
            'product_name': self.product_name,
            'product_url': self.product_url,
            'scrap_status': self.scrap_status,
            'scrap_source': self.scrap_source,
            'product_mapid': self.product_mapid,
            'product_manufacturer_part_number': self.product_manufacturer_part_number,
            'scope': self.scope,
            'file_name': self.file_name,
            'project_id': self.project_id,
            'job_id': self.job_id,
            'update_time': self.update_time.strftime('%a, %d %b %Y %H:%M:%S +0000') if self.update_time else None,
        }
        return json.dumps(obj)


class LxmlActions(Base):
    __tablename__ = u'lxml_actions'

    # column definitions
    id = Column(INTEGER(), Sequence('"Selenium_Actions_id_seq"'), primary_key=True,nullable=False)
    action = Column(VARCHAR(length=50))

    def to_json(self):
        obj = {
            'id': self.id,
            'action': self.action,
        }
        return json.dumps(obj)


class NielsenCategories(Base):
    __tablename__ = u'nielsen_categories'

    # column definitions
    id = Column(INTEGER(),primary_key=True,nullable = False)
    category = Column(VARCHAR(length=500))
    project_id = Column(INTEGER())

    def to_json(self):
        obj = {
            'category': self.category,
            'project_id': self.project_id,
        }
        return json.dumps(obj)


class Products(Base):
    __tablename__ = u'products'

    # column definitions
    product_id = Column(INTEGER(), Sequence('products_product_id_seq'), primary_key=True, nullable=False)
    product_upc = Column(TEXT())
    product_name = Column(TEXT())
    product_price = Column(TEXT())
    site_id = Column(INTEGER())
    product_url = Column(TEXT())
    product_ingredients = Column(TEXT())
    product_brand = Column(TEXT())
    product_category = Column(TEXT())
    product_subcategory1 = Column(TEXT())
    product_subcategory2 = Column(TEXT())
    product_imageurl = Column(TEXT())
    product_subcategory3 = Column(TEXT())
    timestamp = Column(TIMESTAMP(), server_default=text('now()'))
    product_size = Column(TEXT())
    product_dropdown1 = Column(TEXT())
    product_dropdown2 = Column(TEXT())
    product_dpci = Column(TEXT())
    product_desc = Column(TEXT())
    product_packaging = Column(TEXT())
    product_serving_per_container = Column(TEXT())
    product_features = Column(TEXT())
    product_serving_size = Column(TEXT())
    product_flavor = Column(TEXT())
    product_type = Column(TEXT())
    product_assembled_product_dimensions = Column(TEXT())
    product_model = Column(TEXT())
    product_sku = Column(TEXT())
    product_manufacturer_part_number = Column(TEXT())
    product_status = Column(BOOLEAN(), server_default=text('true'))
    product_additional_images = Column(TEXT())
    product_nutrition_info = Column(TEXT())
    product_feeding_instructions = Column(TEXT())
    product_total_reviews = Column(TEXT())
    product_rating = Column(TEXT())
    product_customer_reviews = Column(TEXT())
    product_additional_sizes = Column(TEXT())
    product_rating_image_url = Column(TEXT())

    def to_json(self):
        obj = {
            'product_id': self.product_id,
            'product_upc': self.product_upc,
            'product_name': self.product_name,
            'product_price': self.product_price,
            'site_id': self.site_id,
            'product_url': self.product_url,
            'product_ingredients': self.product_ingredients,
            'product_brand': self.product_brand,
            'product_category': self.product_category,
            'product_subcategory1': self.product_subcategory1,
            'product_subcategory2': self.product_subcategory2,
            'product_imageurl': self.product_imageurl,
            'product_subcategory3': self.product_subcategory3,
            'timestamp': self.timestamp.strftime('%a, %d %b %Y %H:%M:%S +0000') if self.timestamp else None,
            'product_size': self.product_size,
            'product_dropdown1': self.product_dropdown1,
            'product_dropdown2': self.product_dropdown2,
            'product_dpci': self.product_dpci,
            'product_desc': self.product_desc,
            'product_packaging': self.product_packaging,
            'product_serving_per_container': self.product_serving_per_container,
            'product_features': self.product_features,
            'product_serving_size': self.product_serving_size,
            'product_flavor': self.product_flavor,
            'product_type': self.product_type,
            'product_assembled_product_dimensions': self.product_assembled_product_dimensions,
            'product_model': self.product_model,
            'product_sku': self.product_sku,
            'product_manufacturer_part_number': self.product_manufacturer_part_number,
            'product_status': self.product_status,
            'product_additional_images': self.product_additional_images,
            'product_nutrition_info': self.product_nutrition_info,
            'product_feeding_instructions': self.product_feeding_instructions,
            'product_total_reviews': self.product_total_reviews,
            'product_rating': self.product_rating,
            'product_customer_reviews': self.product_customer_reviews,
            'product_additional_sizes': self.product_additional_sizes,
            'product_rating_image_url': self.product_rating_image_url,
        }
        return json.dumps(obj)


class ProjectConfig(Base):
    __tablename__ = u'project_config'

    # column definitions
    project_id = Column(INTEGER(), Sequence('project_config_project_id_seq'), primary_key=True, nullable=False)
    project_name = Column(VARCHAR(length=100))
    primary_site_id = Column(INTEGER())
    secondary_site_ids = Column(VARCHAR(length=100))
    input_file_headers = Column(TEXT())
    output_file_headers = Column(TEXT())
    last_updated_timestamp = Column(TIMESTAMP(), server_default=text('now()'))
    requires_input_file = Column(BOOLEAN(), server_default=text('true'))

    def to_json(self):
        obj = {
            'project_id': self.project_id,
            'project_name': self.project_name,
            'primary_site_id': self.primary_site_id,
            'secondary_site_ids': self.secondary_site_ids,
            'input_file_headers': self.input_file_headers,
            'output_file_headers': self.output_file_headers,
            'last_updated_timestamp': self.last_updated_timestamp.strftime('%a, %d %b %Y %H:%M:%S +0000') if self.last_updated_timestamp else None,
            'requires_input_file': self.requires_input_file,
        }
        return json.dumps(obj)


class ScheduledJobs(Base):
    __tablename__ = u'scheduled_jobs'

    # column definitions
    job_id = Column(INTEGER(), Sequence('scheduled_jobs_job_id_seq'), primary_key=True, nullable=False)
    site_id = Column(INTEGER())
    total_products_scraped = Column(INTEGER())
    total_products_attempted = Column(INTEGER())
    project_id = Column(INTEGER())
    start_time = Column(TIMESTAMP())
    file_name = Column(TEXT())
    end_time = Column(TIMESTAMP())
    job_type = Column(VARCHAR(length=500))
    timeout = Column(INTEGER(), server_default=text('5'))
    status = Column(VARCHAR(length=50))
    update_time = Column(TIMESTAMP())
    comments = Column(TEXT())
    total_products_to_scrape = Column(INTEGER())

    def to_json(self):
        obj = {
            'job_id': self.job_id,
            'site_id': self.site_id,
            'total_products_scraped': self.total_products_scraped,
            'total_products_attempted': self.total_products_attempted,
            'project_id': self.project_id,
            'start_time': self.start_time.strftime('%a, %d %b %Y %H:%M:%S +0000') if self.start_time else None,
            'file_name': self.file_name,
            'end_time': self.end_time.strftime('%a, %d %b %Y %H:%M:%S +0000') if self.end_time else None,
            'job_type': self.job_type,
            'timeout': self.timeout,
            'status': self.status,
            'update_time': self.update_time.strftime('%a, %d %b %Y %H:%M:%S +0000') if self.update_time else None,
            'comments': self.comments,
            'total_products_to_scrape': self.total_products_to_scrape,
        }
        return json.dumps(obj)


class ScraperLogs(Base):
    __tablename__ = u'scraper_logs'

    # column definitions
    id = Column(INTEGER(), Sequence('"scraper_logs_Id_seq"'),primary_key=True, nullable=False)
    site_id = Column(INTEGER())
    start_time = Column(TIMESTAMP())
    end_time = Column(TIMESTAMP())
    comments = Column(TEXT())
    duration_in_seconds = Column(BIGINT())
    average_time_per_product = Column(INTEGER())
    no_of_products_scraped = Column(INTEGER())
    target_no_of_products = Column(BIGINT())

    def to_json(self):
        obj = {
            'id': self.id,
            'site_id': self.site_id,
            'start_time': self.start_time.strftime('%a, %d %b %Y %H:%M:%S +0000') if self.start_time else None,
            'end_time': self.end_time.strftime('%a, %d %b %Y %H:%M:%S +0000') if self.end_time else None,
            'comments': self.comments,
            'duration_in_seconds': self.duration_in_seconds,
            'average_time_per_product': self.average_time_per_product,
            'no_of_products_scraped': self.no_of_products_scraped,
            'target_no_of_products': self.target_no_of_products,
        }
        return json.dumps(obj)

	