import requests
import json , os

import os
# #
# # LIVE
#

'''
NOTE:
    Testing API in local please set Environment='DEV'
    Testing API in Production please set Environment='PROD'
'''
Environment = 'PROD'
# # # Database settings
if Environment == 'PROD':
    __DB_ENGINE__ = "postgresql+psycopg2"
    __DB_USER__ = os.environ['DB_USER']
    __DB_PASSWORD__ = os.environ['DB_PASSWORD']
    __DB_HOST__ = os.environ['DB_HOST']
    __DB_PORT__ = "5432"
    __DB_NAME__ = os.environ['DB_NAME']
    DB_Conn_Str = __DB_ENGINE__+"://"+__DB_USER__+":"+__DB_PASSWORD__+"@"+__DB_HOST__+":"+__DB_PORT__+"/"+__DB_NAME__
    SCRAPPER_IP = os.environ['SCRAPPER_IP']

try:
    GOOGLE_API_BASE_URL = 'https://www.googleapis.com/customsearch/v1?key={}'.format(os.environ['Google_API'])+"{query}"
    #GoogleMap Rest API details
    API_KEY = os.environ['GoogleMapKey']
    URL_FORMAT_PLACE = "https://maps.googleapis.com/maps/api/place/textsearch/json?{query}={SearchString}&key={APIKEY}{pagetoken}{location}{radius}{type}"
    URL_PLACE_DETAILS = "https://maps.googleapis.com/maps/api/place/details/json?placeid={placeid}&fields=website,url,name,rating,formatted_phone_number&key={APIKEY}"
except Exception as e:
    print(str(e))
