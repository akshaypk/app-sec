from unittest import TestCase
import requests
import unittest
# from .settings import *

import json
class TestDbUtils(TestCase):
    def test_get_sites(self):
        Base = "http://127.0.0.1:5000/api/v1/sites"
        response = requests.get(Base + '/?site_id=100000')
        self.assertEqual(response.status_code, 200)

    def test_create_site(self):
        headers = {'content-type': 'application/x-www-form-urlencoded'}

        # "Content-Type: application/x-www-form-urlencoded"
        Base = 'http://127.0.0.1:5000/api/v1/sites/add'
        data = {'site_name': 'karthik', 'site_url': 'ghdjhg', 'require_sitemap_parsing': 'false',
                'require_webpage_parsing': 'false', 'require_dynamic_parsing': 'false', 'parse_status': 'true',
                'site_status': 'false', 'image_download': 'true'}
        response = requests.post(Base, params=data, data=data, headers=headers)
        self.assertEqual(response.status_code, 200)
    def test_update_site(self):
        headers = {'content-type': 'application/x-www-form-urlencoded'}
        Base = 'http://127.0.0.1:5000/api/v1/sites/update'
        data = {'site_id':'14','site_name': 'test', 'site_url': 'test', 'require_sitemap_parsing': 'false',
                'require_webpage_parsing': 'false', 'require_dynamic_parsing': 'false', 'parse_status': 'false',
                'site_status': 'false', 'image_download': 'false'}
        response = requests.put(Base, params=data, data=data, headers=headers)
        self.assertEqual(response.status_code, 200)


    def test_delete_site(self):
            headers = {'content-type': 'application/x-www-form-urlencoded'}
            Base = 'http://127.0.0.1:5000/api/v1/sites/delete'
            data = {'site_id':'80'}
            response = requests.delete(Base, params=data, data=data, headers=headers)
            self.assertEqual(response.status_code, 200)




    def test_get_category_parser_details(self):
        Base = "http://127.0.0.1:5000/api/v1/category_parser_details"
        response = requests.get(Base + '/?site_id=44')
        self.assertEqual(response.status_code, 200)

    def test_create_category_parser_details(self):
        headers = {'content-type': 'application/x-www-form-urlencoded'}

        # "Content-Type: application/x-www-form-urlencoded"
        Base = 'http://127.0.0.1:5000/api/v1/category_parser_details/add'
        data = {'category_name': 'karthik', 'base_search_url': 'ghdjhg', 'require_sitemap_parsing': 'false',
                'require_webpage_parsing': 'false', 'require_dynamic_parsing': 'false', 'parse_status': 'true',
                'site_status': 'false', 'image_download': 'true'}
        response = requests.post(Base, params=data, data=data, headers=headers)
        self.assertEqual(response.status_code, 200)


    def test_update_category_parser_details(self):
        headers = {'content-type': 'application/x-www-form-urlencoded'}
        Base = 'http://127.0.0.1:5000/api/v1/category_parser_details/update'
        data = {'site_id': '81', 'category_name': 'test', 'base_search_url': 'test', 'product_url_xpath': 'test',
                'parse_with_selenium': 'false', 'category_xpath': 'test', 'levels_of_sub_categories': '4',
                'subcategory1_xpath': 'test', 'subcategory2_xpath': 'test','subcategory3_xpath': 'test', 'driver_id': '4'}
        response = requests.put(Base, params=data, data=data, headers=headers)
        self.assertEqual(response.status_code, 200)

    def test_delete_category_parser_details(self):
        headers = {'content-type': 'application/x-www-form-urlencoded'}
        Base = 'http://127.0.0.1:5000/api/v1/category_parser_details/delete'
        data = {'site_id': '81'}
        response = requests.delete(Base, params=data, data=data, headers=headers)
        self.assertEqual(response.status_code, 200)


if __name__ == '__main__':
    unittest.main()
