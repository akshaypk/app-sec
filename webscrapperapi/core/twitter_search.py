##pip install tweepy

import tweepy
import csv
import json

# dump Twitter API credentials
# info={"CONSUMER_KEY":"MsKNhV3Ub9Uk1nkd1XHy0Hw9i",
#   "CONSUMER_SECRET":"oQW8r3sIxnSjBVG2OO4voRfLE11zIpNuk1Ts5kthpD8sETaIWu",
#   "ACCESS_KEY":"1133974858218369024-i6yOw7TfSdOC5paLJXeryKPtSu9YVM",
#   "ACCESS_SECRET":"Y0uT3N2sO4baM7xmb1jsPzM3ryyrAvcIqOKW3Vw8ecpH3"}
#
# with open('twitter_credentials.json','w') as f:
#     f.write(json.dumps(info))
#


class TwitterSearch(object):

    def __init__(self,):
        self.api = None
        self.connection()

    def connection(self):

        # with open('twitter_credentials.json', 'r') as f:
        #     info = json.loads(f.read())
        # #
        info = {"CONSUMER_KEY": "MsKNhV3Ub9Uk1nkd1XHy0Hw9i",
                "CONSUMER_SECRET": "oQW8r3sIxnSjBVG2OO4voRfLE11zIpNuk1Ts5kthpD8sETaIWu",
                "ACCESS_KEY": "1133974858218369024-i6yOw7TfSdOC5paLJXeryKPtSu9YVM",
                "ACCESS_SECRET": "Y0uT3N2sO4baM7xmb1jsPzM3ryyrAvcIqOKW3Vw8ecpH3"}

        consumer_key = info['CONSUMER_KEY']
        consumer_secret = info['CONSUMER_SECRET']
        access_key = info['ACCESS_KEY']
        access_secret = info['ACCESS_SECRET']
        auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
        auth.set_access_token(access_key, access_secret)
        self.api = tweepy.API(auth)

    def get_user_info(self, query):
        if not query.startswith("@"):
            query = "@"+query
        user_info = self.api.get_user(query)
        return self.item_loader([user_info])

    def get_hash_info_method2(self, query, max_tweets):
        searched_tweets = []
        last_id = -1
        while len(searched_tweets) < max_tweets:
            max_id = last_id - 1
            if max_id <= 0:
                max_id = 0
            try:
                new_tweets = self.api.search(q=query, count=100, max_id=str(max_id))
                if not new_tweets:
                    break
                searched_tweets.extend(new_tweets)
                last_id = new_tweets[-1].id
            except tweepy.TweepError as e:
                print(e)
                break
        return self.item_loader(searched_tweets)

    def get_hash_info_method1(self, query, max_tweets):
        searched_tweets = [status for status in tweepy.Cursor(self.api.search, q=query).items(max_tweets)]
        return self.item_loader(searched_tweets)

    def item_loader(self, items, _type="#"):
        return items

if __name__ == '__main__':

    query = "@Mail2Sella"
    obj = TwitterSearch()

    #query = "#Pray_For_Neasamani"
    max_limit = 10
    scrapped_items = obj.get_hash_info_method2(query, max_limit)
    scrapped_items = obj.get_user_info(query)
    print(len(scrapped_items[:max_limit]))
    for item in scrapped_items:
        print(item._json)