from numpy.core.defchararray import lower
from sqlalchemy import create_engine, schema, select, update, insert, and_, or_, delete, outerjoin, bindparam
from sqlalchemy import func, cast, Numeric, asc, desc
from sqlalchemy.orm import sessionmaker
from .settings import *
import psycopg2
from datetime import datetime
from .LoggingManager import get_logger
import json
import requests
from flask import jsonify
from core import settings
from pathlib import Path
import os
import csv
from sqlalchemy import func
# import LOWER
import re
# from sqlalchemy.sql.expressions import func

from pathlib import Path
from pathlib import PurePath
from collections import OrderedDict

results_folder = Path(__file__).parent


# print(settings.__DB_USER__)
# from .config import Base
# from flask import Flask
# conn=config.connect()
# SQLALCHEMY_DATABASE_URL='postgresql://postgres:admin@localhost:5432/scraper'
# with open('C:/web/New folder/webscrapercloud/API/webscrapperapi/core/config.json','r') as f:
#     config=json.load(f)
# config['SQLALCHEMY_DATABASE_URL'] ='postgresql://postgres:admin@localhost:5432/scraper'
class DbUtils(object):
    def __init__(self):
        # self.engine = create_engine(__DB_Conn_Str__)
        self.engine = create_engine(DB_Conn_Str, pool_size=2, max_overflow=0, pool_recycle=3600, pool_pre_ping=True)
        self.metadata = schema.MetaData(bind=self.engine)
        self.sites = schema.Table('sites', self.metadata, autoload=True)
        self.sitemap_parser_details = schema.Table('sitemap_parser_details', self.metadata, autoload=True)
        self.scheduled_jobs = schema.Table('scheduled_jobs', self.metadata, autoload=True)
        self.scheduled_jobss = schema.Table('scheduled_jobs', self.metadata, autoload=True)
        self.periodic_tasks = schema.Table('periodic_tasks', self.metadata, autoload=True)
        self.project_config = schema.Table('project_config', self.metadata, autoload=True)
        self.webpage_parser_details = schema.Table('webpage_parser_details', self.metadata, autoload=True)
        self.dynamic_scraper_file = schema.Table('dynamic_scraper_file', self.metadata, autoload=True)
        self.dynamic_scrapper_details = schema.Table('dynamic_scrapper_details', self.metadata, autoload=True)
        self.location_scraper_file = schema.Table('location_scraper_file', self.metadata, autoload=True)
        self.location_parser_details = schema.Table('location_parser_details', self.metadata, autoload=True)
        self.products = schema.Table('products', self.metadata, autoload=True)
        self.import_products = schema.Table('import_products', self.metadata, autoload=True)
        self.logs = schema.Table('scraper_logs', self.metadata, autoload=True)
        self.selenium_drivers = schema.Table('selenium_drivers', self.metadata, autoload=True)
        self.category_scope = schema.Table('category_scope', self.metadata, autoload=True)
        self.category_parser_details = schema.Table('category_parser_details', self.metadata, autoload=True)
        self.csv_template = schema.Table('csv_template', self.metadata, autoload=True)
        self.receivability_tool_config = schema.Table('receivability_tool_config', self.metadata, autoload=True)
        self.location_webparser_details = schema.Table('location_webparser_details', self.metadata, autoload=True)
        self.scrapped_location = schema.Table('scrapped_location', self.metadata, autoload=True)
        self.crontab_schedule = schema.Table('crontab_schedule', self.metadata, autoload=True)
        self.google_products = schema.Table('google_products', self.metadata, autoload=True)
        self.search_engine_details = schema.Table('search_engine_details', self.metadata, autoload=True)
        self.image_puller_lookup = schema.Table('image_puller_lookup', self.metadata, autoload=True)
        self.jobid_mapping_product = schema.Table('jobid_mapping_product', self.metadata, autoload=True)
        self.ajax_parser_details = schema.Table('ajax_parser_details', self.metadata, autoload=True)
        self.users = schema.Table('auth_user', self.metadata, autoload=True)
        get_logger().info('successfully connected to DB : %s', DB_Conn_Str)

    def close(self):
        try:
            # print (dir(self.engine))
            if self.engine:
                self.engine.dispose()
                get_logger().info('Closed DB connection')
        except Exception as e:
            get_logger().error('Error:', exc_info=True)

    def get_project_details(self, project_id):
        rs_one = {}
        checkProjectExist = ((select([self.project_config])).where(self.project_config.c.project_id == int(project_id)))
        rs = checkProjectExist.execute()
        if rs:
            rs_one = rs.fetchone()
        return rs_one

    # UPC Dynmaic Search function
    def dynamic(self, whereValuesDict={}):
        # URL = "http://10.246.1.8:8888/webscrapper/getDynamicScrappedResults"
        # URL = "http://localhost:8888/webscrapper/getDynamicScrappedResults"
        dynamic_api_url_pattern = 'http://{}/webscrapper/getDynamicScrappedResults'
        URL = dynamic_api_url_pattern.format(settings.SCRAPPER_IP)
        # print (URL)
        PARAMS = {'site_id': whereValuesDict['site_id'],
                  'search_text': whereValuesDict['search_text'],
                  'request_type': whereValuesDict['request_type']}
        try:
            details = requests.post(url=URL, data=PARAMS)
            product_details = json.loads(details.text)
            return product_details
        except Exception as e:
            get_logger().error('Error:', exc_info=True)
            return [{'message': str(e)}]

    # site monitoring function
    def processproducturl(self, whereValuesDict={}):
        # URL = "http://localhost:8888/webscrapper/getProducturlResults"
        producturl_api_url_pattern = 'http://{}/webscrapper/getProducturlResults'
        URL = producturl_api_url_pattern.format(settings.SCRAPPER_IP)
        PARAMS = {'site_id': whereValuesDict['site_id']}

        try:
            details = requests.post(url=URL, data=PARAMS)

            try:
                product_details = json.loads(details.text)
                print(product_details,"88888")
            except Exception as e:
                product_details = {}
            return product_details
        except Exception as e:
            get_logger().error('Error:', exc_info=True)
            return [{'message': str(e)}]

    def create_file_job(self, insertValuesDict={}):
        try:
            print(insertValuesDict)
            self.engine.execute(self.scheduled_jobs.insert(), [insertValuesDict])
            get_logger().debug('added scheduled_jobs details successfully')
            where_clauses = [self.scheduled_jobs.c[key] == value for (key, value) in insertValuesDict.items()]
            get_scheduled_jobs = ((select([self.scheduled_jobs])).where(and_(*where_clauses)))
            rs = get_scheduled_jobs.execute()
            result = rs.fetchone()
            get_logger().debug('Created new scheduled_jobs successfully : %s', result)
            return {'status': 'success', 'data': result[0],
                    'message': 'Created new scheduled_jobs successfully'}
        except Exception as e:
            get_logger().error('Error in creating scheduled_jobs :', exc_info=True)
            return {'status': 'failure', 'message': 'Error in creating scheduled_jobs'}

    def dynamic_csv(self, insertValuesDict={}):
        try:
            # print(insertValuesDict)
            job_type = insertValuesDict.get('job_type',None)
            region = insertValuesDict.get('region', None)
            request_type = insertValuesDict.get('request_type', None)
            newJobDetails = {}
            newJobDetails['site_id'] = insertValuesDict['site_id']
            newJobDetails['start_time'] = datetime.utcnow()
            newJobDetails['job_type'] = 'FILE' if job_type is None else job_type
            newJobDetails['file_name'] = insertValuesDict['file_name']
            newJobDetails['status'] = 'SCHEDULED'
            newJobDetails['category'] = request_type
            if region:
                newJobDetails['region'] = region

            # URL = "http://10.246.1.8:8888/webscrapper/getProductsByCSV"
            # # URL = "http://localhost:8888/webscrapper/getProductsByCSV"
            # PARAMS = {'site_id': insertValuesDict['site_id'],
            #           'file_name': insertValuesDict['file_name']}
            # print("inser", type(insertValuesDict), insertValuesDict)
            self.engine.execute(self.dynamic_scraper_file.insert(), [insertValuesDict])
            newJob = self.create_scheduled_jobs(newJobDetails)
            job_id = newJob['data']
            update_dynamic = self.update_dynamicfile({'file_name': newJobDetails['file_name'], 'job_id': job_id})
            # job_id = newJob
            get_logger().debug('Added files details successfully')
            get_dynamic_file = select([self.dynamic_scraper_file]).where(
                self.dynamic_scraper_file.c.file_name == insertValuesDict['file_name'])
            rs = get_dynamic_file.execute()
            result = rs.fetchone()
            # get_logger().debug('Added files details successfully', result)
            return job_id
        except Exception as e:
            get_logger().error('Error in adding files', exc_info=True)
            return {'status': 'failure', 'message': 'Error in adding files'}

    def dynamic_csv_productname(self, insertValuesDict={}):
        try:
            newJobDetails = {}
            newJobDetails['site_id'] = insertValuesDict['site_id']
            newJobDetails['start_time'] = datetime.utcnow()
            newJobDetails['job_type'] = 'FILES'
            newJobDetails['file_name'] = insertValuesDict['file_name']
            newJobDetails['status'] = 'SCHEDULED'
            print("inser", type(insertValuesDict), insertValuesDict)
            self.engine.execute(self.dynamic_scraper_file.insert(), [insertValuesDict])
            newJob = self.create_scheduled_jobs(newJobDetails)
            job_id = newJob['data']
            update_dynamic = self.update_dynamicfile({'file_name': newJobDetails['file_name'], 'job_id': job_id})
            get_logger().debug('Added files details successfully')
            get_dynamic_file = select([self.dynamic_scraper_file]).where(
                self.dynamic_scraper_file.c.file_name == insertValuesDict['file_name'])
            rs = get_dynamic_file.execute()
            result = rs.fetchone()
            get_logger().debug('Added files details successfully', result)
            return job_id
        except Exception as e:
            get_logger().error('Error in adding files', exc_info=True)
            return {'status': 'failure', 'message': 'Error in adding files'}

    def update_dynamicfile(self, updateValuesDict={}):
        try:
            stmt = (self.dynamic_scraper_file.update().where(
                self.dynamic_scraper_file.c.file_name == updateValuesDict['file_name']).
                    values(updateValuesDict))
            rs = stmt.execute()
        except Exception as e:
            print(e)
            get_logger().error('Error in updating dynamic config :', exc_info=True)
            return {'status': 'failure', 'message': 'Error in updating site details'}

        else:
            get_logger().debug('Updated site successfully.')
            return {'status': 'success', 'message': 'Updated dynamic scrapper file  successfully'}

    def project_csv(self, insertValuesDict={}):
        get_logger().debug('called insert_project_csv_bulk for total products : %s', len(insertValuesDict))
        try:
            self.engine.execute(self.import_products.insert(), insertValuesDict)
        except Exception as e:
            get_logger().error('Error in bulk insert', exc_info=True)
        else:
            get_logger().debug('bulk insert successfull for %s products', len(insertValuesDict))

    def update_importproducts(self, updateValuesDict={}):
        try:
            stmt = (self.import_products.update().where(
                self.import_products.c.file_name == updateValuesDict['file_name']).
                    values(updateValuesDict))
            rs = stmt.execute()
        except Exception as e:
            print(e)
            get_logger().error('Error in updating dynamic config :', exc_info=True)
            return {'status': 'failure', 'message': 'Error in updating site details'}

        else:
            get_logger().debug('Updated site successfully.')
            return {'status': 'success', 'message': 'Updated dynamic scrapper file  successfully'}

    def update_import_productss(self, updateValuesDict={}):
        try:
            stmt = (self.import_products.update().where(
                self.import_products.c.file_name == updateValuesDict['file_name']).
                    values(updateValuesDict))
            rs = stmt.execute()
        except Exception as e:
            print(e)
            get_logger().error('Error in updating dynamic config :', exc_info=True)
            return {'status': 'failure', 'message': 'Error in updating site details'}

        else:
            get_logger().debug('Updated site successfully.')
            return {'status': 'success', 'message': 'Updated dynamic scrapper file  successfully'}

    def location_csv(self, insertValuesDict={}):
        try:
            newJobDetails = {}
            newJobDetails['site_id'] = insertValuesDict['site_id']
            newJobDetails['start_time'] = datetime.utcnow()
            newJobDetails['job_type'] = 'FILELOCATION'
            newJobDetails['file_name'] = insertValuesDict['file_name']
            newJobDetails['status'] = 'SCHEDULED'
            # URL = "http://10.246.1.8:8888/webscrapper/getProductsByCSV"
            # # URL = "http://localhost:8888/webscrapper/getProductsByCSV"
            # PARAMS = {'site_id': insertValuesDict['site_id'],
            #           'file_name': insertValuesDict['file_name']}
            print("inser", type(insertValuesDict), insertValuesDict)
            self.engine.execute(self.location_scraper_file.insert(), [insertValuesDict])
            newJob = self.create_scheduled_jobs(newJobDetails)
            job_id = newJob['data']

            # update_dynamic = self.update_dynamicfile({'file_name': newJobDetails['file_name'], 'job_id': job_id})
            update_locationfile = self.update_locationfile({'file_name': newJobDetails['file_name'], 'job_id': job_id})
            # job_id = newJob
            # get_logger().debug('Added files details successfully')
            get_dynamic_file = select([self.location_scraper_file]).where(
                self.location_scraper_file.c.file_name == insertValuesDict['file_name'])
            rs = get_dynamic_file.execute()
            result = rs.fetchone()
            # get_logger().debug('Added files details successfully', result)
            return job_id
        except Exception as e:
            get_logger().error('Error in adding files', exc_info=True)
            return {'status': 'failure', 'message': 'Error in adding files'}

    def update_locationfile(self, updateValuesDict={}):
        try:
            stmt = (self.location_scraper_file.update().where(
                self.location_scraper_file.c.file_name == updateValuesDict['file_name']).
                    values(updateValuesDict))
            rs = stmt.execute()
        except Exception as e:
            print(e)
            get_logger().error('Error in updating locationfile config :', exc_info=True)
            return {'status': 'failure', 'message': 'Error in updating site details'}

        else:
            get_logger().debug('Updated site successfully.')
            return {'status': 'success', 'message': 'Updated location scrapper file  successfully'}

    def dynamic_products(self, Valuesdict={}):
        upc_list = []
        getUpcvalues = ((select([self.dynamic_scraper_file])).where(self.dynamic_scraper_file.c.job_id == int(
            Valuesdict['job_id'])))
        rs = getUpcvalues.execute()
        results = rs.fetchone()
        print('results', results)
        upcs = json.loads(results[5])
        print(type(upcs), upc_list)
        return upcs

    def get_siteid_by_jobid(self, job_id):
        get_job = ((select([self.scheduled_jobs.c.site_id])).where(and_(self.scheduled_jobs.c.job_id == job_id)))
        rs = get_job.execute()
        data = rs.fetchone()
        res = [x for x in data]
        return res

    def get_location_results(self, Valuesdict={}):
        keywords_list = []
        try:
            getkeywords = ((select([self.scrapped_location])).where(self.scrapped_location.c.job_id == int(
                Valuesdict['job_id'])).order_by(asc(self.scrapped_location.c.timestamp)))
            rs = getkeywords.execute()
            results = rs.fetchall()

            return results
        except Exception as e:
            print(str(e))

    def location_products(self, Valuesdict={}):
        keywords_list = []
        getkeywords = ((select([self.location_scraper_file])).where(self.location_scraper_file.c.job_id == int(
            Valuesdict['job_id'])))
        rs = getkeywords.execute()
        results = rs.fetchone()
        print('results', results)

        keywords = json.loads(results[5])
        print(type(keywords), keywords_list)
        return keywords

    ## Category Parser
    def category_parser(self, insertValuesDict={}):
        try:
            print("insert", insertValuesDict)
            newJobDetails = {}
            newJobDetails['site_id'] = insertValuesDict['site_id']
            newJobDetails['start_time'] = datetime.utcnow()
            newJobDetails['job_type'] = 'CATEGORY'
            newJobDetails['status'] = 'SCHEDULED'
            newJobDetails['category'] = insertValuesDict['category']
            exisitingJob, count = self.get_scheduled_jobs(0, 1, newJobDetails)
            if exisitingJob:
                result = {'STATUS': 'ERROR',
                          'MSG': 'Cannot create the job as there is already an identical job running',
                          'JOB_ID': exisitingJob['job_id']}
            else:
                newJob = self.create_scheduled_jobs(newJobDetails)
                result = {'STATUS': 'SUCCESS', 'MSG': 'CATEGORY PARSER JOB CREATED SUCCESSFULLY'}
            return result
        except Exception as e:
            get_logger().error('Error:', exc_info=True)
            return {'STATUS': 'FAILURE', 'MSG': 'ERROR CRETING JOB'}

    ##Lookup CSV products
    def lookup_csv(self, whereValuesdict={}):
        products = []
        upc_values = []
        dpci_values = []
        sku_values = []
        try:
            header = whereValuesdict['header']
            values = whereValuesdict['file_data']
            if header == 'product_upc':
                rs = select([self.products]).where(self.products.c.product_upc.in_(values))
                result = rs.execute().fetchall()
                for data in result:
                    final_results = dict(data.items())
                    upc_values.append(data['product_upc'])
                    products.append(final_results)
            elif header == 'product_dpci':
                rs = select([self.products]).where(self.products.c.product_dpci.in_(values))
                result = rs.execute()
                for data in result:
                    final_results = dict(data.items())
                    products.append(final_results)
            elif header == 'product_sku':
                rs = select([self.products]).where(self.products.c.product_sku.in_(values))
                result = rs.execute().fetchall()
                for data in result:
                    final_results = dict(data.items())
                    products.append(final_results)
            for value in values:
                if value not in upc_values:
                    products.append({'product_upc': value})
            return products
        except Exception as e:
            get_logger().error('Error in listing category parser details:', exc_info=True)

    def list_sites(self):
        sites_list = []
        rs = select([self.sites]).order_by(asc(self.sites.c.site_id))
        result = rs.execute().fetchall()
        for sites in result:
            sites_list.append({sites.site_id: sites.site_name})
        return sites_list

    # CATEGORY RELATED FUNCTIONS
    def get_category_parser_details(self, offset, limit, whereValuesDict={}):
        categorylist = []
        try:

            j = self.category_parser_details.outerjoin(self.sites,
                                                       self.category_parser_details.c.site_id == self.sites.c.site_id)
            site_id = ''
            category_name = ''
            site_where_dict = {}
            if 'site_name' in whereValuesDict:
                site_where_dict['site_name'] = whereValuesDict['site_name']
                del whereValuesDict['site_name']
            if whereValuesDict or site_where_dict:
                if 'site_id' in whereValuesDict:
                    site_id = whereValuesDict['site_id']
                    del whereValuesDict['site_id']
                if 'category_name' in whereValuesDict:
                    category_name = whereValuesDict['category_name']
                    del whereValuesDict['category_name']
                where_clause_1 = [self.category_parser_details.c[key] == value for (key, value) in
                                  whereValuesDict.items()]
                where_clause_2 = [self.sites.c[key] == value for (key, value) in site_where_dict.items()]
                where_clauses = where_clause_1 + where_clause_2
                # print (whereValuesDict)
                # print(where_clauses)
                if not site_id and not category_name:
                    where_condition_rs = [*where_clauses]

                elif site_id and category_name:
                    where_condition_rs = [*where_clauses, self.category_parser_details.c.site_id == site_id,
                                          self.category_parser_details.c.category_name == category_name]

                elif site_id and not category_name:
                    where_condition_rs = [*where_clauses, self.category_parser_details.c.site_id == site_id]

                elif not site_id and category_name:
                    where_condition_rs = [*where_clauses,
                                          self.category_parser_details.c.category_name == category_name]

                rs = ((select([self.category_parser_details, self.sites.c.site_name])).select_from(j).where(
                    and_(*where_condition_rs))).order_by(
                    asc(self.category_parser_details.c.site_id)).limit(limit).offset(offset)

                rs2 = select([func.count(self.category_parser_details.c.site_id)]).where(
                    and_(*where_condition_rs))

            else:

                rs2 = select([func.count(self.category_parser_details.c.site_id)])
                rs = select([self.category_parser_details, self.sites.c.site_name]).select_from(j).order_by(
                    asc(self.category_parser_details.c.site_id)).limit(limit).offset(offset)
                # print(rs)

                rs2 = select([func.count(self.category_parser_details.c.id)])
                rs = select([self.category_parser_details, self.sites.c.site_name]).select_from(j).order_by(
                    asc(self.category_parser_details.c.id)).limit(limit).offset(offset)

            count = rs2.execute().fetchone()
            result = rs.execute().fetchall()
            for category in result:
                categorylist.append({
                    'category_name': category.category_name,
                    'site_id': category.site_id, 'parse_with_selenium': category.parse_with_selenium,
                    'category_xpath': category.category_xpath,
                    'levels_of_sub_categories': category.levels_of_sub_categories,
                    'subcategory1_xpath': category.subcategory1_xpath,
                    'subcategory2_xpath': category.subcategory2_xpath,
                    'subcategory3_xpath': category.subcategory3_xpath,
                    'driver_id': category.driver_id,
                    'base_search_url': category.base_search_url,
                    'product_url_xpath': category.product_url_xpath,
                    'pagination_xpath': category.pagination_xpath,
                    'zero_depth_xpath': category.zero_depth_xpath,
                    'site_name': category.site_name

                })
            get_logger().debug(
                'Listing category parser configuration using following values. %s' % str(whereValuesDict))
            return categorylist, count[0]

        except Exception as e:
            get_logger().error('Error in listing Category parser details:', exc_info=True)
            return {'status': 'failure', 'message': 'Error in creating Category parser details'}
        else:
            get_logger().debug('Category parser details listed succesfully')
            return {'status': 'failure', 'message': 'Error in creating Category parser details'}

    # To create category parser details
    def create_category_parser_details(self, createValuesDict):
        try:
            self.engine.execute(self.category_parser_details.insert(), [createValuesDict])
            get_logger().debug('created Category parser details successfully.')
            where_clauses = [self.category_parser_details.c[key] == value for (key, value) in createValuesDict.items()]
            get_site = ((select([self.category_parser_details])).where(and_(*where_clauses)))
            rs = get_site.execute()
            result = rs.fetchone()
            get_logger().debug('Created new Category parser config successfully : %s', result)
            return {'status': 'success', 'message': 'Created Category parser details successfully'}
        except Exception as e:
            print(e)
            get_logger().error('Error in creating Category parser details  config :', exc_info=True)
            return {'status': 'failure', 'message': 'Error in creating Category parser details'}

    # update category parser deatils
    def update_category_parser_details(self, updateValuesDict={}):
        try:
            print(updateValuesDict)
            category_name = updateValuesDict['category_name']
            stmt = (self.category_parser_details.update().where(
                and_(self.category_parser_details.c.site_id == updateValuesDict['site_id'],
                     self.category_parser_details.c.category_name == updateValuesDict['category_name'])).
                    values(updateValuesDict))
            rs = stmt.execute()
        except Exception as e:
            print(e)
            get_logger().error('Error in updating Category details config :', exc_info=True)
            return {'status': 'failure', 'message': 'Error in updating site details'}

        else:
            get_logger().debug('Updated Category parser details successfully.')
            return {'status': 'success', 'message': 'Updated Category parser details successfully'}

    # delete category parser details

    def delete_category_parser_details(self, whereValuesDict={}):
        try:
            category_name = whereValuesDict['category_name']
            stmt = (self.category_parser_details.delete().where(
                and_(self.category_parser_details.c.site_id == whereValuesDict['site_id'],
                     self.category_parser_details.c.category_name == whereValuesDict['category_name'])))
            rs = stmt.execute()
        except Exception as e:
            print(e)
            get_logger().error('Error in updating Category details config :', exc_info=True)
            return {'status': 'failure', 'message': 'Error in updating site details'}

        else:
            get_logger().debug('Updated Category parser details successfully.')
            return {'status': 'success', 'message': 'Updated Category parser details successfully'}


    def get_ajax_parser_details(self, offset, limit, whereValuesDict={}):
        ajax_list = []
        try:

            products_list = []
            # print (whereValuesDict,limit,offset)
            j = self.ajax_parser_details.outerjoin(self.sites,
                                                   self.ajax_parser_details.c.site_id == self.sites.c.site_id)
            site_id = ''

            site_where_dict = {}
            if 'site_name' in whereValuesDict:
                site_where_dict['site_name'] = whereValuesDict['site_name']
                del whereValuesDict['site_name']
            if whereValuesDict or site_where_dict:
                if 'site_id' in whereValuesDict:
                    site_id = whereValuesDict['site_id']
                    del whereValuesDict['site_id']

                where_clause_1 = [self.ajax_parser_details.c[key] == value for (key, value) in
                                  whereValuesDict.items()]
                where_clause_2 = [self.sites.c[key] == value for (key, value) in site_where_dict.items()]
                where_clauses = where_clause_1 + where_clause_2
                # print (whereValuesDict)
                # print(where_clauses)
                if not site_id:
                    where_condition_rs = [*where_clauses]


                elif site_id:
                    where_condition_rs = [*where_clauses, self.ajax_parser_details.c.site_id == site_id]


                rs = ((select([self.ajax_parser_details, self.sites.c.site_name])).select_from(j).where(
                    and_(*where_condition_rs))).order_by(
                    asc(self.ajax_parser_details.c.site_id)).limit(limit).offset(offset)

                rs2 = select([func.count(self.ajax_parser_details.c.site_id)]).where(
                    and_(*where_condition_rs))

            else:

                rs2 = select([func.count(self.ajax_parser_details.c.site_id)])
                rs = select([self.ajax_parser_details, self.sites.c.site_name]).select_from(j).order_by(
                    asc(self.ajax_parser_details.c.site_id)).limit(limit).offset(offset)
                # print(rs)

                rs2 = select([func.count(self.ajax_parser_details.c.id)])
                rs = select([self.ajax_parser_details, self.sites.c.site_name]).select_from(j).order_by(
                    asc(self.ajax_parser_details.c.id)).limit(limit).offset(offset)

            count = rs2.execute().fetchone()
            result = rs.execute().fetchall()
            for ajax in result:
                ajax_list.append({
                    'site_id' : ajax.site_id,
                    'request_level':ajax.request_level,
                    'config_details': ajax.config_details,
                    'product_url': ajax.product_url
                })
            return ajax_list, count[0]
        except Exception as e:
            get_logger().error('Error in listing ajax parser details:', exc_info=True)
            return {'status': 'failure', 'message': 'Error in creating ajax parser'}



    # To create ajax parser details
    def create_ajax_parser_details(self, createValuesDict):
        try:
            self.engine.execute(self.ajax_parser_details.insert(), [createValuesDict])
            get_logger().debug('created Ajax parser details successfully.')
            # where_clauses = [self.ajax_parser_details.c[key] == value for (key, value) in createValuesDict.items()]
            get_site = ((select([self.ajax_parser_details]))
                        .where(and_(self.ajax_parser_details.c.site_id == createValuesDict['site_id'])))
            rs = get_site.execute()
            result = rs.fetchone()
            get_logger().debug('Created new Ajax parser config successfully : %s', result)
            return {'status': 'success', 'message': 'Created Ajax parser details successfully'}
        except Exception as e:
            print(e)
            get_logger().error('Error in creating Ajax parser details  config :', exc_info=True)
            return {'status': 'failure', 'message': 'Error in creating Ajax parser details'}


    # update category parser deatils
    def update_ajax_parser_details(self, updateValuesDict={}):
        try:
            print(updateValuesDict)

            stmt = (self.ajax_parser_details.update().where(
                and_(self.ajax_parser_details.c.site_id == updateValuesDict['site_id'])).
                    values(updateValuesDict))
            rs = stmt.execute()
        except Exception as e:
            print(e)
            get_logger().error('Error in updating Ajax details config :', exc_info=True)
            return {'status': 'failure', 'message': 'Error in updating site details'}

        else:
            get_logger().debug('Updated Ajax parser details successfully.')
            return {'status': 'success', 'message': 'Updated Ajax parser details successfully'}

    # delete ajax parser details
    def delete_ajax_parser_details(self, site_id):
        try:
            stmt = (self.ajax_parser_details.delete().where(self.ajax_parser_details.c.site_id == site_id))
            rs = stmt.execute()
            get_logger().debug('Deleting ajax config with following site_id: %s', site_id)
        except Exception as e:
            print(e)
            get_logger().error('Error in updating Ajax details config :', exc_info=True)
            return {'status': 'failure', 'message': 'Error in updating site details'}

        else:
            get_logger().debug('Updated Ajax parser details successfully.')
            return {'status': 'success', 'message': 'Updated Category parser details successfully'}

    def get_catgeory_products(self, whereValuesDict={}):
        try:
            dict_keys = []
            current_date = datetime.utcnow().strftime('%Y_%m_%d_%H_%M_%S')

            category_products = []
            category_list = whereValuesDict['category_list']

            site_id = whereValuesDict['site_id']
            for num in category_list:

                rs = select([self.products]).where(
                    and_(self.products.c.site_id == site_id, self.products.c.product_category.in_([num])))
                final_result = rs.execute().fetchall()
                for details in final_result:
                    upc_dict = dict(details.items())
                    upc_dict.pop('timestamp')
                    category_products.append(upc_dict)

            file_name = str(site_id) + '_' + current_date + '_' + 'CATEGORY.csv'
            final_directory = results_folder / 'results'
            final_file = final_directory.resolve()
            if not os.path.exists(final_file):
                os.makedirs(final_file)

            results_file = results_folder / 'results' / file_name

            print(type(category_products))
            with open(results_file, 'w+') as csvfile:
                for details in category_products:
                    # print(details)
                    dict_keys = list(details.keys())
                # print(dict_keys)
                writer = csv.DictWriter(csvfile, fieldnames=dict_keys)
                writer.writeheader()
                for details in category_products:
                    writer.writerow(details)
            return category_products



        except Exception as e:
            print(e)
            get_logger().error('Error in updating Category details config :', exc_info=True)
            return {'status': 'failure', 'message': 'Error in updating site details'}

    # SITE RELATED FUNCTIONS
    def get_site(self, offset, limit, whereValuesDict={}):
        sites_list = []

        print('offset:', offset, limit, whereValuesDict)
        if whereValuesDict:
            if 'site_url' in whereValuesDict:
                site_url = whereValuesDict['site_url']
                del whereValuesDict['site_url']
            else:
                site_url = ''
            where_clause = [self.sites.c[key] == value for (key, value) in whereValuesDict.items()]
            # print(where_clause)
            rs = select([self.sites]).where(
                and_(*where_clause, self.sites.c.site_url.like('%' + site_url + '%'))).order_by(
                asc(self.sites.c.site_id)).limit(limit).offset(offset)
            # print(rs)
            rs2 = select([func.count(self.sites.c.site_id)]).where(
                and_(*where_clause, self.sites.c.site_url.like('%' + site_url + '%')))
        else:
            rs = select([self.sites]).order_by(asc(self.sites.c.site_id)).limit(limit).offset(offset)
            rs2 = select([func.count(self.sites.c.site_id)])

        count = rs2.execute().fetchone()
        result = rs.execute().fetchall()
        for sites in result:
            sites_list.append({'site_id': sites.site_id, 'site_name': sites.site_name, 'site_url': sites.site_url,
                               'require_sitemap_parsing': sites.require_sitemap_parsing,
                               'require_webpage_parsing': sites.require_webpage_parsing,
                               'require_dynamic_scrapping': sites.require_dynamic_scrapping,
                               'require_category_parsing': sites.require_category_parsing,
                               'require_location_parsing': sites.required_location_parsing,
                               'image_download': sites.image_download,
                               'site_change_status': sites.site_change_status,
                               'expiry_check': sites.expiry_check
                               })

        return sites_list, count[0]

    def get_sites(self, offset, limit, whereValuesDict={}):
        sites_list = []
        # print('offset:', offset, limit, whereValuesDict)
        if whereValuesDict:
            if 'site_url' in whereValuesDict:
                site_url = whereValuesDict['site_url']
                del whereValuesDict['site_url']
            else:
                site_url = ''
            where_clause = [self.sites.c[key] == value for (key, value) in whereValuesDict.items()]
            # print(where_clause)
            rs = select([self.sites]).where(
                and_(*where_clause, self.sites.c.site_url.like('%' + site_url + '%'))).order_by(
                asc(self.sites.c.site_id)).limit(limit).offset(offset)
            # print(rs)
            rs2 = select([func.count(self.sites.c.site_id)]).where(
                and_(*where_clause, self.sites.c.site_url.like('%' + site_url + '%')))
        else:
            rs = select([self.sites]).order_by(asc(self.sites.c.site_id)).limit(limit).offset(offset)
            rs2 = select([func.count(self.sites.c.site_id)])

        count = rs2.execute().fetchone()
        result = rs.execute().fetchall()
        for sites in result:
            try:
                site_dict = {'site_id': sites.site_id, 'site_name': sites.site_name, 'site_url': sites.site_url,
                             'require_sitemap_parsing': sites.require_sitemap_parsing,
                             'require_webpage_parsing': sites.require_webpage_parsing,
                             'require_dynamic_scrapping': sites.require_dynamic_scrapping,
                             'require_category_parsing': sites.require_category_parsing,
                             'require_location_parsing': sites.required_location_parsing,
                             'image_download': sites.image_download,
                             'site_change_status': sites.site_change_status,
                             'site_status':sites.site_status,
                             'parse_status':sites.parse_status,
                             'expiry_check': sites.expiry_check
                             }
                res = select([func.count(self.products.c.product_id)]).where(self.products.c.site_id == sites.site_id)
                # print(res)
                data = res.execute().fetchone()
                # print(data)
                site_dict.update({'total_products': data[0]})
                sites_list.append(site_dict)
            except Exception as e:
                print(str(e))
        return sites_list, count[0]

    def create_site(self, createValuesDict):
        try:
            self.engine.execute(self.sites.insert(), [createValuesDict])
            get_logger().debug('created sites details successfully.')
            where_clauses = [self.sites.c[key] == value for (key, value) in createValuesDict.items()]
            get_site = ((select([self.sites])).where(and_(*where_clauses)))
            rs = get_site.execute()
            result = rs.fetchone()
            get_logger().debug('Created new site config successfully : %s', result)
            return {'status': 'success', 'data': result[0], 'message': 'Created site successfully'}
        except Exception as e:
            print(e)
            get_logger().error('Error in creating site config :', exc_info=True)
            return {'status': 'failure', 'message': 'Error in creating site'}
        #
        # else:
        #     get_logger().debug('Updated site successfully.')
        #     return {'status': 'success', 'message': 'Updated site successfully'}

    def update_site(self, updateValuesDict={}):
        try:
            print(updateValuesDict)
            stmt = (self.sites.update().where(self.sites.c.site_id == updateValuesDict['site_id']).
                    values(updateValuesDict))
            rs = stmt.execute()
        except Exception as e:
            print(e)
            get_logger().error('Error in updating site config :', exc_info=True)
            return {'status': 'failure', 'message': 'Error in updating site details'}

        else:
            get_logger().debug('Updated site successfully.')
            return {'status': 'success', 'message': 'Updated site successfully'}

    def delete_site(self, site_id):
        try:
            self.delete_sitemap_parser_details(site_id)
            self.delete_dynamic_scrapper_details(site_id)
            self.delete_location_parser_details(site_id)
            self.delete_webpage_parser_details(site_id)
            self.delete_location_webparser_details(site_id)

            stmt = (self.sites.delete().where(self.sites.c.site_id == site_id))
            rs = stmt.execute()
            get_logger().debug('Deleted site successfully.')
            return {'status': 'success', 'message': 'Deleted site details  successfully'}

        except Exception as e:
            print(e)
            get_logger().error('Error in deleting site :', exc_info=True)
            return {'status': 'failure', 'message': str(e)}

    def get_sitename(self, offset, limit, whereValuesDict={}):
        sites_list = []
        # print('offset:', offset, limit, whereValuesDict)
        if whereValuesDict:
            if 'site_url' in whereValuesDict:
                site_url = whereValuesDict['site_url']
                del whereValuesDict['site_url']
            else:
                site_url = ''
            where_clause = [func.lower(self.sites.c[key]) == func.lower(value) for (key, value) in
                            whereValuesDict.items()]
            rs = select([self.sites]).where(
                and_(*where_clause, self.sites.c.site_url.like('%' + site_url + '%'))).order_by(
                asc(self.sites.c.site_id)).limit(limit).offset(offset)
            rs2 = select([func.count(self.sites.c.site_id)]).where(
                and_(*where_clause, self.sites.c.site_url.like('%' + site_url + '%')))
        else:
            rs = select([self.sites]).order_by(asc(self.sites.c.site_id)).limit(limit).offset(offset)
            rs2 = select([func.count(self.sites.c.site_id)])

        count = rs2.execute().fetchone()
        result = rs.execute().fetchall()
        for sites in result:
            try:
                site_dict = {'site_id': sites.site_id, 'site_name': sites.site_name, 'site_url': sites.site_url,
                             'require_sitemap_parsing': sites.require_sitemap_parsing,
                             'require_webpage_parsing': sites.require_webpage_parsing,
                             'require_dynamic_scrapping': sites.require_dynamic_scrapping,
                             'require_category_parsing': sites.require_category_parsing,
                             'require_location_parsing': sites.required_location_parsing,
                             'image_download': sites.image_download,
                             'site_change_status': sites.site_change_status,
                             }
                res = select([func.count(self.products.c.product_id)]).where(self.products.c.site_id == sites.site_id)
                data = res.execute().fetchone()
                site_dict.update({'total_products': data[0]})
                sites_list.append(site_dict)
            except Exception as e:
                print(str(e))
        return sites_list, count[0]

    # Job related operations
    # jobs with filter and pagination
    def get_jobs(self, offset, limit, whereValuesDict={}):
        jobs_list = []
        # j = self.scheduled_jobs.join(self.sites, self.scheduled_jobs.c.site_id == self.sites.c.site_id)
        if whereValuesDict:
            where_clauses = [self.scheduled_jobs.c[key] == value for (key, value) in whereValuesDict.items()]
            print(where_clauses)

            rs = ((select([self.scheduled_jobs, self.sites.c.site_name])).where(and_(*where_clauses))).order_by(
                desc(self.scheduled_jobs.c.job_id)).limit(limit).offset(offset)
            rs2 = select([func.count(self.scheduled_jobs.c.job_id)]).where(and_(*where_clauses))
        else:
            rs2 = select([func.count(self.scheduled_jobs.c.job_id)])
            rs = select([self.scheduled_jobs, self.sites.c.site_name]).order_by(
                desc(self.scheduled_jobs.c.job_id)).limit(limit).offset(offset)
        count = rs2.execute().fetchone()
        result = rs.execute().fetchall()
        # data = result.fetchall()
        for job in result:
            jobs_list.append({'job_id': job.job_id,
                              'site_id': job.site_id,
                              'total_products_scraped': job.total_products_scraped,
                              'total_products_attempted': job.total_products_attempted,
                              'project_id': job.project_id,
                              'start_time': str(job.start_time.strftime('%Y-%m-%dT%H:%M:%S')) if job.start_time else '',
                              'file_name': job.file_name,
                              'end_time': str(job.end_time.strftime('%Y-%m-%dT%H:%M:%S')) if job.end_time else '',
                              'job_type': job.job_type,
                              'timeout': job.timeout,
                              'status': job.status,
                              'update_time': str(
                                  job.update_time.strftime('%Y-%m-%dT%H:%M:%S')) if job.update_time else '',
                              'comments': job.comments,
                              'total_products_to_scrape': job.total_products_scraped,
                              })
        return jobs_list, count[0]

    def get_products(self, offset, limit, whereValuesDict={}):

        products_list = []
        # print (whereValuesDict,limit,offset)
        j = self.products.outerjoin(self.sites, self.products.c.site_id == self.sites.c.site_id)

        product_name = ''
        product_upc = ''
        start_date = ''
        end_date = ''
        product_status = ''

        # print(j)
        # print(whereValuesDict)
        # filter data into two different dicts.
        site_where_dict = {}
        if 'site_name' in whereValuesDict:
            site_where_dict['site_name'] = whereValuesDict['site_name']
            del whereValuesDict['site_name']
        if whereValuesDict or site_where_dict:
            if 'product_name' in whereValuesDict:
                product_name = whereValuesDict['product_name']
                del whereValuesDict['product_name']
            if 'product_upc' in whereValuesDict:
                product_upc = whereValuesDict['product_upc']
                # print(product_upc)
                del whereValuesDict['product_upc']
            if 'start_date' in whereValuesDict:
                start_date = whereValuesDict['start_date']
                del whereValuesDict['start_date']
            if 'end_date' in whereValuesDict:
                end_date = whereValuesDict['end_date']
                del whereValuesDict['end_date']

            if 'product_status' in whereValuesDict:
                product_status = whereValuesDict['product_status']
                del whereValuesDict['product_status']

            # else:
            #     product_name = ''
            #     product_upc = ''

            where_clause_1 = [self.products.c[key] == value for (key, value) in whereValuesDict.items()]
            where_clause_2 = [self.sites.c[key] == value for (key, value) in site_where_dict.items()]
            where_clauses = where_clause_1 + where_clause_2
            # print (whereValuesDict)
            # print(where_clauses)

            # rs = ((select([self.products, self.sites.c.site_name])).select_from(j).where(
            #     and_(*where_clauses,self.products.c.product_name.like('%' + product_name + '%')))).order_by(asc(self.products.c.site_id)).limit(limit).offset(offset)
            # rs2 = select([func.count(self.products.c.site_id)]).where(and_(*where_clauses,self.products.c.product_name.like('%' + product_name + '%')))

            where_condition_rs = [*where_clauses]
            if product_upc:
                where_condition_rs.append(self.products.c.product_upc.like(
                    '%' + product_upc + '%'))

            if product_name:
                where_condition_rs.append(self.products.c.product_name.like(
                    '%' + product_name + '%'))

            if start_date and end_date:
                where_condition_rs.append(self.products.c.timestamp.between(start_date, end_date))

            if product_status is not None and product_status !='':
                where_condition_rs.append(self.products.c.product_status == product_status)

            rs = ((select([self.products, self.sites.c.site_name])).select_from(j).where(
                and_(*where_condition_rs))).order_by(
                asc(self.products.c.site_id)).limit(limit).offset(offset)

            rs2 = select([func.count(self.products.c.site_id)]).where(
                and_(*where_condition_rs))

        else:

            rs2 = select([func.count(self.products.c.site_id)])
            rs = select([self.products, self.sites.c.site_name]).select_from(j).order_by(
                asc(self.products.c.site_id)).limit(limit).offset(offset)
            # print(rs)

            rs2 = select([func.count(self.products.c.product_id)])
            rs = select([self.products, self.sites.c.site_name]).select_from(j).order_by(
                asc(self.products.c.product_id)).limit(limit).offset(offset)
        print(rs)

        count = rs2.execute().fetchone()
        result = rs.execute().fetchall()
        # data = result.fetchall()
        for prod in result:
            products_list.append({'product_id': prod.product_id,
                                  'product_upc': prod.product_upc,
                                  'product_name': prod.product_name,
                                  'product_price': prod.product_price,
                                  'site_id': prod.site_id,
                                  'product_url': prod.product_url,
                                  'product_ingredients': prod.product_ingredients,
                                  'product_brand': prod.product_brand,
                                  'product_category': prod.product_category,
                                  'product_subcategory1': prod.product_subcategory1,
                                  'product_subcategory2': prod.product_subcategory2,
                                  'product_imageurl': prod.product_imageurl,
                                  'product_subcategory3': prod.product_subcategory3,
                                  'timestamp': str(
                                      prod.timestamp.strftime('%Y-%m-%dT%H:%M:%S')) if prod.timestamp else '',
                                  'product_size': prod.product_size,
                                  'product_dropdown1': prod.product_dropdown1,
                                  'product_dropdown2': prod.product_dropdown2,
                                  'product_dpci': prod.product_dpci,
                                  'product_desc': prod.product_desc,
                                  'product_packaging': prod.product_packaging,
                                  'product_serving_per_container': prod.product_serving_per_container,
                                  'product_features': prod.product_features,
                                  'product_serving_size': prod.product_serving_size,
                                  'product_flavor': prod.product_flavor,
                                  'product_type': prod.product_type,
                                  'product_assembled_product_dimensions': prod.product_assembled_product_dimensions,
                                  'product_model': prod.product_model,
                                  'product_sku': prod.product_sku,
                                  'product_manufacturer_part_number': prod.product_manufacturer_part_number,
                                  'product_status': prod.product_status,
                                  'product_additional_images': prod.product_additional_images,
                                  'product_nutrition_info': prod.product_nutrition_info,
                                  'product_feeding_instructions': prod.product_feeding_instructions,
                                  'product_total_reviews': prod.product_total_reviews,
                                  'product_rating': prod.product_rating,
                                  'product_customer_reviews': prod.product_customer_reviews,
                                  'product_additional_sizes': prod.product_additional_sizes,
                                  'product_rating_image_url': prod.product_rating_image_url,
                                  'product_discount_price': prod.product_discount_price,
                                  'product_offer_category': prod.product_offer_category,
                                  'site_name': prod.site_name,
                                  'product_recommended_retail_price': prod.product_recommended_retail_price,})
        return products_list, count[0]

    # SITEMAP RELATED OPERATIONS
    def get_sitemap_parser_details(self, offset, limit, whereValuesDict={}):
        sitemaplist = []
        try:
            j = self.sitemap_parser_details.outerjoin(self.sites,
                                                      self.sitemap_parser_details.c.site_id == self.sites.c.site_id)
            site_id = ''
            sitemap_url = ''
            # print(j)
            # print(whereValuesDict)
            # filter data into two different dicts.
            site_where_dict = {}
            if 'site_name' in whereValuesDict:
                site_where_dict['site_name'] = whereValuesDict['site_name']
                del whereValuesDict['site_name']
            if whereValuesDict or site_where_dict:
                if 'site_id' in whereValuesDict:
                    site_id = whereValuesDict['site_id']
                    del whereValuesDict['site_id']
                if 'sitemap_url' in whereValuesDict:
                    sitemap_url = whereValuesDict['sitemap_url']
                    del whereValuesDict['sitemap_url']
                where_clause_1 = [self.sitemap_parser_details.c[key] == value for (key, value) in
                                  whereValuesDict.items()]
                where_clause_2 = [self.sites.c[key] == value for (key, value) in site_where_dict.items()]
                where_clauses = where_clause_1 + where_clause_2
                # print (whereValuesDict)
                # print(where_clauses)
                if not site_id and not sitemap_url:
                    where_condition_rs = [*where_clauses]

                elif site_id and sitemap_url:
                    where_condition_rs = [*where_clauses,
                                          self.sitemap_parser_details.c.site_id == site_id,
                                          self.sitemap_parser_details.c.sitemap_url == sitemap_url]

                elif site_id and not sitemap_url:
                    where_condition_rs = [*where_clauses,
                                          self.sitemap_parser_details.c.site_id == site_id]

                elif not site_id and sitemap_url:
                    where_condition_rs = [*where_clauses,
                                          self.sitemap_parser_details.c.sitemap_url == sitemap_url]

                rs = ((select([self.sitemap_parser_details, self.sites.c.site_name])).select_from(j).where(
                    and_(*where_condition_rs))).order_by(
                    asc(self.sitemap_parser_details.c.site_id)).limit(limit).offset(offset)

                rs2 = select([func.count(self.sitemap_parser_details.c.site_id)]).where(
                    and_(*where_condition_rs))

            else:

                rs2 = select([func.count(self.sitemap_parser_details.c.site_id)])
                rs = select([self.sitemap_parser_details, self.sites.c.site_name]).select_from(j).order_by(
                    asc(self.sitemap_parser_details.c.site_id)).limit(limit).offset(offset)
                # print(rs)

                rs2 = select([func.count(self.sitemap_parser_details.c.sitemap_parser_id)])
                rs = select([self.sitemap_parser_details, self.sites.c.site_name]).select_from(j).order_by(
                    asc(self.sitemap_parser_details.c.sitemap_parser_id)).limit(limit).offset(offset)

            count = rs2.execute().fetchone()
            result = rs.execute().fetchall()

            for sitemap in result:
                sitemaplist.append({
                    'sitemap_parser_id': sitemap.sitemap_parser_id,
                    'site_id': sitemap.site_id, 'sitemap_url': sitemap.sitemap_url,
                    'sitemap_parser_pattern': sitemap.sitemap_parser_pattern,
                    'sitemap_parser_zipped': sitemap.sitemap_parser_zipped,
                    'sitemap_product_tag': sitemap.sitemap_product_tag,
                    'sitemap_product_url_pattern': sitemap.sitemap_product_url_pattern,
                    'sitemap_xml_namespace': sitemap.sitemap_xml_namespace,
                    'site_name': sitemap.site_name
                })
            get_logger().debug('Listing sitemap configuration using following values. %s' % str(whereValuesDict))
            return sitemaplist, count[0]

        except Exception as e:
            get_logger().error('Error in listing sitemap details:', exc_info=True)
            return {'status': 'failure', 'message': 'Error in creating sitemap'}

        # To create sitemap parser details

    def create_sitemap_parser(self, insertValuesDict={}):
        try:

            self.engine.execute(self.sitemap_parser_details.insert(), [insertValuesDict])
            get_logger().debug('added sitemap parser details successfully.')
            where_clauses = [self.sitemap_parser_details.c[key] == value for (key, value) in insertValuesDict.items()]
            get_sitemap = ((select([self.sitemap_parser_details])).where(and_(*where_clauses)))
            rs = get_sitemap.execute()
            result = rs.fetchone()
            get_logger().debug('Creating sitemap config with following values: %s', result)
            return {'status': 'success', 'data': result[0], 'message': 'Created sitemap successfully'}
        except Exception as e:
            get_logger().error('Error in creating sitemap configuration :', exc_info=True)
            return {'status': 'failure', 'message': 'Error in creating sitemap'}

        # To update Sitemap

    def update_sitemap_parser(self, updateValuesDict={}):

        try:
            print(updateValuesDict)
            stmt = (self.sitemap_parser_details.update().where(
                self.sitemap_parser_details.c.site_id == updateValuesDict['site_id']).
                    values(updateValuesDict))
            rs = stmt.execute()
        except Exception as e:
            print(e)
            get_logger().error('Error in updating site config :', exc_info=True)
            return {'status': 'failure', 'message': 'Error in updating sitemap parser details'}

        else:
            get_logger().debug('Updated sitemap parser details successfully.')
            return {'status': 'success', 'message': 'Updated sitemap parser details successfully'}

        # Delete Sitemap Parser

    def delete_sitemap_parser_details(self, site_id):
        try:
            stmt = (self.sitemap_parser_details.delete().where(self.sitemap_parser_details.c.site_id == site_id))
            rs = stmt.execute()
            get_logger().debug('Deleting sitemap config with following site_id: %s', site_id)
        except Exception as e:
            print(e)
            get_logger().error('Error in deleting sitemap :', exc_info=True)
            return {'status': 'failure', 'message': 'Error in deleting site_map'}
        else:
            get_logger().debug('Deleted sitemap successfully.')
            return {'status': 'success', 'message': 'Deleted sitemap details  successfully'}

    # dynamic scrapper details related operations
    # dynamic data with filter , count and paginated results
    def get_dynamic_scrapper_details(self, offset, limit, whereValuesDict={}):
        dynamic_list = []
        try:

            j = self.dynamic_scrapper_details.outerjoin(self.sites,
                                                        self.dynamic_scrapper_details.c.site_id == self.sites.c.site_id)
            search_result_type = ''
            base_search_url = ''

            site_where_dict = {}
            if 'site_name' in whereValuesDict:
                site_where_dict['site_name'] = whereValuesDict['site_name']
                del whereValuesDict['site_name']
            if whereValuesDict or site_where_dict:
                if 'base_search_url' in whereValuesDict:
                    base_search_url = whereValuesDict['base_search_url']
                    del whereValuesDict['base_search_url']
                if 'search_result_type' in whereValuesDict:
                    search_result_type = whereValuesDict['search_result_type']
                    del whereValuesDict['search_result_type']
                where_clause_1 = [self.dynamic_scrapper_details.c[key] == value for (key, value) in
                                  whereValuesDict.items()]
                where_clause_2 = [self.sites.c[key] == value for (key, value) in site_where_dict.items()]
                where_clauses = where_clause_1 + where_clause_2
                # print (whereValuesDict)
                # print(where_clauses)
                if not base_search_url and not search_result_type:
                    where_condition_rs = [*where_clauses]

                elif base_search_url and search_result_type:
                    where_condition_rs = [*where_clauses,
                                          self.dynamic_scrapper_details.c.base_search_url == base_search_url,
                                          self.dynamic_scrapper_details.c.search_result_type == search_result_type]

                elif base_search_url and not search_result_type:
                    where_condition_rs = [*where_clauses,
                                          self.dynamic_scrapper_details.c.base_search_url == base_search_url]

                elif not base_search_url and search_result_type:
                    where_condition_rs = [*where_clauses,
                                          self.dynamic_scrapper_details.c.search_result_type == search_result_type]

                rs = ((select([self.dynamic_scrapper_details, self.sites.c.site_name])).select_from(j).where(
                    and_(*where_condition_rs))).order_by(
                    asc(self.dynamic_scrapper_details.c.site_id)).limit(limit).offset(offset)

                rs2 = select([func.count(self.dynamic_scrapper_details.c.site_id)]).where(
                    and_(*where_condition_rs))

            else:

                rs2 = select([func.count(self.dynamic_scrapper_details.c.site_id)])
                rs = select([self.dynamic_scrapper_details, self.sites.c.site_name]).select_from(j).order_by(
                    asc(self.dynamic_scrapper_details.c.site_id)).limit(limit).offset(offset)
                # print(rs)

                rs2 = select([func.count(self.dynamic_scrapper_details.c.dynamic_scrapper_id)])
                rs = select([self.dynamic_scrapper_details, self.sites.c.site_name]).select_from(j).order_by(
                    asc(self.dynamic_scrapper_details.c.dynamic_scrapper_id)).limit(limit).offset(offset)

            count = rs2.execute().fetchone()
            result = rs.execute().fetchall()
            for dynamic in result:
                dynamic_list.append({
                    'dynamic_scrapper_id': dynamic.dynamic_scrapper_id,
                    'site_id': dynamic.site_id,
                    'base_search_url': dynamic.base_search_url,
                    'search_results_xpath': dynamic.search_results_xpath,
                    'search_result_accuracy': dynamic.search_result_accuracy,
                    'dynamic_scrapper_status': dynamic.dynamic_scrapper_status,
                    'search_result_type': dynamic.search_result_type,
                    'search_result_json_xpath': dynamic.search_result_json_xpath,
                    'parse_with_selenium': dynamic.parse_with_selenium,
                    'append_srchtxt_to_url': dynamic.append_srchtxt_to_url,
                    'driver_id': dynamic.driver_id,
                    'upc_length': dynamic.upc_length,
                    'product_name': dynamic.product_name,
                    'site_name': dynamic.site_name

                })
            return dynamic_list, count[0]
        except Exception as e:
            get_logger().error('Error in listing dynamic parser details:', exc_info=True)
            return {'status': 'failure', 'message': 'Error in creating dynamic parser'}

    # To create dynamic  parser details

    def create_dynamic_scrapper_details(self, createValuesDict):
        try:
            self.engine.execute(self.dynamic_scrapper_details.insert(), [createValuesDict])
            get_logger().debug('created sites details successfully.')
            where_clauses = [self.dynamic_scrapper_details.c[key] == value for (key, value) in createValuesDict.items()]
            get_site = ((select([self.dynamic_scrapper_details])).where(and_(*where_clauses)))
            rs = get_site.execute()
            result = rs.fetchone()
            get_logger().debug('Created new site config successfully : %s', result)
            return {'status': 'success', 'data': result[0], 'message': 'Created site successfully'}
        except Exception as e:
            print(e)

    # To update dynamic scrapper details

    def update_dynamic_scrapper_details(self, updateValuesDict={}):
        try:
            stmt = (self.dynamic_scrapper_details.update().where(
                self.dynamic_scrapper_details.c.site_id == updateValuesDict['site_id']).values(updateValuesDict))
            rs = stmt.execute()
        except Exception as e:
            print(e)
            get_logger().error('Error in creating dynamic scrapper config :', exc_info=True)
            return {'status': 'failure', 'message': 'Error in creating dynamic scrapper details'}

        else:
            get_logger().debug('updated new update dynamic scrapper details  successfully.')
            return {'status': 'success', 'message': 'updated dynamic scrapper details successfully'}

    # To create dynamic scrapper details
    def delete_dynamic_scrapper_details(self, site_id):
        try:
            stmt = (self.dynamic_scrapper_details.delete().where(
                self.dynamic_scrapper_details.c.site_id == site_id))
            rs = stmt.execute()

        except Exception as e:
            print(e)
            get_logger().error('Error in creating dynamic scrapper config :', exc_info=True)
            return {'status': 'failure', 'message': 'Error in deleting dynamic scrapper details'}
        else:
            get_logger().debug('Deleted dynamic scrapper details successfully.')
            return {'status': 'success', 'message': 'Deleted dynamic scrapper details  successfully'}

        # location parser details operations
        # location  parser details with filter and pagination results

    def get_location_parser_details(self, offset, limit, whereValuesDict={}):
        locationparser_list = []
        try:

            j = self.location_parser_details.outerjoin(self.sites,
                                                       self.location_parser_details.c.site_id == self.sites.c.site_id)
            site_id = ''
            parse_with_selenium = ''
            # print(j)
            # print(whereValuesDict)
            # filter data into two different dicts.
            site_where_dict = {}
            if 'site_name' in whereValuesDict:
                site_where_dict['site_name'] = whereValuesDict['site_name']
                del whereValuesDict['site_name']
            if whereValuesDict or site_where_dict:
                if 'site_id' in whereValuesDict:
                    site_id = whereValuesDict['site_id']
                    del whereValuesDict['site_id']
                if 'parse_with_selenium' in whereValuesDict:
                    parse_with_selenium = whereValuesDict['parse_with_selenium']
                    del whereValuesDict['parse_with_selenium']
                where_clause_1 = [self.location_parser_details.c[key] == value for (key, value) in
                                  whereValuesDict.items()]
                where_clause_2 = [self.sites.c[key] == value for (key, value) in site_where_dict.items()]
                where_clauses = where_clause_1 + where_clause_2
                # print (whereValuesDict)
                # print(where_clauses)
                if not site_id and not parse_with_selenium:
                    where_condition_rs = [*where_clauses]

                elif site_id and parse_with_selenium:
                    where_condition_rs = [*where_clauses, self.location_parser_details.c.site_id == site_id,
                                          self.location_parser_details.c.parse_with_selenium == parse_with_selenium]

                elif site_id and not parse_with_selenium:
                    where_condition_rs = [*where_clauses, self.location_parser_details.c.site_id == site_id]

                elif not site_id and parse_with_selenium:
                    where_condition_rs = [*where_clauses,
                                          self.location_parser_details.c.parse_with_selenium == parse_with_selenium]

                rs = ((select([self.location_parser_details, self.sites.c.site_name])).select_from(j).where(
                    and_(*where_condition_rs))).order_by(
                    asc(self.location_parser_details.c.site_id)).limit(limit).offset(offset)

                rs2 = select([func.count(self.location_parser_details.c.site_id)]).where(
                    and_(*where_condition_rs))

            else:

                rs2 = select([func.count(self.location_parser_details.c.site_id)])
                rs = select([self.location_parser_details, self.sites.c.site_name]).select_from(j).order_by(
                    asc(self.location_parser_details.c.site_id)).limit(limit).offset(offset)
                # print(rs)

                rs2 = select([func.count(self.location_parser_details.c.id)])
                rs = select([self.location_parser_details, self.sites.c.site_name]).select_from(j).order_by(
                    asc(self.location_parser_details.c.id)).limit(limit).offset(offset)

            count = rs2.execute().fetchone()
            result = rs.execute().fetchall()
            for location in result:
                locationparser_list.append({
                    'id': location.id,
                    'site_id': location.site_id,
                    'site_url': location.site_url,
                    'search_xpath': location.search_xpath,
                    'category_xpath': location.category_xpath,
                    'subcategory1_xpath': location.subcategory1_xpath,
                    'subcategory2_xpath': location.subcategory2_xpath,
                    'pagination_xpath': location.pagination_xpath,
                    'zero_depth_xpath': location.zero_depth_xpath,
                    'product_url_xpath': location.product_url_xpath,
                    'parse_with_selenium': location.parse_with_selenium,
                    'driver_id': location.driver_id,
                    'site_name': location.site_name})
            get_logger().debug(
                'Listing location parser configuration using following values. %s' % str(whereValuesDict))
            return locationparser_list, count[0]

        except Exception as e:
            get_logger().error('Error in listing location details:', exc_info=True)
            return {'status': 'failure', 'message': 'Error in creating location parser'}


    def create_location_parser_details(self, createValuesDict):
        try:
            self.engine.execute(self.location_parser_details.insert(), [createValuesDict])
            get_logger().debug('created location parser details successfully.')
            where_clauses = [self.location_parser_details.c[key] == value for (key, value) in createValuesDict.items()]
            get_location_parser_details = ((select([self.location_parser_details])).where(and_(*where_clauses)))
            rs = get_location_parser_details.execute()
            result = rs.fetchone()
            get_logger().debug('Created new location parser config successfully : %s', result)
            return {'status': 'success', 'data': result[0],
                    'message': 'Created new location parser config successfully'}
        except Exception as e:
            print(e)
            get_logger().error('Error in creating location parser config :', exc_info=True)
            return {'status': 'failure', 'message': 'Error in creating location parser details'}

    # To update location parser details

    def update_location_parser_details(self, updateValuesDict={}):
        try:
            stmt = (self.location_parser_details.update().where(
                self.location_parser_details.c.site_id == updateValuesDict['site_id']).values(updateValuesDict))
            rs = stmt.execute()
        except Exception as e:
            print(e)
            get_logger().error('Error in creating location parser config :', exc_info=True)
            return {'status': 'failure', 'message': 'Error in creating location parser details'}

        else:
            get_logger().debug('updated new location parser details  successfully.')
            return {'status': 'success', 'message': 'updated location parser details successfully'}

    # To delete location parser details

    def delete_location_parser_details(self, site_id):
        try:
            stmt = (self.location_parser_details.delete().where(self.location_parser_details.c.site_id == site_id))
            rs = stmt.execute()

        except Exception as e:
            print(e)
            get_logger().error('Error in creating location parser config :', exc_info=True)
            return {'status': 'failure', 'message': 'Error in deleting location parser details '}
        else:
            get_logger().debug('Deleted location parser successfully.')
            return {'status': 'success', 'message': 'Deleted location parser details  successfully'}

    # Web page parser details operations
    # Web page  parser details with filter and pagination results

    def get_webpage_parser_details(self, offset, limit, whereValuesDict={}):
        webpage_list = []
        try:

            products_list = []
            # print (whereValuesDict,limit,offset)
            j = self.webpage_parser_details.outerjoin(self.sites,
                                                      self.webpage_parser_details.c.site_id == self.sites.c.site_id)
            site_id = ''
            parse_with_selenium = ''

            site_where_dict = {}
            if 'site_name' in whereValuesDict:
                site_where_dict['site_name'] = whereValuesDict['site_name']
                del whereValuesDict['site_name']
            if whereValuesDict or site_where_dict:
                if 'site_id' in whereValuesDict:
                    site_id = whereValuesDict['site_id']
                    del whereValuesDict['site_id']
                if 'parse_with_selenium' in whereValuesDict:
                    parse_with_selenium = whereValuesDict['parse_with_selenium']
                    del whereValuesDict['parse_with_selenium']
                where_clause_1 = [self.webpage_parser_details.c[key] == value for (key, value) in
                                  whereValuesDict.items()]
                where_clause_2 = [self.sites.c[key] == value for (key, value) in site_where_dict.items()]
                where_clauses = where_clause_1 + where_clause_2
                # print (whereValuesDict)
                # print(where_clauses)
                if not site_id and not parse_with_selenium:
                    where_condition_rs = [*where_clauses]

                elif site_id and parse_with_selenium:
                    where_condition_rs = [*where_clauses, self.webpage_parser_details.c.site_id == site_id,
                                          self.webpage_parser_details.c.parse_with_selenium == parse_with_selenium]

                elif site_id and not parse_with_selenium:
                    where_condition_rs = [*where_clauses, self.webpage_parser_details.c.site_id == site_id]

                elif not site_id and parse_with_selenium:
                    where_condition_rs = [*where_clauses,
                                          self.webpage_parser_details.c.parse_with_selenium == parse_with_selenium]

                rs = ((select([self.webpage_parser_details, self.sites.c.site_name])).select_from(j).where(
                    and_(*where_condition_rs))).order_by(
                    asc(self.webpage_parser_details.c.site_id)).limit(limit).offset(offset)

                rs2 = select([func.count(self.webpage_parser_details.c.site_id)]).where(
                    and_(*where_condition_rs))

            else:

                rs2 = select([func.count(self.webpage_parser_details.c.site_id)])
                rs = select([self.webpage_parser_details, self.sites.c.site_name]).select_from(j).order_by(
                    asc(self.webpage_parser_details.c.site_id)).limit(limit).offset(offset)
                # print(rs)

                rs2 = select([func.count(self.webpage_parser_details.c.webpage_parser_id)])
                rs = select([self.webpage_parser_details, self.sites.c.site_name]).select_from(j).order_by(
                    asc(self.webpage_parser_details.c.webpage_parser_id)).limit(limit).offset(offset)

            count = rs2.execute().fetchone()
            result = rs.execute().fetchall()
            for webpage in result:
                webpage_list.append({
                    'webpage_parser_id': webpage.webpage_parser_id,
                    'site_id': webpage.site_id,
                    'product_upc_xpath': webpage.product_upc_xpath,
                    'product_name_xpath': webpage.product_name_xpath,
                    'product_price_xpath': webpage.product_price_xpath,
                    'product_packaging_xpath': webpage.product_packaging_xpath,
                    'product_imageurl_xpath': webpage.product_imageurl_xpath,
                    'webpage_parser_status': webpage.webpage_parser_status,
                    'product_ingredients_xpath': webpage.product_ingredients_xpath,
                    'product_brand_xpath': webpage.product_brand_xpath,
                    'product_category_xpath': webpage.product_category_xpath,
                    'product_subcategory1_xpath': webpage.product_subcategory1_xpath,
                    'product_subcategory2_xpath': webpage.product_subcategory2_xpath,
                    'product_subcategory3_xpath': webpage.product_subcategory3_xpath,
                    'parse_with_selenium': webpage.parse_with_selenium,
                    'product_size_xpath': webpage.product_size_xpath,
                    'driver_id': webpage.driver_id,
                    'product_dropdown1_xpath': webpage.product_dropdown1_xpath,
                    'product_dropdown2_xpath': webpage.product_dropdown2_xpath,
                    'product_dpci_xpath': webpage.product_dpci_xpath,
                    'product_desc_xpath': webpage.product_desc_xpath,
                    'product_serving_per_container_xpath': webpage.product_serving_per_container_xpath,
                    'product_features_xpath': webpage.product_features_xpath,
                    'product_serving_size_xpath': webpage.product_serving_size_xpath,
                    'product_flavor_xpath': webpage.product_flavor_xpath,
                    'product_type_xpath': webpage.product_type_xpath,
                    'product_assembled_product_dimensions_xpath': webpage.product_assembled_product_dimensions_xpath,
                    'product_model_xpath': webpage.product_model_xpath,
                    'product_sku_xpath': webpage.product_sku_xpath,
                    'product_manufacturer_part_number_xpath': webpage.product_manufacturer_part_number_xpath,
                    'product_additional_images_xpath': webpage.product_additional_images_xpath,
                    'product_nutrition_info_xpath': webpage.product_nutrition_info_xpath,
                    'product_feeding_instructions_xpath': webpage.product_feeding_instructions_xpath,
                    'product_total_reviews_xpath': webpage.product_total_reviews_xpath,
                    'product_rating_xpath': webpage.product_rating_xpath,
                    'product_customer_reviews_xpath': webpage.product_customer_reviews_xpath,
                    'product_additional_sizes_xpath': webpage.product_additional_sizes_xpath,
                    'product_rating_image_url_xpath': webpage.product_rating_image_url_xpath,
                    'test_product_url': webpage.test_product_url,
                    'site_name': webpage.site_name,
                    'product_discount_price_xpath': webpage.product_discount_price_xpath,
                    'product_offer_category_xpath': webpage.product_offer_category_xpath,
                    'product_recommended_retail_price_xpath': webpage.product_recommended_retail_price_xpath,

                })
            return webpage_list, count[0]
        except Exception as e:
            get_logger().error('Error in listing web page parser details:', exc_info=True)
            return {'status': 'failure', 'message': 'Error in creating webpage parser'}


    # To create Webpage parser details

    def create_webpage_parser_details(self, createValuesDict):
        try:
            self.engine.execute(self.webpage_parser_details.insert(), [createValuesDict])
            get_logger().debug('created web page parser details successfully.')
            where_clauses = [self.webpage_parser_details.c[key] == value for (key, value) in createValuesDict.items()]
            get_webpage_parser_details = ((select([self.webpage_parser_details])).where(and_(*where_clauses)))
            rs = get_webpage_parser_details.execute()
            result = rs.fetchone()
            get_logger().debug('Created new webpage parser config successfully : %s', result)
            return {'status': 'success', 'data': result[0], 'message': 'Created new webpage parser config successfully'}
        except Exception as e:
            print(e)
            get_logger().error('Error in creating webpage parser config :', exc_info=True)
            return {'status': 'failure', 'message': 'Error in creating webpage parser details'}


    def update_webpage_parser_details(self, updateValuesDict={}):
        try:
            stmt = (self.webpage_parser_details.update().where(
                self.webpage_parser_details.c.site_id == updateValuesDict['site_id']).values(updateValuesDict))
            rs = stmt.execute()
        except Exception as e:
            print(e)
            get_logger().error('Error in creating webpage parser config :', exc_info=True)
            return {'status': 'failure', 'message': 'Error in creating webpage parser details'}

        else:
            get_logger().debug('updated new webpage parser details  successfully.')
            return {'status': 'success', 'message': 'updated web page parser details successfully'}

    def delete_webpage_parser_details(self, site_id):
        try:
            stmt = (self.webpage_parser_details.delete().where(self.webpage_parser_details.c.site_id == site_id))
            rs = stmt.execute()

        except Exception as e:
            print(e)
            get_logger().error('Error in creating webapge parser config :', exc_info=True)
            return {'status': 'failure', 'message': 'Error in deleting webpage parser details '}
        else:
            get_logger().debug('Deleted webpage parser successfully.')
            return {'status': 'success', 'message': 'Deleted webpage parser details  successfully'}


    def get_location_webparser_details(self, offset, limit, whereValuesDict={}):
        locationwebparser_list = []
        try:
            j = self.location_webparser_details.outerjoin(self.sites,
                                                          self.location_webparser_details.c.site_id == self.sites.c.site_id)
            site_id = ''
            # filter data into two different dicts.
            site_where_dict = {}
            if 'site_name' in whereValuesDict:
                site_where_dict['site_name'] = whereValuesDict['site_name']
                del whereValuesDict['site_name']
            if whereValuesDict or site_where_dict:
                if 'site_id' in whereValuesDict:
                    site_id = whereValuesDict['site_id']
                    del whereValuesDict['site_id']
                where_clause_1 = [self.location_webparser_details.c[key] == value for (key, value) in
                                  whereValuesDict.items()]
                where_clause_2 = [self.sites.c[key] == value for (key, value) in site_where_dict.items()]
                where_clauses = where_clause_1 + where_clause_2
                # print (whereValuesDict)
                # print(where_clauses)
                if not site_id:
                    where_condition_rs = [*where_clauses]
                elif site_id:
                    where_condition_rs = [*where_clauses,
                                          self.location_webparser_details.c.site_id == site_id]
                rs = ((select([self.location_webparser_details, self.sites.c.site_name])).select_from(j).where(
                    and_(*where_condition_rs))).order_by(
                    asc(self.location_webparser_details.c.site_id)).limit(limit).offset(offset)

                rs2 = select([func.count(self.location_webparser_details.c.site_id)]).where(
                    and_(*where_condition_rs))

            else:

                rs2 = select([func.count(self.location_webparser_details.c.site_id)])
                rs = select([self.location_webparser_details, self.sites.c.site_name]).select_from(j).order_by(
                    asc(self.location_webparser_details.c.site_id)).limit(limit).offset(offset)
                # print(rs)

                rs2 = select([func.count(self.location_webparser_details.c.id)])
                rs = select([self.location_webparser_details, self.sites.c.site_name]).select_from(j).order_by(
                    asc(self.location_webparser_details.c.id)).limit(limit).offset(offset)

            count = rs2.execute().fetchone()
            result = rs.execute().fetchall()
            for location in result:
                # print('location=',location)
                locationwebparser_list.append({
                    'id': location.id,
                    'site_id': location.site_id,
                    'storename_xpath': location.storename_xpath,
                    'chainname_xpath': location.chainname_xpath,
                    'address_xpath': location.address_xpath,
                    'country_xpath': location.country_xpath,
                    'state_xpath': location.state_xpath,
                    'city_xpath': location.city_xpath,
                    'latitude_xpath': location.latitude_xpath,
                    'longitude_xpath': location.longitude_xpath,
                    'phone_xpath': location.phone_xpath,
                    'email_xpath': location.email_xpath,
                    'workinghours_xpath': location.workinghours_xpath,
                    'storeurl_xpath': location.storeurl_xpath,
                    'site_name': location.site_name
                })
            return locationwebparser_list, count[0]

        except Exception as e:
            get_logger().error('Error in listing sitemap details:', exc_info=True)
            return {'status': 'failure', 'message': 'Error in creating sitemap'}

    # To create location webparser details
    def create_location_webparser_details(self, createValuesDict):
        try:
            self.engine.execute(self.location_webparser_details.insert(), [createValuesDict])
            get_logger().debug('created location webparser details successfully.')
            where_clauses = [self.location_webparser_details.c[key] == value for (key, value) in
                             createValuesDict.items()]
            get_location_webparser_details = ((select([self.location_webparser_details])).where(and_(*where_clauses)))
            rs = get_location_webparser_details.execute()
            result = rs.fetchone()
            get_logger().debug('Created new location webparser config successfully : %s', result)
            return {'status': 'success', 'data': result[0],
                    'message': 'Created new location webparser config successfully'}
        except Exception as e:
            print(e)
            get_logger().error('Error in creating location webparser config :', exc_info=True)
            return {'status': 'failure', 'message': 'Error in creating location webparser details'}

    def update_location_webparser_details(self, updateValuesDict={}):
        try:
            stmt = (self.location_webparser_details.update().where(
                self.location_webparser_details.c.site_id == updateValuesDict['site_id']).values(updateValuesDict))
            rs = stmt.execute()
            # print('stmt=',stmt )
            # print('rs=',rs)
        except Exception as e:
            print(e)
            get_logger().error('Error in creating location webparser config :', exc_info=True)
            return {'status': 'failure', 'message': 'Error in creating location parser details'}

        else:
            get_logger().debug('updated new location webparser details  successfully.')
            return {'status': 'success', 'message': 'updated location webparser details successfully'}

    # To delete location webparser details

    def delete_location_webparser_details(self, site_id):
        try:
            stmt = (self.location_webparser_details.delete().where(
                self.location_webparser_details.c.site_id == site_id))
            rs = stmt.execute()

        except Exception as e:
            print(e)
            get_logger().error('Error in creating location webparser config :', exc_info=True)
            return {'status': 'failure', 'message': 'Error in deleting location webparser details '}
        else:
            get_logger().debug('Deleted location webparser successfully.')
            return {'status': 'success', 'message': 'Deleted location webparser details  successfully'}

    def get_scrapped_location_filter(self, site_id):
        try:
            # query = ((select([self.scrapped_location.c.address,
            #                         self.scrapped_location.c.city,
            #                         self.scrapped_location.c.state,
            #                         self.scrapped_location.c.country])).where(self.scrapped_location.c.site_id == site_id))
            #
            query = ((select([self.scrapped_location])).where(self.scrapped_location.c.site_id == site_id))
            rs = query.execute()
            result = rs.fetchall()
            scrappedlocation_list = []
            for location in result:
                scrappedlocation_list.append({
                    'id': location.id,
                    'site_id': location.site_id,
                    'storename': location.storename,
                    'chainname': location.chainname,
                    'address': location.address,
                    'country': location.country,
                    'state': location.state,
                    'city': location.city,
                    'latitude': location.latitude,
                    'longitude': location.longitude,
                    'phone': location.phone,
                    'email': location.email,
                    'workinghours': location.workinghours,
                    'storeurl': location.storeurl,
                })
            return scrappedlocation_list
        except Exception as e:
            print(str(e))
            return []

    def get_scrapped_location(self, offset, limit, whereValuesDict={}):
        scrappedlocation_list = []
        print('offset:', offset, limit, whereValuesDict)
        if whereValuesDict:
            where_clauses = [self.scrapped_location.c[key] == value for (key, value) in
                             whereValuesDict.items()]

            rs = ((select([self.scrapped_location])).where(and_(*where_clauses))).order_by(
                asc(self.scrapped_location.c.site_id)) \
                .limit(limit).offset(offset)
            rs2 = select([func.count(self.scrapped_location.c.site_id)]).where(and_(*where_clauses))
        else:
            rs2 = select([func.count(self.scrapped_location.c.site_id)])

            rs = select([self.scrapped_location]).order_by(
                asc(self.scrapped_location.c.site_id)).limit(limit).offset(offset)
        count = rs2.execute().fetchone()
        result = rs.execute().fetchall()
        for location in result:
            # print(location)
            scrappedlocation_list.append({
                'id': location.id,
                'site_id': location.site_id,
                'storename': location.storename,
                'chainname': location.chainname,
                'address': location.address,
                'country': location.country,
                'state': location.state,
                'city': location.city,
                'latitude': location.latitude,
                'longitude': location.longitude,
                'phone': location.phone,
                'email': location.email,
                'workinghours': location.workinghours,
                'storeurl': location.storeurl,
            })
        return scrappedlocation_list, count[0]

    # scrape products using csv operations
    # scrape products using csv with filter and pagination results

    def get_csv_template(self, offset, limit, whereValuesDict):
        templatelist = []
        if whereValuesDict:
            where_clauses = [self.csv_template.c[key] == value for (key, value) in whereValuesDict.items()]
            rs = ((select([self.csv_template])).where(and_(*where_clauses))).order_by(
                asc(self.csv_template.c.site_id)).limit(limit).offset(offset)
            rs2 = select([func.count(self.csv_template.c.site_id)]).where(
                and_(*where_clauses))
        else:
            rs2 = select([func.count(self.csv_template.c.site_id)])

            rs = select([self.csv_template]).order_by(
                asc(self.csv_template.c.site_id)).limit(limit).offset(offset)
        count = rs2.execute().fetchone()
        # return result
        result = rs.execute().fetchall()
        # data = result.fetchall()
        for template in result:
            templatelist.append(
                {
                    'id': template.id,
                    'site_id': template.site_id,
                    'product_url_column_in_csv': template.product_url_column_in_csv,
                    'search_columns_in_csv': template.search_columns_in_csv,
                    'search_sites_id': template.search_sites_id,
                    'filter_column': template.filter_column,
                    'filter_value': template.filter_value,
                    'CreationDate': template.CreationDate,
                    'product_upc_column': template.product_upc_column,

                })
        return templatelist, count[0]

    def create_csv_template(self, createValuesDict):
        try:
            self.engine.execute(self.csv_template.insert(), [createValuesDict])
            get_logger().debug('created csv template details successfully.')
            where_clauses = [self.csv_template.c[key] == value for (key, value) in createValuesDict.items()]
            get_template = ((select([self.csv_template])).where(and_(*where_clauses)))
            rs = get_template.execute()
            result = rs.fetchone()
            get_logger().debug('Created new csv template config successfully : %s', result)
        except Exception as e:
            print(e)
            get_logger().error('Error in creating template config :', exc_info=True)
            return {'status': 'failure', 'message': 'Error in creating site'}
        else:
            get_logger().debug('Created new csv template successfully.')
            return {'status': 'success', 'message': 'Created csv template successfully'}


    def get_receivability_tool_config(self, offset, limit, whereValuesDict={}):
        receivabilitylist = []

        print('offset:', offset, limit, whereValuesDict)
        if whereValuesDict:
            where_clauses = [self.receivability_tool_config.c[key] == value for (key, value) in whereValuesDict.items()]
            rs = ((select([self.receivability_tool_config])).where(and_(*where_clauses))).order_by(
                asc(self.receivability_tool_config.c.domain)).limit(limit).offset(offset)
            rs2 = select([func.count(self.receivability_tool_config.c.domain)]).where(and_(*where_clauses))
        else:
            rs2 = select([func.count(self.receivability_tool_config.c.domain)])

            rs = select([self.receivability_tool_config]).order_by(
                asc(self.receivability_tool_config.c.domain)).limit(
                limit).offset(offset)
        count = rs2.execute().fetchone()
        result = rs.execute().fetchall()
        print(count)

        for webpage1 in result:
            receivabilitylist.append({
                'domain': webpage1.domain,
                'main_method': webpage1.main_method,
                'channel_name_xpath': webpage1.channel_name_xpath,
                'channel_number_xpath': webpage1.channel_number_xpath,
                'structure': webpage1.structure,
                'callback': webpage1.callback,
                'scroll_function': webpage1.scroll_function,
                'scroll_reset_function': webpage1.scroll_reset_function,
                'load_url': webpage1.load_url,
                'channel_name': webpage1.channel_name,
                'sort': webpage1.sort, })
        return receivabilitylist, count[0]


    def create_receivability_tool_config(self, createValuesDict):
        try:
            self.engine.execute(self.receivability_tool_config.insert(), [createValuesDict])
            get_logger().debug('created receivability tool config details successfully.')
            where_clauses = [self.receivability_tool_config.c[key] == value for (key, value) in
                             createValuesDict.items()]
            get_receivability_tool_config = (
                (select([self.receivability_tool_config])).where(and_(*where_clauses)))
            rs = get_receivability_tool_config.execute()
            result = rs.fetchone()
            get_logger().debug('Created new receivability tool config successfully : %s', result)
        except Exception as e:
            print(e)
            get_logger().error('Error in creating receivability tool config :', exc_info=True)
            return {'status': 'failure', 'message': 'Error in creating receivability tool config'}
        else:
            get_logger().debug('Created new receivability tool config Successfully.')
            return {'status': 'success', 'message': 'Created receivability tool config successfully'}

    def update_receivability_tool_config(self, updateValuesDict={}):
        try:
            stmt = (self.receivability_tool_config.update().where(
                self.receivability_tool_config.c.domain == updateValuesDict['domain']).values(updateValuesDict))
            rs = stmt.execute()
        except Exception as e:
            print(e)
            get_logger().error('Error in creating receivability_tool_config :', exc_info=True)
            return {'status': 'failure', 'message': 'Error in creating receivability_tool_config'}

        else:
            get_logger().debug('Created new receivability_tool_config Successfully.')
            return {'status': 'success', 'message': 'Created receivability_tool_config successfully'}

    def delete_receivability_tool_config(self, domain):
        try:
            stmt = (self.receivability_tool_config.delete().where(
                self.receivability_tool_config.c.domain == domain))
            rs = stmt.execute()

        except Exception as e:
            print(e)
            get_logger().error('Error in creating receivability_tool_config :', exc_info=True)
            return {'status': 'failure', 'message': 'Error in deleting receivability_tool_config'}
        else:
            get_logger().debug('Deleted receivability_tool_config Successfully.')
            return {'status': 'success', 'message': 'Deleted receivability_tool_config details successfully'}

    def row_dict(self, result):
        result_data = []
        for row in result:
            row_as_dict = dict(row)
            result_data.append(row_as_dict)
        return result_data

    def get_data_import_products(self, Valuesdict={}):
        print('called')
        upc_list = []
        getUpcvalues = ((select([self.import_products])).where(self.import_products.c.job_id == int(
            Valuesdict['job_id'])))
        rs = getUpcvalues.execute()
        results = rs.fetchall()
        print('results', results)
        # upcs = json.loads(self.row_dict(results))
        # print(type(upcs), upc_list)
        return self.row_dict(results)

    def get_import_products(self, offset, limit, whereValuesDict={}):
        jobs_list = []
        if whereValuesDict:
            where_clauses = [self.import_products.c[key] == value for (key, value) in whereValuesDict.items()]
            # print(where_clauses)

            rs = ((select([self.import_products])).where(and_(*where_clauses))).order_by(
                asc(self.import_products.c.file_name)).limit(limit).offset(offset)
            rs2 = select([func.count(self.import_products.c.file_name)]).where(and_(*where_clauses))
        else:
            rs2 = select([func.count(self.import_products.c.file_name)])
            rs = select([self.import_products]).order_by(asc(self.import_products.c.file_name)).limit(
                limit).offset(offset)
        count = rs2.execute().fetchone()
        result = rs.execute().fetchall()
        # data = result.fetchall()
        for job in result:
            jobs_list.append({'id': job.id,
                              'period_code': job.period_code,
                              'product_upc': job.product_upc,
                              'source_id': job.source_id,
                              'product_category': job.product_category,
                              'product_subcategory1': job.product_subcategory1,
                              'product_subcategory2': job.product_subcategory2,
                              'product_subcategory3': job.product_subcategory3,
                              'product_brand': job.product_brand,
                              'product_name': job.product_name,
                              'product_url': job.product_url,
                              'scrap_status': job.scrap_status,
                              'scrap_source': job.scrap_source,
                              'product_mapid': job.product_mapid,
                              'product_discount_price': job.product_discount_price,
                              'product_offer_category': job.product_offer_category,
                              'product_sku': job.product_sku,
                              'product_desc': job.product_desc,
                              'product_ingredients': job.product_ingredients,
                              'product_additional_images': job.product_additional_images,
                              'product_manufacturer_part_number': job.product_manufacturer_part_number,
                              'scope': job.scope,
                              'file_name': job.file_name,
                              'product_price': job.product_price,
                              'product_imageurl': job.product_imageurl,
                              'project_id': job.project_id,
                              'job_id': job.job_id,
                              'update_time': str(
                                  job.update_time.strftime('%Y-%m-%dT%H:%M:%S')) if job.update_time else '', })
        return jobs_list, count[0]


    def get_site_details(self, req_site_id):
        getSiteInfo = (
            (select([self.sites])).where(and_(self.sites.c.site_status == True, self.sites.c.site_id == req_site_id)))
        rs = getSiteInfo.execute()
        return rs.fetchone()

    def get_site_name_by_id(self, req_site_id):
        getSiteName = ((select([self.sites.c.site_name])).where(
            and_(self.sites.c.site_status == True, self.sites.c.site_id == req_site_id)))
        rs = getSiteName.execute()
        return rs.fetchone()

    ##Webelement realted opearations
    def webpage_add_column(self, column_name, column_datatype):
        try:
            get_logger().debug('Creating new column with following names %s', column_name, column_datatype)
            querrystring = ('ALTER TABLE "webpage_parser_details" ADD COLUMN "product_%s_xpath" %s') % \
                           (column_name, column_datatype)
            with self.engine.connect() as con:
                con.execute(querrystring)
        except Exception as e:
            get_logger().error('Error in creating new column:', exc_info=True)
            return {'status': 'failure', 'message': 'Error in creating new column'}
        else:
            get_logger().debug('Created new column successfully.')
            return {'status': 'success', 'message': 'Added New Column successfully'}

    def product_add_column(self, column_name, column_datatype):
        try:
            get_logger().debug('Creating new column with following names %s', column_name, column_datatype)
            querrystring = ('ALTER TABLE "products" ADD COLUMN "product_%s" %s') % \
                           (column_name, column_datatype)
            with self.engine.connect() as con:
                con.execute(querrystring)
        except Exception as e:
            get_logger().error('Error in deleting sitemap :', exc_info=True)
            return {'status': 'failure', 'message': 'Error in creating column'}
        else:
            get_logger().debug('Created new column successfully.')
            return {'status': 'success', 'message': 'Added New Column successfully'}

    def insert_products_bulk(self, insertValues):

        get_logger().debug('called insert_products_bulk for total products : %s', len(insertValues))
        try:
            self.engine.execute(self.products.insert(), insertValues)
        except Exception as e:
            get_logger().error('Error in bulk insert', exc_info=True)
        else:
            get_logger().debug('bulk insert successfull for %s products', len(insertValues))

    def insert_products_single(self, recordToInsert):
        # dt = datetime.now()
        get_logger().debug('insert_products_single : %s', recordToInsert)
        try:
            checkProductExist = None
            if ('site_id' in recordToInsert and recordToInsert['site_id']):
                if ('product_upc' in recordToInsert and recordToInsert['product_upc']):
                    checkProductExist = ((select([self.products])).where(
                        and_(self.products.c.product_upc == recordToInsert['product_upc'],
                             self.products.c.site_id == recordToInsert['site_id'])))
                elif ('product_name' in recordToInsert and recordToInsert['product_name']):
                    checkProductExist = ((select([self.products])).where(
                        and_(self.products.c.product_name == recordToInsert['product_name'],
                             self.products.c.site_id == recordToInsert['site_id'])))
                else:
                    get_logger().error(
                        'Nither product_upc nor product_name was found for the input insert record :[%s]',
                        recordToInsert)
            else:
                get_logger().error('Site Id not found in input insert record :[%s]', recordToInsert)

            if (checkProductExist != None):
                rs = checkProductExist.execute()
                if (rs.rowcount <= 0):
                    self.engine.execute(self.products.insert(), [recordToInsert])
                else:
                    get_logger().info('Input insert product already found in db:[%s]', recordToInsert)
        except Exception as e:
            get_logger().error('Error insert product single', exc_info=True)
        else:
            get_logger().debug('insert product single successfull')

    def get_products_list(self):
        ProductsList = []
        result = ''
        try:

            queryString = (
                'SELECT "webpage_parser_details".site_id,"webpage_parser_details".product_upc_xpath, "webpage_parser_details".product_name_xpath, "webpage_parser_details".product_price_xpath,"webpage_parser_details".product_packaging_xpath, "webpage_parser_details".product_imageurl_xpath, "webpage_parser_details".product_ingredients_xpath,"webpage_parser_details".product_brand_xpath,"webpage_parser_details".product_serving_size_xpath,"webpage_parser_details".product_manufacturer_part_number_xpath,"webpage_parser_details".product_subcategory1_xpath,"webpage_parser_details".product_subcategory2_xpath,"webpage_parser_details".product_subcategory3_xpath,"webpage_parser_details".product_size_xpath  FROM "sites", "webpage_parser_details" WHERE "sites"."site_id" = "webpage_parser_details"."site_id"')

            with self.engine.connect() as con:
                rs = con.execute(queryString)
                for rec in rs:
                    print(rec)
                    ProductsList.append({'id': rec['site_id'], 'product_upc': rec['product_upc_xpath'],
                                         'product_name': rec['product_name_xpath'],
                                         'product_url': rec['product_imageurl_xpath'],
                                         'product_brand': rec['product_brand_xpath'],
                                         'product_manufacturer_part_number': rec[
                                             'product_manufacturer_part_number_xpath'],
                                         'product_category': rec['product_category'],
                                         'product_subcategory1': rec['product_subcategory1_xpath'],
                                         'product_subcategory2': rec['product_subcategory2_xpath'],
                                         'product_subcategory3': rec['product_subcategory3_xpath'],
                                         })

        except:
            get_logger().error('Error in get_products_list', exc_info=True)

        return ProductsList

    def get_product_id(self, upc, site_id):
        product_id = ''
        checkProductExist = ((select([self.products])).where(
            and_(self.products.c.product_upc == upc, self.products.c.site_id == int(site_id))))
        rs = checkProductExist.execute()
        if rs:
            rs_one = rs.fetchone()
            if rs_one:
                product_id = rs_one['product_id']

        return product_id

    def update_import_products(self, productId, updateValuesDict):
        try:
            updateValuesDict['update_time'] = datetime.utcnow()
            upd = (update(self.importProducts).where(self.importProducts.c.id == productId).values(updateValuesDict))
            upd.execute()
            get_logger().debug('updated import_products successfull with values : %s', updateValuesDict)
        except Exception as e:
            get_logger().error('Error in updating import_products :', exc_info=True)
        else:
            get_logger().debug('Updated import_products id : %s with values : %s', productId, updateValuesDict)

    def get_scheduled_running_job(self, project_id):
        get_job = ((select([self.scheduled_jobs])).where(
            and_(self.scheduled_jobs.c.project_id == project_id, or_(self.scheduled_jobs.c.status == 'SCHEDULED',
                                                                     self.scheduled_jobs.c.status == 'STARTED'))))
        rs = get_job.execute()
        result = rs.fetchall()
        return result

    def get_scheduled_job_search(self, whereValuesDict):
        try:
            where_clauses = [self.scheduled_jobs.c[key] == value
                             for (key, value) in whereValuesDict.items()]
            getjobInfo = (
                (select([self.scheduled_jobs])).where(and_(*where_clauses,
                                                           or_(self.scheduled_jobs.c.status == 'SCHEDULED',
                                                               self.scheduled_jobs.c.status == 'STARTED'))))
            rs = getjobInfo.execute()
            result = rs.fetchone()
            return result
        except Exception as e:
            get_logger().error('Error in get_scheduled_job_search :', exc_info=True)
            return []

    def get_scrapped_product_url_list(self, site_id):
        getProdUrls = ((select([self.products.c.product_url])).where(self.products.c.site_id == site_id))
        rs = getProdUrls.execute()
        result = rs.fetchall()
        result = [str(r) for r, in result]
        return result

    def get_scheduled_running_jobs(self, site_id):
        get_job = ((select([self.scheduled_jobs])).where(
            and_(self.scheduled_jobs.c.site_id == site_id, or_(self.scheduled_jobs.c.status == 'SCHEDULED',
                                                               self.scheduled_jobs.c.status == 'STARTED'))))
        rs = get_job.execute()
        result = rs.fetchall()
        return result

    # Scheduled Job realted operations
    # Listing scheduled jobs
    def get_scheduled_jobs(self, offset, limit, whereValuesDict={}):
        jobs_list = []
        if whereValuesDict:
            where_clauses = [self.scheduled_jobs.c[key] == value for (key, value) in whereValuesDict.items()]
            # print(where_clauses)

            rs = ((select([self.scheduled_jobs])).where(and_(*where_clauses))).order_by(
                desc(self.scheduled_jobs.c.job_id)).limit(limit).offset(offset)
            rs2 = select([func.count(self.scheduled_jobs.c.job_id)]).where(and_(*where_clauses))
        else:
            rs2 = select([func.count(self.scheduled_jobs.c.job_id)])
            rs = select([self.scheduled_jobs]).order_by(desc(self.scheduled_jobs.c.job_id)).limit(limit).offset(
                offset)
        count = rs2.execute().fetchone()
        result = rs.execute().fetchall()
        # data = result.fetchall()
        for job in result:
            jobs_list.append({'job_id': job.job_id,
                              'site_id': job.site_id,
                              'total_products_scraped': job.total_products_scraped,
                              'total_products_attempted': job.total_products_attempted,
                              'project_id': job.project_id,
                              'start_time': str(
                                  job.start_time.strftime('%Y-%m-%dT%H:%M:%S')) if job.start_time else '',
                              'file_name': job.file_name,
                              'end_time': str(job.end_time.strftime('%Y-%m-%dT%H:%M:%S')) if job.end_time else '',
                              'job_type': job.job_type,
                              'timeout': job.timeout,
                              'status': job.status,
                              'update_time': str(
                                  job.update_time.strftime('%Y-%m-%dT%H:%M:%S')) if job.update_time else '',
                              'comments': job.comments,
                              'total_products_to_scrape': job.total_products_to_scrape})
        return jobs_list, count[0]

    # Create Scheduled Jobs
    def create_scheduled_jobs(self, updateValuesDict):
        try:
            self.engine.execute(self.scheduled_jobs.insert(), [updateValuesDict])
            get_logger().debug('added scheduled_jobs details successfully.')
            where_clauses = [self.scheduled_jobs.c[key] == value for (key, value) in updateValuesDict.items()]
            get_scheduled_jobs = ((select([self.scheduled_jobs])).where(and_(*where_clauses)))
            rs = get_scheduled_jobs.execute()
            result = rs.fetchone()
            get_logger().debug('Created new scheduled_jobs successfully : %s', result)
            return {'status': 'success', 'data': result[0],
                    'message': 'Created new scheduled_jobs successfully'}
        except Exception as e:
            print(e)
            get_logger().error('Error in creating scheduled_jobs :', exc_info=True)
            return {'status': 'failure', 'message': 'Error in creating scheduled_jobs'}


    # Delete Scheduled Jobs
    def delete_scheduled_jobs(self, deletequery={}):
        try:
            print(deletequery)
            where_clauses = [self.scheduled_jobs.c[key] == value for (key, value) in deletequery.items()]
            stmt = (self.scheduled_jobs.delete().where(and_(*where_clauses)))
            rs = stmt.execute()

        except Exception as e:
            print(e)
            get_logger().error('Error in creating scheduled_jobs :', exc_info=True)
            return {'status': 'failure', 'message': 'Error in deleting scheduled_jobs'}
        else:
            get_logger().debug('Deleted scheduled_jobs Successfully.')
            return {'status': 'success', 'message': 'Deleted scheduled_jobs details  successfully'}

    # Update Scheduled jobs
    def update_scheduled_jobs(self, updateValuesDict={}):
        try:
            stmt = (self.scheduled_jobs.update().where(
                self.scheduled_jobs.c.job_id == updateValuesDict['job_id']).values(updateValuesDict))
            rs = stmt.execute()
        except Exception as e:
            print(e)
            get_logger().error('Error in scheduled_jobs config :', exc_info=True)
            return {'status': 'failure', 'message': 'Error in updating scheduled_jobs'}

        else:
            get_logger().debug('Updated new scheduled_jobs Successfully.')
            return {'status': 'success', 'message': 'Created site successfully'}

    def get_pending_schedule_jobs(self, lessThanTime):
        try:
            get_job = ((select([self.scheduled_jobs])).where(
                and_(self.scheduled_jobs.c.status == 'SCHEDULED', self.scheduled_jobs.c.start_time < lessThanTime)))
            rs = get_job.execute()
            result = rs.fetchall()
            get_logger().debug('found job detail(s) : %s', result)
            return result
        except Exception as e:
            get_logger().error('Error in finding schedule job :', exc_info=True)

    # Project Configuration related operation
    # Create Project Configuration
    def create_project_config(self, createValuesDict):
        try:
            stmt = (self.project_config.insert().values(createValuesDict))
            rs = stmt.execute()
            print(rs)
            get_logger().debug('created project config details successfully.')
            where_clauses = [self.project_config.c[key] == value for (key, value) in createValuesDict.items()]
            get_project = ((select([self.project_config])).where(and_(*where_clauses)))
            rs = get_project.execute()
            result = rs.fetchone()
            get_logger().debug('Created new project config successfully : %s', result)
            return {'status': 'success', 'data': result[0],
                    'message': 'Created new project config successfully'}
        except Exception as e:
            print(e)
            get_logger().error('Error in creating site config :', exc_info=True)
            return {'status': 'failure', 'message': 'Error in creating site'}


    # List project configurations
    def get_project_config(self, offset, limit, whereValuesDict={}):
        project_list = []
        try:
            if whereValuesDict:

                where_clauses = [self.project_config.c[key] == value for (key, value) in whereValuesDict.items()]
                rs = ((select([self.project_config])).where(and_(*where_clauses))).order_by(
                    asc(self.project_config.c.project_id)).limit(limit).offset(offset)
                rs2 = select([func.count(self.project_config.c.project_id)]).where(and_(*where_clauses))
            else:

                rs2 = select([func.count(self.project_config.c.project_id)])
                rs = select([self.project_config]).order_by(asc(self.project_config.c.project_id)).limit(limit).offset(
                    offset)
            count = rs2.execute().fetchone()
            print(count)
            result = rs.execute().fetchall()
            for projects in result:
                project_list.append({
                    'project_id': projects.project_id,
                    'project_name': projects.project_name,
                    'primary_site_id': projects.primary_site_id,
                    'secondary_site_ids': projects.secondary_site_ids,
                    'input_file_headers': projects.input_file_headers,
                    'output_file_headers': projects.output_file_headers,
                    'last_updated_timestamp': str(projects.last_updated_timestamp.strftime('%Y-%m-%dT%H:%M:%S'))
                    if projects.last_updated_timestamp else '',
                    'requires_input_file': projects.requires_input_file})

            get_logger().debug('Getting project config using following values : %s', whereValuesDict)
            return project_list, count[0]
        except Exception as e:
            get_logger().error('Error in listing project configuration :', exc_info=True)
            return {'status': 'failure', 'message': 'Error in listing projects '}

    # Update Project Configuration
    def update_project_config(self, updateValuesDict={}):
        try:
            stmt = (self.project_config.update().
                    where(self.project_config.c.project_id == updateValuesDict['project_id']).values(updateValuesDict))
            rs = stmt.execute()
            get_logger().debug('Updating new project configuration using these values : %s', updateValuesDict)
        except Exception as e:
            print(e)
            get_logger().error('Error in updating project configuration :', exc_info=True)
            return {'status': 'failure', 'message': 'Error in updating project configuration'}

        else:
            get_logger().debug('Updated new project configuration successfully.')
            return {'status': 'success', 'message': 'Updated new project configuration successfully.'}

    # Delete Project Configuration
    def delete_project_config(self, project_id):
        try:
            stmt = (self.project_config.delete().where(self.project_config.c.project_id == project_id))
            rs = stmt.execute()
            get_logger().debug('Deleting project configuration using project_id %s', project_id)
        except Exception as e:
            print(e)
            get_logger().error('Error in deleting project config :', exc_info=True)
            return {'status': 'failure', 'message': 'Error in deleting project config '}
        else:
            get_logger().debug('Deleted project configuration successfully.')
            return {'status': 'success', 'message': 'Deleted project configuration successfully'}

    def get_project_name(self, offset, limit, whereValuesDict={}):
        project_list = []
        try:
            if whereValuesDict:

                where_clauses = [func.lower(self.project_config.c[key]) == func.lower(value) for (key, value) in
                                 whereValuesDict.items()]
                rs = ((select([self.project_config])).where(and_(*where_clauses))).order_by(
                    asc(self.project_config.c.project_id)).limit(limit).offset(offset)
                rs2 = select([func.count(self.project_config.c.project_id)]).where(and_(*where_clauses))
            else:

                rs2 = select([func.count(self.project_config.c.project_id)])
                rs = select([self.project_config]).order_by(asc(self.project_config.c.project_id)).limit(limit).offset(
                    offset)
            count = rs2.execute().fetchone()
            print(count)
            result = rs.execute().fetchall()
            for projects in result:
                project_list.append({
                    'project_id': projects.project_id,
                    'project_name': projects.project_name,
                    'primary_site_id': projects.primary_site_id,
                    'secondary_site_ids': projects.secondary_site_ids,
                    'input_file_headers': projects.input_file_headers,
                    'output_file_headers': projects.output_file_headers,
                    'last_updated_timestamp': str(projects.last_updated_timestamp.strftime('%Y-%m-%dT%H:%M:%S'))
                    if projects.last_updated_timestamp else '',
                    'requires_input_file': projects.requires_input_file})
            get_logger().debug('Getting project config using following values : %s', whereValuesDict)
            return project_list, count[0]
        except Exception as e:
            get_logger().error('Error in listing project configuration :', exc_info=True)
            return {'status': 'failure', 'message': 'Error in listing projects '}

    ##Category Related operations
    # Selecting Catgory
    def get_category_scope(self, offset, limit, whereValuesDict={}):
        category_list = []
        try:
            where_clauses = [self.category_scope.c[key] == value for (key, value) in whereValuesDict.items()]
            rs = ((select([self.category_scope])).where(and_(*where_clauses))).order_by(
                asc(self.category_scope.c.project_id)).limit(limit).offset(offset)
            rs2 = select([func.count(self.category_scope.c.project_id)]).where(and_(*where_clauses))
            # stmt = select([self.category_scope]).where(self.category_scope.c.project_id == whereValuesDict['project_id'])
            # rs2 = select([func.count(self.category_scope.c.project_id)]).where(self.category_scope.c.project_id == whereValuesDict['project_id'])
            # result = stmt.execute().fetchall()
            # count = rs2.execute().fetchone()
            count = rs2.execute().fetchone()
            result = rs.execute().fetchall()
            for scopes in result:
                category_list.append({
                    'category': scopes.category,
                    'scope': scopes.scope,
                    'project_id': scopes.project_id
                })
            get_logger().debug('Listing category scope using these values %s', whereValuesDict)
            return category_list, count[0]
        except Exception as e:
            print(e)
            get_logger().error('Error in listing category :', exc_info=True)
            return {'status': 'failure', 'message': 'Error in listing category '}

    # Update Category configurations
    def update_category_scope(self, updateValuesDict):
        try:
            stmt = (self.category_scope.update().where(self.category_scope.c.category == updateValuesDict['category']
                                                       and self.category_scope.project_id == updateValuesDict[
                                                           'project_id']).values(updateValuesDict))
            rs = stmt.execute()
            get_logger().debug('Updating category scope using these values:%s', updateValuesDict)
        except Exception as e:
            get_logger().error('Error in Updating values:', exc_info=True)
            return {'status': 'failure', 'message': 'Error in updating category scope'}
        else:
            get_logger().debug('Updated category scope successfully')
            return {'status': 'success', 'message': 'Updated category scope'}

    # Create Category configurations
    def create_category_scope(self, insertValuesDict={}):
        try:
            self.engine.execute(self.category_scope.insert(), [insertValuesDict])
            get_logger().debug('added category details successfully.')
            where_clauses = [self.category_scope.c[key] == value for (key, value) in insertValuesDict.items()]
            get_category = ((select([self.category_scope])).where(and_(*where_clauses)))
            rs = get_category.execute()
            result = rs.fetchone()
            get_logger().debug('Created new category successfully : %s', result)
        except Exception as e:
            print(e)
            get_logger().error('Error in creating new category :', exc_info=True)
            return {'status': 'failure', 'message': 'Error in creating new category'}

        else:
            get_logger().debug('Created new category successfully .')
            return {'status': 'success', 'message': 'New category successfully'}

    def update_lxml_webpage_parser_detail(self, webpage_parser_id, updateValuesDict):
        try:
            upd = (update(self.webpage_parser_details).where(
                self.webpage_parser_details.c.webpage_parser_id == webpage_parser_id).values(updateValuesDict))
            upd.execute()
            get_logger().debug('updated webpage_parser_detail details successfull with values : %s', updateValuesDict)
        except Exception as e:
            get_logger().error('Error in webpage_parser_detail :', exc_info=True)
        else:
            get_logger().debug('Updated webpage_parser_detail: %s with values : %s', webpage_parser_id,
                               updateValuesDict)

    def get_products_list(self, whereValuesDict):
        try:
            productlist = []
            if whereValuesDict:
                where_clauses = [self.products.c[key] == value for (key, value) in whereValuesDict.iteritems()]

                get_product = ((select([self.products])).where(and_(*where_clauses)))
                rs = get_product.execute()
            else:
                # print('else')
                get_product = (select([self.products]))
                rs = get_product.execute()
            result = rs.fetchall()
            return result
        except Exception as e:

            get_logger().error('Error in finding schedule job :', exc_info=True)

    def get_sites_list(self, whereValuesDict):
        try:
            productlist = []
            if whereValuesDict:
                where_clauses = [self.products.c[key] == value for (key, value) in whereValuesDict.iteritems()]

                get_product = ((select([self.products])).where(and_(*where_clauses)))
                rs = get_product.execute()
            else:
                # print('else')
                get_product = (select([self.products]))
                rs = get_product.execute()
            result = rs.fetchall()
            return result
        except Exception as e:
            get_logger().error('Error in finding schedule job :', exc_info=True)

    def get_status_bool(self, request, key_name):
        user_input = request.form.get(key_name, 'false')
        if user_input:
            if user_input == False:
                value = False
            elif user_input == True:
                value = True
            else:
                value = True if user_input == 'true' else False
        else:
            value = False
        # print(key_name, user_input, value)
        return value

    def get_googleapi_products(self,whereValuesDict):
        try:
            where_clauses = [self.google_products.c[key] == value\
                             for (key, value) in whereValuesDict.items()]

            pro_download = ((select([self.google_products.c.job_id.label('Job ID'),
                                     self.google_products.c.product_upc.label('Product UPC'),
                                     self.google_products.c.product_name.label('Product Name'),
                                     self.google_products.c.product_urls.label('Product URLs'),
                                     self.google_products.c.product_images.label('Product Images'),
                                     self.google_products.c.product_predicted_images.label('Product Predicted Images')])).where(and_(*where_clauses)))

            rs = pro_download.execute()
            result = rs.fetchall()
            return result
        except Exception as e:
            get_logger().error('Error in creating cron job config :', exc_info=True)
            return None

    def get_product_details_download(self, whereValuesDict):
        try:
            site_id = whereValuesDict["site_id"]
            product_name = whereValuesDict.get("product_name")
            product_upc = whereValuesDict.get("product_upc")
            site_name = whereValuesDict.get("site_name")
            product_status = whereValuesDict.get("product_status")
            start_date = ''
            end_date =''
            if 'start_date' in whereValuesDict:
                start_date = whereValuesDict['start_date']
                del whereValuesDict['start_date']
            if 'end_date' in whereValuesDict:
                end_date = whereValuesDict['end_date']
                del whereValuesDict['end_date']

            where_clauses = []
            if site_id:
                where_clauses.append(self.products.c.site_id == site_id)

            if product_upc:
                where_clauses.append(self.products.c.product_upc.like(
                    '%' + product_upc + '%'))

            if product_name:
                where_clauses.append(self.products.c.product_name.like(
                    '%' + product_name + '%'))

            if start_date and end_date:
                where_clauses.append(self.products.c.timestamp.between(start_date, end_date))

            if product_status is not None and product_status !='':
                where_clauses.append(self.products.c.product_status == product_status)

            print (where_clauses)

            if not where_clauses:
                return None

            pro_download = ((select([self.products.c.product_upc.label('Product UPC'), \
                                     # self.products.c.site_id.label('Site ID'), \
                                     self.products.c.product_name.label('Product Name'), \
                                     self.products.c.product_price.label('Price'), \
                                     self.products.c.product_url.label('Product URL'), \
                                     self.products.c.product_ingredients.label('Ingredients'), \
                                     self.products.c.product_brand.label('Brand'), \
                                     self.products.c.product_category.label('Category'), \
                                     self.products.c.product_subcategory1.label('Subcategory 1'), \
                                     self.products.c.product_subcategory2.label('Subcategory 2'), \
                                     self.products.c.product_imageurl.label('Image URL'), \
                                     self.products.c.product_subcategory3.label('Subcategory 3'), \
                                     self.products.c.product_size.label('Size'), \
                                     self.products.c.product_dropdown1.label('Dropdown 1'), \
                                     self.products.c.product_dropdown2.label('Dropdown 2'), \
                                     self.products.c.product_dpci.label('DPCI'), \
                                     self.products.c.product_desc.label('DESC'), \
                                     self.products.c.product_packaging.label('Packaging'), \
                                     self.products.c.product_serving_per_container.label('Serving Per Container'), \
                                     self.products.c.product_features.label('Features'), \
                                     self.products.c.product_serving_size.label('Serving Size'), \
                                     self.products.c.product_flavor.label('Flavor'), \
                                     self.products.c.product_type.label('Type'), \
                                     self.products.c.product_assembled_product_dimensions.label(
                                         'Assembled Product Dimensions'), \
                                     self.products.c.product_model.label('Model'), \
                                     self.products.c.product_sku.label('SKU'), \
                                     self.products.c.product_manufacturer_part_number.label('Manufacturer Part Number'), \
                                     self.products.c.product_status.label('Status'), \
                                     self.products.c.product_additional_images.label('Additional Images'), \
                                     self.products.c.product_nutrition_info.label('Nutrition Info'), \
                                     self.products.c.product_feeding_instructions.label('Feeding Instructions'), \
                                     self.products.c.product_total_reviews.label('Total Reviews'), \
                                     self.products.c.product_rating.label('Rating'), \
                                     self.products.c.product_customer_reviews.label('Customer Reviews'), \
                                     self.products.c.product_additional_sizes.label('Additional Sizes'), \
                                     self.products.c.product_discount_price.label('Discount Price'), \
                                     self.products.c.product_offer_category.label('Offer Category'), \
                                     self.products.c.product_recommended_retail_price.label('Product Recommended Retail Price'), \
                                     self.products.c.product_rating_image_url.label('Rating Image URL')])).where(
                and_(*where_clauses))).order_by(desc(self.products.c.timestamp))

            rs = pro_download.execute()
            result = rs.fetchall()
            # print (result[0])
            return result
        except Exception as e:
            print(str(e))
            get_logger().error('Error product download :', exc_info=True)
            return None

    def get_cronjob(self, whereValuesDict):

        where_clauses = [self.crontab_schedule.c[key] == value for (key, value) in
                         whereValuesDict.items()]
        getjobInfo = (
            (select([self.crontab_schedule])).where(and_(*where_clauses)))
        rs = getjobInfo.execute()
        return rs.fetchall()

    def get_cron_details(self, ):
        query_exc = self.periodic_tasks.join(self.crontab_schedule,
                                             self.periodic_tasks.c.cron_id == self.crontab_schedule.c.cron_id)
        # rs = query_exc.execute()

        query = (select([self.crontab_schedule.c.minute, \
                         self.crontab_schedule.c.hour, \
                         self.crontab_schedule.c.day_of_week, \
                         self.crontab_schedule.c.day_of_month, \
                         self.crontab_schedule.c.month_of_year, \
                         self.crontab_schedule.c.last_executed, \
                         self.periodic_tasks])).select_from(query_exc)
        rs = query.execute()
        data = rs.fetchall()
        return data

    def get_cronjobs(self, whereValuesDict):

        where_clauses = [self.periodic_tasks.c[key] == value for (key, value) in whereValuesDict.items()]

        # where_clauses = [self.periodic_tasks.c.site_id,self.periodic_tasks.c.category,
        #                  whereValuesDict.items()]
        getjobInfo = (
            (select([self.periodic_tasks])).where(and_(*where_clauses)))
        rs = getjobInfo.execute()
        return rs.fetchall()

    def create_cronjob(self, createValuesDict):
        try:
            self.engine.execute(self.crontab_schedule.insert(), [createValuesDict])
            get_logger().debug('created cronjob details successfully.')
            where_clauses = [self.crontab_schedule.c[key] == value for (key, value) in createValuesDict.items()]
            get_cronjob = ((select([self.crontab_schedule])).where(and_(*where_clauses)))
            rs = get_cronjob.execute()
            result = rs.fetchone()
            get_logger().debug('Created new cronjob config successfully : %s', result)
            return {'status': 'success', 'data': result[0], 'message': 'Created new cron job config successfully'}
        except Exception as e:
            print(e)
            get_logger().error('Error in creating cron job config :', exc_info=True)
            return {'status': 'failure', 'message': 'Error in creating cron job details'}


    def update_cronjob(self, updateValuesDict={}):
        try:
            stmt = (self.crontab_schedule.update().where(
                self.crontab_schedule.c.cron_id == updateValuesDict['cron_id']).values(updateValuesDict))
            rs = stmt.execute()
        except Exception as e:
            print(e)
            get_logger().error('Error in creating cron job config :', exc_info=True)
            return {'status': 'failure', 'message': 'Error in creating cron job details'}

        else:
            get_logger().debug('updated new cron job details  successfully.')
            return {'status': 'success', 'message': 'updated cron job details successfully'}

    def get_scheduled_jobss(self, offset, limit, whereValuesDict={}):
        jobs_list = []
        if whereValuesDict:
            where_clauses = [self.scheduled_jobs.c[key] == value for (key, value) in whereValuesDict.items()]
            # print(where_clauses)

            rs = ((select([self.scheduled_jobs])).where(and_(*where_clauses))).order_by(
                desc(self.scheduled_jobs.c.job_id)).limit(limit).offset(offset)
            rs2 = select([func.count(self.scheduled_jobs.c.job_id)]).where(and_(*where_clauses))
        else:
            rs2 = select([func.count(self.scheduled_jobs.c.job_id)])
            rs = select([self.scheduled_jobs]).order_by(desc(self.scheduled_jobs.c.job_id)).limit(limit).offset(
                offset)
        count = rs2.execute().fetchone()
        result = rs.execute().fetchall()
        # data = result.fetchall()
        for job in result:
            jobs_list.append({'job_id': job.job_id,
                              'site_id': job.site_id,
                              'total_products_scraped': job.total_products_scraped,
                              'total_products_attempted': job.total_products_attempted,
                              'project_id': job.project_id,
                              'start_time': str(
                                  job.start_time.strftime('%Y-%m-%dT%H:%M:%S')) if job.start_time else '',
                              'file_name': job.file_name,
                              'end_time': str(job.end_time.strftime('%Y-%m-%dT%H:%M:%S')) if job.end_time else '',
                              'job_type': job.job_type,
                              'timeout': job.timeout,
                              'status': job.status,
                              'update_time': str(
                                  job.update_time.strftime('%Y-%m-%dT%H:%M:%S')) if job.update_time else '',
                              'comments': job.comments,
                              'total_products_to_scrape': job.total_products_to_scrape,
                              'category': job.category,

                              })
        return jobs_list, count[0]

    def get_periodictask(self, whereValuesDict):

        where_clauses = [self.periodic_tasks.c[key] == value for (key, value) in
                         whereValuesDict.items()]
        getperiodictaskInfo = (
            (select([self.periodic_tasks])).where(and_(*where_clauses)))
        rs = getperiodictaskInfo.execute()
        return rs.fetchall()

    def create_periodictask(self, createValuesDict):
        try:
            print("insert", createValuesDict)
            self.engine.execute(self.periodic_tasks.insert(), [createValuesDict])
            get_logger().debug('created periodic_tasks details successfully.')
            where_clauses = [self.periodic_tasks.c[key] == value for (key, value) in createValuesDict.items()]
            get_cronjob = ((select([self.periodic_tasks])).where(and_(*where_clauses)))
            rs = get_cronjob.execute()
            result = rs.fetchone()
            get_logger().debug('Created new periodic_tasks config successfully : %s', result)
            return {'status': 'success', 'data': result[0], 'message': 'Created new periodic_tasks config successfully'}
        except Exception as e:
            get_logger().debug('Error %s' % str(e))
            get_logger().error('Error in creating periodic_tasks config :', exc_info=True)
            return {'status': 'failure', 'message': 'Error in creating periodic_tasks details'}

    def update_periodictask(self, updateValuesDict={}):
        try:
            stmt = (self.periodic_tasks.update().where(and_(
                self.periodic_tasks.c.job_id == updateValuesDict['job_id'],
                self.periodic_tasks.c.site_id == updateValuesDict['site_id'])).values(updateValuesDict))
            rs = stmt.execute()
        except Exception as e:
            print(e)
            get_logger().error('Error in creating cron job config :', exc_info=True)
            return {'status': 'failure', 'message': 'Error in creating cron job details'}

        else:
            get_logger().debug('updated new cron job details  successfully.')
            return {'status': 'success', 'message': 'updated cron job details successfully'}

    def get_search_engine_details(self,whereValuesDict):
        try:
            where_clauses = [self.search_engine_details.c[key] == value for (key, value) in
                             whereValuesDict.items()]
            getSiteInfo = (
                (select([self.search_engine_details.c.cx_key,
                         self.search_engine_details.c.region,
                         self.search_engine_details.c.website,
                         self.search_engine_details.c.api_key])).where(and_(*where_clauses)))
            rs = getSiteInfo.execute()
            return rs.fetchall()
        except Exception as e:
            get_logger().error('Error in get_search_engine_details', exc_info=True)

    def update_search_engine_details(self, updateValuesDict={}):
        try:
            print(updateValuesDict)
            stmt = (self.search_engine_details.update().where(self.search_engine_details.c.region == updateValuesDict['region']).
                    values(updateValuesDict))
            rs = stmt.execute()
        except Exception as e:
            print(e)
            get_logger().error('Error in updating search_engine_details config :', exc_info=True)

    def insert_search_engine_details(self, createValuesDict):
        try:
            self.engine.execute(self.search_engine_details.insert(), [createValuesDict])
            get_logger().debug('inserted google product details successfully.')
            return True
        except Exception as e:
            get_logger().debug('Error %s' % str(e))
            return False

    def get_google_products_details(self,job_run_id):
        try:
            query_template="""select 
                               'Google' as site_id,
                               'Google' as site_name,
                               gl.product_upc as product_upc,
                               gl.product_name as product_name,
                               gl.product_urls as product_url,
                               gl.product_predicted_images as product_additional_images,
                               dy.scrape_using as scrape_using,
                               dy.description as description from google_products gl
                            inner join import_products dy on (dy.product_upc=gl.product_upc or dy.product_name=gl.product_name)
                            where dy.job_id={job_id} and gl.job_id={job_id} and gl.product_urls ->> 0 is not null"""
            query = query_template.format(job_id=job_run_id)
            # print(query)
            fet = self.engine.execute(query)
            result_set = fet.fetchall()
            return result_set
        except Exception as e:
            get_logger().error('Error in get_google_products_details :', exc_info=True)
            return []

    def get_project_details_json_file(self,job_run_id):
        try:
            query_template="""select prod.site_id,
                                        imp.scrape_using,
                                        prod.product_upc,
                                        prod.product_name,
                                        prod.product_price,
                                        prod.product_url,
                                        prod.product_desc,
                                        prod.product_brand,
                                        prod.product_imageurl,
                                        prod.product_additional_images,
                                        imp.description
                                        from jobid_mapping_product job
                                        inner join products prod on job.product_id = prod.product_id
                                        inner join import_products imp on imp.id = job.import_product_id
                                        where job.job_id = {job_id}"""
            query = query_template.format(job_id=job_run_id)
            # print(query)
            fet = self.engine.execute(query)
            result_set = fet.fetchall()
            return result_set
        except Exception as e:
            get_logger().error('Error in get_project_details_json_file :', exc_info=True)
            return []

    def get_product_meta_info(self,site_ids):
        try:
            query_template = """select site_id,product_id,product_name
                                from products where site_id in ({sites})
                                            """
            query = query_template.format(sites=site_ids)
            fet = self.engine.execute(query)
            return fet
        except Exception as e:
            get_logger().error('Error in get_product_meta_info :', exc_info=True)
            return []

    def get_import_product_desc(self,job_run_id):
        try:
            query_template = """select imp.id,imp.description -> 'Product Name' as product_name 
                                from import_products as imp
                                where job_id = {job_id}"""
            query = query_template.format(job_id=job_run_id)
            fet = self.engine.execute(query)
            return fet
        except Exception as e:
            get_logger().error('Error in insert_data_job_id_map_table :', exc_info=True)
            return []

    def insert_data_job_id_map_table(self,job_run_id,site_ids):
        try:
            query_template = """insert into jobid_mapping_product(site_id,job_id,product_id,import_product_id)
                                        select prod.site_id,
                                        imp.job_id,
                                        prod.product_id,
                                        imp.id
                                        from import_products imp
                                        inner join products prod 
                                            on imp.product_upc=prod.product_upc
                                        where imp.job_id={job_id} 
                                            and prod.site_id in ({sites})
                                            """
            query = query_template.format(job_id=job_run_id,sites=site_ids)
            fet = self.engine.execute(query)
            return fet
        except Exception as e:
            # print (str(e))
            get_logger().error('Error in insert_data_job_id_map_table :', exc_info=True)
            return []

    def get_site_names(self,site_ids):
        try:
            query = """select site_url,site_id,site_name,require_sitemap_parsing,
                        require_dynamic_scrapping,require_category_parsing                        
                        from sites where site_id in ({sites})""".format(sites=','.join(site_ids))
            # print(query)
            exe = query.format(sites=site_ids)
            fet = self.engine.execute(exe)
            return fet
        except Exception as e:
            # print (str(e))
            get_logger().error('Error in get_site_names :', exc_info=True)
            return []

    def get_image_puller_lookup_by_query(self, whereValuesDict):

        try:
            query = """select lk.job_id,
                        sj.total_products_to_scrape,
                        sj.total_products_scraped,
                        sj.total_products_attempted,
                        sj.end_time,
                        lk.project_id,
                        lk.site_ids,
                        sj.start_time,
                        lk.file_name,
                        lk.total,
                        sj.status 
                        from image_puller_lookup lk
                        left join scheduled_jobs sj on lk.file_name = sj.file_name"""
            # print(query)
            where_list = []
            where_string = ""
            condition1 = ""
            condition2 = ""
            if 'job_id' in whereValuesDict and whereValuesDict['job_id']:
                condition1 = " sj.job_id={job_id}".format(job_id=whereValuesDict['job_id'])

            if 'project_id' in whereValuesDict and whereValuesDict['project_id']:
                condition2 = "sj.project_id={project_id}".format(project_id=whereValuesDict['project_id'])

            if condition1:
                where_list.append(condition1)
            if condition2:
                where_list.append(condition2)

            if where_list:
                where_string = ' and '.join(where_list)
                if where_string:
                    where_string = " where "+where_string

            print(where_string)
            exec_query = query+where_string if where_string else query
            fet = self.engine.execute(exec_query+' order by lk.job_id desc')
            return fet
        except Exception as e:
            # print (str(e))
            get_logger().error('Error in get_image_puller_lookup_by_query :', exc_info=True)
            return []

    def insert_image_puller_lookup(self,recordToInsert):
        try:
            inserted = self.engine.execute(self.image_puller_lookup.insert(), [recordToInsert])
            inserted_id = inserted.inserted_primary_key[0]
            print (inserted_id)
        except Exception as e:
            get_logger().error("error in insert_image_puller_lookup %s", str(e))

    def get_image_puller_lookup(self, whereValuesDict):

        where_clauses = [self.image_puller_lookup.c[key] == value for (key, value) in
                         whereValuesDict.items()]
        info = (
            (select([self.image_puller_lookup])).where(and_(*where_clauses)))
        rs = info.execute()
        return rs.fetchall()

    def insert_jobid_mapping_product(self,recordToInsert):
        try:
            inserted = self.engine.execute(self.jobid_mapping_product.insert(), [recordToInsert])
            inserted_id = inserted.inserted_primary_key[0]
            print (inserted_id)
        except Exception as e:
            get_logger().error("error in insert_jobid_mapping_product %s", str(e))


    def get_user_with_password(self, offset, limit, whereValuesDict={}, type = None):
        users_list = []

        if whereValuesDict:
            where_clause = [self.users.c[key] == value for (
                key, value) in whereValuesDict.items()]
            print(where_clause)
            rs = select([self.users]).where(
                and_(*where_clause)).order_by(
                asc(self.users.c.id)).limit(limit).offset(offset)
            # print(rs)
            rs2 = select([func.count(self.users.c.id)]).where(
                and_(*where_clause))
        else:
            rs = select([self.users]).order_by(
                asc(self.users.c.id)).limit(limit).offset(offset)
            rs2 = select([func.count(self.users.c.id)])

        count = rs2.execute().fetchone()
        result = rs.execute().fetchall()
        for user in result:
            try:
                user_dict = {

                    'id': user.id,
                    'first_name': user.first_name,
                    'last_name': user.last_name,
                    'username': user.username,
                    'email': user.email,
                    'password':user.password,
                    'active': user.active,
                    'last_login': user.last_login,
                    'login_count': user.login_count,
                    'fail_login_count': user.fail_login_count,
                    'created_on': user.created_on,
                }

                if(type is not None):
                    user_dict['password'] = user.password

                users_list.append(user_dict)
            except Exception as e:
                print(str(e))
        return users_list, count[0]


    def get_users(self, offset, limit, whereValuesDict={}, type = None):
        users_list = []

        if whereValuesDict:
            where_clause = [self.users.c[key] == value for (
                key, value) in whereValuesDict.items()]
            print(where_clause)
            rs = select([self.users]).where(
                and_(*where_clause)).order_by(
                asc(self.users.c.id)).limit(limit).offset(offset)
            # print(rs)
            rs2 = select([func.count(self.users.c.id)]).where(
                and_(*where_clause))
        else:
            rs = select([self.users]).order_by(
                asc(self.users.c.id)).limit(limit).offset(offset)
            rs2 = select([func.count(self.users.c.id)])

        count = rs2.execute().fetchone()
        result = rs.execute().fetchall()
        for user in result:
            try:
                user_dict = {

                    'id': user.id,
                    'first_name': user.first_name,
                    'last_name': user.last_name,
                    'username': user.username,
                    'email': user.email,
                    # 'password':user.password,
                    'active': user.active,
                    'last_login': user.last_login,
                    'login_count': user.login_count,
                    'fail_login_count': user.fail_login_count,
                    'created_on': user.created_on,
                }

                if(type is not None):
                    user_dict['password'] = user.password

                users_list.append(user_dict)
            except Exception as e:
                print(str(e))
        return users_list, count[0]


    def create_user(self, createValuesDict):
        try:
            self.engine.execute(self.users.insert(), [createValuesDict])
            # get_logger().debug('created user details successfully.')
            where_clauses = [self.users.c[key] == value for (
                key, value) in createValuesDict.items()]
            get_user = ((select([self.users])).where(and_(*where_clauses)))
            rs = get_user.execute()
            result = rs.fetchone()
            # get_logger().debug('Created new user successfully : %s', result)
            return {'status': 'success', 'data': result[0], 'message': 'Created user successfully'}
        except Exception as e:
            print(e)
            # get_logger().error('Error in creating user config :', exc_info=True)
            return {'status': 'failure', 'message': 'Error in creating user'}

    def update_user(self, updateValuesDict={}):
        try:
            # self.engine.execute(self.users.update(), [updateValuesDict])

            print(updateValuesDict)
            stmt = (self.users.update().where(self.users.c.username == updateValuesDict['username']).
                    values(updateValuesDict))
            rs = stmt.execute()
        except Exception as e:
            print(e)
            # get_logger().error('Error in updating user config :', exc_info=True)
            return {'status': 'failure', 'message': 'Error in updating user details'}

        else:
            # get_logger().debug('Updated user successfully.')
            return {'status': 'success', 'message': 'Updated user successfully'}


    def delete_user(self, id):
        try:
            stmt = (self.users.delete().where(self.users.c.id == id))
            rs = stmt.execute()
            #get_logger().debug('Deleting user config with following id: %s', id)
        except Exception as e:
            print(e)
            #get_logger().error('Error in deleting user :', exc_info=True)
            return {'status': 'failure', 'message': 'Error in deleting user'}
        else:
            #get_logger().debug('Deleted user successfully.')
            return {'status': 'success', 'message': 'Deleted user details  successfully'}










def is_json(myjson):
    try:
        print('checking for json', myjson)
        json_object = json.loads(myjson)
    except ValueError as e:
        return False
    return True


def convertToJson(xV):
    jsonDict = [{'type': 'get', 'xpath': ''}]

    stIndx = xV.rindex('/')
    lstVal = xV[(stIndx + 1):]

    if '@' in lstVal:
        jsonDict[0]['xpath'] = xV[:stIndx]
        jsonDict[0]['attribute'] = lstVal[1:]
    elif 'text()' in lstVal:
        jsonDict[0]['xpath'] = xV[:stIndx]
    else:
        jsonDict[0]['xpath'] = xV

    jsStr = json.dumps(jsonDict)
    return jsStr

# if __name__ == '__main__':

# DbUtils=DbUtils()
# xpathColumnNames=DbUtils.getXpathColumnsFromWebpageParser('product_.*_xpath')
# print(xpathColumnNames)
# webPageParserDetails=DbUtils.get_lxml_webpage_parser_details()
# print(webPageParserDetails)


# for webPageParserDetail in webPageParserDetails:
# 	updWebPageParserDetail={}
# 	id=webPageParserDetail['webpage_parser_id']

# 	print ('parsing id',id)
# 	for xpathCol in xpathColumnNames:
# 		if xpathCol in webPageParserDetail and webPageParserDetail[xpathCol]:
# 			xpathValue=webPageParserDetail[xpathCol]
# 			print(xpathCol,xpathValue)
# 			if not is_json(xpathValue):
# 				updWebPageParserDetail[xpathCol]=convertToJson(xpathValue)
# 	print(updWebPageParserDetail)

# 	if updWebPageParserDetail:
# 		DbUtils.update_lxml_webpage_parser_detail(id,updWebPageParserDetail)
# 		print 'successfully Updated..!!'
# 		print ('id :',id,' ;; UpdatedDetails :',updWebPageParserDetail)




