__author__='Anilkumar'
from flask import Flask,request,jsonify
from googleapiclient.discovery import build
import json

app=Flask(__name__)
DEVELOPER_KEY = "AIzaSyCLro3qdHPM1fgFeXoflg3DGs-sJXwH-hg"
YOUTUBE_API_SERVICE_NAME = "youtube"
YOUTUBE_API_VERSION = "v3"

# creating Youtube Resource Object
youtube_object = build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION,
                           developerKey=DEVELOPER_KEY)


def you_tube(name,item_count,page,channel_id,order_by=None):
    search_query={}
    search_query["q"] = name
    if channel_id:
        search_query["channelId"] = channel_id
    search_query["part"] = "id, snippet"
    search_query["maxResults"] = int(item_count)

    if order_by:
        search_query["order"] = order_by

    count=0
    next_page_token=None
    list_data = []
    play_list_data = []

    while page > count:
        count += 1
        if next_page_token:
            search_query["pageToken"]=next_page_token

        search_keyword = youtube_object.search().list(**search_query).execute()
        youtube_data = {'Youtube': {}}

        next_page_token = search_keyword.get("nextPageToken", None)
        print(count,next_page_token,search_query)
        for result in search_keyword.get("items", []):
            dict_data = {}
            play_list = {}
            if result['id']['kind'] == "youtube#video":
                dict_data['videourl']="https://www.youtube.com/watch?v=" + str(result["id"]["videoId"])
                dict_data['videoId']=result['id']['videoId']
                dict_data['title'] = result['snippet']['title']

                video_response = youtube_object.videos().list(part='statistics,snippet',
                                             id=result['id'][
                                                 'videoId']).execute()
                dict_data['like'] = 0
                dict_data['dislike'] = 0
                dict_data['commentCount'] = 0
                dict_data['viewCount'] = 0
                dict_data['favoriteCount'] = 0
                dict_data['categoryId'] = 0
                dict_data['videoTitle'] = ""

                for response in video_response.get('items',[]):
                    if 'likeCount' in response['statistics']:
                        dict_data['Like']=response['statistics']['likeCount']

                    if 'dislike' in response['statistics']:
                        dict_data['dislike']=response['statistics']['dislikeCount']

                    if 'commentCount' in response['statistics']:
                        dict_data['commentCount']=response['statistics']['commentCount']

                    if 'viewCount' in response['statistics']:
                        dict_data['viewCount']=response['statistics']['viewCount']

                    if 'favoriteCount' in response['statistics']:
                        dict_data['viewCount']=response['statistics']['favoriteCount']

                    if 'videoTitle' in  response['snippet']:
                        dict_data['videoTitle']=response['snippet']['channelTitle']

                    if 'categoryId' in response['snippet']:
                        dict_data['categoryId']=response['snippet']['categoryId']

                    list_data.append(dict_data)
            elif result['id']['kind'] == "youtube#channel":
                youtube_data['channelId']=result["id"]["channelId"]
                youtube_data['title']=result["snippet"]["title"]
                youtube_data['description']=result['snippet']['description']
                youtube_data['channel_url']=result['snippet']['thumbnails']['default']['url']
            elif result['id']['kind'] == "youtube#playlist":
                play_list['playlist_title']=result["snippet"]["title"]
                play_list['playlistId']=result["id"]["playlistId"]
                play_list['description']=result['snippet']['description']
                play_list['play_list_url']=result['snippet']['thumbnails']['default']['url']
                play_list_data.append(play_list)

    youtube_data['Youtube']['video_list']=list_data
    youtube_data['Youtube']['Play_list']=play_list_data
    return youtube_data

# if __name__=='__main__':
#     app.run(host="0.0.0.0",debug=True)