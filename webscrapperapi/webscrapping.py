from flask import Flask
from flask_cors import CORS
from apis import blueprint as api

app = Flask(__name__)
CORS(app)
app.register_blueprint(api, url_prefix='/api')
#api.init_app(app)

# app.run(debug=True)
app.run(host="0.0.0.0",debug=True)
