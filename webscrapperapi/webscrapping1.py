from flask import Flask, abort, request, jsonify,Blueprint
from flask_restful import Api,Resource
#from flask_sqlalchemy import SQLAlchemy
from utils import DbUtils
import decimal,datetime,json
import json
from models import Sites
import models
from flask_swagger_ui import get_swaggerui_blueprint
# Session is a class
sess = models.session
app = Flask(__name__)
blueprint = Blueprint('api',__name__,url_prefix='/api')
api = Api(blueprint, doc='/documentation')

#db = SQLAlchemy(app)

# @app.route('/api/v2/jobs/page')
# def view():
# 	return jsonify(get_paginated_list(
# 		Event, 
# 		'/api/v2/events/page', 
# 		start=request.args.get('start', 1), 
# 		limit=request.args.get('limit', 20)
# 	))

# def alchemyencoder(obj):
#     """JSON encoder function for SQLAlchemy special classes."""
#     if isinstance(obj, datetime.date):
#         return obj.isoformat()
#     elif isinstance(obj, decimal.Decimal):
#         return float(obj)

# SWAGGER_URL = '/api/docs'  # URL for exposing Swagger UI (without trailing '/')
# API_URL = 'http://127.0.0.1:5000/v2/swagger.json'  # Our API url (can of course be a local resource)

# # Call factory function to create our blueprint
# swaggerui_blueprint = get_swaggerui_blueprint(
#     SWAGGER_URL,  # Swagger UI static files will be mapped to '{SWAGGER_URL}/dist/'
#     API_URL,
#     config={  # Swagger UI config overrides
#         'app_name': "Test application"
#     },
#     # oauth_config={  # OAuth config. See https://github.com/swagger-api/swagger-ui#oauth2-configuration .
#     #    'clientId': "your-client-id",
#     #    'clientSecret': "your-client-secret-if-required",
#     #    'realm': "your-realms",
#     #    'appName': "your-app-name",
#     #    'scopeSeparator': " ",
#     #    'additionalQueryStringParams': {'test': "hello"}
#     # }
# )

# # Register blueprint at URL
# # (URL must match the one given to factory function above)
# app.register_blueprint(swaggerui_blueprint, url_prefix=SWAGGER_URL)


class Site(Resource):
		
	def get(self):
		dbutils = DbUtils()
		test = sess.query(Sites.to_json).all()
		print(test)
		sites =dbutils.get_sites()
		data = get_paginated_list(test,'/sites',start=request.args.get('start', 1),limit=request.args.get('limit', 20))
		return data

	def post(self,site_name):
		print(request.site_name)
		dbutils = DbUtils()

class Job(Resource):
		
	def get(self):
		dbutils = DbUtils()
		jobs =dbutils.get_schedule_jobs({})
		data = get_paginated_list(jobs,'/jobs',start=request.args.get('start', 1),limit=request.args.get('limit', 20))
		return data
		#return json.dumps(data,default = alchemyencoder)

class Product(Resource):
	def get(self):
		productlist = []
		dbutils = DbUtils()
		products = dbutils.get_products_list({})
		data = get_paginated_list(products,'/products',start=request.args.get('start', 1),limit=request.args.get('limit', 20))
		for prod in data['results']:
			productlist.append({'product_id':prod.product_id,
			'product_upc':prod.product_upc,
			'product_name':prod.product_name,
			'product_price':prod.product_price,
			'site_id':prod.site_id,
			'product_url':prod.product_url,
			'product_ingredients':prod.product_ingredients,
			'product_brand':prod.product_brand,
			'product_category':prod.product_category,
			'product_subcategory1':prod.product_subcategory1,
			'product_subcategory2':prod.product_subcategory2,
			'product_imageurl':prod.product_imageurl,
			'product_subcategory3':prod.product_subcategory3,
			'timestamp': str(prod.timestamp.strftime('%Y-%m-%dT%H:%M:%S')) if prod.timestamp else '',
			'product_size':prod.product_size,
			'product_dropdown1':prod.product_dropdown1,
			'product_dropdown2':prod.product_dropdown2,
			'product_dpci':prod.product_dpci,
			'product_desc':prod.product_desc,
			'product_packaging':prod.product_packaging,
			'product_serving_per_container':prod.product_serving_per_container,
			'product_features':prod.product_features,
			'product_serving_size':prod.product_serving_size,
			'product_flavor':prod.product_flavor,
			'product_type':prod.product_type,
			'product_assembled_product_dimensions':prod.product_assembled_product_dimensions,
			'product_model':prod.product_model,
			'product_sku':prod.product_sku,
			'product_manufacturer_part_number':prod.product_manufacturer_part_number,
			'product_status':prod.product_status,
			'product_additional_images':prod.product_additional_images,
			'product_nutrition_info':prod.product_nutrition_info,
			'product_feeding_instructions':prod.product_feeding_instructions,
			'product_total_reviews':prod.product_total_reviews,
			'product_rating':prod.product_rating,
			'product_customer_reviews':prod.product_customer_reviews,
			'product_additional_sizes':prod.product_additional_sizes,
			'product_rating_image_url':prod.product_rating_image_url,
			'site_name':""})

		data = {'start': data['start'], 'limit': data['limit'], 'count': data['count'], 'previous': data['previous'], 'next': data['next'], 'results':productlist}
		
		return data



api.add_resource(Site, '/sites')
api.add_resource(Product, '/products')
api.add_resource(Job, '/jobs')


if __name__ == '__main__':
	#app.run(host='0.0.0.0',debug=True)
	app.run(debug=True)
